/**
 * 
 */
 
 
function changeSubject(value){
	$(':checkbox').each(function(i,item){ 
		if(item.name!="subjectOption" && item.name!="resourceOption"){
			this.checked = item.defaultChecked; 
		}
	}); 
	if(value=="member"){
		 $("#organisation_div").hide(500);
		 $("#organisationId").attr('disabled','disabled');
		 $("#organisationRoleId").attr('disabled','disabled');
		 $("#organisationSectorId").attr('disabled','disabled');
		 $("#member_div").show(400);
		 
	}else{
		 $("#member_div").hide(500);
		 $("#memberFunctionId").attr('disabled','disabled');
		 $("#memberRoleId").attr('disabled','disabled');
		 $("#memberLevelId").attr('disabled','disabled');
		 $("#organisation_div").show(400);
	}
	
}
function validateForm(){
	$("#myForm").submit(function(){
		  alert("Submitted");
		});
	//alert('ok');
	return false;
}

function changeOption(divId,selectName){
	
	//var val = $('input:checkbox[name='+selectName+']:checked').prop('checked');
	if( $('input:checkbox[name='+selectName+']:checked').prop('checked')){
		$('#'+divId).removeAttr('disabled')
	}else{
		$('#'+divId).attr('disabled', 'disabled');
	}
}

function changeDivVisibility(divId){
	var dispValue = $('#'+divId).css('display');
	if(dispValue=="none"){
		$("#"+divId).show(400);
	}else{
		$("#"+divId).hide(400);
			
	}
}

function updateField(inputField,dropDownId){
	
	var valu = $('#'+inputField).val();
	
	var mySelect = $('#'+dropDownId);
	mySelect.append(
	        $('<option selected></option>').val(valu).html(valu)
	    );
	
	$('#'+inputField).val('');
}