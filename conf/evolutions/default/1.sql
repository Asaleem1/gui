# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table action (
  action_id                 bigint auto_increment not null,
  action_name               varchar(255),
  action_in_use             varchar(255),
  organisation_id           bigint,
  constraint pk_action primary key (action_id))
;

create table action_in_use (
  action_in_use_id          bigint auto_increment not null,
  action_id                 bigint,
  action_value_id           bigint,
  constraint pk_action_in_use primary key (action_in_use_id))
;

create table action_property_file (
  action_id                 bigint auto_increment not null,
  action_type               varchar(255),
  action_value              varchar(255),
  organisation_id           bigint,
  constraint pk_action_property_file primary key (action_id))
;

create table combining_algorithm (
  combining_algorithm_id    bigint auto_increment not null,
  combining_algorithm       varchar(255),
  organisation_id           bigint,
  constraint pk_combining_algorithm primary key (combining_algorithm_id))
;

create table gproperty_file (
  g_property_file_id        bigint auto_increment not null,
  g_property_file_name      varchar(255),
  g_property_type           varchar(255),
  organisation_id           bigint,
  constraint pk_gproperty_file primary key (g_property_file_id))
;

create table gproperty_group_values (
  g_property_group_value_id bigint auto_increment not null,
  sub_type                  varchar(255),
  value                     varchar(255),
  g_property_group_id       bigint,
  constraint pk_gproperty_group_values primary key (g_property_group_value_id))
;

create table gproperty_groups (
  g_property_group_id       bigint auto_increment not null,
  g_property_file_id        bigint,
  constraint pk_gproperty_groups primary key (g_property_group_id))
;

create table graph_entity (
  graph_id                  bigint auto_increment not null,
  main_type                 varchar(255) not null,
  sub_type                  varchar(255) not null,
  value                     varchar(255) not null,
  organisation_id           bigint,
  constraint uq_graph_entity_1 unique (main_type,sub_type,value,organisation_id),
  constraint pk_graph_entity primary key (graph_id))
;

create table graph_entity_value (
  graph_entity_value_id     bigint auto_increment not null,
  main_type                 varchar(255),
  sub_type                  varchar(255),
  value                     varchar(255),
  organisation_id           bigint,
  constraint pk_graph_entity_value primary key (graph_entity_value_id))
;

create table group_in_use (
  group_in_use_id           bigint auto_increment not null,
  policy_id                 bigint,
  owned_by_rule_group_id    bigint,
  used_rule_group_id        bigint,
  constraint pk_group_in_use primary key (group_in_use_id))
;

create table join_file (
  join_file_id              bigint auto_increment not null,
  join_file_name            varchar(255),
  join_type                 varchar(255),
  organisation_id           bigint,
  constraint pk_join_file primary key (join_file_id))
;

create table join_group_values (
  property_group_value_id   bigint auto_increment not null,
  sub_type                  varchar(255),
  value                     varchar(255),
  join_group_id             bigint,
  constraint pk_join_group_values primary key (property_group_value_id))
;

create table join_groups (
  join_group_id             bigint auto_increment not null,
  join_file_id              bigint,
  constraint pk_join_groups primary key (join_group_id))
;

create table organisation (
  organisation_id           bigint auto_increment not null,
  organisation_name         varchar(255),
  organisation_address      varchar(255),
  organisation_type         varchar(255),
  contact_number            varchar(255),
  email                     varchar(255),
  date_joining              datetime,
  constraint pk_organisation primary key (organisation_id))
;

create table permission (
  permission_id             bigint auto_increment not null,
  permission_value          varchar(255),
  organisation_id           bigint,
  constraint pk_permission primary key (permission_id))
;

create table policy (
  policy_id                 bigint auto_increment not null,
  policy_name               varchar(255),
  policy_description        varchar(255),
  organisation_id           bigint,
  constraint pk_policy primary key (policy_id))
;

create table properties_file (
  property_file_id          bigint auto_increment not null,
  property_file_name        varchar(255),
  seed                      integer,
  property_type             varchar(255),
  organisation_id           bigint,
  constraint pk_properties_file primary key (property_file_id))
;

create table property_group_values (
  property_group_value_id   bigint auto_increment not null,
  sub_type                  varchar(255),
  value                     varchar(255),
  selected_option           varchar(255),
  property_group_id         bigint,
  constraint pk_property_group_values primary key (property_group_value_id))
;

create table property_groups (
  property_group_id         bigint auto_increment not null,
  name_prefix               varchar(255),
  combination_count         bigint,
  small_instance_count      bigint,
  medium_instance_count     bigint,
  large_instance_count      bigint,
  property_file_id          bigint,
  constraint pk_property_groups primary key (property_group_id))
;

create table resource (
  resource_id               bigint auto_increment not null,
  asset_confidentiality_use varchar(255),
  asset_integrity_use       varchar(255),
  asset_type_use            varchar(255),
  constraint pk_resource primary key (resource_id))
;

create table resource_in_use (
  resource_in_use_id        bigint auto_increment not null,
  resource_id               bigint,
  resource_value_id         bigint,
  constraint pk_resource_in_use primary key (resource_in_use_id))
;

create table rule (
  rule_id                   bigint auto_increment not null,
  rule_name                 varchar(255),
  rule_description          varchar(255),
  subject_id                bigint,
  resource_id               bigint,
  action_id                 bigint,
  permission_id             bigint,
  policy_id                 bigint,
  constraint pk_rule primary key (rule_id))
;

create table rule_group (
  rule_group_id             bigint auto_increment not null,
  rule_group_name           varchar(255),
  combining_algorithm_id    bigint,
  group_description         varchar(255),
  subject_id                bigint,
  resource_id               bigint,
  action_id                 bigint,
  policy_id                 bigint,
  is_base_group             tinyint(1) default 0,
  constraint pk_rule_group primary key (rule_group_id))
;

create table rule_in_use (
  rule_in_use_id            bigint auto_increment not null,
  rule_group_id             bigint,
  rule_id                   bigint,
  policy_id                 bigint,
  constraint pk_rule_in_use primary key (rule_in_use_id))
;

create table subject (
  subject_id                bigint auto_increment not null,
  subject_type              varchar(255),
  organization_use          varchar(255),
  organization_role_use     varchar(255),
  organization_sector_use   varchar(255),
  member_function_use       varchar(255),
  member_role_use           varchar(255),
  member_level_use          varchar(255),
  constraint pk_subject primary key (subject_id))
;

create table subject_in_use (
  subject_in_use_id         bigint auto_increment not null,
  subject_id                bigint,
  subject_value_id          bigint,
  constraint pk_subject_in_use primary key (subject_in_use_id))
;

create table user (
  user_id                   bigint auto_increment not null,
  email                     varchar(255),
  contact_number            varchar(255),
  first_name                varchar(255),
  sur_name                  varchar(255),
  date_joining              datetime,
  username                  varchar(255),
  password                  varchar(255),
  type                      varchar(255),
  organisation_id           bigint,
  constraint uq_user_email unique (email),
  constraint uq_user_username unique (username),
  constraint pk_user primary key (user_id))
;

alter table action add constraint fk_action_organisationId_1 foreign key (organisation_id) references organisation (organisation_id) on delete restrict on update restrict;
create index ix_action_organisationId_1 on action (organisation_id);
alter table action_in_use add constraint fk_action_in_use_actionId_2 foreign key (action_id) references action (action_id) on delete restrict on update restrict;
create index ix_action_in_use_actionId_2 on action_in_use (action_id);
alter table action_in_use add constraint fk_action_in_use_actionValueId_3 foreign key (action_value_id) references action_property_file (action_id) on delete restrict on update restrict;
create index ix_action_in_use_actionValueId_3 on action_in_use (action_value_id);
alter table action_property_file add constraint fk_action_property_file_organisationId_4 foreign key (organisation_id) references organisation (organisation_id) on delete restrict on update restrict;
create index ix_action_property_file_organisationId_4 on action_property_file (organisation_id);
alter table combining_algorithm add constraint fk_combining_algorithm_organisationId_5 foreign key (organisation_id) references organisation (organisation_id) on delete restrict on update restrict;
create index ix_combining_algorithm_organisationId_5 on combining_algorithm (organisation_id);
alter table gproperty_file add constraint fk_gproperty_file_organisationId_6 foreign key (organisation_id) references organisation (organisation_id) on delete restrict on update restrict;
create index ix_gproperty_file_organisationId_6 on gproperty_file (organisation_id);
alter table gproperty_group_values add constraint fk_gproperty_group_values_gPropertyGroupId_7 foreign key (g_property_group_id) references gproperty_groups (g_property_group_id) on delete restrict on update restrict;
create index ix_gproperty_group_values_gPropertyGroupId_7 on gproperty_group_values (g_property_group_id);
alter table gproperty_groups add constraint fk_gproperty_groups_gPropertyFileId_8 foreign key (g_property_file_id) references gproperty_file (g_property_file_id) on delete restrict on update restrict;
create index ix_gproperty_groups_gPropertyFileId_8 on gproperty_groups (g_property_file_id);
alter table graph_entity add constraint fk_graph_entity_organisationId_9 foreign key (organisation_id) references organisation (organisation_id) on delete restrict on update restrict;
create index ix_graph_entity_organisationId_9 on graph_entity (organisation_id);
alter table graph_entity_value add constraint fk_graph_entity_value_organisationId_10 foreign key (organisation_id) references organisation (organisation_id) on delete restrict on update restrict;
create index ix_graph_entity_value_organisationId_10 on graph_entity_value (organisation_id);
alter table group_in_use add constraint fk_group_in_use_policyId_11 foreign key (policy_id) references policy (policy_id) on delete restrict on update restrict;
create index ix_group_in_use_policyId_11 on group_in_use (policy_id);
alter table group_in_use add constraint fk_group_in_use_ownedByRuleGroupId_12 foreign key (owned_by_rule_group_id) references rule_group (rule_group_id) on delete restrict on update restrict;
create index ix_group_in_use_ownedByRuleGroupId_12 on group_in_use (owned_by_rule_group_id);
alter table group_in_use add constraint fk_group_in_use_usedRuleGroupId_13 foreign key (used_rule_group_id) references rule_group (rule_group_id) on delete restrict on update restrict;
create index ix_group_in_use_usedRuleGroupId_13 on group_in_use (used_rule_group_id);
alter table join_file add constraint fk_join_file_organisationId_14 foreign key (organisation_id) references organisation (organisation_id) on delete restrict on update restrict;
create index ix_join_file_organisationId_14 on join_file (organisation_id);
alter table join_group_values add constraint fk_join_group_values_joinGroupId_15 foreign key (join_group_id) references join_groups (join_group_id) on delete restrict on update restrict;
create index ix_join_group_values_joinGroupId_15 on join_group_values (join_group_id);
alter table join_groups add constraint fk_join_groups_joinFileId_16 foreign key (join_file_id) references join_file (join_file_id) on delete restrict on update restrict;
create index ix_join_groups_joinFileId_16 on join_groups (join_file_id);
alter table permission add constraint fk_permission_organisationId_17 foreign key (organisation_id) references organisation (organisation_id) on delete restrict on update restrict;
create index ix_permission_organisationId_17 on permission (organisation_id);
alter table policy add constraint fk_policy_organisationId_18 foreign key (organisation_id) references organisation (organisation_id) on delete restrict on update restrict;
create index ix_policy_organisationId_18 on policy (organisation_id);
alter table properties_file add constraint fk_properties_file_organisationId_19 foreign key (organisation_id) references organisation (organisation_id) on delete restrict on update restrict;
create index ix_properties_file_organisationId_19 on properties_file (organisation_id);
alter table property_group_values add constraint fk_property_group_values_propertyGroupId_20 foreign key (property_group_id) references property_groups (property_group_id) on delete restrict on update restrict;
create index ix_property_group_values_propertyGroupId_20 on property_group_values (property_group_id);
alter table property_groups add constraint fk_property_groups_propertyFileId_21 foreign key (property_file_id) references properties_file (property_file_id) on delete restrict on update restrict;
create index ix_property_groups_propertyFileId_21 on property_groups (property_file_id);
alter table resource_in_use add constraint fk_resource_in_use_resourceId_22 foreign key (resource_id) references resource (resource_id) on delete restrict on update restrict;
create index ix_resource_in_use_resourceId_22 on resource_in_use (resource_id);
alter table resource_in_use add constraint fk_resource_in_use_resourceValueId_23 foreign key (resource_value_id) references graph_entity_value (graph_entity_value_id) on delete restrict on update restrict;
create index ix_resource_in_use_resourceValueId_23 on resource_in_use (resource_value_id);
alter table rule add constraint fk_rule_subjectId_24 foreign key (subject_id) references subject (subject_id) on delete restrict on update restrict;
create index ix_rule_subjectId_24 on rule (subject_id);
alter table rule add constraint fk_rule_resourceId_25 foreign key (resource_id) references resource (resource_id) on delete restrict on update restrict;
create index ix_rule_resourceId_25 on rule (resource_id);
alter table rule add constraint fk_rule_actionId_26 foreign key (action_id) references action (action_id) on delete restrict on update restrict;
create index ix_rule_actionId_26 on rule (action_id);
alter table rule add constraint fk_rule_permissionId_27 foreign key (permission_id) references permission (permission_id) on delete restrict on update restrict;
create index ix_rule_permissionId_27 on rule (permission_id);
alter table rule add constraint fk_rule_policyId_28 foreign key (policy_id) references policy (policy_id) on delete restrict on update restrict;
create index ix_rule_policyId_28 on rule (policy_id);
alter table rule_group add constraint fk_rule_group_combiningAlgorithmId_29 foreign key (combining_algorithm_id) references combining_algorithm (combining_algorithm_id) on delete restrict on update restrict;
create index ix_rule_group_combiningAlgorithmId_29 on rule_group (combining_algorithm_id);
alter table rule_group add constraint fk_rule_group_subjectId_30 foreign key (subject_id) references subject (subject_id) on delete restrict on update restrict;
create index ix_rule_group_subjectId_30 on rule_group (subject_id);
alter table rule_group add constraint fk_rule_group_resourceId_31 foreign key (resource_id) references resource (resource_id) on delete restrict on update restrict;
create index ix_rule_group_resourceId_31 on rule_group (resource_id);
alter table rule_group add constraint fk_rule_group_actionId_32 foreign key (action_id) references action (action_id) on delete restrict on update restrict;
create index ix_rule_group_actionId_32 on rule_group (action_id);
alter table rule_group add constraint fk_rule_group_policyId_33 foreign key (policy_id) references policy (policy_id) on delete restrict on update restrict;
create index ix_rule_group_policyId_33 on rule_group (policy_id);
alter table rule_in_use add constraint fk_rule_in_use_ruleGroupId_34 foreign key (rule_group_id) references rule_group (rule_group_id) on delete restrict on update restrict;
create index ix_rule_in_use_ruleGroupId_34 on rule_in_use (rule_group_id);
alter table rule_in_use add constraint fk_rule_in_use_ruleId_35 foreign key (rule_id) references rule (rule_id) on delete restrict on update restrict;
create index ix_rule_in_use_ruleId_35 on rule_in_use (rule_id);
alter table rule_in_use add constraint fk_rule_in_use_policyId_36 foreign key (policy_id) references policy (policy_id) on delete restrict on update restrict;
create index ix_rule_in_use_policyId_36 on rule_in_use (policy_id);
alter table subject_in_use add constraint fk_subject_in_use_subjectId_37 foreign key (subject_id) references subject (subject_id) on delete restrict on update restrict;
create index ix_subject_in_use_subjectId_37 on subject_in_use (subject_id);
alter table subject_in_use add constraint fk_subject_in_use_subjectValueId_38 foreign key (subject_value_id) references graph_entity_value (graph_entity_value_id) on delete restrict on update restrict;
create index ix_subject_in_use_subjectValueId_38 on subject_in_use (subject_value_id);
alter table user add constraint fk_user_organisationId_39 foreign key (organisation_id) references organisation (organisation_id) on delete restrict on update restrict;
create index ix_user_organisationId_39 on user (organisation_id);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table action;

drop table action_in_use;

drop table action_property_file;

drop table combining_algorithm;

drop table gproperty_file;

drop table gproperty_group_values;

drop table gproperty_groups;

drop table graph_entity;

drop table graph_entity_value;

drop table group_in_use;

drop table join_file;

drop table join_group_values;

drop table join_groups;

drop table organisation;

drop table permission;

drop table policy;

drop table properties_file;

drop table property_group_values;

drop table property_groups;

drop table resource;

drop table resource_in_use;

drop table rule;

drop table rule_group;

drop table rule_in_use;

drop table subject;

drop table subject_in_use;

drop table user;

SET FOREIGN_KEY_CHECKS=1;

