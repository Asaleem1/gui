// @SOURCE:C:/Users/HP/Documents/gui/conf/routes
// @HASH:8b03cef06ae561deda3d1d95f00d7b842a451743
// @DATE:Mon Mar 21 16:33:45 GMT 2016

import Routes.{prefix => _prefix, defaultPrefix => _defaultPrefix}
import play.core._
import play.core.Router._
import play.core.Router.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._
import _root_.controllers.Assets.Asset
import _root_.play.libs.F

import Router.queryString


// @LINE:118
// @LINE:116
// @LINE:113
// @LINE:109
// @LINE:108
// @LINE:107
// @LINE:106
// @LINE:105
// @LINE:104
// @LINE:103
// @LINE:100
// @LINE:99
// @LINE:98
// @LINE:97
// @LINE:96
// @LINE:95
// @LINE:93
// @LINE:91
// @LINE:90
// @LINE:89
// @LINE:87
// @LINE:86
// @LINE:85
// @LINE:84
// @LINE:83
// @LINE:82
// @LINE:81
// @LINE:80
// @LINE:78
// @LINE:77
// @LINE:76
// @LINE:74
// @LINE:73
// @LINE:72
// @LINE:71
// @LINE:70
// @LINE:68
// @LINE:67
// @LINE:64
// @LINE:63
// @LINE:62
// @LINE:61
// @LINE:60
// @LINE:59
// @LINE:58
// @LINE:56
// @LINE:55
// @LINE:54
// @LINE:52
// @LINE:51
// @LINE:50
// @LINE:48
// @LINE:47
// @LINE:46
// @LINE:44
// @LINE:43
// @LINE:42
// @LINE:40
// @LINE:39
// @LINE:38
// @LINE:36
// @LINE:35
// @LINE:34
// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:28
// @LINE:27
// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:22
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:8
// @LINE:7
package controllers {

// @LINE:113
class ReverseAssets {


// @LINE:113
def at(file:String): Call = {
   implicit val _rrc = new ReverseRouteContext(Map(("path", "/public")))
   Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[PathBindable[String]].unbind("file", file))
}
                        

}
                          

// @LINE:40
// @LINE:39
// @LINE:38
class ReverseSubjectController {


// @LINE:39
def addSubjects(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "addSubjects")
}
                        

// @LINE:40
def editSubjects(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "editSubjects")
}
                        

// @LINE:38
def submitAddSubject(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "submitAddSubject")
}
                        

}
                          

// @LINE:52
// @LINE:51
// @LINE:50
// @LINE:28
// @LINE:27
// @LINE:26
// @LINE:25
// @LINE:22
class ReverseRuleController {


// @LINE:22
def combineRule(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "combineRule")
}
                        

// @LINE:52
def submitAddPermissions(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "submitAddPermissions")
}
                        

// @LINE:26
def addRule(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "addRule")
}
                        

// @LINE:27
def addRules(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "addRules")
}
                        

// @LINE:51
def editPermissions(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "editPermissions")
}
                        

// @LINE:28
def submitCombineRule(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "submitCombineRule")
}
                        

// @LINE:50
def addPermissions(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "addPermissions")
}
                        

// @LINE:25
def addNewRule(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "addNewRule")
}
                        

}
                          

// @LINE:100
// @LINE:95
// @LINE:93
// @LINE:89
// @LINE:87
// @LINE:86
// @LINE:85
// @LINE:84
// @LINE:83
// @LINE:82
// @LINE:81
// @LINE:80
// @LINE:78
// @LINE:77
// @LINE:76
// @LINE:68
// @LINE:67
// @LINE:63
// @LINE:60
// @LINE:59
// @LINE:58
class ReversePropertyFileController {


// @LINE:63
def addMemberPropertyGroup(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "addMemberPropertyGroup")
}
                        

// @LINE:68
def submitPropertyValue(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "submitPropertyValue")
}
                        

// @LINE:60
def actionProperty(playframework_escape_type:String): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "actionProperty" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("type", playframework_escape_type)))))
}
                        

// @LINE:80
def updateActionValue(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "updateActionValue")
}
                        

// @LINE:93
def PropertyEdit(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "PropertyEdit")
}
                        

// @LINE:87
def deleteAlgorithm(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "deleteAlgorithm")
}
                        

// @LINE:76
def updatePropertyFiles(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "updatePropertyFiles")
}
                        

// @LINE:67
def addPropertyValue(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "addPropertyValue")
}
                        

// @LINE:58
def memberProperty(playframework_escape_type:String): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "memberProperty" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("type", playframework_escape_type)))))
}
                        

// @LINE:77
def fetchAllValues(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "fetchAllValues")
}
                        

// @LINE:100
def submitAssetPropertyEdit(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "submitAssetPropertyEdit")
}
                        

// @LINE:95
def groups(playframework_escape_type:String): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "groups" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("type", playframework_escape_type)))))
}
                        

// @LINE:82
def updateAttributeValue(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "updateAttributeValue")
}
                        

// @LINE:59
def assetProperty(playframework_escape_type:String): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "assetProperty" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("type", playframework_escape_type)))))
}
                        

// @LINE:86
def updateAlgorithm(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "updateAlgorithm")
}
                        

// @LINE:89
def deleteGroup(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "deleteGroup")
}
                        

// @LINE:81
def deleteActionValue(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "deleteActionValue")
}
                        

// @LINE:85
def deletePermission(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "deletePermission")
}
                        

// @LINE:78
def fetchActionValues(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "fetchActionValues")
}
                        

// @LINE:83
def deleteAttributeValue(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "deleteAttributeValue")
}
                        

// @LINE:84
def updatePermission(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "updatePermission")
}
                        

}
                          

// @LINE:48
// @LINE:47
// @LINE:46
class ReverseCombiningAlgorithmController {


// @LINE:47
def addCombiningAlgorithm(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "addCombiningAlogrithm")
}
                        

// @LINE:48
def editCombiningAlgorithm(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "editCombiningAlgorithm")
}
                        

// @LINE:46
def submitAddCombiningAlgorithm(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "sumbitAddCombiningAlgorithm")
}
                        

}
                          

// @LINE:99
// @LINE:98
// @LINE:97
// @LINE:96
// @LINE:90
// @LINE:74
// @LINE:73
// @LINE:64
class ReverseGroupController {


// @LINE:96
def groupProperty(playframework_escape_type:String): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "groupProperty" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("type", playframework_escape_type)))))
}
                        

// @LINE:97
def groupFileGroups(playframework_escape_type:String): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "groupFileGroups" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("type", playframework_escape_type)))))
}
                        

// @LINE:74
def submitGroupFile(playframework_escape_type:String): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "submitGroupFile" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("type", playframework_escape_type)))))
}
                        

// @LINE:64
def addActionPropertyGroup(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "addActionPropertyGroup")
}
                        

// @LINE:73
def addGroupPropertyGroup(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "addGroupPropertyGroup")
}
                        

// @LINE:98
def editGroupPropertyGroup(playframework_escape_type:String, groupId:Long): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "editGroupPropertyGroup" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("type", playframework_escape_type)), Some(implicitly[QueryStringBindable[Long]].unbind("groupId", groupId)))))
}
                        

// @LINE:99
def GPropertyEdit(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "GPropertyEdit")
}
                        

// @LINE:90
def deleteGPropertyGroup(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "deleteGPropertyGroup")
}
                        

}
                          

// @LINE:36
// @LINE:34
class ReverseActionController {


// @LINE:36
def submitAddAction(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "submitAddActions")
}
                        

// @LINE:34
def addActions(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "addActions")
}
                        

}
                          

// @LINE:118
// @LINE:116
// @LINE:91
// @LINE:72
// @LINE:71
// @LINE:70
// @LINE:62
// @LINE:61
// @LINE:35
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:8
// @LINE:7
class ReverseApplication {


// @LINE:35
def editAttributes(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "editAttributes")
}
                        

// @LINE:91
def deleteFile(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "deleteFile")
}
                        

// @LINE:16
def submitUser(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "submitUser")
}
                        

// @LINE:15
def addUser(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "addUser")
}
                        

// @LINE:9
def adminHome(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "adminHome")
}
                        

// @LINE:61
def newFile(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "newFile")
}
                        

// @LINE:71
def submitPropertyFile(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "submitPropertyFile")
}
                        

// @LINE:70
def propertyFiles(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "propertyFiles")
}
                        

// @LINE:21
def result(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "result")
}
                        

// @LINE:19
def policies(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "policies")
}
                        

// @LINE:62
def submitMemberFile(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "submitMemberFile")
}
                        

// @LINE:12
def about(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "aboutUs")
}
                        

// @LINE:14
def print(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "print")
}
                        

// @LINE:11
def logout(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "logout")
}
                        

// @LINE:8
def home(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "home")
}
                        

// @LINE:20
def runPolicy(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "runPolicy")
}
                        

// @LINE:118
def getMainTypes(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "getMainTypes")
}
                        

// @LINE:7
def authenticate(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "authenticate")
}
                        

// @LINE:72
def exportAttributes(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "exportAttributes")
}
                        

// @LINE:17
def index(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix)
}
                        

// @LINE:13
def contact(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "contact")
}
                        

// @LINE:116
def javascriptRoutes(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "javascriptRoutes")
}
                        

// @LINE:10
def login(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "login")
}
                        

}
                          

// @LINE:44
// @LINE:43
// @LINE:42
class ReverseResourceController {


// @LINE:44
def editResources(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "editResources")
}
                        

// @LINE:43
def addResources(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "addResources")
}
                        

// @LINE:42
def submitAddResource(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "submitAddResource")
}
                        

}
                          

// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:24
class ReversePolicyController {


// @LINE:31
def editPolicy(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "editPolicy")
}
                        

// @LINE:32
def editPolicySubmit(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "editPolicySubmit")
}
                        

// @LINE:30
def submitActionPolicy(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "submitActionPolicy")
}
                        

// @LINE:24
def newPolicy(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "newPolicy")
}
                        

}
                          

// @LINE:109
// @LINE:108
// @LINE:107
// @LINE:106
// @LINE:105
// @LINE:104
// @LINE:103
class ReverseJoinFileController {


// @LINE:105
def editJoinPropertyGroup(playframework_escape_type:String, groupId:Long): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "editJoinPropertyGroup" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("type", playframework_escape_type)), Some(implicitly[QueryStringBindable[Long]].unbind("groupId", groupId)))))
}
                        

// @LINE:106
def JoinPropertyEdit(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "JoinPropertyEdit")
}
                        

// @LINE:104
def joinFileGroups(playframework_escape_type:String): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "joinFileGroups" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("type", playframework_escape_type)))))
}
                        

// @LINE:103
def joinGroupFile(playframework_escape_type:String): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "joinGroupFile" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("type", playframework_escape_type)))))
}
                        

// @LINE:109
def addNewGroup(playframework_escape_type:String): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "addNewGroup" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("type", playframework_escape_type)))))
}
                        

// @LINE:108
def deleteJoinPropertyGroup(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "deleteJoinPropertyGroup")
}
                        

// @LINE:107
def submitJoinGroupFile(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "submitJoinGroupFile")
}
                        

}
                          

// @LINE:56
// @LINE:55
// @LINE:54
class ReverseGraphController {


// @LINE:55
def upload(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "upload")
}
                        

// @LINE:56
def parseGraphFile(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "parseGraphFile")
}
                        

// @LINE:54
def graphIntialize(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "graphIntialize")
}
                        

}
                          
}
                  


// @LINE:118
// @LINE:116
// @LINE:113
// @LINE:109
// @LINE:108
// @LINE:107
// @LINE:106
// @LINE:105
// @LINE:104
// @LINE:103
// @LINE:100
// @LINE:99
// @LINE:98
// @LINE:97
// @LINE:96
// @LINE:95
// @LINE:93
// @LINE:91
// @LINE:90
// @LINE:89
// @LINE:87
// @LINE:86
// @LINE:85
// @LINE:84
// @LINE:83
// @LINE:82
// @LINE:81
// @LINE:80
// @LINE:78
// @LINE:77
// @LINE:76
// @LINE:74
// @LINE:73
// @LINE:72
// @LINE:71
// @LINE:70
// @LINE:68
// @LINE:67
// @LINE:64
// @LINE:63
// @LINE:62
// @LINE:61
// @LINE:60
// @LINE:59
// @LINE:58
// @LINE:56
// @LINE:55
// @LINE:54
// @LINE:52
// @LINE:51
// @LINE:50
// @LINE:48
// @LINE:47
// @LINE:46
// @LINE:44
// @LINE:43
// @LINE:42
// @LINE:40
// @LINE:39
// @LINE:38
// @LINE:36
// @LINE:35
// @LINE:34
// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:28
// @LINE:27
// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:22
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:8
// @LINE:7
package controllers.javascript {
import ReverseRouteContext.empty

// @LINE:113
class ReverseAssets {


// @LINE:113
def at : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Assets.at",
   """
      function(file) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
   """
)
                        

}
              

// @LINE:40
// @LINE:39
// @LINE:38
class ReverseSubjectController {


// @LINE:39
def addSubjects : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.SubjectController.addSubjects",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "addSubjects"})
      }
   """
)
                        

// @LINE:40
def editSubjects : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.SubjectController.editSubjects",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "editSubjects"})
      }
   """
)
                        

// @LINE:38
def submitAddSubject : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.SubjectController.submitAddSubject",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "submitAddSubject"})
      }
   """
)
                        

}
              

// @LINE:52
// @LINE:51
// @LINE:50
// @LINE:28
// @LINE:27
// @LINE:26
// @LINE:25
// @LINE:22
class ReverseRuleController {


// @LINE:22
def combineRule : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.RuleController.combineRule",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "combineRule"})
      }
   """
)
                        

// @LINE:52
def submitAddPermissions : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.RuleController.submitAddPermissions",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "submitAddPermissions"})
      }
   """
)
                        

// @LINE:26
def addRule : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.RuleController.addRule",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "addRule"})
      }
   """
)
                        

// @LINE:27
def addRules : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.RuleController.addRules",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "addRules"})
      }
   """
)
                        

// @LINE:51
def editPermissions : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.RuleController.editPermissions",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "editPermissions"})
      }
   """
)
                        

// @LINE:28
def submitCombineRule : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.RuleController.submitCombineRule",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "submitCombineRule"})
      }
   """
)
                        

// @LINE:50
def addPermissions : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.RuleController.addPermissions",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "addPermissions"})
      }
   """
)
                        

// @LINE:25
def addNewRule : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.RuleController.addNewRule",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "addNewRule"})
      }
   """
)
                        

}
              

// @LINE:100
// @LINE:95
// @LINE:93
// @LINE:89
// @LINE:87
// @LINE:86
// @LINE:85
// @LINE:84
// @LINE:83
// @LINE:82
// @LINE:81
// @LINE:80
// @LINE:78
// @LINE:77
// @LINE:76
// @LINE:68
// @LINE:67
// @LINE:63
// @LINE:60
// @LINE:59
// @LINE:58
class ReversePropertyFileController {


// @LINE:63
def addMemberPropertyGroup : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.addMemberPropertyGroup",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "addMemberPropertyGroup"})
      }
   """
)
                        

// @LINE:68
def submitPropertyValue : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.submitPropertyValue",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "submitPropertyValue"})
      }
   """
)
                        

// @LINE:60
def actionProperty : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.actionProperty",
   """
      function(type) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "actionProperty" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("type", type)])})
      }
   """
)
                        

// @LINE:80
def updateActionValue : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.updateActionValue",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "updateActionValue"})
      }
   """
)
                        

// @LINE:93
def PropertyEdit : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.PropertyEdit",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "PropertyEdit"})
      }
   """
)
                        

// @LINE:87
def deleteAlgorithm : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.deleteAlgorithm",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "deleteAlgorithm"})
      }
   """
)
                        

// @LINE:76
def updatePropertyFiles : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.updatePropertyFiles",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "updatePropertyFiles"})
      }
   """
)
                        

// @LINE:67
def addPropertyValue : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.addPropertyValue",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "addPropertyValue"})
      }
   """
)
                        

// @LINE:58
def memberProperty : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.memberProperty",
   """
      function(type) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "memberProperty" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("type", type)])})
      }
   """
)
                        

// @LINE:77
def fetchAllValues : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.fetchAllValues",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "fetchAllValues"})
      }
   """
)
                        

// @LINE:100
def submitAssetPropertyEdit : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.submitAssetPropertyEdit",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "submitAssetPropertyEdit"})
      }
   """
)
                        

// @LINE:95
def groups : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.groups",
   """
      function(type) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "groups" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("type", type)])})
      }
   """
)
                        

// @LINE:82
def updateAttributeValue : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.updateAttributeValue",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "updateAttributeValue"})
      }
   """
)
                        

// @LINE:59
def assetProperty : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.assetProperty",
   """
      function(type) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assetProperty" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("type", type)])})
      }
   """
)
                        

// @LINE:86
def updateAlgorithm : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.updateAlgorithm",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "updateAlgorithm"})
      }
   """
)
                        

// @LINE:89
def deleteGroup : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.deleteGroup",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "deleteGroup"})
      }
   """
)
                        

// @LINE:81
def deleteActionValue : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.deleteActionValue",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "deleteActionValue"})
      }
   """
)
                        

// @LINE:85
def deletePermission : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.deletePermission",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "deletePermission"})
      }
   """
)
                        

// @LINE:78
def fetchActionValues : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.fetchActionValues",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "fetchActionValues"})
      }
   """
)
                        

// @LINE:83
def deleteAttributeValue : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.deleteAttributeValue",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "deleteAttributeValue"})
      }
   """
)
                        

// @LINE:84
def updatePermission : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PropertyFileController.updatePermission",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "updatePermission"})
      }
   """
)
                        

}
              

// @LINE:48
// @LINE:47
// @LINE:46
class ReverseCombiningAlgorithmController {


// @LINE:47
def addCombiningAlgorithm : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CombiningAlgorithmController.addCombiningAlgorithm",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "addCombiningAlogrithm"})
      }
   """
)
                        

// @LINE:48
def editCombiningAlgorithm : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CombiningAlgorithmController.editCombiningAlgorithm",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "editCombiningAlgorithm"})
      }
   """
)
                        

// @LINE:46
def submitAddCombiningAlgorithm : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CombiningAlgorithmController.submitAddCombiningAlgorithm",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "sumbitAddCombiningAlgorithm"})
      }
   """
)
                        

}
              

// @LINE:99
// @LINE:98
// @LINE:97
// @LINE:96
// @LINE:90
// @LINE:74
// @LINE:73
// @LINE:64
class ReverseGroupController {


// @LINE:96
def groupProperty : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.GroupController.groupProperty",
   """
      function(type) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "groupProperty" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("type", type)])})
      }
   """
)
                        

// @LINE:97
def groupFileGroups : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.GroupController.groupFileGroups",
   """
      function(type) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "groupFileGroups" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("type", type)])})
      }
   """
)
                        

// @LINE:74
def submitGroupFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.GroupController.submitGroupFile",
   """
      function(type) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "submitGroupFile" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("type", type)])})
      }
   """
)
                        

// @LINE:64
def addActionPropertyGroup : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.GroupController.addActionPropertyGroup",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "addActionPropertyGroup"})
      }
   """
)
                        

// @LINE:73
def addGroupPropertyGroup : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.GroupController.addGroupPropertyGroup",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "addGroupPropertyGroup"})
      }
   """
)
                        

// @LINE:98
def editGroupPropertyGroup : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.GroupController.editGroupPropertyGroup",
   """
      function(type,groupId) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "editGroupPropertyGroup" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("type", type), (""" + implicitly[QueryStringBindable[Long]].javascriptUnbind + """)("groupId", groupId)])})
      }
   """
)
                        

// @LINE:99
def GPropertyEdit : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.GroupController.GPropertyEdit",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "GPropertyEdit"})
      }
   """
)
                        

// @LINE:90
def deleteGPropertyGroup : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.GroupController.deleteGPropertyGroup",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "deleteGPropertyGroup"})
      }
   """
)
                        

}
              

// @LINE:36
// @LINE:34
class ReverseActionController {


// @LINE:36
def submitAddAction : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ActionController.submitAddAction",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "submitAddActions"})
      }
   """
)
                        

// @LINE:34
def addActions : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ActionController.addActions",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "addActions"})
      }
   """
)
                        

}
              

// @LINE:118
// @LINE:116
// @LINE:91
// @LINE:72
// @LINE:71
// @LINE:70
// @LINE:62
// @LINE:61
// @LINE:35
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:8
// @LINE:7
class ReverseApplication {


// @LINE:35
def editAttributes : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.editAttributes",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "editAttributes"})
      }
   """
)
                        

// @LINE:91
def deleteFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.deleteFile",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "deleteFile"})
      }
   """
)
                        

// @LINE:16
def submitUser : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.submitUser",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "submitUser"})
      }
   """
)
                        

// @LINE:15
def addUser : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.addUser",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "addUser"})
      }
   """
)
                        

// @LINE:9
def adminHome : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.adminHome",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "adminHome"})
      }
   """
)
                        

// @LINE:61
def newFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.newFile",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "newFile"})
      }
   """
)
                        

// @LINE:71
def submitPropertyFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.submitPropertyFile",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "submitPropertyFile"})
      }
   """
)
                        

// @LINE:70
def propertyFiles : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.propertyFiles",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "propertyFiles"})
      }
   """
)
                        

// @LINE:21
def result : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.result",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "result"})
      }
   """
)
                        

// @LINE:19
def policies : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.policies",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "policies"})
      }
   """
)
                        

// @LINE:62
def submitMemberFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.submitMemberFile",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "submitMemberFile"})
      }
   """
)
                        

// @LINE:12
def about : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.about",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "aboutUs"})
      }
   """
)
                        

// @LINE:14
def print : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.print",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "print"})
      }
   """
)
                        

// @LINE:11
def logout : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.logout",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "logout"})
      }
   """
)
                        

// @LINE:8
def home : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.home",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "home"})
      }
   """
)
                        

// @LINE:20
def runPolicy : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.runPolicy",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "runPolicy"})
      }
   """
)
                        

// @LINE:118
def getMainTypes : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.getMainTypes",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "getMainTypes"})
      }
   """
)
                        

// @LINE:7
def authenticate : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.authenticate",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "authenticate"})
      }
   """
)
                        

// @LINE:72
def exportAttributes : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.exportAttributes",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "exportAttributes"})
      }
   """
)
                        

// @LINE:17
def index : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.index",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + """"})
      }
   """
)
                        

// @LINE:13
def contact : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.contact",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "contact"})
      }
   """
)
                        

// @LINE:116
def javascriptRoutes : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.javascriptRoutes",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "javascriptRoutes"})
      }
   """
)
                        

// @LINE:10
def login : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.login",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "login"})
      }
   """
)
                        

}
              

// @LINE:44
// @LINE:43
// @LINE:42
class ReverseResourceController {


// @LINE:44
def editResources : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ResourceController.editResources",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "editResources"})
      }
   """
)
                        

// @LINE:43
def addResources : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ResourceController.addResources",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "addResources"})
      }
   """
)
                        

// @LINE:42
def submitAddResource : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ResourceController.submitAddResource",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "submitAddResource"})
      }
   """
)
                        

}
              

// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:24
class ReversePolicyController {


// @LINE:31
def editPolicy : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PolicyController.editPolicy",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "editPolicy"})
      }
   """
)
                        

// @LINE:32
def editPolicySubmit : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PolicyController.editPolicySubmit",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "editPolicySubmit"})
      }
   """
)
                        

// @LINE:30
def submitActionPolicy : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PolicyController.submitActionPolicy",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "submitActionPolicy"})
      }
   """
)
                        

// @LINE:24
def newPolicy : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.PolicyController.newPolicy",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "newPolicy"})
      }
   """
)
                        

}
              

// @LINE:109
// @LINE:108
// @LINE:107
// @LINE:106
// @LINE:105
// @LINE:104
// @LINE:103
class ReverseJoinFileController {


// @LINE:105
def editJoinPropertyGroup : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.JoinFileController.editJoinPropertyGroup",
   """
      function(type,groupId) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "editJoinPropertyGroup" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("type", type), (""" + implicitly[QueryStringBindable[Long]].javascriptUnbind + """)("groupId", groupId)])})
      }
   """
)
                        

// @LINE:106
def JoinPropertyEdit : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.JoinFileController.JoinPropertyEdit",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "JoinPropertyEdit"})
      }
   """
)
                        

// @LINE:104
def joinFileGroups : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.JoinFileController.joinFileGroups",
   """
      function(type) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "joinFileGroups" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("type", type)])})
      }
   """
)
                        

// @LINE:103
def joinGroupFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.JoinFileController.joinGroupFile",
   """
      function(type) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "joinGroupFile" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("type", type)])})
      }
   """
)
                        

// @LINE:109
def addNewGroup : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.JoinFileController.addNewGroup",
   """
      function(type) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "addNewGroup" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("type", type)])})
      }
   """
)
                        

// @LINE:108
def deleteJoinPropertyGroup : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.JoinFileController.deleteJoinPropertyGroup",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "deleteJoinPropertyGroup"})
      }
   """
)
                        

// @LINE:107
def submitJoinGroupFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.JoinFileController.submitJoinGroupFile",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "submitJoinGroupFile"})
      }
   """
)
                        

}
              

// @LINE:56
// @LINE:55
// @LINE:54
class ReverseGraphController {


// @LINE:55
def upload : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.GraphController.upload",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "upload"})
      }
   """
)
                        

// @LINE:56
def parseGraphFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.GraphController.parseGraphFile",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "parseGraphFile"})
      }
   """
)
                        

// @LINE:54
def graphIntialize : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.GraphController.graphIntialize",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "graphIntialize"})
      }
   """
)
                        

}
              
}
        


// @LINE:118
// @LINE:116
// @LINE:113
// @LINE:109
// @LINE:108
// @LINE:107
// @LINE:106
// @LINE:105
// @LINE:104
// @LINE:103
// @LINE:100
// @LINE:99
// @LINE:98
// @LINE:97
// @LINE:96
// @LINE:95
// @LINE:93
// @LINE:91
// @LINE:90
// @LINE:89
// @LINE:87
// @LINE:86
// @LINE:85
// @LINE:84
// @LINE:83
// @LINE:82
// @LINE:81
// @LINE:80
// @LINE:78
// @LINE:77
// @LINE:76
// @LINE:74
// @LINE:73
// @LINE:72
// @LINE:71
// @LINE:70
// @LINE:68
// @LINE:67
// @LINE:64
// @LINE:63
// @LINE:62
// @LINE:61
// @LINE:60
// @LINE:59
// @LINE:58
// @LINE:56
// @LINE:55
// @LINE:54
// @LINE:52
// @LINE:51
// @LINE:50
// @LINE:48
// @LINE:47
// @LINE:46
// @LINE:44
// @LINE:43
// @LINE:42
// @LINE:40
// @LINE:39
// @LINE:38
// @LINE:36
// @LINE:35
// @LINE:34
// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:28
// @LINE:27
// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:22
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:8
// @LINE:7
package controllers.ref {


// @LINE:113
class ReverseAssets {


// @LINE:113
def at(path:String, file:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Assets.at(path, file), HandlerDef(this.getClass.getClassLoader, "", "controllers.Assets", "at", Seq(classOf[String], classOf[String]), "GET", """ Map static resources from the /public folder to the /assets URL path""", _prefix + """assets/$file<.+>""")
)
                      

}
                          

// @LINE:40
// @LINE:39
// @LINE:38
class ReverseSubjectController {


// @LINE:39
def addSubjects(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.SubjectController.addSubjects(), HandlerDef(this.getClass.getClassLoader, "", "controllers.SubjectController", "addSubjects", Seq(), "GET", """""", _prefix + """addSubjects""")
)
                      

// @LINE:40
def editSubjects(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.SubjectController.editSubjects(), HandlerDef(this.getClass.getClassLoader, "", "controllers.SubjectController", "editSubjects", Seq(), "GET", """""", _prefix + """editSubjects""")
)
                      

// @LINE:38
def submitAddSubject(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.SubjectController.submitAddSubject(), HandlerDef(this.getClass.getClassLoader, "", "controllers.SubjectController", "submitAddSubject", Seq(), "POST", """""", _prefix + """submitAddSubject""")
)
                      

}
                          

// @LINE:52
// @LINE:51
// @LINE:50
// @LINE:28
// @LINE:27
// @LINE:26
// @LINE:25
// @LINE:22
class ReverseRuleController {


// @LINE:22
def combineRule(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.RuleController.combineRule(), HandlerDef(this.getClass.getClassLoader, "", "controllers.RuleController", "combineRule", Seq(), "GET", """""", _prefix + """combineRule""")
)
                      

// @LINE:52
def submitAddPermissions(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.RuleController.submitAddPermissions(), HandlerDef(this.getClass.getClassLoader, "", "controllers.RuleController", "submitAddPermissions", Seq(), "POST", """""", _prefix + """submitAddPermissions""")
)
                      

// @LINE:26
def addRule(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.RuleController.addRule(), HandlerDef(this.getClass.getClassLoader, "", "controllers.RuleController", "addRule", Seq(), "POST", """""", _prefix + """addRule""")
)
                      

// @LINE:27
def addRules(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.RuleController.addRules(), HandlerDef(this.getClass.getClassLoader, "", "controllers.RuleController", "addRules", Seq(), "GET", """""", _prefix + """addRules""")
)
                      

// @LINE:51
def editPermissions(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.RuleController.editPermissions(), HandlerDef(this.getClass.getClassLoader, "", "controllers.RuleController", "editPermissions", Seq(), "GET", """""", _prefix + """editPermissions""")
)
                      

// @LINE:28
def submitCombineRule(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.RuleController.submitCombineRule(), HandlerDef(this.getClass.getClassLoader, "", "controllers.RuleController", "submitCombineRule", Seq(), "POST", """""", _prefix + """submitCombineRule""")
)
                      

// @LINE:50
def addPermissions(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.RuleController.addPermissions(), HandlerDef(this.getClass.getClassLoader, "", "controllers.RuleController", "addPermissions", Seq(), "GET", """""", _prefix + """addPermissions""")
)
                      

// @LINE:25
def addNewRule(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.RuleController.addNewRule(), HandlerDef(this.getClass.getClassLoader, "", "controllers.RuleController", "addNewRule", Seq(), "POST", """""", _prefix + """addNewRule""")
)
                      

}
                          

// @LINE:100
// @LINE:95
// @LINE:93
// @LINE:89
// @LINE:87
// @LINE:86
// @LINE:85
// @LINE:84
// @LINE:83
// @LINE:82
// @LINE:81
// @LINE:80
// @LINE:78
// @LINE:77
// @LINE:76
// @LINE:68
// @LINE:67
// @LINE:63
// @LINE:60
// @LINE:59
// @LINE:58
class ReversePropertyFileController {


// @LINE:63
def addMemberPropertyGroup(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.addMemberPropertyGroup(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "addMemberPropertyGroup", Seq(), "POST", """""", _prefix + """addMemberPropertyGroup""")
)
                      

// @LINE:68
def submitPropertyValue(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.submitPropertyValue(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "submitPropertyValue", Seq(), "POST", """""", _prefix + """submitPropertyValue""")
)
                      

// @LINE:60
def actionProperty(playframework_escape_type:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.actionProperty(playframework_escape_type), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "actionProperty", Seq(classOf[String]), "GET", """""", _prefix + """actionProperty""")
)
                      

// @LINE:80
def updateActionValue(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.updateActionValue(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "updateActionValue", Seq(), "POST", """""", _prefix + """updateActionValue""")
)
                      

// @LINE:93
def PropertyEdit(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.PropertyEdit(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "PropertyEdit", Seq(), "GET", """""", _prefix + """PropertyEdit""")
)
                      

// @LINE:87
def deleteAlgorithm(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.deleteAlgorithm(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "deleteAlgorithm", Seq(), "POST", """""", _prefix + """deleteAlgorithm""")
)
                      

// @LINE:76
def updatePropertyFiles(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.updatePropertyFiles(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "updatePropertyFiles", Seq(), "POST", """""", _prefix + """updatePropertyFiles""")
)
                      

// @LINE:67
def addPropertyValue(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.addPropertyValue(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "addPropertyValue", Seq(), "GET", """""", _prefix + """addPropertyValue""")
)
                      

// @LINE:58
def memberProperty(playframework_escape_type:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.memberProperty(playframework_escape_type), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "memberProperty", Seq(classOf[String]), "GET", """""", _prefix + """memberProperty""")
)
                      

// @LINE:77
def fetchAllValues(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.fetchAllValues(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "fetchAllValues", Seq(), "POST", """""", _prefix + """fetchAllValues""")
)
                      

// @LINE:100
def submitAssetPropertyEdit(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.submitAssetPropertyEdit(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "submitAssetPropertyEdit", Seq(), "POST", """""", _prefix + """submitAssetPropertyEdit""")
)
                      

// @LINE:95
def groups(playframework_escape_type:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.groups(playframework_escape_type), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "groups", Seq(classOf[String]), "GET", """""", _prefix + """groups""")
)
                      

// @LINE:82
def updateAttributeValue(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.updateAttributeValue(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "updateAttributeValue", Seq(), "POST", """""", _prefix + """updateAttributeValue""")
)
                      

// @LINE:59
def assetProperty(playframework_escape_type:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.assetProperty(playframework_escape_type), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "assetProperty", Seq(classOf[String]), "GET", """""", _prefix + """assetProperty""")
)
                      

// @LINE:86
def updateAlgorithm(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.updateAlgorithm(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "updateAlgorithm", Seq(), "POST", """""", _prefix + """updateAlgorithm""")
)
                      

// @LINE:89
def deleteGroup(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.deleteGroup(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "deleteGroup", Seq(), "POST", """""", _prefix + """deleteGroup""")
)
                      

// @LINE:81
def deleteActionValue(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.deleteActionValue(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "deleteActionValue", Seq(), "POST", """""", _prefix + """deleteActionValue""")
)
                      

// @LINE:85
def deletePermission(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.deletePermission(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "deletePermission", Seq(), "POST", """""", _prefix + """deletePermission""")
)
                      

// @LINE:78
def fetchActionValues(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.fetchActionValues(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "fetchActionValues", Seq(), "POST", """""", _prefix + """fetchActionValues""")
)
                      

// @LINE:83
def deleteAttributeValue(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.deleteAttributeValue(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "deleteAttributeValue", Seq(), "POST", """""", _prefix + """deleteAttributeValue""")
)
                      

// @LINE:84
def updatePermission(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PropertyFileController.updatePermission(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "updatePermission", Seq(), "POST", """""", _prefix + """updatePermission""")
)
                      

}
                          

// @LINE:48
// @LINE:47
// @LINE:46
class ReverseCombiningAlgorithmController {


// @LINE:47
def addCombiningAlgorithm(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.CombiningAlgorithmController.addCombiningAlgorithm(), HandlerDef(this.getClass.getClassLoader, "", "controllers.CombiningAlgorithmController", "addCombiningAlgorithm", Seq(), "GET", """""", _prefix + """addCombiningAlogrithm""")
)
                      

// @LINE:48
def editCombiningAlgorithm(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.CombiningAlgorithmController.editCombiningAlgorithm(), HandlerDef(this.getClass.getClassLoader, "", "controllers.CombiningAlgorithmController", "editCombiningAlgorithm", Seq(), "GET", """""", _prefix + """editCombiningAlgorithm""")
)
                      

// @LINE:46
def submitAddCombiningAlgorithm(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.CombiningAlgorithmController.submitAddCombiningAlgorithm(), HandlerDef(this.getClass.getClassLoader, "", "controllers.CombiningAlgorithmController", "submitAddCombiningAlgorithm", Seq(), "POST", """""", _prefix + """sumbitAddCombiningAlgorithm""")
)
                      

}
                          

// @LINE:99
// @LINE:98
// @LINE:97
// @LINE:96
// @LINE:90
// @LINE:74
// @LINE:73
// @LINE:64
class ReverseGroupController {


// @LINE:96
def groupProperty(playframework_escape_type:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.GroupController.groupProperty(playframework_escape_type), HandlerDef(this.getClass.getClassLoader, "", "controllers.GroupController", "groupProperty", Seq(classOf[String]), "GET", """""", _prefix + """groupProperty""")
)
                      

// @LINE:97
def groupFileGroups(playframework_escape_type:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.GroupController.groupFileGroups(playframework_escape_type), HandlerDef(this.getClass.getClassLoader, "", "controllers.GroupController", "groupFileGroups", Seq(classOf[String]), "GET", """""", _prefix + """groupFileGroups""")
)
                      

// @LINE:74
def submitGroupFile(playframework_escape_type:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.GroupController.submitGroupFile(playframework_escape_type), HandlerDef(this.getClass.getClassLoader, "", "controllers.GroupController", "submitGroupFile", Seq(classOf[String]), "GET", """""", _prefix + """submitGroupFile""")
)
                      

// @LINE:64
def addActionPropertyGroup(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.GroupController.addActionPropertyGroup(), HandlerDef(this.getClass.getClassLoader, "", "controllers.GroupController", "addActionPropertyGroup", Seq(), "POST", """""", _prefix + """addActionPropertyGroup""")
)
                      

// @LINE:73
def addGroupPropertyGroup(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.GroupController.addGroupPropertyGroup(), HandlerDef(this.getClass.getClassLoader, "", "controllers.GroupController", "addGroupPropertyGroup", Seq(), "POST", """""", _prefix + """addGroupPropertyGroup""")
)
                      

// @LINE:98
def editGroupPropertyGroup(playframework_escape_type:String, groupId:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.GroupController.editGroupPropertyGroup(playframework_escape_type, groupId), HandlerDef(this.getClass.getClassLoader, "", "controllers.GroupController", "editGroupPropertyGroup", Seq(classOf[String], classOf[Long]), "GET", """""", _prefix + """editGroupPropertyGroup""")
)
                      

// @LINE:99
def GPropertyEdit(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.GroupController.GPropertyEdit(), HandlerDef(this.getClass.getClassLoader, "", "controllers.GroupController", "GPropertyEdit", Seq(), "POST", """""", _prefix + """GPropertyEdit""")
)
                      

// @LINE:90
def deleteGPropertyGroup(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.GroupController.deleteGPropertyGroup(), HandlerDef(this.getClass.getClassLoader, "", "controllers.GroupController", "deleteGPropertyGroup", Seq(), "POST", """""", _prefix + """deleteGPropertyGroup""")
)
                      

}
                          

// @LINE:36
// @LINE:34
class ReverseActionController {


// @LINE:36
def submitAddAction(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.ActionController.submitAddAction(), HandlerDef(this.getClass.getClassLoader, "", "controllers.ActionController", "submitAddAction", Seq(), "POST", """""", _prefix + """submitAddActions""")
)
                      

// @LINE:34
def addActions(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.ActionController.addActions(), HandlerDef(this.getClass.getClassLoader, "", "controllers.ActionController", "addActions", Seq(), "GET", """""", _prefix + """addActions""")
)
                      

}
                          

// @LINE:118
// @LINE:116
// @LINE:91
// @LINE:72
// @LINE:71
// @LINE:70
// @LINE:62
// @LINE:61
// @LINE:35
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:8
// @LINE:7
class ReverseApplication {


// @LINE:35
def editAttributes(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.editAttributes(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "editAttributes", Seq(), "GET", """""", _prefix + """editAttributes""")
)
                      

// @LINE:91
def deleteFile(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.deleteFile(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "deleteFile", Seq(), "POST", """""", _prefix + """deleteFile""")
)
                      

// @LINE:16
def submitUser(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.submitUser(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "submitUser", Seq(), "POST", """""", _prefix + """submitUser""")
)
                      

// @LINE:15
def addUser(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.addUser(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "addUser", Seq(), "GET", """""", _prefix + """addUser""")
)
                      

// @LINE:9
def adminHome(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.adminHome(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "adminHome", Seq(), "GET", """""", _prefix + """adminHome""")
)
                      

// @LINE:61
def newFile(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.newFile(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "newFile", Seq(), "GET", """""", _prefix + """newFile""")
)
                      

// @LINE:71
def submitPropertyFile(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.submitPropertyFile(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "submitPropertyFile", Seq(), "POST", """""", _prefix + """submitPropertyFile""")
)
                      

// @LINE:70
def propertyFiles(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.propertyFiles(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "propertyFiles", Seq(), "GET", """""", _prefix + """propertyFiles""")
)
                      

// @LINE:21
def result(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.result(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "result", Seq(), "GET", """""", _prefix + """result""")
)
                      

// @LINE:19
def policies(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.policies(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "policies", Seq(), "GET", """""", _prefix + """policies""")
)
                      

// @LINE:62
def submitMemberFile(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.submitMemberFile(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "submitMemberFile", Seq(), "POST", """""", _prefix + """submitMemberFile""")
)
                      

// @LINE:12
def about(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.about(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "about", Seq(), "GET", """""", _prefix + """aboutUs""")
)
                      

// @LINE:14
def print(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.print(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "print", Seq(), "GET", """""", _prefix + """print""")
)
                      

// @LINE:11
def logout(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.logout(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "logout", Seq(), "GET", """""", _prefix + """logout""")
)
                      

// @LINE:8
def home(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.home(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "home", Seq(), "GET", """""", _prefix + """home""")
)
                      

// @LINE:20
def runPolicy(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.runPolicy(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "runPolicy", Seq(), "GET", """""", _prefix + """runPolicy""")
)
                      

// @LINE:118
def getMainTypes(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.getMainTypes(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "getMainTypes", Seq(), "POST", """""", _prefix + """getMainTypes""")
)
                      

// @LINE:7
def authenticate(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.authenticate(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "authenticate", Seq(), "POST", """ Home page""", _prefix + """authenticate""")
)
                      

// @LINE:72
def exportAttributes(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.exportAttributes(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "exportAttributes", Seq(), "GET", """""", _prefix + """exportAttributes""")
)
                      

// @LINE:17
def index(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.index(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "index", Seq(), "GET", """""", _prefix + """""")
)
                      

// @LINE:13
def contact(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.contact(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "contact", Seq(), "GET", """""", _prefix + """contact""")
)
                      

// @LINE:116
def javascriptRoutes(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.javascriptRoutes(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "javascriptRoutes", Seq(), "GET", """Ajax Stuff""", _prefix + """javascriptRoutes""")
)
                      

// @LINE:10
def login(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.login(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "login", Seq(), "GET", """""", _prefix + """login""")
)
                      

}
                          

// @LINE:44
// @LINE:43
// @LINE:42
class ReverseResourceController {


// @LINE:44
def editResources(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.ResourceController.editResources(), HandlerDef(this.getClass.getClassLoader, "", "controllers.ResourceController", "editResources", Seq(), "GET", """""", _prefix + """editResources""")
)
                      

// @LINE:43
def addResources(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.ResourceController.addResources(), HandlerDef(this.getClass.getClassLoader, "", "controllers.ResourceController", "addResources", Seq(), "GET", """""", _prefix + """addResources""")
)
                      

// @LINE:42
def submitAddResource(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.ResourceController.submitAddResource(), HandlerDef(this.getClass.getClassLoader, "", "controllers.ResourceController", "submitAddResource", Seq(), "POST", """""", _prefix + """submitAddResource""")
)
                      

}
                          

// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:24
class ReversePolicyController {


// @LINE:31
def editPolicy(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PolicyController.editPolicy(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PolicyController", "editPolicy", Seq(), "GET", """""", _prefix + """editPolicy""")
)
                      

// @LINE:32
def editPolicySubmit(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PolicyController.editPolicySubmit(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PolicyController", "editPolicySubmit", Seq(), "POST", """""", _prefix + """editPolicySubmit""")
)
                      

// @LINE:30
def submitActionPolicy(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PolicyController.submitActionPolicy(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PolicyController", "submitActionPolicy", Seq(), "POST", """""", _prefix + """submitActionPolicy""")
)
                      

// @LINE:24
def newPolicy(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.PolicyController.newPolicy(), HandlerDef(this.getClass.getClassLoader, "", "controllers.PolicyController", "newPolicy", Seq(), "GET", """""", _prefix + """newPolicy""")
)
                      

}
                          

// @LINE:109
// @LINE:108
// @LINE:107
// @LINE:106
// @LINE:105
// @LINE:104
// @LINE:103
class ReverseJoinFileController {


// @LINE:105
def editJoinPropertyGroup(playframework_escape_type:String, groupId:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.JoinFileController.editJoinPropertyGroup(playframework_escape_type, groupId), HandlerDef(this.getClass.getClassLoader, "", "controllers.JoinFileController", "editJoinPropertyGroup", Seq(classOf[String], classOf[Long]), "GET", """""", _prefix + """editJoinPropertyGroup""")
)
                      

// @LINE:106
def JoinPropertyEdit(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.JoinFileController.JoinPropertyEdit(), HandlerDef(this.getClass.getClassLoader, "", "controllers.JoinFileController", "JoinPropertyEdit", Seq(), "POST", """""", _prefix + """JoinPropertyEdit""")
)
                      

// @LINE:104
def joinFileGroups(playframework_escape_type:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.JoinFileController.joinFileGroups(playframework_escape_type), HandlerDef(this.getClass.getClassLoader, "", "controllers.JoinFileController", "joinFileGroups", Seq(classOf[String]), "GET", """""", _prefix + """joinFileGroups""")
)
                      

// @LINE:103
def joinGroupFile(playframework_escape_type:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.JoinFileController.joinGroupFile(playframework_escape_type), HandlerDef(this.getClass.getClassLoader, "", "controllers.JoinFileController", "joinGroupFile", Seq(classOf[String]), "GET", """""", _prefix + """joinGroupFile""")
)
                      

// @LINE:109
def addNewGroup(playframework_escape_type:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.JoinFileController.addNewGroup(playframework_escape_type), HandlerDef(this.getClass.getClassLoader, "", "controllers.JoinFileController", "addNewGroup", Seq(classOf[String]), "GET", """""", _prefix + """addNewGroup""")
)
                      

// @LINE:108
def deleteJoinPropertyGroup(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.JoinFileController.deleteJoinPropertyGroup(), HandlerDef(this.getClass.getClassLoader, "", "controllers.JoinFileController", "deleteJoinPropertyGroup", Seq(), "POST", """""", _prefix + """deleteJoinPropertyGroup""")
)
                      

// @LINE:107
def submitJoinGroupFile(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.JoinFileController.submitJoinGroupFile(), HandlerDef(this.getClass.getClassLoader, "", "controllers.JoinFileController", "submitJoinGroupFile", Seq(), "POST", """""", _prefix + """submitJoinGroupFile""")
)
                      

}
                          

// @LINE:56
// @LINE:55
// @LINE:54
class ReverseGraphController {


// @LINE:55
def upload(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.GraphController.upload(), HandlerDef(this.getClass.getClassLoader, "", "controllers.GraphController", "upload", Seq(), "POST", """""", _prefix + """upload""")
)
                      

// @LINE:56
def parseGraphFile(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.GraphController.parseGraphFile(), HandlerDef(this.getClass.getClassLoader, "", "controllers.GraphController", "parseGraphFile", Seq(), "GET", """""", _prefix + """parseGraphFile""")
)
                      

// @LINE:54
def graphIntialize(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.GraphController.graphIntialize(), HandlerDef(this.getClass.getClassLoader, "", "controllers.GraphController", "graphIntialize", Seq(), "GET", """""", _prefix + """graphIntialize""")
)
                      

}
                          
}
        
    