// @SOURCE:C:/Users/HP/Documents/gui/conf/routes
// @HASH:8b03cef06ae561deda3d1d95f00d7b842a451743
// @DATE:Mon Mar 21 16:33:45 GMT 2016


import play.core._
import play.core.Router._
import play.core.Router.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._
import _root_.controllers.Assets.Asset
import _root_.play.libs.F

import Router.queryString

object Routes extends Router.Routes {

import ReverseRouteContext.empty

private var _prefix = "/"

def setPrefix(prefix: String) {
  _prefix = prefix
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" }


// @LINE:7
private[this] lazy val controllers_Application_authenticate0_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("authenticate"))))
private[this] lazy val controllers_Application_authenticate0_invoker = createInvoker(
controllers.Application.authenticate(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "authenticate", Nil,"POST", """ Home page""", Routes.prefix + """authenticate"""))
        

// @LINE:8
private[this] lazy val controllers_Application_home1_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("home"))))
private[this] lazy val controllers_Application_home1_invoker = createInvoker(
controllers.Application.home(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "home", Nil,"GET", """""", Routes.prefix + """home"""))
        

// @LINE:9
private[this] lazy val controllers_Application_adminHome2_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("adminHome"))))
private[this] lazy val controllers_Application_adminHome2_invoker = createInvoker(
controllers.Application.adminHome(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "adminHome", Nil,"GET", """""", Routes.prefix + """adminHome"""))
        

// @LINE:10
private[this] lazy val controllers_Application_login3_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("login"))))
private[this] lazy val controllers_Application_login3_invoker = createInvoker(
controllers.Application.login(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "login", Nil,"GET", """""", Routes.prefix + """login"""))
        

// @LINE:11
private[this] lazy val controllers_Application_logout4_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("logout"))))
private[this] lazy val controllers_Application_logout4_invoker = createInvoker(
controllers.Application.logout(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "logout", Nil,"GET", """""", Routes.prefix + """logout"""))
        

// @LINE:12
private[this] lazy val controllers_Application_about5_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("aboutUs"))))
private[this] lazy val controllers_Application_about5_invoker = createInvoker(
controllers.Application.about(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "about", Nil,"GET", """""", Routes.prefix + """aboutUs"""))
        

// @LINE:13
private[this] lazy val controllers_Application_contact6_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("contact"))))
private[this] lazy val controllers_Application_contact6_invoker = createInvoker(
controllers.Application.contact(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "contact", Nil,"GET", """""", Routes.prefix + """contact"""))
        

// @LINE:14
private[this] lazy val controllers_Application_print7_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("print"))))
private[this] lazy val controllers_Application_print7_invoker = createInvoker(
controllers.Application.print(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "print", Nil,"GET", """""", Routes.prefix + """print"""))
        

// @LINE:15
private[this] lazy val controllers_Application_addUser8_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addUser"))))
private[this] lazy val controllers_Application_addUser8_invoker = createInvoker(
controllers.Application.addUser(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "addUser", Nil,"GET", """""", Routes.prefix + """addUser"""))
        

// @LINE:16
private[this] lazy val controllers_Application_submitUser9_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("submitUser"))))
private[this] lazy val controllers_Application_submitUser9_invoker = createInvoker(
controllers.Application.submitUser(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "submitUser", Nil,"POST", """""", Routes.prefix + """submitUser"""))
        

// @LINE:17
private[this] lazy val controllers_Application_index10_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix))))
private[this] lazy val controllers_Application_index10_invoker = createInvoker(
controllers.Application.index(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "index", Nil,"GET", """""", Routes.prefix + """"""))
        

// @LINE:19
private[this] lazy val controllers_Application_policies11_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("policies"))))
private[this] lazy val controllers_Application_policies11_invoker = createInvoker(
controllers.Application.policies(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "policies", Nil,"GET", """""", Routes.prefix + """policies"""))
        

// @LINE:20
private[this] lazy val controllers_Application_runPolicy12_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("runPolicy"))))
private[this] lazy val controllers_Application_runPolicy12_invoker = createInvoker(
controllers.Application.runPolicy(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "runPolicy", Nil,"GET", """""", Routes.prefix + """runPolicy"""))
        

// @LINE:21
private[this] lazy val controllers_Application_result13_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("result"))))
private[this] lazy val controllers_Application_result13_invoker = createInvoker(
controllers.Application.result(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "result", Nil,"GET", """""", Routes.prefix + """result"""))
        

// @LINE:22
private[this] lazy val controllers_RuleController_combineRule14_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("combineRule"))))
private[this] lazy val controllers_RuleController_combineRule14_invoker = createInvoker(
controllers.RuleController.combineRule(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.RuleController", "combineRule", Nil,"GET", """""", Routes.prefix + """combineRule"""))
        

// @LINE:24
private[this] lazy val controllers_PolicyController_newPolicy15_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("newPolicy"))))
private[this] lazy val controllers_PolicyController_newPolicy15_invoker = createInvoker(
controllers.PolicyController.newPolicy(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PolicyController", "newPolicy", Nil,"GET", """""", Routes.prefix + """newPolicy"""))
        

// @LINE:25
private[this] lazy val controllers_RuleController_addNewRule16_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addNewRule"))))
private[this] lazy val controllers_RuleController_addNewRule16_invoker = createInvoker(
controllers.RuleController.addNewRule(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.RuleController", "addNewRule", Nil,"POST", """""", Routes.prefix + """addNewRule"""))
        

// @LINE:26
private[this] lazy val controllers_RuleController_addRule17_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addRule"))))
private[this] lazy val controllers_RuleController_addRule17_invoker = createInvoker(
controllers.RuleController.addRule(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.RuleController", "addRule", Nil,"POST", """""", Routes.prefix + """addRule"""))
        

// @LINE:27
private[this] lazy val controllers_RuleController_addRules18_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addRules"))))
private[this] lazy val controllers_RuleController_addRules18_invoker = createInvoker(
controllers.RuleController.addRules(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.RuleController", "addRules", Nil,"GET", """""", Routes.prefix + """addRules"""))
        

// @LINE:28
private[this] lazy val controllers_RuleController_submitCombineRule19_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("submitCombineRule"))))
private[this] lazy val controllers_RuleController_submitCombineRule19_invoker = createInvoker(
controllers.RuleController.submitCombineRule(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.RuleController", "submitCombineRule", Nil,"POST", """""", Routes.prefix + """submitCombineRule"""))
        

// @LINE:30
private[this] lazy val controllers_PolicyController_submitActionPolicy20_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("submitActionPolicy"))))
private[this] lazy val controllers_PolicyController_submitActionPolicy20_invoker = createInvoker(
controllers.PolicyController.submitActionPolicy(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PolicyController", "submitActionPolicy", Nil,"POST", """""", Routes.prefix + """submitActionPolicy"""))
        

// @LINE:31
private[this] lazy val controllers_PolicyController_editPolicy21_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("editPolicy"))))
private[this] lazy val controllers_PolicyController_editPolicy21_invoker = createInvoker(
controllers.PolicyController.editPolicy(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PolicyController", "editPolicy", Nil,"GET", """""", Routes.prefix + """editPolicy"""))
        

// @LINE:32
private[this] lazy val controllers_PolicyController_editPolicySubmit22_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("editPolicySubmit"))))
private[this] lazy val controllers_PolicyController_editPolicySubmit22_invoker = createInvoker(
controllers.PolicyController.editPolicySubmit(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PolicyController", "editPolicySubmit", Nil,"POST", """""", Routes.prefix + """editPolicySubmit"""))
        

// @LINE:34
private[this] lazy val controllers_ActionController_addActions23_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addActions"))))
private[this] lazy val controllers_ActionController_addActions23_invoker = createInvoker(
controllers.ActionController.addActions(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.ActionController", "addActions", Nil,"GET", """""", Routes.prefix + """addActions"""))
        

// @LINE:35
private[this] lazy val controllers_Application_editAttributes24_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("editAttributes"))))
private[this] lazy val controllers_Application_editAttributes24_invoker = createInvoker(
controllers.Application.editAttributes(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "editAttributes", Nil,"GET", """""", Routes.prefix + """editAttributes"""))
        

// @LINE:36
private[this] lazy val controllers_ActionController_submitAddAction25_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("submitAddActions"))))
private[this] lazy val controllers_ActionController_submitAddAction25_invoker = createInvoker(
controllers.ActionController.submitAddAction(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.ActionController", "submitAddAction", Nil,"POST", """""", Routes.prefix + """submitAddActions"""))
        

// @LINE:38
private[this] lazy val controllers_SubjectController_submitAddSubject26_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("submitAddSubject"))))
private[this] lazy val controllers_SubjectController_submitAddSubject26_invoker = createInvoker(
controllers.SubjectController.submitAddSubject(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.SubjectController", "submitAddSubject", Nil,"POST", """""", Routes.prefix + """submitAddSubject"""))
        

// @LINE:39
private[this] lazy val controllers_SubjectController_addSubjects27_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addSubjects"))))
private[this] lazy val controllers_SubjectController_addSubjects27_invoker = createInvoker(
controllers.SubjectController.addSubjects(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.SubjectController", "addSubjects", Nil,"GET", """""", Routes.prefix + """addSubjects"""))
        

// @LINE:40
private[this] lazy val controllers_SubjectController_editSubjects28_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("editSubjects"))))
private[this] lazy val controllers_SubjectController_editSubjects28_invoker = createInvoker(
controllers.SubjectController.editSubjects(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.SubjectController", "editSubjects", Nil,"GET", """""", Routes.prefix + """editSubjects"""))
        

// @LINE:42
private[this] lazy val controllers_ResourceController_submitAddResource29_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("submitAddResource"))))
private[this] lazy val controllers_ResourceController_submitAddResource29_invoker = createInvoker(
controllers.ResourceController.submitAddResource(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.ResourceController", "submitAddResource", Nil,"POST", """""", Routes.prefix + """submitAddResource"""))
        

// @LINE:43
private[this] lazy val controllers_ResourceController_addResources30_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addResources"))))
private[this] lazy val controllers_ResourceController_addResources30_invoker = createInvoker(
controllers.ResourceController.addResources(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.ResourceController", "addResources", Nil,"GET", """""", Routes.prefix + """addResources"""))
        

// @LINE:44
private[this] lazy val controllers_ResourceController_editResources31_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("editResources"))))
private[this] lazy val controllers_ResourceController_editResources31_invoker = createInvoker(
controllers.ResourceController.editResources(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.ResourceController", "editResources", Nil,"GET", """""", Routes.prefix + """editResources"""))
        

// @LINE:46
private[this] lazy val controllers_CombiningAlgorithmController_submitAddCombiningAlgorithm32_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("sumbitAddCombiningAlgorithm"))))
private[this] lazy val controllers_CombiningAlgorithmController_submitAddCombiningAlgorithm32_invoker = createInvoker(
controllers.CombiningAlgorithmController.submitAddCombiningAlgorithm(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.CombiningAlgorithmController", "submitAddCombiningAlgorithm", Nil,"POST", """""", Routes.prefix + """sumbitAddCombiningAlgorithm"""))
        

// @LINE:47
private[this] lazy val controllers_CombiningAlgorithmController_addCombiningAlgorithm33_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addCombiningAlogrithm"))))
private[this] lazy val controllers_CombiningAlgorithmController_addCombiningAlgorithm33_invoker = createInvoker(
controllers.CombiningAlgorithmController.addCombiningAlgorithm(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.CombiningAlgorithmController", "addCombiningAlgorithm", Nil,"GET", """""", Routes.prefix + """addCombiningAlogrithm"""))
        

// @LINE:48
private[this] lazy val controllers_CombiningAlgorithmController_editCombiningAlgorithm34_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("editCombiningAlgorithm"))))
private[this] lazy val controllers_CombiningAlgorithmController_editCombiningAlgorithm34_invoker = createInvoker(
controllers.CombiningAlgorithmController.editCombiningAlgorithm(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.CombiningAlgorithmController", "editCombiningAlgorithm", Nil,"GET", """""", Routes.prefix + """editCombiningAlgorithm"""))
        

// @LINE:50
private[this] lazy val controllers_RuleController_addPermissions35_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addPermissions"))))
private[this] lazy val controllers_RuleController_addPermissions35_invoker = createInvoker(
controllers.RuleController.addPermissions(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.RuleController", "addPermissions", Nil,"GET", """""", Routes.prefix + """addPermissions"""))
        

// @LINE:51
private[this] lazy val controllers_RuleController_editPermissions36_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("editPermissions"))))
private[this] lazy val controllers_RuleController_editPermissions36_invoker = createInvoker(
controllers.RuleController.editPermissions(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.RuleController", "editPermissions", Nil,"GET", """""", Routes.prefix + """editPermissions"""))
        

// @LINE:52
private[this] lazy val controllers_RuleController_submitAddPermissions37_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("submitAddPermissions"))))
private[this] lazy val controllers_RuleController_submitAddPermissions37_invoker = createInvoker(
controllers.RuleController.submitAddPermissions(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.RuleController", "submitAddPermissions", Nil,"POST", """""", Routes.prefix + """submitAddPermissions"""))
        

// @LINE:54
private[this] lazy val controllers_GraphController_graphIntialize38_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("graphIntialize"))))
private[this] lazy val controllers_GraphController_graphIntialize38_invoker = createInvoker(
controllers.GraphController.graphIntialize(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.GraphController", "graphIntialize", Nil,"GET", """""", Routes.prefix + """graphIntialize"""))
        

// @LINE:55
private[this] lazy val controllers_GraphController_upload39_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("upload"))))
private[this] lazy val controllers_GraphController_upload39_invoker = createInvoker(
controllers.GraphController.upload(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.GraphController", "upload", Nil,"POST", """""", Routes.prefix + """upload"""))
        

// @LINE:56
private[this] lazy val controllers_GraphController_parseGraphFile40_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("parseGraphFile"))))
private[this] lazy val controllers_GraphController_parseGraphFile40_invoker = createInvoker(
controllers.GraphController.parseGraphFile(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.GraphController", "parseGraphFile", Nil,"GET", """""", Routes.prefix + """parseGraphFile"""))
        

// @LINE:58
private[this] lazy val controllers_PropertyFileController_memberProperty41_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("memberProperty"))))
private[this] lazy val controllers_PropertyFileController_memberProperty41_invoker = createInvoker(
controllers.PropertyFileController.memberProperty(fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "memberProperty", Seq(classOf[String]),"GET", """""", Routes.prefix + """memberProperty"""))
        

// @LINE:59
private[this] lazy val controllers_PropertyFileController_assetProperty42_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assetProperty"))))
private[this] lazy val controllers_PropertyFileController_assetProperty42_invoker = createInvoker(
controllers.PropertyFileController.assetProperty(fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "assetProperty", Seq(classOf[String]),"GET", """""", Routes.prefix + """assetProperty"""))
        

// @LINE:60
private[this] lazy val controllers_PropertyFileController_actionProperty43_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("actionProperty"))))
private[this] lazy val controllers_PropertyFileController_actionProperty43_invoker = createInvoker(
controllers.PropertyFileController.actionProperty(fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "actionProperty", Seq(classOf[String]),"GET", """""", Routes.prefix + """actionProperty"""))
        

// @LINE:61
private[this] lazy val controllers_Application_newFile44_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("newFile"))))
private[this] lazy val controllers_Application_newFile44_invoker = createInvoker(
controllers.Application.newFile(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "newFile", Nil,"GET", """""", Routes.prefix + """newFile"""))
        

// @LINE:62
private[this] lazy val controllers_Application_submitMemberFile45_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("submitMemberFile"))))
private[this] lazy val controllers_Application_submitMemberFile45_invoker = createInvoker(
controllers.Application.submitMemberFile(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "submitMemberFile", Nil,"POST", """""", Routes.prefix + """submitMemberFile"""))
        

// @LINE:63
private[this] lazy val controllers_PropertyFileController_addMemberPropertyGroup46_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addMemberPropertyGroup"))))
private[this] lazy val controllers_PropertyFileController_addMemberPropertyGroup46_invoker = createInvoker(
controllers.PropertyFileController.addMemberPropertyGroup(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "addMemberPropertyGroup", Nil,"POST", """""", Routes.prefix + """addMemberPropertyGroup"""))
        

// @LINE:64
private[this] lazy val controllers_GroupController_addActionPropertyGroup47_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addActionPropertyGroup"))))
private[this] lazy val controllers_GroupController_addActionPropertyGroup47_invoker = createInvoker(
controllers.GroupController.addActionPropertyGroup(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.GroupController", "addActionPropertyGroup", Nil,"POST", """""", Routes.prefix + """addActionPropertyGroup"""))
        

// @LINE:67
private[this] lazy val controllers_PropertyFileController_addPropertyValue48_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addPropertyValue"))))
private[this] lazy val controllers_PropertyFileController_addPropertyValue48_invoker = createInvoker(
controllers.PropertyFileController.addPropertyValue(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "addPropertyValue", Nil,"GET", """""", Routes.prefix + """addPropertyValue"""))
        

// @LINE:68
private[this] lazy val controllers_PropertyFileController_submitPropertyValue49_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("submitPropertyValue"))))
private[this] lazy val controllers_PropertyFileController_submitPropertyValue49_invoker = createInvoker(
controllers.PropertyFileController.submitPropertyValue(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "submitPropertyValue", Nil,"POST", """""", Routes.prefix + """submitPropertyValue"""))
        

// @LINE:70
private[this] lazy val controllers_Application_propertyFiles50_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("propertyFiles"))))
private[this] lazy val controllers_Application_propertyFiles50_invoker = createInvoker(
controllers.Application.propertyFiles(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "propertyFiles", Nil,"GET", """""", Routes.prefix + """propertyFiles"""))
        

// @LINE:71
private[this] lazy val controllers_Application_submitPropertyFile51_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("submitPropertyFile"))))
private[this] lazy val controllers_Application_submitPropertyFile51_invoker = createInvoker(
controllers.Application.submitPropertyFile(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "submitPropertyFile", Nil,"POST", """""", Routes.prefix + """submitPropertyFile"""))
        

// @LINE:72
private[this] lazy val controllers_Application_exportAttributes52_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("exportAttributes"))))
private[this] lazy val controllers_Application_exportAttributes52_invoker = createInvoker(
controllers.Application.exportAttributes(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "exportAttributes", Nil,"GET", """""", Routes.prefix + """exportAttributes"""))
        

// @LINE:73
private[this] lazy val controllers_GroupController_addGroupPropertyGroup53_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addGroupPropertyGroup"))))
private[this] lazy val controllers_GroupController_addGroupPropertyGroup53_invoker = createInvoker(
controllers.GroupController.addGroupPropertyGroup(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.GroupController", "addGroupPropertyGroup", Nil,"POST", """""", Routes.prefix + """addGroupPropertyGroup"""))
        

// @LINE:74
private[this] lazy val controllers_GroupController_submitGroupFile54_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("submitGroupFile"))))
private[this] lazy val controllers_GroupController_submitGroupFile54_invoker = createInvoker(
controllers.GroupController.submitGroupFile(fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.GroupController", "submitGroupFile", Seq(classOf[String]),"GET", """""", Routes.prefix + """submitGroupFile"""))
        

// @LINE:76
private[this] lazy val controllers_PropertyFileController_updatePropertyFiles55_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("updatePropertyFiles"))))
private[this] lazy val controllers_PropertyFileController_updatePropertyFiles55_invoker = createInvoker(
controllers.PropertyFileController.updatePropertyFiles(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "updatePropertyFiles", Nil,"POST", """""", Routes.prefix + """updatePropertyFiles"""))
        

// @LINE:77
private[this] lazy val controllers_PropertyFileController_fetchAllValues56_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("fetchAllValues"))))
private[this] lazy val controllers_PropertyFileController_fetchAllValues56_invoker = createInvoker(
controllers.PropertyFileController.fetchAllValues(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "fetchAllValues", Nil,"POST", """""", Routes.prefix + """fetchAllValues"""))
        

// @LINE:78
private[this] lazy val controllers_PropertyFileController_fetchActionValues57_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("fetchActionValues"))))
private[this] lazy val controllers_PropertyFileController_fetchActionValues57_invoker = createInvoker(
controllers.PropertyFileController.fetchActionValues(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "fetchActionValues", Nil,"POST", """""", Routes.prefix + """fetchActionValues"""))
        

// @LINE:80
private[this] lazy val controllers_PropertyFileController_updateActionValue58_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("updateActionValue"))))
private[this] lazy val controllers_PropertyFileController_updateActionValue58_invoker = createInvoker(
controllers.PropertyFileController.updateActionValue(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "updateActionValue", Nil,"POST", """""", Routes.prefix + """updateActionValue"""))
        

// @LINE:81
private[this] lazy val controllers_PropertyFileController_deleteActionValue59_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("deleteActionValue"))))
private[this] lazy val controllers_PropertyFileController_deleteActionValue59_invoker = createInvoker(
controllers.PropertyFileController.deleteActionValue(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "deleteActionValue", Nil,"POST", """""", Routes.prefix + """deleteActionValue"""))
        

// @LINE:82
private[this] lazy val controllers_PropertyFileController_updateAttributeValue60_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("updateAttributeValue"))))
private[this] lazy val controllers_PropertyFileController_updateAttributeValue60_invoker = createInvoker(
controllers.PropertyFileController.updateAttributeValue(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "updateAttributeValue", Nil,"POST", """""", Routes.prefix + """updateAttributeValue"""))
        

// @LINE:83
private[this] lazy val controllers_PropertyFileController_deleteAttributeValue61_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("deleteAttributeValue"))))
private[this] lazy val controllers_PropertyFileController_deleteAttributeValue61_invoker = createInvoker(
controllers.PropertyFileController.deleteAttributeValue(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "deleteAttributeValue", Nil,"POST", """""", Routes.prefix + """deleteAttributeValue"""))
        

// @LINE:84
private[this] lazy val controllers_PropertyFileController_updatePermission62_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("updatePermission"))))
private[this] lazy val controllers_PropertyFileController_updatePermission62_invoker = createInvoker(
controllers.PropertyFileController.updatePermission(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "updatePermission", Nil,"POST", """""", Routes.prefix + """updatePermission"""))
        

// @LINE:85
private[this] lazy val controllers_PropertyFileController_deletePermission63_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("deletePermission"))))
private[this] lazy val controllers_PropertyFileController_deletePermission63_invoker = createInvoker(
controllers.PropertyFileController.deletePermission(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "deletePermission", Nil,"POST", """""", Routes.prefix + """deletePermission"""))
        

// @LINE:86
private[this] lazy val controllers_PropertyFileController_updateAlgorithm64_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("updateAlgorithm"))))
private[this] lazy val controllers_PropertyFileController_updateAlgorithm64_invoker = createInvoker(
controllers.PropertyFileController.updateAlgorithm(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "updateAlgorithm", Nil,"POST", """""", Routes.prefix + """updateAlgorithm"""))
        

// @LINE:87
private[this] lazy val controllers_PropertyFileController_deleteAlgorithm65_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("deleteAlgorithm"))))
private[this] lazy val controllers_PropertyFileController_deleteAlgorithm65_invoker = createInvoker(
controllers.PropertyFileController.deleteAlgorithm(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "deleteAlgorithm", Nil,"POST", """""", Routes.prefix + """deleteAlgorithm"""))
        

// @LINE:89
private[this] lazy val controllers_PropertyFileController_deleteGroup66_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("deleteGroup"))))
private[this] lazy val controllers_PropertyFileController_deleteGroup66_invoker = createInvoker(
controllers.PropertyFileController.deleteGroup(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "deleteGroup", Nil,"POST", """""", Routes.prefix + """deleteGroup"""))
        

// @LINE:90
private[this] lazy val controllers_GroupController_deleteGPropertyGroup67_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("deleteGPropertyGroup"))))
private[this] lazy val controllers_GroupController_deleteGPropertyGroup67_invoker = createInvoker(
controllers.GroupController.deleteGPropertyGroup(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.GroupController", "deleteGPropertyGroup", Nil,"POST", """""", Routes.prefix + """deleteGPropertyGroup"""))
        

// @LINE:91
private[this] lazy val controllers_Application_deleteFile68_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("deleteFile"))))
private[this] lazy val controllers_Application_deleteFile68_invoker = createInvoker(
controllers.Application.deleteFile(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "deleteFile", Nil,"POST", """""", Routes.prefix + """deleteFile"""))
        

// @LINE:93
private[this] lazy val controllers_PropertyFileController_PropertyEdit69_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("PropertyEdit"))))
private[this] lazy val controllers_PropertyFileController_PropertyEdit69_invoker = createInvoker(
controllers.PropertyFileController.PropertyEdit(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "PropertyEdit", Nil,"GET", """""", Routes.prefix + """PropertyEdit"""))
        

// @LINE:95
private[this] lazy val controllers_PropertyFileController_groups70_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("groups"))))
private[this] lazy val controllers_PropertyFileController_groups70_invoker = createInvoker(
controllers.PropertyFileController.groups(fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "groups", Seq(classOf[String]),"GET", """""", Routes.prefix + """groups"""))
        

// @LINE:96
private[this] lazy val controllers_GroupController_groupProperty71_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("groupProperty"))))
private[this] lazy val controllers_GroupController_groupProperty71_invoker = createInvoker(
controllers.GroupController.groupProperty(fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.GroupController", "groupProperty", Seq(classOf[String]),"GET", """""", Routes.prefix + """groupProperty"""))
        

// @LINE:97
private[this] lazy val controllers_GroupController_groupFileGroups72_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("groupFileGroups"))))
private[this] lazy val controllers_GroupController_groupFileGroups72_invoker = createInvoker(
controllers.GroupController.groupFileGroups(fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.GroupController", "groupFileGroups", Seq(classOf[String]),"GET", """""", Routes.prefix + """groupFileGroups"""))
        

// @LINE:98
private[this] lazy val controllers_GroupController_editGroupPropertyGroup73_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("editGroupPropertyGroup"))))
private[this] lazy val controllers_GroupController_editGroupPropertyGroup73_invoker = createInvoker(
controllers.GroupController.editGroupPropertyGroup(fakeValue[String], fakeValue[Long]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.GroupController", "editGroupPropertyGroup", Seq(classOf[String], classOf[Long]),"GET", """""", Routes.prefix + """editGroupPropertyGroup"""))
        

// @LINE:99
private[this] lazy val controllers_GroupController_GPropertyEdit74_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("GPropertyEdit"))))
private[this] lazy val controllers_GroupController_GPropertyEdit74_invoker = createInvoker(
controllers.GroupController.GPropertyEdit(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.GroupController", "GPropertyEdit", Nil,"POST", """""", Routes.prefix + """GPropertyEdit"""))
        

// @LINE:100
private[this] lazy val controllers_PropertyFileController_submitAssetPropertyEdit75_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("submitAssetPropertyEdit"))))
private[this] lazy val controllers_PropertyFileController_submitAssetPropertyEdit75_invoker = createInvoker(
controllers.PropertyFileController.submitAssetPropertyEdit(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.PropertyFileController", "submitAssetPropertyEdit", Nil,"POST", """""", Routes.prefix + """submitAssetPropertyEdit"""))
        

// @LINE:103
private[this] lazy val controllers_JoinFileController_joinGroupFile76_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("joinGroupFile"))))
private[this] lazy val controllers_JoinFileController_joinGroupFile76_invoker = createInvoker(
controllers.JoinFileController.joinGroupFile(fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.JoinFileController", "joinGroupFile", Seq(classOf[String]),"GET", """""", Routes.prefix + """joinGroupFile"""))
        

// @LINE:104
private[this] lazy val controllers_JoinFileController_joinFileGroups77_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("joinFileGroups"))))
private[this] lazy val controllers_JoinFileController_joinFileGroups77_invoker = createInvoker(
controllers.JoinFileController.joinFileGroups(fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.JoinFileController", "joinFileGroups", Seq(classOf[String]),"GET", """""", Routes.prefix + """joinFileGroups"""))
        

// @LINE:105
private[this] lazy val controllers_JoinFileController_editJoinPropertyGroup78_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("editJoinPropertyGroup"))))
private[this] lazy val controllers_JoinFileController_editJoinPropertyGroup78_invoker = createInvoker(
controllers.JoinFileController.editJoinPropertyGroup(fakeValue[String], fakeValue[Long]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.JoinFileController", "editJoinPropertyGroup", Seq(classOf[String], classOf[Long]),"GET", """""", Routes.prefix + """editJoinPropertyGroup"""))
        

// @LINE:106
private[this] lazy val controllers_JoinFileController_JoinPropertyEdit79_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("JoinPropertyEdit"))))
private[this] lazy val controllers_JoinFileController_JoinPropertyEdit79_invoker = createInvoker(
controllers.JoinFileController.JoinPropertyEdit(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.JoinFileController", "JoinPropertyEdit", Nil,"POST", """""", Routes.prefix + """JoinPropertyEdit"""))
        

// @LINE:107
private[this] lazy val controllers_JoinFileController_submitJoinGroupFile80_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("submitJoinGroupFile"))))
private[this] lazy val controllers_JoinFileController_submitJoinGroupFile80_invoker = createInvoker(
controllers.JoinFileController.submitJoinGroupFile(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.JoinFileController", "submitJoinGroupFile", Nil,"POST", """""", Routes.prefix + """submitJoinGroupFile"""))
        

// @LINE:108
private[this] lazy val controllers_JoinFileController_deleteJoinPropertyGroup81_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("deleteJoinPropertyGroup"))))
private[this] lazy val controllers_JoinFileController_deleteJoinPropertyGroup81_invoker = createInvoker(
controllers.JoinFileController.deleteJoinPropertyGroup(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.JoinFileController", "deleteJoinPropertyGroup", Nil,"POST", """""", Routes.prefix + """deleteJoinPropertyGroup"""))
        

// @LINE:109
private[this] lazy val controllers_JoinFileController_addNewGroup82_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addNewGroup"))))
private[this] lazy val controllers_JoinFileController_addNewGroup82_invoker = createInvoker(
controllers.JoinFileController.addNewGroup(fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.JoinFileController", "addNewGroup", Seq(classOf[String]),"GET", """""", Routes.prefix + """addNewGroup"""))
        

// @LINE:113
private[this] lazy val controllers_Assets_at83_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/"),DynamicPart("file", """.+""",false))))
private[this] lazy val controllers_Assets_at83_invoker = createInvoker(
controllers.Assets.at(fakeValue[String], fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """ Map static resources from the /public folder to the /assets URL path""", Routes.prefix + """assets/$file<.+>"""))
        

// @LINE:116
private[this] lazy val controllers_Application_javascriptRoutes84_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("javascriptRoutes"))))
private[this] lazy val controllers_Application_javascriptRoutes84_invoker = createInvoker(
controllers.Application.javascriptRoutes,
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "javascriptRoutes", Nil,"GET", """Ajax Stuff""", Routes.prefix + """javascriptRoutes"""))
        

// @LINE:118
private[this] lazy val controllers_Application_getMainTypes85_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("getMainTypes"))))
private[this] lazy val controllers_Application_getMainTypes85_invoker = createInvoker(
controllers.Application.getMainTypes(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "getMainTypes", Nil,"POST", """""", Routes.prefix + """getMainTypes"""))
        
def documentation = List(("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """authenticate""","""controllers.Application.authenticate()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """home""","""controllers.Application.home()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """adminHome""","""controllers.Application.adminHome()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """login""","""controllers.Application.login()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """logout""","""controllers.Application.logout()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """aboutUs""","""controllers.Application.about()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """contact""","""controllers.Application.contact()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """print""","""controllers.Application.print()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addUser""","""controllers.Application.addUser()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """submitUser""","""controllers.Application.submitUser()"""),("""GET""", prefix,"""controllers.Application.index()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """policies""","""controllers.Application.policies()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """runPolicy""","""controllers.Application.runPolicy()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """result""","""controllers.Application.result()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """combineRule""","""controllers.RuleController.combineRule()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """newPolicy""","""controllers.PolicyController.newPolicy()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addNewRule""","""controllers.RuleController.addNewRule()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addRule""","""controllers.RuleController.addRule()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addRules""","""controllers.RuleController.addRules()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """submitCombineRule""","""controllers.RuleController.submitCombineRule()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """submitActionPolicy""","""controllers.PolicyController.submitActionPolicy()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """editPolicy""","""controllers.PolicyController.editPolicy()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """editPolicySubmit""","""controllers.PolicyController.editPolicySubmit()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addActions""","""controllers.ActionController.addActions()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """editAttributes""","""controllers.Application.editAttributes()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """submitAddActions""","""controllers.ActionController.submitAddAction()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """submitAddSubject""","""controllers.SubjectController.submitAddSubject()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addSubjects""","""controllers.SubjectController.addSubjects()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """editSubjects""","""controllers.SubjectController.editSubjects()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """submitAddResource""","""controllers.ResourceController.submitAddResource()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addResources""","""controllers.ResourceController.addResources()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """editResources""","""controllers.ResourceController.editResources()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """sumbitAddCombiningAlgorithm""","""controllers.CombiningAlgorithmController.submitAddCombiningAlgorithm()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addCombiningAlogrithm""","""controllers.CombiningAlgorithmController.addCombiningAlgorithm()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """editCombiningAlgorithm""","""controllers.CombiningAlgorithmController.editCombiningAlgorithm()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addPermissions""","""controllers.RuleController.addPermissions()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """editPermissions""","""controllers.RuleController.editPermissions()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """submitAddPermissions""","""controllers.RuleController.submitAddPermissions()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """graphIntialize""","""controllers.GraphController.graphIntialize()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """upload""","""controllers.GraphController.upload()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """parseGraphFile""","""controllers.GraphController.parseGraphFile()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """memberProperty""","""controllers.PropertyFileController.memberProperty(type:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assetProperty""","""controllers.PropertyFileController.assetProperty(type:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """actionProperty""","""controllers.PropertyFileController.actionProperty(type:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """newFile""","""controllers.Application.newFile()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """submitMemberFile""","""controllers.Application.submitMemberFile()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addMemberPropertyGroup""","""controllers.PropertyFileController.addMemberPropertyGroup()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addActionPropertyGroup""","""controllers.GroupController.addActionPropertyGroup()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addPropertyValue""","""controllers.PropertyFileController.addPropertyValue()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """submitPropertyValue""","""controllers.PropertyFileController.submitPropertyValue()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """propertyFiles""","""controllers.Application.propertyFiles()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """submitPropertyFile""","""controllers.Application.submitPropertyFile()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """exportAttributes""","""controllers.Application.exportAttributes()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addGroupPropertyGroup""","""controllers.GroupController.addGroupPropertyGroup()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """submitGroupFile""","""controllers.GroupController.submitGroupFile(type:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """updatePropertyFiles""","""controllers.PropertyFileController.updatePropertyFiles()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """fetchAllValues""","""controllers.PropertyFileController.fetchAllValues()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """fetchActionValues""","""controllers.PropertyFileController.fetchActionValues()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """updateActionValue""","""controllers.PropertyFileController.updateActionValue()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """deleteActionValue""","""controllers.PropertyFileController.deleteActionValue()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """updateAttributeValue""","""controllers.PropertyFileController.updateAttributeValue()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """deleteAttributeValue""","""controllers.PropertyFileController.deleteAttributeValue()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """updatePermission""","""controllers.PropertyFileController.updatePermission()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """deletePermission""","""controllers.PropertyFileController.deletePermission()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """updateAlgorithm""","""controllers.PropertyFileController.updateAlgorithm()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """deleteAlgorithm""","""controllers.PropertyFileController.deleteAlgorithm()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """deleteGroup""","""controllers.PropertyFileController.deleteGroup()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """deleteGPropertyGroup""","""controllers.GroupController.deleteGPropertyGroup()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """deleteFile""","""controllers.Application.deleteFile()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """PropertyEdit""","""controllers.PropertyFileController.PropertyEdit()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """groups""","""controllers.PropertyFileController.groups(type:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """groupProperty""","""controllers.GroupController.groupProperty(type:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """groupFileGroups""","""controllers.GroupController.groupFileGroups(type:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """editGroupPropertyGroup""","""controllers.GroupController.editGroupPropertyGroup(type:String, groupId:Long)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """GPropertyEdit""","""controllers.GroupController.GPropertyEdit()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """submitAssetPropertyEdit""","""controllers.PropertyFileController.submitAssetPropertyEdit()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """joinGroupFile""","""controllers.JoinFileController.joinGroupFile(type:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """joinFileGroups""","""controllers.JoinFileController.joinFileGroups(type:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """editJoinPropertyGroup""","""controllers.JoinFileController.editJoinPropertyGroup(type:String, groupId:Long)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """JoinPropertyEdit""","""controllers.JoinFileController.JoinPropertyEdit()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """submitJoinGroupFile""","""controllers.JoinFileController.submitJoinGroupFile()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """deleteJoinPropertyGroup""","""controllers.JoinFileController.deleteJoinPropertyGroup()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addNewGroup""","""controllers.JoinFileController.addNewGroup(type:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """javascriptRoutes""","""controllers.Application.javascriptRoutes"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """getMainTypes""","""controllers.Application.getMainTypes()""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]]
}}
      

def routes:PartialFunction[RequestHeader,Handler] = {

// @LINE:7
case controllers_Application_authenticate0_route(params) => {
   call { 
        controllers_Application_authenticate0_invoker.call(controllers.Application.authenticate())
   }
}
        

// @LINE:8
case controllers_Application_home1_route(params) => {
   call { 
        controllers_Application_home1_invoker.call(controllers.Application.home())
   }
}
        

// @LINE:9
case controllers_Application_adminHome2_route(params) => {
   call { 
        controllers_Application_adminHome2_invoker.call(controllers.Application.adminHome())
   }
}
        

// @LINE:10
case controllers_Application_login3_route(params) => {
   call { 
        controllers_Application_login3_invoker.call(controllers.Application.login())
   }
}
        

// @LINE:11
case controllers_Application_logout4_route(params) => {
   call { 
        controllers_Application_logout4_invoker.call(controllers.Application.logout())
   }
}
        

// @LINE:12
case controllers_Application_about5_route(params) => {
   call { 
        controllers_Application_about5_invoker.call(controllers.Application.about())
   }
}
        

// @LINE:13
case controllers_Application_contact6_route(params) => {
   call { 
        controllers_Application_contact6_invoker.call(controllers.Application.contact())
   }
}
        

// @LINE:14
case controllers_Application_print7_route(params) => {
   call { 
        controllers_Application_print7_invoker.call(controllers.Application.print())
   }
}
        

// @LINE:15
case controllers_Application_addUser8_route(params) => {
   call { 
        controllers_Application_addUser8_invoker.call(controllers.Application.addUser())
   }
}
        

// @LINE:16
case controllers_Application_submitUser9_route(params) => {
   call { 
        controllers_Application_submitUser9_invoker.call(controllers.Application.submitUser())
   }
}
        

// @LINE:17
case controllers_Application_index10_route(params) => {
   call { 
        controllers_Application_index10_invoker.call(controllers.Application.index())
   }
}
        

// @LINE:19
case controllers_Application_policies11_route(params) => {
   call { 
        controllers_Application_policies11_invoker.call(controllers.Application.policies())
   }
}
        

// @LINE:20
case controllers_Application_runPolicy12_route(params) => {
   call { 
        controllers_Application_runPolicy12_invoker.call(controllers.Application.runPolicy())
   }
}
        

// @LINE:21
case controllers_Application_result13_route(params) => {
   call { 
        controllers_Application_result13_invoker.call(controllers.Application.result())
   }
}
        

// @LINE:22
case controllers_RuleController_combineRule14_route(params) => {
   call { 
        controllers_RuleController_combineRule14_invoker.call(controllers.RuleController.combineRule())
   }
}
        

// @LINE:24
case controllers_PolicyController_newPolicy15_route(params) => {
   call { 
        controllers_PolicyController_newPolicy15_invoker.call(controllers.PolicyController.newPolicy())
   }
}
        

// @LINE:25
case controllers_RuleController_addNewRule16_route(params) => {
   call { 
        controllers_RuleController_addNewRule16_invoker.call(controllers.RuleController.addNewRule())
   }
}
        

// @LINE:26
case controllers_RuleController_addRule17_route(params) => {
   call { 
        controllers_RuleController_addRule17_invoker.call(controllers.RuleController.addRule())
   }
}
        

// @LINE:27
case controllers_RuleController_addRules18_route(params) => {
   call { 
        controllers_RuleController_addRules18_invoker.call(controllers.RuleController.addRules())
   }
}
        

// @LINE:28
case controllers_RuleController_submitCombineRule19_route(params) => {
   call { 
        controllers_RuleController_submitCombineRule19_invoker.call(controllers.RuleController.submitCombineRule())
   }
}
        

// @LINE:30
case controllers_PolicyController_submitActionPolicy20_route(params) => {
   call { 
        controllers_PolicyController_submitActionPolicy20_invoker.call(controllers.PolicyController.submitActionPolicy())
   }
}
        

// @LINE:31
case controllers_PolicyController_editPolicy21_route(params) => {
   call { 
        controllers_PolicyController_editPolicy21_invoker.call(controllers.PolicyController.editPolicy())
   }
}
        

// @LINE:32
case controllers_PolicyController_editPolicySubmit22_route(params) => {
   call { 
        controllers_PolicyController_editPolicySubmit22_invoker.call(controllers.PolicyController.editPolicySubmit())
   }
}
        

// @LINE:34
case controllers_ActionController_addActions23_route(params) => {
   call { 
        controllers_ActionController_addActions23_invoker.call(controllers.ActionController.addActions())
   }
}
        

// @LINE:35
case controllers_Application_editAttributes24_route(params) => {
   call { 
        controllers_Application_editAttributes24_invoker.call(controllers.Application.editAttributes())
   }
}
        

// @LINE:36
case controllers_ActionController_submitAddAction25_route(params) => {
   call { 
        controllers_ActionController_submitAddAction25_invoker.call(controllers.ActionController.submitAddAction())
   }
}
        

// @LINE:38
case controllers_SubjectController_submitAddSubject26_route(params) => {
   call { 
        controllers_SubjectController_submitAddSubject26_invoker.call(controllers.SubjectController.submitAddSubject())
   }
}
        

// @LINE:39
case controllers_SubjectController_addSubjects27_route(params) => {
   call { 
        controllers_SubjectController_addSubjects27_invoker.call(controllers.SubjectController.addSubjects())
   }
}
        

// @LINE:40
case controllers_SubjectController_editSubjects28_route(params) => {
   call { 
        controllers_SubjectController_editSubjects28_invoker.call(controllers.SubjectController.editSubjects())
   }
}
        

// @LINE:42
case controllers_ResourceController_submitAddResource29_route(params) => {
   call { 
        controllers_ResourceController_submitAddResource29_invoker.call(controllers.ResourceController.submitAddResource())
   }
}
        

// @LINE:43
case controllers_ResourceController_addResources30_route(params) => {
   call { 
        controllers_ResourceController_addResources30_invoker.call(controllers.ResourceController.addResources())
   }
}
        

// @LINE:44
case controllers_ResourceController_editResources31_route(params) => {
   call { 
        controllers_ResourceController_editResources31_invoker.call(controllers.ResourceController.editResources())
   }
}
        

// @LINE:46
case controllers_CombiningAlgorithmController_submitAddCombiningAlgorithm32_route(params) => {
   call { 
        controllers_CombiningAlgorithmController_submitAddCombiningAlgorithm32_invoker.call(controllers.CombiningAlgorithmController.submitAddCombiningAlgorithm())
   }
}
        

// @LINE:47
case controllers_CombiningAlgorithmController_addCombiningAlgorithm33_route(params) => {
   call { 
        controllers_CombiningAlgorithmController_addCombiningAlgorithm33_invoker.call(controllers.CombiningAlgorithmController.addCombiningAlgorithm())
   }
}
        

// @LINE:48
case controllers_CombiningAlgorithmController_editCombiningAlgorithm34_route(params) => {
   call { 
        controllers_CombiningAlgorithmController_editCombiningAlgorithm34_invoker.call(controllers.CombiningAlgorithmController.editCombiningAlgorithm())
   }
}
        

// @LINE:50
case controllers_RuleController_addPermissions35_route(params) => {
   call { 
        controllers_RuleController_addPermissions35_invoker.call(controllers.RuleController.addPermissions())
   }
}
        

// @LINE:51
case controllers_RuleController_editPermissions36_route(params) => {
   call { 
        controllers_RuleController_editPermissions36_invoker.call(controllers.RuleController.editPermissions())
   }
}
        

// @LINE:52
case controllers_RuleController_submitAddPermissions37_route(params) => {
   call { 
        controllers_RuleController_submitAddPermissions37_invoker.call(controllers.RuleController.submitAddPermissions())
   }
}
        

// @LINE:54
case controllers_GraphController_graphIntialize38_route(params) => {
   call { 
        controllers_GraphController_graphIntialize38_invoker.call(controllers.GraphController.graphIntialize())
   }
}
        

// @LINE:55
case controllers_GraphController_upload39_route(params) => {
   call { 
        controllers_GraphController_upload39_invoker.call(controllers.GraphController.upload())
   }
}
        

// @LINE:56
case controllers_GraphController_parseGraphFile40_route(params) => {
   call { 
        controllers_GraphController_parseGraphFile40_invoker.call(controllers.GraphController.parseGraphFile())
   }
}
        

// @LINE:58
case controllers_PropertyFileController_memberProperty41_route(params) => {
   call(params.fromQuery[String]("type", None)) { (playframework_escape_type) =>
        controllers_PropertyFileController_memberProperty41_invoker.call(controllers.PropertyFileController.memberProperty(playframework_escape_type))
   }
}
        

// @LINE:59
case controllers_PropertyFileController_assetProperty42_route(params) => {
   call(params.fromQuery[String]("type", None)) { (playframework_escape_type) =>
        controllers_PropertyFileController_assetProperty42_invoker.call(controllers.PropertyFileController.assetProperty(playframework_escape_type))
   }
}
        

// @LINE:60
case controllers_PropertyFileController_actionProperty43_route(params) => {
   call(params.fromQuery[String]("type", None)) { (playframework_escape_type) =>
        controllers_PropertyFileController_actionProperty43_invoker.call(controllers.PropertyFileController.actionProperty(playframework_escape_type))
   }
}
        

// @LINE:61
case controllers_Application_newFile44_route(params) => {
   call { 
        controllers_Application_newFile44_invoker.call(controllers.Application.newFile())
   }
}
        

// @LINE:62
case controllers_Application_submitMemberFile45_route(params) => {
   call { 
        controllers_Application_submitMemberFile45_invoker.call(controllers.Application.submitMemberFile())
   }
}
        

// @LINE:63
case controllers_PropertyFileController_addMemberPropertyGroup46_route(params) => {
   call { 
        controllers_PropertyFileController_addMemberPropertyGroup46_invoker.call(controllers.PropertyFileController.addMemberPropertyGroup())
   }
}
        

// @LINE:64
case controllers_GroupController_addActionPropertyGroup47_route(params) => {
   call { 
        controllers_GroupController_addActionPropertyGroup47_invoker.call(controllers.GroupController.addActionPropertyGroup())
   }
}
        

// @LINE:67
case controllers_PropertyFileController_addPropertyValue48_route(params) => {
   call { 
        controllers_PropertyFileController_addPropertyValue48_invoker.call(controllers.PropertyFileController.addPropertyValue())
   }
}
        

// @LINE:68
case controllers_PropertyFileController_submitPropertyValue49_route(params) => {
   call { 
        controllers_PropertyFileController_submitPropertyValue49_invoker.call(controllers.PropertyFileController.submitPropertyValue())
   }
}
        

// @LINE:70
case controllers_Application_propertyFiles50_route(params) => {
   call { 
        controllers_Application_propertyFiles50_invoker.call(controllers.Application.propertyFiles())
   }
}
        

// @LINE:71
case controllers_Application_submitPropertyFile51_route(params) => {
   call { 
        controllers_Application_submitPropertyFile51_invoker.call(controllers.Application.submitPropertyFile())
   }
}
        

// @LINE:72
case controllers_Application_exportAttributes52_route(params) => {
   call { 
        controllers_Application_exportAttributes52_invoker.call(controllers.Application.exportAttributes())
   }
}
        

// @LINE:73
case controllers_GroupController_addGroupPropertyGroup53_route(params) => {
   call { 
        controllers_GroupController_addGroupPropertyGroup53_invoker.call(controllers.GroupController.addGroupPropertyGroup())
   }
}
        

// @LINE:74
case controllers_GroupController_submitGroupFile54_route(params) => {
   call(params.fromQuery[String]("type", None)) { (playframework_escape_type) =>
        controllers_GroupController_submitGroupFile54_invoker.call(controllers.GroupController.submitGroupFile(playframework_escape_type))
   }
}
        

// @LINE:76
case controllers_PropertyFileController_updatePropertyFiles55_route(params) => {
   call { 
        controllers_PropertyFileController_updatePropertyFiles55_invoker.call(controllers.PropertyFileController.updatePropertyFiles())
   }
}
        

// @LINE:77
case controllers_PropertyFileController_fetchAllValues56_route(params) => {
   call { 
        controllers_PropertyFileController_fetchAllValues56_invoker.call(controllers.PropertyFileController.fetchAllValues())
   }
}
        

// @LINE:78
case controllers_PropertyFileController_fetchActionValues57_route(params) => {
   call { 
        controllers_PropertyFileController_fetchActionValues57_invoker.call(controllers.PropertyFileController.fetchActionValues())
   }
}
        

// @LINE:80
case controllers_PropertyFileController_updateActionValue58_route(params) => {
   call { 
        controllers_PropertyFileController_updateActionValue58_invoker.call(controllers.PropertyFileController.updateActionValue())
   }
}
        

// @LINE:81
case controllers_PropertyFileController_deleteActionValue59_route(params) => {
   call { 
        controllers_PropertyFileController_deleteActionValue59_invoker.call(controllers.PropertyFileController.deleteActionValue())
   }
}
        

// @LINE:82
case controllers_PropertyFileController_updateAttributeValue60_route(params) => {
   call { 
        controllers_PropertyFileController_updateAttributeValue60_invoker.call(controllers.PropertyFileController.updateAttributeValue())
   }
}
        

// @LINE:83
case controllers_PropertyFileController_deleteAttributeValue61_route(params) => {
   call { 
        controllers_PropertyFileController_deleteAttributeValue61_invoker.call(controllers.PropertyFileController.deleteAttributeValue())
   }
}
        

// @LINE:84
case controllers_PropertyFileController_updatePermission62_route(params) => {
   call { 
        controllers_PropertyFileController_updatePermission62_invoker.call(controllers.PropertyFileController.updatePermission())
   }
}
        

// @LINE:85
case controllers_PropertyFileController_deletePermission63_route(params) => {
   call { 
        controllers_PropertyFileController_deletePermission63_invoker.call(controllers.PropertyFileController.deletePermission())
   }
}
        

// @LINE:86
case controllers_PropertyFileController_updateAlgorithm64_route(params) => {
   call { 
        controllers_PropertyFileController_updateAlgorithm64_invoker.call(controllers.PropertyFileController.updateAlgorithm())
   }
}
        

// @LINE:87
case controllers_PropertyFileController_deleteAlgorithm65_route(params) => {
   call { 
        controllers_PropertyFileController_deleteAlgorithm65_invoker.call(controllers.PropertyFileController.deleteAlgorithm())
   }
}
        

// @LINE:89
case controllers_PropertyFileController_deleteGroup66_route(params) => {
   call { 
        controllers_PropertyFileController_deleteGroup66_invoker.call(controllers.PropertyFileController.deleteGroup())
   }
}
        

// @LINE:90
case controllers_GroupController_deleteGPropertyGroup67_route(params) => {
   call { 
        controllers_GroupController_deleteGPropertyGroup67_invoker.call(controllers.GroupController.deleteGPropertyGroup())
   }
}
        

// @LINE:91
case controllers_Application_deleteFile68_route(params) => {
   call { 
        controllers_Application_deleteFile68_invoker.call(controllers.Application.deleteFile())
   }
}
        

// @LINE:93
case controllers_PropertyFileController_PropertyEdit69_route(params) => {
   call { 
        controllers_PropertyFileController_PropertyEdit69_invoker.call(controllers.PropertyFileController.PropertyEdit())
   }
}
        

// @LINE:95
case controllers_PropertyFileController_groups70_route(params) => {
   call(params.fromQuery[String]("type", None)) { (playframework_escape_type) =>
        controllers_PropertyFileController_groups70_invoker.call(controllers.PropertyFileController.groups(playframework_escape_type))
   }
}
        

// @LINE:96
case controllers_GroupController_groupProperty71_route(params) => {
   call(params.fromQuery[String]("type", None)) { (playframework_escape_type) =>
        controllers_GroupController_groupProperty71_invoker.call(controllers.GroupController.groupProperty(playframework_escape_type))
   }
}
        

// @LINE:97
case controllers_GroupController_groupFileGroups72_route(params) => {
   call(params.fromQuery[String]("type", None)) { (playframework_escape_type) =>
        controllers_GroupController_groupFileGroups72_invoker.call(controllers.GroupController.groupFileGroups(playframework_escape_type))
   }
}
        

// @LINE:98
case controllers_GroupController_editGroupPropertyGroup73_route(params) => {
   call(params.fromQuery[String]("type", None), params.fromQuery[Long]("groupId", None)) { (playframework_escape_type, groupId) =>
        controllers_GroupController_editGroupPropertyGroup73_invoker.call(controllers.GroupController.editGroupPropertyGroup(playframework_escape_type, groupId))
   }
}
        

// @LINE:99
case controllers_GroupController_GPropertyEdit74_route(params) => {
   call { 
        controllers_GroupController_GPropertyEdit74_invoker.call(controllers.GroupController.GPropertyEdit())
   }
}
        

// @LINE:100
case controllers_PropertyFileController_submitAssetPropertyEdit75_route(params) => {
   call { 
        controllers_PropertyFileController_submitAssetPropertyEdit75_invoker.call(controllers.PropertyFileController.submitAssetPropertyEdit())
   }
}
        

// @LINE:103
case controllers_JoinFileController_joinGroupFile76_route(params) => {
   call(params.fromQuery[String]("type", None)) { (playframework_escape_type) =>
        controllers_JoinFileController_joinGroupFile76_invoker.call(controllers.JoinFileController.joinGroupFile(playframework_escape_type))
   }
}
        

// @LINE:104
case controllers_JoinFileController_joinFileGroups77_route(params) => {
   call(params.fromQuery[String]("type", None)) { (playframework_escape_type) =>
        controllers_JoinFileController_joinFileGroups77_invoker.call(controllers.JoinFileController.joinFileGroups(playframework_escape_type))
   }
}
        

// @LINE:105
case controllers_JoinFileController_editJoinPropertyGroup78_route(params) => {
   call(params.fromQuery[String]("type", None), params.fromQuery[Long]("groupId", None)) { (playframework_escape_type, groupId) =>
        controllers_JoinFileController_editJoinPropertyGroup78_invoker.call(controllers.JoinFileController.editJoinPropertyGroup(playframework_escape_type, groupId))
   }
}
        

// @LINE:106
case controllers_JoinFileController_JoinPropertyEdit79_route(params) => {
   call { 
        controllers_JoinFileController_JoinPropertyEdit79_invoker.call(controllers.JoinFileController.JoinPropertyEdit())
   }
}
        

// @LINE:107
case controllers_JoinFileController_submitJoinGroupFile80_route(params) => {
   call { 
        controllers_JoinFileController_submitJoinGroupFile80_invoker.call(controllers.JoinFileController.submitJoinGroupFile())
   }
}
        

// @LINE:108
case controllers_JoinFileController_deleteJoinPropertyGroup81_route(params) => {
   call { 
        controllers_JoinFileController_deleteJoinPropertyGroup81_invoker.call(controllers.JoinFileController.deleteJoinPropertyGroup())
   }
}
        

// @LINE:109
case controllers_JoinFileController_addNewGroup82_route(params) => {
   call(params.fromQuery[String]("type", None)) { (playframework_escape_type) =>
        controllers_JoinFileController_addNewGroup82_invoker.call(controllers.JoinFileController.addNewGroup(playframework_escape_type))
   }
}
        

// @LINE:113
case controllers_Assets_at83_route(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        controllers_Assets_at83_invoker.call(controllers.Assets.at(path, file))
   }
}
        

// @LINE:116
case controllers_Application_javascriptRoutes84_route(params) => {
   call { 
        controllers_Application_javascriptRoutes84_invoker.call(controllers.Application.javascriptRoutes)
   }
}
        

// @LINE:118
case controllers_Application_getMainTypes85_route(params) => {
   call { 
        controllers_Application_getMainTypes85_invoker.call(controllers.Application.getMainTypes())
   }
}
        
}

}
     