// @SOURCE:C:/Users/HP/Documents/gui/conf/routes
// @HASH:8b03cef06ae561deda3d1d95f00d7b842a451743
// @DATE:Mon Mar 21 16:33:45 GMT 2016

package controllers;

public class routes {
public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets();
public static final controllers.ReverseSubjectController SubjectController = new controllers.ReverseSubjectController();
public static final controllers.ReverseRuleController RuleController = new controllers.ReverseRuleController();
public static final controllers.ReversePropertyFileController PropertyFileController = new controllers.ReversePropertyFileController();
public static final controllers.ReverseCombiningAlgorithmController CombiningAlgorithmController = new controllers.ReverseCombiningAlgorithmController();
public static final controllers.ReverseGroupController GroupController = new controllers.ReverseGroupController();
public static final controllers.ReverseActionController ActionController = new controllers.ReverseActionController();
public static final controllers.ReverseApplication Application = new controllers.ReverseApplication();
public static final controllers.ReverseResourceController ResourceController = new controllers.ReverseResourceController();
public static final controllers.ReversePolicyController PolicyController = new controllers.ReversePolicyController();
public static final controllers.ReverseJoinFileController JoinFileController = new controllers.ReverseJoinFileController();
public static final controllers.ReverseGraphController GraphController = new controllers.ReverseGraphController();

public static class javascript {
public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets();
public static final controllers.javascript.ReverseSubjectController SubjectController = new controllers.javascript.ReverseSubjectController();
public static final controllers.javascript.ReverseRuleController RuleController = new controllers.javascript.ReverseRuleController();
public static final controllers.javascript.ReversePropertyFileController PropertyFileController = new controllers.javascript.ReversePropertyFileController();
public static final controllers.javascript.ReverseCombiningAlgorithmController CombiningAlgorithmController = new controllers.javascript.ReverseCombiningAlgorithmController();
public static final controllers.javascript.ReverseGroupController GroupController = new controllers.javascript.ReverseGroupController();
public static final controllers.javascript.ReverseActionController ActionController = new controllers.javascript.ReverseActionController();
public static final controllers.javascript.ReverseApplication Application = new controllers.javascript.ReverseApplication();
public static final controllers.javascript.ReverseResourceController ResourceController = new controllers.javascript.ReverseResourceController();
public static final controllers.javascript.ReversePolicyController PolicyController = new controllers.javascript.ReversePolicyController();
public static final controllers.javascript.ReverseJoinFileController JoinFileController = new controllers.javascript.ReverseJoinFileController();
public static final controllers.javascript.ReverseGraphController GraphController = new controllers.javascript.ReverseGraphController();
}
          

public static class ref {
public static final controllers.ref.ReverseAssets Assets = new controllers.ref.ReverseAssets();
public static final controllers.ref.ReverseSubjectController SubjectController = new controllers.ref.ReverseSubjectController();
public static final controllers.ref.ReverseRuleController RuleController = new controllers.ref.ReverseRuleController();
public static final controllers.ref.ReversePropertyFileController PropertyFileController = new controllers.ref.ReversePropertyFileController();
public static final controllers.ref.ReverseCombiningAlgorithmController CombiningAlgorithmController = new controllers.ref.ReverseCombiningAlgorithmController();
public static final controllers.ref.ReverseGroupController GroupController = new controllers.ref.ReverseGroupController();
public static final controllers.ref.ReverseActionController ActionController = new controllers.ref.ReverseActionController();
public static final controllers.ref.ReverseApplication Application = new controllers.ref.ReverseApplication();
public static final controllers.ref.ReverseResourceController ResourceController = new controllers.ref.ReverseResourceController();
public static final controllers.ref.ReversePolicyController PolicyController = new controllers.ref.ReversePolicyController();
public static final controllers.ref.ReverseJoinFileController JoinFileController = new controllers.ref.ReverseJoinFileController();
public static final controllers.ref.ReverseGraphController GraphController = new controllers.ref.ReverseGraphController();
}
          
}
          