
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object joinPropertyGroup extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[String,String,List[GraphEntityValue],List[GraphEntityValue],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(message: String, 
fileType:String,
firsts: List[GraphEntityValue], 
seconds: List[GraphEntityValue],
labels: List[String]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*5.22*/("""

"""),_display_(/*8.2*/main(message)/*8.15*/{_display_(Seq[Any](format.raw/*8.16*/("""
"""),format.raw/*9.1*/("""<div id="page-wrapper">
	"""),_display_(/*10.3*/helper/*10.9*/.form(action=routes.JoinFileController.submitJoinGroupFile(),'id->"myForm_property",'name->"myForm_property")/*10.118*/{_display_(Seq[Any](format.raw/*10.119*/("""
	"""),format.raw/*11.2*/("""<div class="row">
		<input type="hidden" value=""""),_display_(/*12.32*/fileType),format.raw/*12.40*/("""" name="fileType" id="fileType"/>
		<div class="col-lg-12">
			<div class="panel panel-default"
				style="border: 0; margin-bottom: 10px; box-shadow: none;">
				<div class="panel-heading"
					style="font-size: 20px; text-align: center; margin-bottom: 10px;">
					<i><b>Create """),_display_(/*18.20*/fileType),format.raw/*18.28*/("""  """),format.raw/*18.30*/("""Group</b> </i>
				</div>

				<div class="row">

					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step1 &#8594; <b>Select Value(s)</b>
							</div>
							<div class="panel-body">
								<div class="row ">
									<div class="col-lg-6" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*31.27*/labels/*31.33*/.get(0)),format.raw/*31.40*/(""" """),format.raw/*31.41*/("""</label>  <select required class="form-control" id=""""),_display_(/*31.94*/labels/*31.100*/.get(0)),format.raw/*31.107*/(""""
											name=""""),_display_(/*32.19*/labels/*32.25*/.get(0)),format.raw/*32.32*/("""" >
											<option value="" disabled selected>select """),_display_(/*33.55*/labels/*33.61*/.get(0)),format.raw/*33.68*/("""</option>
										
											"""),_display_(/*35.13*/for(first <- firsts) yield /*35.33*/ {_display_(Seq[Any](format.raw/*35.35*/("""
											"""),format.raw/*36.12*/("""<option value=""""),_display_(/*36.28*/first/*36.33*/.getValue()),format.raw/*36.44*/("""">
												"""),_display_(/*37.14*/first/*37.19*/.getValue()),format.raw/*37.30*/("""</option> """)))}),format.raw/*37.41*/("""
										"""),format.raw/*38.11*/("""</select>
									</div>
									<div class="col-lg-6" style="margin-bottom: 10px;">
										<label> Select
										 """),_display_(/*42.13*/{labels.get(1)}),format.raw/*42.28*/("""(s)
										</label> 
										<select multiple="multiple" required class="form-control" id=""""),_display_(/*44.74*/labels/*44.80*/.get(1)),format.raw/*44.87*/("""[]"
											name=""""),_display_(/*45.19*/labels/*45.25*/.get(1)),format.raw/*45.32*/("""[]">
											<option value="" disabled selected>select """),_display_(/*46.55*/labels/*46.61*/.get(1)),format.raw/*46.68*/("""</option>
											"""),_display_(/*47.13*/for(second <- seconds) yield /*47.35*/ {_display_(Seq[Any](format.raw/*47.37*/("""
											"""),format.raw/*48.12*/("""<option value=""""),_display_(/*48.28*/second/*48.34*/.getValue()),format.raw/*48.45*/("""">
												"""),_display_(/*49.14*/second/*49.20*/.getValue()),format.raw/*49.31*/("""</option> """)))}),format.raw/*49.42*/("""
										"""),format.raw/*50.11*/("""</select> 
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			Step 2 &#8594; <b>Select Operation</b>
		</div>
		<div class="panel-body">
			<div class="row" style="margin-bottom: 10px;">
				<div class="col-lg-4">
					<button type="submit" value="more" name="submitValue1"
						 class="btn btn-success col-lg-12 btn-block">
						<b>Save and Add Another Group</b>
					</button>
				</div>
				<div class="col-lg-4">
					<button type="submit" value="continue" name="submitValue1"
						 class="btn btn-primary col-lg-12 btn-block">
						<b>Save and Go Back</b>
					</button>
				</div>
				<div class="col-lg-4">
					<a class="btn btn-danger col-lg-12 btn-block" onClick="location.href = '"""),_display_(/*81.79*/routes/*81.85*/.Application.newFile()),format.raw/*81.107*/("""'">
						<b>Go Back</b>
					</a>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*88.3*/("""
"""),format.raw/*89.1*/("""</div>

""")))}),format.raw/*91.2*/("""
"""))}
  }

  def render(message:String,fileType:String,firsts:List[GraphEntityValue],seconds:List[GraphEntityValue],labels:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(message,fileType,firsts,seconds,labels)

  def f:((String,String,List[GraphEntityValue],List[GraphEntityValue],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (message,fileType,firsts,seconds,labels) => apply(message,fileType,firsts,seconds,labels)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/joinPropertyGroup.scala.html
                  HASH: 45849aace052dd6f2bca428fdb8afaafd01deacc
                  MATRIX: 801->1|1031->128|1061->152|1082->165|1120->166|1148->168|1201->195|1215->201|1334->310|1374->311|1404->314|1481->364|1510->372|1824->659|1853->667|1883->669|2282->1041|2297->1047|2325->1054|2354->1055|2434->1108|2450->1114|2479->1121|2527->1142|2542->1148|2570->1155|2656->1214|2671->1220|2699->1227|2761->1262|2797->1282|2837->1284|2878->1297|2921->1313|2935->1318|2967->1329|3011->1346|3025->1351|3057->1362|3099->1373|3139->1385|3294->1513|3330->1528|3456->1627|3471->1633|3499->1640|3549->1663|3564->1669|3592->1676|3679->1736|3694->1742|3722->1749|3772->1772|3810->1794|3850->1796|3891->1809|3934->1825|3949->1831|3981->1842|4025->1859|4040->1865|4072->1876|4114->1887|4154->1899|5035->2753|5050->2759|5094->2781|5206->2863|5235->2865|5276->2876
                  LINES: 26->1|33->5|35->8|35->8|35->8|36->9|37->10|37->10|37->10|37->10|38->11|39->12|39->12|45->18|45->18|45->18|58->31|58->31|58->31|58->31|58->31|58->31|58->31|59->32|59->32|59->32|60->33|60->33|60->33|62->35|62->35|62->35|63->36|63->36|63->36|63->36|64->37|64->37|64->37|64->37|65->38|69->42|69->42|71->44|71->44|71->44|72->45|72->45|72->45|73->46|73->46|73->46|74->47|74->47|74->47|75->48|75->48|75->48|75->48|76->49|76->49|76->49|76->49|77->50|108->81|108->81|108->81|115->88|116->89|118->91
                  -- GENERATED --
              */
          