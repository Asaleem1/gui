
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object print extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[String,List[Policy],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String, policies: List[Policy]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*1.41*/(""" 
"""),_display_(/*3.2*/main("print")/*3.15*/{_display_(Seq[Any](format.raw/*3.16*/("""
"""),format.raw/*4.1*/("""<div id="page-wrapper">
	"""),_display_(/*5.3*/helper/*5.9*/.form(action=routes.PolicyController.submitActionPolicy(),'id->"printPolicy",'name->"printPolicy")/*5.107*/{_display_(Seq[Any](format.raw/*5.108*/("""

	"""),format.raw/*7.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default" style="border: 0; box-shadow: none;">
				
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step1 &#8594; <b>Select Policy</b>
							</div>
							<div class="panel-body">
								<div id="organization_div">

									<div class="row">
										<div class="col-lg-12" style="margin-bottom: 10px;">
											<label> Select Policy </label> <select class="form-control"
												 name="policyId" id="policyId" style="width:100%;"> """),_display_(/*23.66*/for(policy <-
												policies) yield /*24.22*/ {_display_(Seq[Any](format.raw/*24.24*/("""
												"""),format.raw/*25.13*/("""<option style="width:100%;" value=""""),_display_(/*25.49*/policy/*25.55*/.getPolicyId()),format.raw/*25.69*/("""">
													"""),_display_(/*26.15*/policy/*26.21*/.getPolicyName()),format.raw/*26.37*/("""</option> """)))}),format.raw/*26.48*/("""
											"""),format.raw/*27.12*/("""</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row" style="margin-bottom: 10px;">
					<div class="col-lg-12">
						<button type="submit" value="continue" name="submitValue1"
							id="submitValue1" class="btn btn-success col-lg-12 btn-block">
							Step 2 &#8594; <b>Print Group</b>
						</button>
					</div>
				</div>
			</div>
		</div>
		""")))}),format.raw/*45.4*/("""
	"""),format.raw/*46.2*/("""</div>
	""")))}))}
  }

  def render(title:String,policies:List[Policy]): play.twirl.api.HtmlFormat.Appendable = apply(title,policies)

  def f:((String,List[Policy]) => play.twirl.api.HtmlFormat.Appendable) = (title,policies) => apply(title,policies)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:49 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/print.scala.html
                  HASH: b7ca69caf7d1307e3d69c8fd7ff44deb714628e9
                  MATRIX: 736->1|878->40|907->62|928->75|966->76|994->78|1046->105|1059->111|1166->209|1205->210|1236->215|1880->832|1932->868|1972->870|2014->884|2077->920|2092->926|2127->940|2172->958|2187->964|2224->980|2266->991|2307->1004|2780->1447|2810->1450
                  LINES: 26->1|29->1|30->3|30->3|30->3|31->4|32->5|32->5|32->5|32->5|34->7|50->23|51->24|51->24|52->25|52->25|52->25|52->25|53->26|53->26|53->26|53->26|54->27|72->45|73->46
                  -- GENERATED --
              */
          