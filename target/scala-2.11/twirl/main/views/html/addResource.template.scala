
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object addResource extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[String],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(resourceTypes:List[String],resourceSubTypes:List[String]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*1.60*/("""
"""),_display_(/*3.2*/main("Add New Resource")/*3.26*/{_display_(Seq[Any](format.raw/*3.27*/("""

"""),format.raw/*5.1*/("""<script>
	$(document).ready(function() """),format.raw/*6.31*/("""{"""),format.raw/*6.32*/("""
		"""),format.raw/*7.3*/("""$("#addResourceInput").focus();
	"""),format.raw/*8.2*/("""}"""),format.raw/*8.3*/(""");
</script>
<div id="page-wrapper">

	"""),_display_(/*12.3*/helper/*12.9*/.form(action=routes.ResourceController.submitAddResource(),'id->"addResource",'name->"addResource")/*12.108*/{_display_(Seq[Any](format.raw/*12.109*/("""
	"""),format.raw/*13.2*/("""<div id="result"></div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default"
				style="border: 0; margin-bottom: 10px; box-shadow: none;">
				<div class="panel-heading"
					style="font-size: 20px; text-align: center; margin-bottom: 10px;">
					<i><b>Add New Resource*</b> </i>
				</div>

				<div class="row">
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step1 &#8594; <b>Select Resource Type</b>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label> Resource Type </label> <select required class="form-control"
											name="resourceType" id="resourceTypeOptions">
											<option selected="selected" disabled="disabled" value="">Select
												Resource Type</option>
												"""),_display_(/*36.14*/for(resourceType <- resourceTypes) yield /*36.48*/{_display_(Seq[Any](format.raw/*36.49*/("""
													"""),format.raw/*37.14*/("""<option value=""""),_display_(/*37.30*/resourceType),format.raw/*37.42*/("""">"""),_display_(/*37.45*/resourceType),format.raw/*37.57*/("""</option>
												""")))}),format.raw/*38.14*/("""
										"""),format.raw/*39.11*/("""</select>
									</div>
									
								</div>
							</div>
						</div>
					</div>

					<!-- ;;;;;;;;;; -->
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step2 &#8594; <b>Select Resource Sub Type</b>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label> Resource Sub Type </label> <select required class="form-control"
											name="resourceSubType" id="resourceSubTypeOption">
											<option selected="selected" disabled="disabled" value="">Select
												Resource Sub Type</option>
												"""),_display_(/*60.14*/for(resourceSubType <- resourceSubTypes) yield /*60.54*/{_display_(Seq[Any](format.raw/*60.55*/("""
													"""),format.raw/*61.14*/("""<option value=""""),_display_(/*61.30*/resourceSubType),format.raw/*61.45*/("""">"""),_display_(/*61.48*/resourceSubType),format.raw/*61.63*/("""</option>
												""")))}),format.raw/*62.14*/("""
										"""),format.raw/*63.11*/("""</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- ;;;;;;; -->

					<div class="col-lg-12" style="margin: 0 auto">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step3 &#8594; <b>Select Resource Value</b>
							</div>
							<div class="panel-body">
								<div class="row">

									<div class="col-lg-12">
										<label>Add New Resource Value</label><span
											style="font-size: 10px;">(multiple resources separated
											by comma)</span> <input required type="text" class="form-control "
											name="newResourceValue" id="addResourceInput" />
									</div>
								</div>
							</div>
						</div>
					</div>



				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					Step 4 &#8594; <b>Select Operation</b>
				</div>
				<div class="panel-body">
					<div class="row" style="margin-bottom: 10px;">
						<div class="col-lg-4">
							<button type="submit" value="more" name="submitValue1"
								id="submitValue1" class="btn btn-success col-lg-12 btn-block">
								<b>Save and Add More Resources</b>
							</button>
						</div>
						<div class="col-lg-4">
							<button type="submit" value="continue" name="submitValue1"
								id="submitValue1" class="btn btn-primary col-lg-12 btn-block">
								<b>Save and Go Back</b>
							</button>
						</div>
						<div class="col-lg-4">
							<a onClick="location.href = '"""),_display_(/*113.38*/routes/*113.44*/.Application.adminHome()),format.raw/*113.68*/("""'" 
							class="btn btn-info col-lg-12 btn-block">
								<b>Go Back</b>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*123.3*/("""
"""),format.raw/*124.1*/("""</div>
""")))}),format.raw/*125.2*/("""
"""))}
  }

  def render(resourceTypes:List[String],resourceSubTypes:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(resourceTypes,resourceSubTypes)

  def f:((List[String],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (resourceTypes,resourceSubTypes) => apply(resourceTypes,resourceSubTypes)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:47 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/addResource.scala.html
                  HASH: afcbd9a893d6f61cb25f4b70194cc9816128e4a9
                  MATRIX: 748->1|909->59|937->81|969->105|1007->106|1037->110|1104->150|1132->151|1162->155|1222->189|1249->190|1319->234|1333->240|1442->339|1482->340|1512->343|2451->1255|2501->1289|2540->1290|2583->1305|2626->1321|2659->1333|2689->1336|2722->1348|2777->1372|2817->1384|3544->2084|3600->2124|3639->2125|3682->2140|3725->2156|3761->2171|3791->2174|3827->2189|3882->2213|3922->2225|5472->3747|5488->3753|5534->3777|5728->3940|5758->3942|5798->3951
                  LINES: 26->1|29->1|30->3|30->3|30->3|32->5|33->6|33->6|34->7|35->8|35->8|39->12|39->12|39->12|39->12|40->13|63->36|63->36|63->36|64->37|64->37|64->37|64->37|64->37|65->38|66->39|87->60|87->60|87->60|88->61|88->61|88->61|88->61|88->61|89->62|90->63|140->113|140->113|140->113|150->123|151->124|152->125
                  -- GENERATED --
              */
          