
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object policies extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[String,List[Policy],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String, policies: List[Policy]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*1.41*/(""" 

"""),_display_(/*4.2*/main(title)/*4.13*/{_display_(Seq[Any](format.raw/*4.14*/("""
"""),format.raw/*5.1*/("""<div id="page-wrapper">
	"""),_display_(/*6.3*/if(policies.size()!=0)/*6.25*/{_display_(Seq[Any](format.raw/*6.26*/("""
	"""),_display_(/*7.3*/helper/*7.9*/.form(action=routes.PolicyController.submitActionPolicy(),'id->"printGroup",'name->"printGroup")/*7.105*/{_display_(Seq[Any](format.raw/*7.106*/("""

	"""),format.raw/*9.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default" style="border: 0; box-shadow: none;">

				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step 1 &#8594; <b>Select Policy</b>
							</div>
							<div class="panel-body">
								<div id="organization_div">

									<div class="row">
										<div class="col-lg-12" style="margin-bottom: 10px;">
											<label> Select Policy </label> <select class="form-control"
												name="policyId" id="policyId" style="width: 100%;">
												"""),_display_(/*26.14*/for(policy <- policies) yield /*26.37*/ {_display_(Seq[Any](format.raw/*26.39*/("""
												"""),format.raw/*27.13*/("""<option style="width: 100%;" value=""""),_display_(/*27.50*/policy/*27.56*/.getPolicyId()),format.raw/*27.70*/("""">
													"""),_display_(/*28.15*/policy/*28.21*/.getPolicyName()),format.raw/*28.37*/("""</option> """)))}),format.raw/*28.48*/("""
											"""),format.raw/*29.12*/("""</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						Step 2 &#8594; <b>Select Operation</b>
					</div>
					<div class="panel-body">
						<div class="row" style="margin-bottom: 10px;">
							<div class="col-lg-4">
								<button type="submit" value="edit" name="submitValue1"
									id="submitValue1" class="btn btn-primary col-lg-12 btn-block">
									Edit Policy</b>
								</button>
							</div>
							<div class="col-lg-4">
								<button type="submit" value="print" name="submitValue1"
									id="submitValue1" class="btn btn-success col-lg-12 btn-block">
									Print Policy</b>
								</button>
							</div>
							<div class="col-lg-4">
								<button type="submit" value="delete" name="submitValue1"
									id="submitValue1" class="btn btn-danger col-lg-12 btn-block">
									Delete Policy</b>
								</button>
							</div>
						</div>
					</div>
				</div>
				""")))}),format.raw/*64.6*/(""" """)))}/*64.8*/else/*64.12*/{_display_(Seq[Any](format.raw/*64.13*/("""
				"""),format.raw/*65.5*/("""<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading" style="text-align:center">
								<b>No Policy in System!!</b>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<button type="submit" onClick="location.href = '"""),_display_(/*74.60*/routes/*74.66*/.Application.home()),format.raw/*74.85*/("""'" class="btn btn-primary col-lg-12 btn-block">
										<b>Go Back</b>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				""")))}),format.raw/*83.6*/("""
			"""),format.raw/*84.4*/("""</div>
			""")))}))}
  }

  def render(title:String,policies:List[Policy]): play.twirl.api.HtmlFormat.Appendable = apply(title,policies)

  def f:((String,List[Policy]) => play.twirl.api.HtmlFormat.Appendable) = (title,policies) => apply(title,policies)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/policies.scala.html
                  HASH: c7c061119892301916be2fb1ff1586c2296eff0a
                  MATRIX: 739->1|881->40|912->65|931->76|969->77|997->79|1049->106|1079->128|1117->129|1146->133|1159->139|1264->235|1303->236|1334->241|1988->868|2027->891|2067->893|2109->907|2173->944|2188->950|2223->964|2268->982|2283->988|2320->1004|2362->1015|2403->1028|3499->2094|3519->2096|3532->2100|3571->2101|3604->2107|4018->2494|4033->2500|4073->2519|4291->2707|4323->2712
                  LINES: 26->1|29->1|31->4|31->4|31->4|32->5|33->6|33->6|33->6|34->7|34->7|34->7|34->7|36->9|53->26|53->26|53->26|54->27|54->27|54->27|54->27|55->28|55->28|55->28|55->28|56->29|91->64|91->64|91->64|91->64|92->65|101->74|101->74|101->74|110->83|111->84
                  -- GENERATED --
              */
          