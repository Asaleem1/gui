
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object editSubjects extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[List[GraphEntityValue],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(subjectValues:List[GraphEntityValue]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import java.math.BigInteger; var i=1;var k=0;

Seq[Any](format.raw/*1.40*/("""
"""),_display_(/*3.2*/main("Example")/*3.17*/{_display_(Seq[Any](format.raw/*3.18*/("""

"""),format.raw/*5.1*/("""<div id="page-wrapper">
"""),_display_(/*6.2*/if(subjectValues.size()!=0)/*6.29*/{_display_(Seq[Any](format.raw/*6.30*/("""
	"""),format.raw/*7.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">All Subjects</div>
				<div class="panel-body" style="overflow: auto; height: 430px;">
					<table class="table table-hover">
						<thead>
							<th>Index</th>
							<th>Subject Type</th>
							<th>Subject Sub Type</th>
							<th>Subject Name</th>
							<th>Operations</th>
						</thead>
						<tbody>
							"""),_display_(/*21.9*/for(subjectValue <- subjectValues) yield /*21.43*/{_display_(Seq[Any](format.raw/*21.44*/("""
							"""),format.raw/*22.8*/("""<tr>
								<td>"""),_display_(/*23.14*/i),format.raw/*23.15*/("""</td>
								<td>"""),_display_(/*24.14*/subjectValue/*24.26*/.getMainType()),format.raw/*24.40*/("""</td>
								<td>"""),_display_(/*25.14*/subjectValue/*25.26*/.getSubType()),format.raw/*25.39*/("""</td>
								<td>"""),_display_(/*26.14*/subjectValue/*26.26*/.getValue()),format.raw/*26.37*/("""</td>
								<td>
									<div class="row">
										<div class="col-lg-6">
											<a onclick="edit('"""),_display_(/*30.31*/subjectValue/*30.43*/.getGraphEntityId()),format.raw/*30.62*/("""')"
												class=" btn btn-info btn-block">Edit</a>
										</div>
										<div class="col-lg-6">
											<a onclick="deleteItem('"""),_display_(/*34.37*/subjectValue/*34.49*/.getGraphEntityId()),format.raw/*34.68*/("""')"
												class=" btn btn-danger btn-block">Delete</a>
										</div>
									</div>
								</td>
							</tr>
							"""),_display_(/*40.9*/{i=i+1}),format.raw/*40.16*/(""" """)))}),format.raw/*40.18*/("""
						"""),format.raw/*41.7*/("""</tbody>

					</table>
				</div>
			</div>
		</div>
	</div>
	<div id="modalDiv">
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">Edit Subject</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-12">
							<label>Edit Subject Type</label>
								<input type="text" class="form-control "
										name="editSubjectType" id='editSubjectType' />
							</div>
							<div class="col-lg-12">
							<label>Edit Subject Sub Type</label>
								<input type="text" class="form-control "
										name="editSubjectSubType" id='editSubjectSubType' />
							</div>
							<div class="col-lg-12">
							<label>Edit Subject Value</label>
								<input type="text" class="form-control "
										name="editSubjectValue" id='editSubjectValue' />
							</div>
						</div>
						
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary">Update Subject</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	""")))}/*89.3*/else/*89.7*/{_display_(Seq[Any](format.raw/*89.8*/("""
	"""),format.raw/*90.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading" style="text-align: center">
					<b>No Subject in DomainModel yet!!</b>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12" style="margin-bottom: 10px;">
							<button type="submit"
								onClick="location.href = '"""),_display_(/*100.36*/routes/*100.42*/.Application.adminHome()),format.raw/*100.66*/("""'"
								class="btn btn-primary col-lg-12 btn-block">
								<b>Go Back</b>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*110.3*/("""
"""),format.raw/*111.1*/("""</div>
<script>
	function edit(id) """),format.raw/*113.20*/("""{"""),format.raw/*113.21*/("""
		
		"""),format.raw/*115.3*/("""if (id != null) """),format.raw/*115.19*/("""{"""),format.raw/*115.20*/("""
			"""),format.raw/*116.4*/("""$("#myModal").modal('show');
		"""),format.raw/*117.3*/("""}"""),format.raw/*117.4*/("""

		
	"""),format.raw/*120.2*/("""}"""),format.raw/*120.3*/("""
	"""),format.raw/*121.2*/("""function deleteItem(id) """),format.raw/*121.26*/("""{"""),format.raw/*121.27*/("""
		"""),format.raw/*122.3*/("""if (id != null) """),format.raw/*122.19*/("""{"""),format.raw/*122.20*/("""
			"""),format.raw/*123.4*/("""$("#myModal").modal('show');
		"""),format.raw/*124.3*/("""}"""),format.raw/*124.4*/("""
	"""),format.raw/*125.2*/("""}"""),format.raw/*125.3*/("""
"""),format.raw/*126.1*/("""</script>
""")))}),format.raw/*127.2*/("""
"""))}
  }

  def render(subjectValues:List[GraphEntityValue]): play.twirl.api.HtmlFormat.Appendable = apply(subjectValues)

  def f:((List[GraphEntityValue]) => play.twirl.api.HtmlFormat.Appendable) = (subjectValues) => apply(subjectValues)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/editSubjects.scala.html
                  HASH: ff623f353713289f06f0ac8aaebcc0efa32e7e56
                  MATRIX: 746->1|917->39|945->90|968->105|1006->106|1036->110|1087->136|1122->163|1160->164|1189->167|1658->610|1708->644|1747->645|1783->654|1829->673|1851->674|1898->694|1919->706|1954->720|2001->740|2022->752|2056->765|2103->785|2124->797|2156->808|2296->921|2317->933|2357->952|2531->1099|2552->1111|2592->1130|2753->1265|2781->1272|2814->1274|2849->1282|4371->2786|4383->2790|4421->2791|4451->2794|4859->3174|4875->3180|4921->3204|5123->3375|5153->3377|5219->3414|5249->3415|5285->3423|5330->3439|5360->3440|5393->3445|5453->3477|5482->3478|5519->3487|5548->3488|5579->3491|5632->3515|5662->3516|5694->3520|5739->3536|5769->3537|5802->3542|5862->3574|5891->3575|5922->3578|5951->3579|5981->3581|6024->3593
                  LINES: 26->1|29->1|30->3|30->3|30->3|32->5|33->6|33->6|33->6|34->7|48->21|48->21|48->21|49->22|50->23|50->23|51->24|51->24|51->24|52->25|52->25|52->25|53->26|53->26|53->26|57->30|57->30|57->30|61->34|61->34|61->34|67->40|67->40|67->40|68->41|116->89|116->89|116->89|117->90|127->100|127->100|127->100|137->110|138->111|140->113|140->113|142->115|142->115|142->115|143->116|144->117|144->117|147->120|147->120|148->121|148->121|148->121|149->122|149->122|149->122|150->123|151->124|151->124|152->125|152->125|153->126|154->127
                  -- GENERATED --
              */
          