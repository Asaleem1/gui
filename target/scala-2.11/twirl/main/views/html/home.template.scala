
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object home extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](_display_(/*1.2*/main("Example")/*1.17*/{_display_(Seq[Any](format.raw/*1.18*/("""

"""),format.raw/*3.1*/("""<script>setTimeout(function()"""),format.raw/*3.30*/("""{"""),format.raw/*3.31*/("""$('#errorId').fadeOut();"""),format.raw/*3.55*/("""}"""),format.raw/*3.56*/(""", 5000);</script>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">Manage Policy Files</div>
				<div class="panel-body">
					"""),_display_(/*10.7*/if(flash.get("message"))/*10.31*/{_display_(Seq[Any](format.raw/*10.32*/("""
					"""),format.raw/*11.6*/("""<h4 style="text-align: center; color: red" id="errorId">"""),_display_(/*11.63*/flash/*11.68*/.get("message")),format.raw/*11.83*/("""</h4>
					""")))}),format.raw/*12.7*/("""
					"""),format.raw/*13.6*/("""<div class="row ">
						<div class="col-lg-6" style="margin-bottom: 10px;">
							<button
								onclick="location.href = '"""),_display_(/*16.36*/routes/*16.42*/.PolicyController.newPolicy()),format.raw/*16.71*/("""'"
								class="btn btn-success btn-block">Create New Policy</button>
						</div>
						<div class="col-lg-6" style="margin-bottom: 10px;">

							<button
								onclick="location.href = '"""),_display_(/*22.36*/routes/*22.42*/.Application.policies()),format.raw/*22.65*/("""'"
								class="btn btn-primary btn-block">List All Policies</button>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-6" style="margin-bottom: 10px;">
							<button onclick="location.href = '"""),_display_(/*29.43*/routes/*29.49*/.Application.runPolicy()),format.raw/*29.73*/("""'"
							class="btn btn-danger btn-block">Run Test</button>
						</div>
						<div class="col-lg-6" style="margin-bottom: 10px;">
							<button onclick="location.href = '"""),_display_(/*33.43*/routes/*33.49*/.Application.result()),format.raw/*33.70*/("""'"
								class="btn btn-primary btn-block">View Results</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



</div>


""")))}),format.raw/*47.2*/("""
"""))}
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/home.scala.html
                  HASH: afc980e01481b3f12620e8fb01e2866b65b5e324
                  MATRIX: 797->1|820->16|858->17|888->21|944->50|972->51|1023->75|1051->76|1300->299|1333->323|1372->324|1406->331|1490->388|1504->393|1540->408|1583->421|1617->428|1774->558|1789->564|1839->593|2066->793|2081->799|2125->822|2380->1050|2395->1056|2440->1080|2645->1258|2660->1264|2702->1285|2889->1442
                  LINES: 29->1|29->1|29->1|31->3|31->3|31->3|31->3|31->3|38->10|38->10|38->10|39->11|39->11|39->11|39->11|40->12|41->13|44->16|44->16|44->16|50->22|50->22|50->22|57->29|57->29|57->29|61->33|61->33|61->33|75->47
                  -- GENERATED --
              */
          