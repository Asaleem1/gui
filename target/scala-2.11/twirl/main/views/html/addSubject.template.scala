
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object addSubject extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[String],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(subjectTypes:List[String],subjectSubTypes:List[String]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*1.58*/("""
"""),_display_(/*3.2*/main("Add New Subject")/*3.25*/{_display_(Seq[Any](format.raw/*3.26*/("""

"""),format.raw/*5.1*/("""<script>
	$(document).ready(function() """),format.raw/*6.31*/("""{"""),format.raw/*6.32*/("""
		"""),format.raw/*7.3*/("""$("#addSubjectInput").focus();
	"""),format.raw/*8.2*/("""}"""),format.raw/*8.3*/(""");
</script>
<div id="page-wrapper">

	"""),_display_(/*12.3*/helper/*12.9*/.form(action=routes.SubjectController.submitAddSubject(),'id->"addSubject",'name->"addSubject")/*12.104*/{_display_(Seq[Any](format.raw/*12.105*/("""
	"""),format.raw/*13.2*/("""<div id="result"></div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default"
				style="border: 0; margin-bottom: 10px; box-shadow: none;">
				<div class="panel-heading"
					style="font-size: 20px; text-align: center; margin-bottom: 10px;">
					<i><b>Add New Subject*</b> </i>
				</div>

				<div class="row">
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step1 &#8594; <b>Select Subject Type</b>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label> Subject Type </label> <select required class="form-control"
											name="subjectType" id="subjectTypeOptions">
											<option selected="selected" disabled="disabled" value="">Select
												Subject Type</option>
											"""),_display_(/*36.13*/for(subjectType <- subjectTypes) yield /*36.45*/{_display_(Seq[Any](format.raw/*36.46*/("""
												"""),format.raw/*37.13*/("""<option value=""""),_display_(/*37.29*/subjectType),format.raw/*37.40*/("""">"""),_display_(/*37.43*/subjectType),format.raw/*37.54*/("""</option>
											""")))}),format.raw/*38.13*/("""
										"""),format.raw/*39.11*/("""</select>
									</div>
									
								</div>
							</div>
						</div>
					</div>
	
					<!-- ;;;;;;;;;; -->
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step2 &#8594; <b>Select Subject Sub Type</b>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label> Subject Sub Type </label> <select required class="form-control"
											name="subjectSubType" id="subjectSubTypeOption">
											<option selected="selected" disabled="disabled" value="">Select
												Subject Sub Type</option>
											"""),_display_(/*60.13*/for(subjectSubType <- subjectSubTypes) yield /*60.51*/{_display_(Seq[Any](format.raw/*60.52*/("""
												"""),format.raw/*61.13*/("""<option value=""""),_display_(/*61.29*/subjectSubType),format.raw/*61.43*/("""">"""),_display_(/*61.46*/subjectSubType),format.raw/*61.60*/("""</option>
											""")))}),format.raw/*62.13*/("""
										"""),format.raw/*63.11*/("""</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- ;;;;;;; -->

					<div class="col-lg-12" style="margin: 0 auto">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step3 &#8594; <b>Select Subject Value</b>
							</div>
							<div class="panel-body">
								<div class="row">

									<div class="col-lg-12">
										<label>Add New Subject Value</label><span
											style="font-size: 10px;">(multiple subjects separated
											by comma)</span> <input required type="text" class="form-control "
											name="newSubjectValue" id="addSubjectInput" />
									</div>
								</div>
							</div>
						</div>
					</div>



				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					Step 4 &#8594; <b>Select Operation</b>
				</div>
				<div class="panel-body">
					<div class="row" style="margin-bottom: 10px;">
						<div class="col-lg-4">
							<button type="submit" value="more" name="submitValue1"
								id="submitValue1" class="btn btn-success col-lg-12 btn-block">
								<b>Save and Add More Subjects</b>
							</button>
						</div>
						<div class="col-lg-4">
							<button type="submit" value="continue" name="submitValue1"
								id="submitValue1" class="btn btn-primary col-lg-12 btn-block">
								<b>Save and Go Back</b>
							</button>
						</div>
						<div class="col-lg-4">
							<a onClick="location.href = '"""),_display_(/*113.38*/routes/*113.44*/.Application.adminHome()),format.raw/*113.68*/("""'" 
							class="btn btn-info col-lg-12 btn-block">
								<b>Go Back</b>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*123.3*/("""
"""),format.raw/*124.1*/("""</div>
""")))}),format.raw/*125.2*/("""
"""))}
  }

  def render(subjectTypes:List[String],subjectSubTypes:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(subjectTypes,subjectSubTypes)

  def f:((List[String],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (subjectTypes,subjectSubTypes) => apply(subjectTypes,subjectSubTypes)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:47 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/addSubject.scala.html
                  HASH: af8dd25e351d83fcc7318b7d7cb857d5fb517b48
                  MATRIX: 747->1|906->57|934->79|965->102|1003->103|1033->107|1100->147|1128->148|1158->152|1217->185|1244->186|1314->230|1328->236|1433->331|1473->332|1503->335|2435->1240|2483->1272|2522->1273|2564->1287|2607->1303|2639->1314|2669->1317|2701->1328|2755->1351|2795->1363|3517->2058|3571->2096|3610->2097|3652->2111|3695->2127|3730->2141|3760->2144|3795->2158|3849->2181|3889->2193|5433->3709|5449->3715|5495->3739|5689->3902|5719->3904|5759->3913
                  LINES: 26->1|29->1|30->3|30->3|30->3|32->5|33->6|33->6|34->7|35->8|35->8|39->12|39->12|39->12|39->12|40->13|63->36|63->36|63->36|64->37|64->37|64->37|64->37|64->37|65->38|66->39|87->60|87->60|87->60|88->61|88->61|88->61|88->61|88->61|89->62|90->63|140->113|140->113|140->113|150->123|151->124|152->125
                  -- GENERATED --
              */
          