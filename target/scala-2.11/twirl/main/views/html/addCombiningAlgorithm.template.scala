
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
import helper._
/**/
object addCombiningAlgorithm extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](_display_(/*3.2*/main("Add New CombiningAlgorithm")/*3.36*/{_display_(Seq[Any](format.raw/*3.37*/("""

"""),format.raw/*5.1*/("""<script>
	$(document).ready(function() """),format.raw/*6.31*/("""{"""),format.raw/*6.32*/("""
		"""),format.raw/*7.3*/("""$("#addCombiningAlgorithmInput").focus();
	"""),format.raw/*8.2*/("""}"""),format.raw/*8.3*/(""");
</script>
<div id="page-wrapper">

	"""),_display_(/*12.3*/helper/*12.9*/.form(action=routes.CombiningAlgorithmController.submitAddCombiningAlgorithm(),'id->"addCombiningAlgorithm",'name->"addCombiningAlgorithm")/*12.148*/{_display_(Seq[Any](format.raw/*12.149*/("""
	"""),format.raw/*13.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default"
				style="border: 0; margin-bottom: 10px; box-shadow: none;">
				<div class="panel-heading"
					style="font-size: 20px; text-align: center; margin-bottom: 10px;">
					<i><b>Add New CombiningAlgorithm*</b> </i>
				</div>


				<div class="col-lg-12" style="margin: 0 auto">
					<div class="panel panel-default">
						<div class="panel-heading">
							Step 1 &#8594; <b>Select CombiningAlgorithm Value</b>
						</div>
						<div class="panel-body">
							<div class="row">

								<div class="col-lg-12">
									<label>Add New CombiningAlgorithm Value</label><span
										style="font-size: 10px;">(multiple combiningAlgorithm
										separated by comma)</span> <input required type="text"
										class="form-control " name="newCombiningAlgorithmValue"
										id="addCombiningAlgorithmInput" />
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							Step 2 &#8594; <b>Select Operation</b>
						</div>
						<div class="panel-body">
							<div class="row" style="margin-bottom: 10px;">
								<div class="col-lg-4">
									<button type="submit" value="more" name="submitValue1"
										id="submitValue1" class="btn btn-success col-lg-12 btn-block">
										<b>Save and Add More CombiningAlgorithm</b>
									</button>
								</div>
								<div class="col-lg-4">
									<button type="submit" value="continue" name="submitValue1"
										id="submitValue1" class="btn btn-primary col-lg-12 btn-block">
										<b>Save and Go Back</b>
									</button>
								</div>
								<div class="col-lg-4">
									<a onClick="location.href = '"""),_display_(/*62.40*/routes/*62.46*/.Application.adminHome()),format.raw/*62.70*/("""'"
										class="btn btn-info col-lg-12 btn-block"> <b>Go Back</b>
									</a>
								</div>
							</div>
						</div>
					</div>

				</div>


			</div>
		</div>


	</div>
	""")))}),format.raw/*78.3*/("""
"""),format.raw/*79.1*/("""</div>
""")))}),format.raw/*80.2*/("""
"""))}
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:47 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/addCombiningAlgorithm.scala.html
                  HASH: 2889b8df905610786ed00087ddcf851fcfea72ff
                  MATRIX: 829->22|871->56|909->57|939->61|1006->101|1034->102|1064->106|1134->150|1161->151|1231->195|1245->201|1394->340|1434->341|1464->344|3312->2165|3327->2171|3372->2195|3601->2394|3630->2396|3669->2405
                  LINES: 29->3|29->3|29->3|31->5|32->6|32->6|33->7|34->8|34->8|38->12|38->12|38->12|38->12|39->13|88->62|88->62|88->62|104->78|105->79|106->80
                  -- GENERATED --
              */
          