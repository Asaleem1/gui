
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object result extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[String,List[Policy],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String, policies: List[Policy]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*1.41*/(""" 
"""),_display_(/*3.2*/main(title)/*3.13*/{_display_(Seq[Any](format.raw/*3.14*/("""
"""),format.raw/*4.1*/("""<div id="page-wrapper">
	"""),_display_(/*5.3*/if(policies.size()!=0)/*5.25*/{_display_(Seq[Any](format.raw/*5.26*/("""
	"""),_display_(/*6.3*/helper/*6.9*/.form(action=routes.PolicyController.submitActionPolicy(),'id->"printGroup",'name->"printGroup")/*6.105*/{_display_(Seq[Any](format.raw/*6.106*/("""

	"""),format.raw/*8.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default" style="border: 0; box-shadow: none;">

				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<b>Select Policy Test Detail</b>
							</div>
							<div class="panel-body">
								<div id="organization_div">

									<div class="row">
										<div class="col-lg-4" style="margin-bottom: 10px;">
											<label> Select Policy </label> <select class="form-control"
												name="policyId" id="policyId" style="width: 100%;">
												"""),_display_(/*25.14*/for(policy <- policies) yield /*25.37*/ {_display_(Seq[Any](format.raw/*25.39*/("""
												"""),format.raw/*26.13*/("""<option style="width: 100%;" value=""""),_display_(/*26.50*/policy/*26.56*/.getPolicyId()),format.raw/*26.70*/("""">
													"""),_display_(/*27.15*/policy/*27.21*/.getPolicyName()),format.raw/*27.37*/("""</option> """)))}),format.raw/*27.48*/("""
											"""),format.raw/*28.12*/("""</select>
										</div>
										<div class="col-lg-3" style="margin-bottom: 10px;">
											<label> Test Date </label> <select
												class="form-control" name="policyId" id="policyId"
												style="width: 100%;">
												<option style="width: 100%;" value="Xacml2.0">
													24 nov 2015</option>
												<option style="width: 100%;" value="Xacml3.0">
													28 nov 2015</option>
											</select>
										</div>
										<div class="col-lg-5" style="margin-bottom: 0px;margin-top:23.5px;">
											<div class="col-lg-6" style="margin-bottom: 10px;">
												<a href="#"
													class="btn btn-success btn-block">
													<b>View Results</b>
												</a>
											</div>
											<div class="col-lg-6" style="margin-bottom: 10px;">
												<a href="#"
													class="btn btn-danger btn-block">
													<b>Go Back</b>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							
							<div class="panel-body">
								<div id="organization_div">
									<div class="row">
										<div class="col-lg-6" style="margin-bottom: 10px;">
											<label> PDPs Comparison Result </label> 
											<div class="col-lg-12" style="margin-bottom:10px">
											<img src=""""),_display_(/*70.23*/routes/*70.29*/.Assets.at("images/result1-time-measure.png")),format.raw/*70.74*/("""" width="100%" height="250px"/>
											</div>
										</div>
										<div class="col-lg-6" style="margin-bottom: 10px;">
											<label> PDPs Performance Ratio Result </label> 
											<div class="col-lg-12" style="margin-bottom:10px">
											<img src=""""),_display_(/*76.23*/routes/*76.29*/.Assets.at("images/result2-performance-factor.png")),format.raw/*76.80*/("""" width="100%" height="250px" />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				""")))}),format.raw/*86.6*/(""" """)))}/*86.8*/else/*86.12*/{_display_(Seq[Any](format.raw/*86.13*/("""
				"""),format.raw/*87.5*/("""<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading" style="text-align: center">
								<b>No Policy in System!!</b>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<button type="submit"
											onClick="location.href = '"""),_display_(/*97.39*/routes/*97.45*/.Application.home()),format.raw/*97.64*/("""'"
											class="btn btn-primary col-lg-12 btn-block">
											<b>Go Back</b>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				""")))}),format.raw/*107.6*/("""
			"""),format.raw/*108.4*/("""</div>
			""")))}))}
  }

  def render(title:String,policies:List[Policy]): play.twirl.api.HtmlFormat.Appendable = apply(title,policies)

  def f:((String,List[Policy]) => play.twirl.api.HtmlFormat.Appendable) = (title,policies) => apply(title,policies)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:49 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/result.scala.html
                  HASH: e3077ac334aa9fd996561ff51d08f8cae0be7643
                  MATRIX: 737->1|879->40|908->63|927->74|965->75|993->77|1045->104|1075->126|1113->127|1142->131|1155->137|1260->233|1299->234|1330->239|1980->862|2019->885|2059->887|2101->901|2165->938|2180->944|2215->958|2260->976|2275->982|2312->998|2354->1009|2395->1022|3889->2489|3904->2495|3970->2540|4275->2818|4290->2824|4362->2875|4557->3040|4577->3042|4590->3046|4629->3047|4662->3053|5089->3453|5104->3459|5144->3478|5376->3679|5409->3684
                  LINES: 26->1|29->1|30->3|30->3|30->3|31->4|32->5|32->5|32->5|33->6|33->6|33->6|33->6|35->8|52->25|52->25|52->25|53->26|53->26|53->26|53->26|54->27|54->27|54->27|54->27|55->28|97->70|97->70|97->70|103->76|103->76|103->76|113->86|113->86|113->86|113->86|114->87|124->97|124->97|124->97|134->107|135->108
                  -- GENERATED --
              */
          