
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object adminHome extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(uploaded:String):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.19*/("""

"""),_display_(/*3.2*/main("Admin ControlPanel")/*3.28*/{_display_(Seq[Any](format.raw/*3.29*/("""
"""),format.raw/*4.1*/("""<script>setTimeout(function()"""),format.raw/*4.30*/("""{"""),format.raw/*4.31*/("""$('#errorId').fadeOut();"""),format.raw/*4.55*/("""}"""),format.raw/*4.56*/(""", 5000);</script>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
		"""),_display_(/*8.4*/if(flash.get("message"))/*8.28*/{_display_(Seq[Any](format.raw/*8.29*/("""
			"""),format.raw/*9.4*/("""<h4 style="text-align: center;color:red" id="errorId">"""),_display_(/*9.59*/flash/*9.64*/.get("message")),format.raw/*9.79*/("""</h4>
		""")))}),format.raw/*10.4*/("""
		
			"""),format.raw/*12.4*/("""<div class="panel panel-default">
				<div class="panel-heading">Actions for domain model</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12" style="margin-bottom: 10px;">
							<button
								onclick="location.href = '"""),_display_(/*18.36*/routes/*18.42*/.GraphController.graphIntialize()),format.raw/*18.75*/("""'"
								class="btn btn-success btn-block">"""),_display_(/*19.44*/if(uploaded=="yes")/*19.63*/{_display_(Seq[Any](format.raw/*19.64*/("""Reset Domain Model""")))}/*19.83*/else/*19.87*/{_display_(Seq[Any](format.raw/*19.88*/("""Intialize Domain
								Model """)))}),format.raw/*20.16*/("""</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">Manage static model attributes</div>
				<div class="panel-body">
					<div class="row ">
						<div class="col-lg-4" style="margin-bottom: 10px;">
							<button
								onclick="location.href = '"""),_display_(/*35.36*/routes/*35.42*/.PropertyFileController.addPropertyValue()),format.raw/*35.84*/("""'"
								class="btn btn-primary btn-block">Add Attribute Values
							</button>
						</div>
						<div class="col-lg-4" style="margin-bottom: 10px;">
							<button
								onclick="location.href = '"""),_display_(/*41.36*/routes/*41.42*/.Application.editAttributes()),format.raw/*41.71*/("""'"
								class="btn btn-primary btn-block">View/Edit Attributes</button>
						</div>
						<div class="col-lg-4" style="margin-bottom: 10px;">
							<button
								onclick="location.href = '"""),_display_(/*46.36*/routes/*46.42*/.Application.exportAttributes()),format.raw/*46.73*/("""'"
								class="btn btn-primary btn-block">Export Attributes to domainModel</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">Manage policy model attributes</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-6" style="margin-bottom: 10px;">
							<button
								onclick="location.href = '"""),_display_(/*62.36*/routes/*62.42*/.CombiningAlgorithmController.addCombiningAlgorithm()),format.raw/*62.95*/("""'"
								class="btn btn-info btn-block">Add Combining Algorithm</button>
						</div>
						<div class="col-lg-6" style="margin-bottom: 10px;">
							<button
								onclick="location.href = '"""),_display_(/*67.36*/routes/*67.42*/.CombiningAlgorithmController.editCombiningAlgorithm()),format.raw/*67.96*/("""'"
								class="btn btn-info btn-block">View/Edit Combining Algorithm</button>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6" style="margin-bottom: 10px;">
							<button
								onclick="location.href = '"""),_display_(/*74.36*/routes/*74.42*/.RuleController.addPermissions()),format.raw/*74.74*/("""'"
								class="btn btn-info btn-block">Add Permissions</button>
						</div>
						<div class="col-lg-6" style="margin-bottom: 10px;">
							<button
								onclick="location.href = '"""),_display_(/*79.36*/routes/*79.42*/.RuleController.editPermissions()),format.raw/*79.75*/("""'"
								class="btn btn-info btn-block">View/Edit Permission</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">Manage Property Files</div>
				<div class="panel-body">
					<div class="row ">
						<div class="col-lg-6" style="margin-bottom: 10px;">
							<button onclick="location.href = '"""),_display_(/*94.43*/routes/*94.49*/.Application.newFile()),format.raw/*94.71*/("""'"
								class="btn btn-success btn-block">Create New Property
								File</button>
						</div>
						<div class="col-lg-6" style="margin-bottom: 10px;">

							<button
								onclick="location.href = '"""),_display_(/*101.36*/routes/*101.42*/.Application.propertyFiles()),format.raw/*101.70*/("""'"
								class="btn btn-primary btn-block">List All Property
								Files</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


""")))}),format.raw/*113.2*/("""
"""))}
  }

  def render(uploaded:String): play.twirl.api.HtmlFormat.Appendable = apply(uploaded)

  def f:((String) => play.twirl.api.HtmlFormat.Appendable) = (uploaded) => apply(uploaded)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:47 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/adminHome.scala.html
                  HASH: bd510d9e70324809ebc5359f2dcd16cc80e70e2d
                  MATRIX: 727->1|832->18|862->23|896->49|934->50|962->52|1018->81|1046->82|1097->106|1125->107|1244->201|1276->225|1314->226|1345->231|1426->286|1439->291|1474->306|1514->316|1550->325|1840->588|1855->594|1909->627|1983->674|2011->693|2050->694|2088->713|2101->717|2140->718|2204->751|2630->1150|2645->1156|2708->1198|2945->1408|2960->1414|3010->1443|3238->1644|3253->1650|3305->1681|3808->2157|3823->2163|3897->2216|4125->2417|4140->2423|4215->2477|4486->2721|4501->2727|4554->2759|4774->2952|4789->2958|4843->2991|5315->3436|5330->3442|5373->3464|5617->3680|5633->3686|5683->3714|5885->3885
                  LINES: 26->1|29->1|31->3|31->3|31->3|32->4|32->4|32->4|32->4|32->4|36->8|36->8|36->8|37->9|37->9|37->9|37->9|38->10|40->12|46->18|46->18|46->18|47->19|47->19|47->19|47->19|47->19|47->19|48->20|63->35|63->35|63->35|69->41|69->41|69->41|74->46|74->46|74->46|90->62|90->62|90->62|95->67|95->67|95->67|102->74|102->74|102->74|107->79|107->79|107->79|122->94|122->94|122->94|129->101|129->101|129->101|141->113
                  -- GENERATED --
              */
          