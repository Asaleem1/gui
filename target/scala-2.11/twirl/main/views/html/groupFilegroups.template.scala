
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object groupFileGroups extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[String,List[GPropertyGroups],String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String,
 groups: List[GPropertyGroups],
 fileType:String):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*3.18*/(""" 

"""),_display_(/*6.2*/main(title)/*6.13*/{_display_(Seq[Any](format.raw/*6.14*/("""

"""),format.raw/*8.1*/("""<div id="page-wrapper">
<script>
function deleteItem() """),format.raw/*10.23*/("""{"""),format.raw/*10.24*/("""
	"""),format.raw/*11.2*/("""var id = $("#groupId").val();
		if (id != null) """),format.raw/*12.19*/("""{"""),format.raw/*12.20*/("""
			"""),format.raw/*13.4*/("""var option = confirm("Are you sure to delete This Action Type?");

			if (option) """),format.raw/*15.16*/("""{"""),format.raw/*15.17*/("""
				"""),format.raw/*16.5*/("""$.ajax("""),format.raw/*16.12*/("""{"""),format.raw/*16.13*/("""
					"""),format.raw/*17.6*/("""type : "POST",
					url : "/deleteGPropertyGroup",
					data : """),format.raw/*19.13*/("""{"""),format.raw/*19.14*/("""
						"""),format.raw/*20.7*/(""""groupId" : id,
					"""),format.raw/*21.6*/("""}"""),format.raw/*21.7*/(""",
					success : function(data) """),format.raw/*22.31*/("""{"""),format.raw/*22.32*/("""
						"""),format.raw/*23.7*/("""$('#groupId option[value="'+id+'"]').remove();
						
					"""),format.raw/*25.6*/("""}"""),format.raw/*25.7*/("""
				"""),format.raw/*26.5*/("""}"""),format.raw/*26.6*/(""");
			"""),format.raw/*27.4*/("""}"""),format.raw/*27.5*/("""

		"""),format.raw/*29.3*/("""}"""),format.raw/*29.4*/("""
	"""),format.raw/*30.2*/("""}"""),format.raw/*30.3*/("""
"""),format.raw/*31.1*/("""</script>

	"""),_display_(/*33.3*/helper/*33.9*/.form(action=routes.GroupController.GPropertyEdit(),'id->"editGroup",'name->"editGroup")/*33.97*/{_display_(Seq[Any](format.raw/*33.98*/("""
	"""),format.raw/*34.2*/("""<input type="hidden" value=""""),_display_(/*34.31*/fileType),format.raw/*34.39*/("""" name="fileType"/>
			
	"""),_display_(/*36.3*/if(groups.size()!=0)/*36.23*/{_display_(Seq[Any](format.raw/*36.24*/("""
	
	"""),format.raw/*38.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default" style="border: 0; box-shadow: none;">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step 1 &#8594; <b>Select Property Group</b>
							</div>
							<div class="panel-body">
								<div >

									<div class="row">
										<div class="col-lg-12" style="margin-bottom: 10px;">
											<label> Select Group </label> <select class="form-control"
												name="groupId" id="groupId" style="width: 100%;">
												"""),_display_(/*54.14*/for(group <- groups) yield /*54.34*/ {_display_(Seq[Any](format.raw/*54.36*/("""
												"""),format.raw/*55.13*/("""<option style="width: 100%;" value=""""),_display_(/*55.50*/group/*55.55*/.getgPropertyGroupId()),format.raw/*55.77*/("""">
													"""),_display_(/*56.15*/group/*56.20*/.getgPropertyGroupId()),format.raw/*56.42*/("""</option> """)))}),format.raw/*56.53*/("""
											"""),format.raw/*57.12*/("""</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						Step 2 &#8594; <b>Select Operation</b>
					</div>
					<div class="panel-body">
						<div class="row" style="margin-bottom: 10px;">
							<div class="col-lg-3">
								<button type="submit" value="edit" name="submitValue1"
									id="submitValue1" class="btn btn-primary col-lg-12 btn-block">
									Edit Group</b>
								</button>
							</div>
							<div class="col-lg-3">
								<a  onclick="deleteItem()" class="btn btn-danger col-lg-12 btn-block">
									Delete Group</b>
								</a>
							</div>
							<div class="col-lg-3">
								<button type="submit" value="newGroup" name="submitValue1"
									id="submitValue1" class="btn btn-success col-lg-12 btn-block">
									Add New Group</b>
								</button>
							</div>
							<div class="col-lg-3">
								<a  onClick="location.href = '"""),_display_(/*89.40*/routes/*89.46*/.Application.home()),format.raw/*89.65*/("""'" class="btn btn-primary col-lg-12 btn-block">
										<b>Go Back</b>
								</a>
							</div>
						</div>
					</div>
				</div>
				""")))}/*96.6*/else/*96.10*/{_display_(Seq[Any](format.raw/*96.11*/("""
				"""),format.raw/*97.5*/("""<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading" style="text-align:center">
								<b>No Group Found!!</b>
							</div>
							<div class="panel-body">
								<div class="row">
								
									<div class="col-lg-6" style="margin-bottom: 10px;">
										<a  onClick="location.href = '"""),_display_(/*107.42*/routes/*107.48*/.Application.home()),format.raw/*107.67*/("""'" class="btn btn-primary col-lg-12 btn-block">
										<b>Go Back</b>
										</a>
									</div>
									<div class="col-lg-6" style="margin-bottom: 10px;">
										<button type="submit" value="newGroup" name="submitValue1" class="btn btn-primary col-lg-12 btn-block">
										<b>Add New Group</b>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				""")))}),format.raw/*121.6*/("""
			"""),format.raw/*122.4*/("""</div>
			""")))}),format.raw/*123.5*/("""
			""")))}))}
  }

  def render(title:String,groups:List[GPropertyGroups],fileType:String): play.twirl.api.HtmlFormat.Appendable = apply(title,groups,fileType)

  def f:((String,List[GPropertyGroups],String) => play.twirl.api.HtmlFormat.Appendable) = (title,groups,fileType) => apply(title,groups,fileType)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/groupFileGroups.scala.html
                  HASH: 9fc9bfd5ddb19447927faa43b9ef17728adcbb37
                  MATRIX: 762->1|932->68|963->93|982->104|1020->105|1050->109|1135->166|1164->167|1194->170|1271->219|1300->220|1332->225|1444->309|1473->310|1506->316|1541->323|1570->324|1604->331|1697->396|1726->397|1761->405|1810->427|1838->428|1899->461|1928->462|1963->470|2051->531|2079->532|2112->538|2140->539|2174->546|2202->547|2235->553|2263->554|2293->557|2321->558|2350->560|2391->575|2405->581|2502->669|2541->670|2571->673|2627->702|2656->710|2710->738|2739->758|2778->759|2811->765|3447->1374|3483->1394|3523->1396|3565->1410|3629->1447|3643->1452|3686->1474|3731->1492|3745->1497|3788->1519|3830->1530|3871->1543|4929->2574|4944->2580|4984->2599|5150->2747|5163->2751|5202->2752|5235->2758|5636->3131|5652->3137|5693->3156|6154->3586|6187->3591|6230->3603
                  LINES: 26->1|31->3|33->6|33->6|33->6|35->8|37->10|37->10|38->11|39->12|39->12|40->13|42->15|42->15|43->16|43->16|43->16|44->17|46->19|46->19|47->20|48->21|48->21|49->22|49->22|50->23|52->25|52->25|53->26|53->26|54->27|54->27|56->29|56->29|57->30|57->30|58->31|60->33|60->33|60->33|60->33|61->34|61->34|61->34|63->36|63->36|63->36|65->38|81->54|81->54|81->54|82->55|82->55|82->55|82->55|83->56|83->56|83->56|83->56|84->57|116->89|116->89|116->89|123->96|123->96|123->96|124->97|134->107|134->107|134->107|148->121|149->122|150->123
                  -- GENERATED --
              */
          