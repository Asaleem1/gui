
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object login extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Form[Application.Login],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(userForm: Form[Application.Login]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import java.lang.String; val username="template"
import helper._

Seq[Any](format.raw/*1.37*/("""
"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/main("Login")/*5.15*/{_display_(Seq[Any](format.raw/*5.16*/("""

"""),format.raw/*7.1*/("""<div class="container theme-showcase" role="main">
	"""),_display_(/*8.3*/if(session.get("connected"))/*8.31*/{_display_(Seq[Any](format.raw/*8.32*/("""
	"""),format.raw/*9.2*/("""<div>hello """),_display_(/*9.14*/session/*9.21*/.get("connected")),format.raw/*9.38*/("""</div>
	""")))}),format.raw/*10.3*/("""
	"""),format.raw/*11.2*/("""<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-panel panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Sign In</h3>
					</div>
					"""),_display_(/*18.7*/if(userForm.hasGlobalErrors)/*18.35*/ {_display_(Seq[Any](format.raw/*18.37*/("""
						"""),format.raw/*19.7*/("""<p class="error">"""),_display_(/*19.25*/userForm/*19.33*/.globalError.message),format.raw/*19.53*/("""</p>
					""")))}),format.raw/*20.7*/("""
					"""),format.raw/*21.6*/("""<div class="panel-body">
					
					
						"""),_display_(/*24.8*/helper/*24.14*/.form(routes.Application.authenticate)/*24.52*/ {_display_(Seq[Any](format.raw/*24.54*/("""
						 
						   """),format.raw/*26.10*/("""<p>
						       <input type="email" name="email" class="form-control" placeholder="Email" value=""""),_display_(/*27.96*/userForm(username)/*27.114*/.value),format.raw/*27.120*/("""">
						   </p>
						   <p>
						       <input type="password" class="form-control" name="password" placeholder="Password">
						   </p>
						   <p>
						       <button type="submit" class="col-lg-6 btn btn-lg btn-success btn-block" >Login</button>
						   </p>
						""")))}),format.raw/*35.8*/("""
					
					"""),format.raw/*37.6*/("""</div>
				</div>
			</div>
		</div>
	</div>

</div>


""")))}),format.raw/*46.2*/("""
"""))}
  }

  def render(userForm:Form[Application.Login]): play.twirl.api.HtmlFormat.Appendable = apply(userForm)

  def f:((Form[Application.Login]) => play.twirl.api.HtmlFormat.Appendable) = (userForm) => apply(userForm)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/login.scala.html
                  HASH: 98bac8d9d1a9cb27168ee1db25f9404e926daac5
                  MATRIX: 740->1|927->36|955->107|983->110|1004->123|1042->124|1072->128|1151->182|1187->210|1225->211|1254->214|1292->226|1307->233|1344->250|1384->260|1414->263|1677->500|1714->528|1754->530|1789->538|1834->556|1851->564|1892->584|1934->596|1968->603|2041->650|2056->656|2103->694|2143->696|2191->716|2318->816|2346->834|2374->840|2688->1124|2729->1138|2824->1203
                  LINES: 26->1|30->1|31->4|32->5|32->5|32->5|34->7|35->8|35->8|35->8|36->9|36->9|36->9|36->9|37->10|38->11|45->18|45->18|45->18|46->19|46->19|46->19|46->19|47->20|48->21|51->24|51->24|51->24|51->24|53->26|54->27|54->27|54->27|62->35|64->37|73->46
                  -- GENERATED --
              */
          