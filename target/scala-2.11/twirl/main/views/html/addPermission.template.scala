
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
import helper._
/**/
object addPermission extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](_display_(/*2.2*/main("Add New Permission")/*2.28*/{_display_(Seq[Any](format.raw/*2.29*/("""

"""),format.raw/*4.1*/("""<script>
	$(document).ready(function() """),format.raw/*5.31*/("""{"""),format.raw/*5.32*/("""
		"""),format.raw/*6.3*/("""$("#addPermissionInput").focus();
	"""),format.raw/*7.2*/("""}"""),format.raw/*7.3*/(""");
</script>
<div id="page-wrapper">

	"""),_display_(/*11.3*/helper/*11.9*/.form(action=routes.RuleController.submitAddPermissions(),'id->"addPermissions",'name->"addPermissions")/*11.113*/{_display_(Seq[Any](format.raw/*11.114*/("""
	"""),format.raw/*12.2*/("""<div id="result"></div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default"
				style="border: 0; margin-bottom: 10px; box-shadow: none;">
				<div class="panel-heading"
					style="font-size: 20px; text-align: center; margin-bottom: 10px;">
					<i><b>Add New Permission Type*</b> </i>
				</div>

				<div class="row">
					<div class="col-lg-12" style="margin: 0 auto">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step 1 &#8594; <b>Add Permission Value</b>
							</div>
							<div class="panel-body">
								<div class="row">

									<div class="col-lg-12">
										<label>Add New Permission Value</label><span
											style="font-size: 10px;">(multiple permissions separated
											by comma)</span> <input required type="text" class="form-control "
											name="newPermissionValue" id="addPermissionInput" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					Step 2 &#8594; <b>Select Operation</b>
				</div>
				<div class="panel-body">
					<div class="row" style="margin-bottom: 10px;">
						<div class="col-lg-4">
							<button type="submit" value="more" name="submitValue1"
								id="submitValue1" class="btn btn-success col-lg-12 btn-block">
								<b>Save and Add More Permissions</b>
							</button>
						</div>
						<div class="col-lg-4">
							<button type="submit" value="continue" name="submitValue1"
								id="submitValue1" class="btn btn-primary col-lg-12 btn-block">
								<b>Save and Go Back</b>
							</button>
						</div>
						<div class="col-lg-4">
							<a onClick="location.href = '"""),_display_(/*62.38*/routes/*62.44*/.Application.adminHome()),format.raw/*62.68*/("""'"
								class="btn btn-info col-lg-12 btn-block"> <b>Go Back</b>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*71.3*/("""
"""),format.raw/*72.1*/("""</div>
""")))}),format.raw/*73.2*/("""
"""))}
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:47 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/addPermission.scala.html
                  HASH: 233dc191ac2252a4507cce376124eab30b520e52
                  MATRIX: 821->20|855->46|893->47|923->51|990->91|1018->92|1048->96|1110->132|1137->133|1207->177|1221->183|1335->287|1375->288|1405->291|3208->2067|3223->2073|3268->2097|3452->2251|3481->2253|3520->2262
                  LINES: 29->2|29->2|29->2|31->4|32->5|32->5|33->6|34->7|34->7|38->11|38->11|38->11|38->11|39->12|89->62|89->62|89->62|98->71|99->72|100->73
                  -- GENERATED --
              */
          