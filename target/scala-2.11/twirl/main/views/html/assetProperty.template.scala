
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object assetProperty extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template6[String,List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(message: String, 
firsts: List[GraphEntityValue], 
seconds: List[GraphEntityValue], 
thirds: List[GraphEntityValue], 
fourths: List[GraphEntityValue],
labels: List[String]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*6.22*/("""

"""),_display_(/*9.2*/main(message)/*9.15*/{_display_(Seq[Any](format.raw/*9.16*/("""
"""),format.raw/*10.1*/("""<div id="page-wrapper">
	"""),_display_(/*11.3*/helper/*11.9*/.form(action=routes.PropertyFileController.addMemberPropertyGroup(),'id->"myForm_property",'name->"myForm_property")/*11.125*/{_display_(Seq[Any](format.raw/*11.126*/("""
	"""),format.raw/*12.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default"
				style="border: 0; margin-bottom: 10px; box-shadow: none;">
				<div class="panel-heading"
					style="font-size: 20px; text-align: center; margin-bottom: 10px;">
					<i><b>Create Asset Property Group</b> </i>
				</div>

				<div class="row">

					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step1 &#8594; <b>Select Value(s)</b>
							</div>
							<div class="panel-body">
								<div class="row ">
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*31.27*/labels/*31.33*/.get(0)),format.raw/*31.40*/("""s </label> <label
											class="checkbox-inline"> <input type="checkbox"
											value=""""),_display_(/*33.20*/labels/*33.26*/.get(0)),format.raw/*33.33*/("""" name="firstOption"
											onchange="changeOption('firstIds','firstOption'); changeDivVisibility('firstChoiceSpan');">
											In Use
										</label> <select class="form-control" id="firstIds"
											disabled="disabled" name="firstIds[]" multiple>
											"""),_display_(/*38.13*/for(first <- firsts) yield /*38.33*/ {_display_(Seq[Any](format.raw/*38.35*/("""
											"""),format.raw/*39.12*/("""<option value=""""),_display_(/*39.28*/first/*39.33*/.getValue()),format.raw/*39.44*/("""">
												"""),_display_(/*40.14*/first/*40.19*/.getValue()),format.raw/*40.30*/("""</option> """)))}),format.raw/*40.41*/("""
										"""),format.raw/*41.11*/("""</select> <span id="firstChoiceSpan"
											style="display: none; font-size: 12px; font-weight: 400 !important;">
											<label style="font-size: 12px; font-weight: 400;">
												Option for Selected """),_display_(/*44.34*/labels/*44.40*/.get(0)),format.raw/*44.47*/(""" """),format.raw/*44.48*/("""</label> <label
											class="radio-in-line"><input type="radio"
												name="firstChoice" id="firstChoice" value="fixed"
												checked> Fixed </label> <label class="radio-in-line"><input
												type="radio" name="firstChoice" id="firstChoice"
												value="omit" checked> Omit </label>
										</span>
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*53.27*/labels/*53.33*/.get(1)),format.raw/*53.40*/("""s
										</label> <label
											class="checkbox-inline"> <input type="checkbox"
											value=""""),_display_(/*56.20*/labels/*56.26*/.get(1)),format.raw/*56.33*/("""" name="secondChoiceOption"
											onchange="changeOption('secondChoiceId','secondChoiceOption'); changeDivVisibility('secondChoiceSpan');">
											In Use
										</label> <select class="form-control" id="secondChoiceId"
											disabled="disabed" name="secondChoiceId[]" multiple>
											"""),_display_(/*61.13*/for(second <- seconds) yield /*61.35*/ {_display_(Seq[Any](format.raw/*61.37*/("""
											"""),format.raw/*62.12*/("""<option value=""""),_display_(/*62.28*/second/*62.34*/.getValue()),format.raw/*62.45*/("""">
												"""),_display_(/*63.14*/second/*63.20*/.getValue()),format.raw/*63.31*/("""</option> """)))}),format.raw/*63.42*/("""
										"""),format.raw/*64.11*/("""</select> <span id="secondChoiceSpan"
											style="display: none; font-size: 12px; font-weight: 400 !important;">
											<label style="font-size: 12px; font-weight: 400;">
												Option for Selected """),_display_(/*67.34*/labels/*67.40*/.get(1)),format.raw/*67.47*/(""" """),format.raw/*67.48*/("""</label> <label class="radio-in-line"><input
												type="radio" name="secondChoice" id="secondChoice"
												value="fixed" checked> Fixed </label> <label
											class="radio-in-line"><input type="radio"
												name="secondChoice" id="secondChoice" value="omit" checked>
												Omit </label>
										</span>
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*76.27*/labels/*76.33*/.get(2)),format.raw/*76.40*/("""s </label> <label class="checkbox-inline">
											<input type="checkbox" value=""""),_display_(/*77.43*/labels/*77.49*/.get(2)),format.raw/*77.56*/("""" name="thirdChoiceOption"
											onchange="changeOption('thirdChoiceId','thirdChoiceOption'); changeDivVisibility('thirdChoiceSpan');">
											In Use
										</label> <select class="form-control" id="thirdChoiceId"
											disabled="disabled" name="thirdChoiceId[]" multiple>
											"""),_display_(/*82.13*/for(third <- thirds) yield /*82.33*/{_display_(Seq[Any](format.raw/*82.34*/("""
											"""),format.raw/*83.12*/("""<option value=""""),_display_(/*83.28*/third/*83.33*/.getValue()),format.raw/*83.44*/("""">"""),_display_(/*83.47*/third/*83.52*/.getValue()),format.raw/*83.63*/("""</option>
											""")))}),format.raw/*84.13*/("""
										"""),format.raw/*85.11*/("""</select> <span id="thirdChoiceSpan"
											style="display: none; font-size: 12px; font-weight: 400 !important;">
											<label style="font-size: 12px; font-weight: 400;">
												Option for Selected """),_display_(/*88.34*/labels/*88.40*/.get(2)),format.raw/*88.47*/(""" """),format.raw/*88.48*/("""</label> <label class="radio-in-line"><input
												type="radio" name="thirdChoice" id="thirdChoice" value="fixed"
												checked> Fixed </label> <label class="radio-in-line"><input
												type="radio" name="thirdChoice" id="thirdChoice" value="omit"
												checked> Omit </label>
										</span>
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*96.27*/labels/*96.33*/.get(3)),format.raw/*96.40*/("""s </label> <label class="checkbox-inline">
											<input type="checkbox" value=""""),_display_(/*97.43*/labels/*97.49*/.get(3)),format.raw/*97.56*/("""" name="fourthChoiceOption"
											onchange="changeOption('fourthChoiceId','fourthChoiceOption'); changeDivVisibility('fourthChoiceSpan');">
											In Use
										</label> <select class="form-control" id="fourthChoiceId"
											disabled="disabled" name="fourthChoiceId[]" multiple>
											"""),_display_(/*102.13*/for(fourth <- fourths) yield /*102.35*/{_display_(Seq[Any](format.raw/*102.36*/("""
											"""),format.raw/*103.12*/("""<option value=""""),_display_(/*103.28*/fourth/*103.34*/.getValue()),format.raw/*103.45*/("""">"""),_display_(/*103.48*/fourth/*103.54*/.getValue()),format.raw/*103.65*/("""</option>
											""")))}),format.raw/*104.13*/("""
										"""),format.raw/*105.11*/("""</select> <span id="fourthChoiceSpan"
											style="display: none; font-size: 12px; font-weight: 400 !important;">
											<label style="font-size: 12px; font-weight: 400;">
												Option for Selected """),_display_(/*108.34*/labels/*108.40*/.get(3)),format.raw/*108.47*/(""" """),format.raw/*108.48*/("""</label> <label class="radio-in-line"><input
												type="radio" name="fourthChoice" id="fourthChoice" value="fixed"
												checked> Fixed </label> <label class="radio-in-line"><input
												type="radio" name="fourthChoice" id="fourthChoice" value="omit"
												checked> Omit </label>
										</span>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step2 &#8594; <b>Set Instance Count</b>
							</div>
							<div class="panel-body">
								<div class="row ">
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Attribute Combination Count </label> <input
											type="number" placeholder="0" min="0" class="form-control"
											required name="attributeCombinationCount">
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Small </label> <input type="number" placeholder="0"
											min="0" class="form-control" required
											name="smallInstanceCount">
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Medium </label> <input type="number" placeholder="0"
											min="0" class="form-control" required
											name="mediumInstanceCount">
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Large </label> <input type="number" placeholder="0"
											min="0" class="form-control" required
											name="largeInstanceCount">
									</div>
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label>
										Prefixes for Member<i><span style="font-size:10px;">(Comma seperated prefixes)</span></i>
										</label>
										<input required class="form-control"
											placeholder="Enter Name PreFixes" type="text"
											name="namePrefixes" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			Step 3 &#8594; <b>Select Operation</b>
		</div>
		<div class="panel-body">
			<div class="row" style="margin-bottom: 10px;">
				<div class="col-lg-4">
					<button type="submit" value="continue" name="submitValue1"
						 class="btn btn-primary col-lg-12 btn-block">
						<b>Save and Go Back</b>
					</button>
				</div>
				<div class="col-lg-4">
					<button type="submit" value="more" name="submitValue1"
						 class="btn btn-success col-lg-12 btn-block">
						<b>Save and Add Another Group</b>
					</button>
				</div>
				<div class="col-lg-4">
					<a class="btn btn-danger col-lg-12 btn-block" onClick="location.href = '"""),_display_(/*183.79*/routes/*183.85*/.Application.newFile()),format.raw/*183.107*/("""'">
						<b>Go Back</b>
					</a>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*190.3*/("""
"""),format.raw/*191.1*/("""</div>

""")))}),format.raw/*193.2*/("""
"""))}
  }

  def render(message:String,firsts:List[GraphEntityValue],seconds:List[GraphEntityValue],thirds:List[GraphEntityValue],fourths:List[GraphEntityValue],labels:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(message,firsts,seconds,thirds,fourths,labels)

  def f:((String,List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (message,firsts,seconds,thirds,fourths,labels) => apply(message,firsts,seconds,thirds,fourths,labels)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:47 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/assetProperty.scala.html
                  HASH: 9c8485e490f6ee36f47194e4bec7032ad81bb2d3
                  MATRIX: 836->1|1117->179|1147->203|1168->216|1206->217|1235->219|1288->246|1302->252|1428->368|1468->369|1498->372|2182->1029|2197->1035|2225->1042|2350->1140|2365->1146|2393->1153|2700->1433|2736->1453|2776->1455|2817->1468|2860->1484|2874->1489|2906->1500|2950->1517|2964->1522|2996->1533|3038->1544|3078->1556|3321->1772|3336->1778|3364->1785|3393->1786|3862->2228|3877->2234|3905->2241|4041->2350|4056->2356|4084->2363|4423->2675|4461->2697|4501->2699|4542->2712|4585->2728|4600->2734|4632->2745|4676->2762|4691->2768|4723->2779|4765->2790|4805->2802|5049->3019|5064->3025|5092->3032|5121->3033|5594->3479|5609->3485|5637->3492|5750->3578|5765->3584|5793->3591|6127->3898|6163->3918|6202->3919|6243->3932|6286->3948|6300->3953|6332->3964|6362->3967|6376->3972|6408->3983|6462->4006|6502->4018|6745->4234|6760->4240|6788->4247|6817->4248|7274->4678|7289->4684|7317->4691|7430->4777|7445->4783|7473->4790|7814->5103|7853->5125|7893->5126|7935->5139|7979->5155|7995->5161|8028->5172|8059->5175|8075->5181|8108->5192|8163->5215|8204->5227|8449->5444|8465->5450|8494->5457|8524->5458|11361->8267|11377->8273|11422->8295|11535->8377|11565->8379|11607->8390
                  LINES: 26->1|34->6|36->9|36->9|36->9|37->10|38->11|38->11|38->11|38->11|39->12|58->31|58->31|58->31|60->33|60->33|60->33|65->38|65->38|65->38|66->39|66->39|66->39|66->39|67->40|67->40|67->40|67->40|68->41|71->44|71->44|71->44|71->44|80->53|80->53|80->53|83->56|83->56|83->56|88->61|88->61|88->61|89->62|89->62|89->62|89->62|90->63|90->63|90->63|90->63|91->64|94->67|94->67|94->67|94->67|103->76|103->76|103->76|104->77|104->77|104->77|109->82|109->82|109->82|110->83|110->83|110->83|110->83|110->83|110->83|110->83|111->84|112->85|115->88|115->88|115->88|115->88|123->96|123->96|123->96|124->97|124->97|124->97|129->102|129->102|129->102|130->103|130->103|130->103|130->103|130->103|130->103|130->103|131->104|132->105|135->108|135->108|135->108|135->108|210->183|210->183|210->183|217->190|218->191|220->193
                  -- GENERATED --
              */
          