
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object newPolicy extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.17*/("""

 """),_display_(/*3.3*/main(title)/*3.14*/{_display_(Seq[Any](format.raw/*3.15*/("""
"""),format.raw/*4.1*/("""<div id="page-wrapper">
	"""),_display_(/*5.3*/helper/*5.9*/.form(action=routes.RuleController.addNewRule(),'id->"myForm",'name->"myForm")/*5.87*/{_display_(Seq[Any](format.raw/*5.88*/("""
	"""),format.raw/*6.2*/("""<div class="row" >
		<div class="col-lg-12">
			<div class="panel panel-default" style="border: 0; box-shadow: none;">
				<div class="panel-heading"
					style="font-size: 20px; text-align: center; margin-bottom: 10px;">
					<i><b>Add New Policy*</b> </i>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<b>Enter Policy Details</b>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label style="margin-right: 5px">Policy Name</label>
										 <input
											required class="form-control" placeholder="Enter Policy Name"
											type="text" name="policyName"   />
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label style="margin-right: 5px">Policy Description</label>
										<textarea required class="form-control"
											placeholder="Enter Policy Description" type="text"
											name="policyDescription" ></textarea>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<button type="submit" value="continue" name="submitValue1"
											id="submitValue1" class="btn btn-success col-lg-12 btn-block">
											<b>Save and Continue</b>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		""")))}),format.raw/*50.4*/("""
	"""),format.raw/*51.2*/("""</div>
	""")))}))}
  }

  def render(title:String): play.twirl.api.HtmlFormat.Appendable = apply(title)

  def f:((String) => play.twirl.api.HtmlFormat.Appendable) = (title) => apply(title)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/newPolicy.scala.html
                  HASH: 825649f7fc1594cc042216e8d7f9c0003e211a03
                  MATRIX: 727->1|830->16|861->22|880->33|918->34|946->36|998->63|1011->69|1097->147|1135->148|1164->151|2774->1731|2804->1734
                  LINES: 26->1|29->1|31->3|31->3|31->3|32->4|33->5|33->5|33->5|33->5|34->6|78->50|79->51
                  -- GENERATED --
              */
          