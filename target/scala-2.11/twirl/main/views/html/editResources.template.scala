
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object editResources extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[List[GraphEntityValue],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(resourceValues:List[GraphEntityValue]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import java.math.BigInteger; var i=1;var k=0;

Seq[Any](format.raw/*1.41*/("""
"""),_display_(/*3.2*/main("Example")/*3.17*/{_display_(Seq[Any](format.raw/*3.18*/("""

"""),format.raw/*5.1*/("""<div id="page-wrapper">

"""),_display_(/*7.2*/if(resourceValues.size()!=0)/*7.30*/{_display_(Seq[Any](format.raw/*7.31*/("""
	"""),format.raw/*8.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">All Resources</div>
				<div class="panel-body" style="overflow: auto; height: 430px;">
					<table class="table table-hover">
						<thead>
							<th>Index</th>
							<th>Resource Type</th>
							<th>Resource Sub Type</th>
							<th>Resource Name</th>
							<th>Operations</th>
						</thead>
						<tbody>
							"""),_display_(/*22.9*/for(resourceValue <- resourceValues) yield /*22.45*/{_display_(Seq[Any](format.raw/*22.46*/("""
							"""),format.raw/*23.8*/("""<tr>
								<td>"""),_display_(/*24.14*/i),format.raw/*24.15*/("""</td>
								<td>"""),_display_(/*25.14*/resourceValue/*25.27*/.getMainType()),format.raw/*25.41*/("""</td>
								<td>"""),_display_(/*26.14*/resourceValue/*26.27*/.getSubType()),format.raw/*26.40*/("""</td>
								<td>"""),_display_(/*27.14*/resourceValue/*27.27*/.getValue()),format.raw/*27.38*/("""</td>
								<td>
									<div class="row">
										<div class="col-lg-6">
											<a onclick="edit('"""),_display_(/*31.31*/resourceValue/*31.44*/.getGraphEntityId()),format.raw/*31.63*/("""')"
												class=" btn btn-info btn-block">Edit</a>
										</div>
										<div class="col-lg-6">
											<a onclick="deleteItem('"""),_display_(/*35.37*/resourceValue/*35.50*/.getGraphEntityId()),format.raw/*35.69*/("""')"
												class=" btn btn-danger btn-block">Delete</a>
										</div>
									</div>
								</td>
							</tr>
							"""),_display_(/*41.9*/{i=i+1}),format.raw/*41.16*/(""" """)))}),format.raw/*41.18*/("""
						"""),format.raw/*42.7*/("""</tbody>

					</table>
				</div>
			</div>
		</div>
	</div>
	<div id="modalDiv">
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">Edit Resource</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-12">
							<label>Edit Resource Type</label>
								<input type="text" class="form-control "
										name="editResourceType" id='editResourceType' />
							</div>
							<div class="col-lg-12">
							<label>Edit Resource Sub Type</label>
								<input type="text" class="form-control "
										name="editResourceSubType" id='editResourceSubType' />
							</div>
							<div class="col-lg-12">
							<label>Edit Resource Value</label>
								<input type="text" class="form-control "
										name="editResourceValue" id='editResourceValue' />
							</div>
						</div>
						
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary">Update Resource</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	""")))}/*90.3*/else/*90.7*/{_display_(Seq[Any](format.raw/*90.8*/("""
	"""),format.raw/*91.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading" style="text-align: center">
					<b>No Resource in DomainModel yet!!</b>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12" style="margin-bottom: 10px;">
							<button type="submit"
								onClick="location.href = '"""),_display_(/*101.36*/routes/*101.42*/.Application.adminHome()),format.raw/*101.66*/("""'"
								class="btn btn-primary col-lg-12 btn-block">
								<b>Go Back</b>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*111.3*/("""
"""),format.raw/*112.1*/("""</div>
<script>
	function edit(id) """),format.raw/*114.20*/("""{"""),format.raw/*114.21*/("""
		
		"""),format.raw/*116.3*/("""if (id != null) """),format.raw/*116.19*/("""{"""),format.raw/*116.20*/("""
		
			"""),format.raw/*118.4*/("""$("#myModal").modal('show');
			//alert(id);
		"""),format.raw/*120.3*/("""}"""),format.raw/*120.4*/("""

		
	"""),format.raw/*123.2*/("""}"""),format.raw/*123.3*/("""
	"""),format.raw/*124.2*/("""function deleteItem(id) """),format.raw/*124.26*/("""{"""),format.raw/*124.27*/("""
		"""),format.raw/*125.3*/("""if (id != null) """),format.raw/*125.19*/("""{"""),format.raw/*125.20*/("""
			"""),format.raw/*126.4*/("""$("#myModal").modal('show');
			
		"""),format.raw/*128.3*/("""}"""),format.raw/*128.4*/("""
	"""),format.raw/*129.2*/("""}"""),format.raw/*129.3*/("""
"""),format.raw/*130.1*/("""</script>
""")))}),format.raw/*131.2*/("""
"""))}
  }

  def render(resourceValues:List[GraphEntityValue]): play.twirl.api.HtmlFormat.Appendable = apply(resourceValues)

  def f:((List[GraphEntityValue]) => play.twirl.api.HtmlFormat.Appendable) = (resourceValues) => apply(resourceValues)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/editResources.scala.html
                  HASH: dce8a80b3b16cfb4823ab0eebddd100f3e532c1e
                  MATRIX: 747->1|919->40|947->91|970->106|1008->107|1038->111|1091->139|1127->167|1165->168|1194->171|1667->618|1719->654|1758->655|1794->664|1840->683|1862->684|1909->704|1931->717|1966->731|2013->751|2035->764|2069->777|2116->797|2138->810|2170->821|2310->934|2332->947|2372->966|2546->1113|2568->1126|2608->1145|2769->1280|2797->1287|2830->1289|2865->1297|4398->2812|4410->2816|4448->2817|4478->2820|4887->3201|4903->3207|4949->3231|5151->3402|5181->3404|5247->3441|5277->3442|5313->3450|5358->3466|5388->3467|5425->3476|5502->3525|5531->3526|5568->3535|5597->3536|5628->3539|5681->3563|5711->3564|5743->3568|5788->3584|5818->3585|5851->3590|5916->3627|5945->3628|5976->3631|6005->3632|6035->3634|6078->3646
                  LINES: 26->1|29->1|30->3|30->3|30->3|32->5|34->7|34->7|34->7|35->8|49->22|49->22|49->22|50->23|51->24|51->24|52->25|52->25|52->25|53->26|53->26|53->26|54->27|54->27|54->27|58->31|58->31|58->31|62->35|62->35|62->35|68->41|68->41|68->41|69->42|117->90|117->90|117->90|118->91|128->101|128->101|128->101|138->111|139->112|141->114|141->114|143->116|143->116|143->116|145->118|147->120|147->120|150->123|150->123|151->124|151->124|151->124|152->125|152->125|152->125|153->126|155->128|155->128|156->129|156->129|157->130|158->131
                  -- GENERATED --
              */
          