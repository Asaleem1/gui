
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object groupProperty extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[String,String,List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(message: String, 
fileType:String,
firsts: List[GraphEntityValue], 
seconds: List[GraphEntityValue], 
thirds: List[GraphEntityValue], 
fourths: List[GraphEntityValue],
labels: List[String]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*7.22*/("""

"""),_display_(/*10.2*/main(message)/*10.15*/{_display_(Seq[Any](format.raw/*10.16*/("""
"""),format.raw/*11.1*/("""<div id="page-wrapper">
	"""),_display_(/*12.3*/helper/*12.9*/.form(action=routes.GroupController.addGroupPropertyGroup(),'id->"myForm_property",'name->"myForm_property")/*12.117*/{_display_(Seq[Any](format.raw/*12.118*/("""
	"""),format.raw/*13.2*/("""<div class="row">
		<input type="hidden" value=""""),_display_(/*14.32*/fileType),format.raw/*14.40*/("""" name="fileType" id="fileType"/>
		<div class="col-lg-12">
			<div class="panel panel-default"
				style="border: 0; margin-bottom: 10px; box-shadow: none;">
				<div class="panel-heading"
					style="font-size: 20px; text-align: center; margin-bottom: 10px;">
					<i><b>Create """),_display_(/*20.20*/fileType),format.raw/*20.28*/("""  """),format.raw/*20.30*/("""Group</b> </i>
				</div>

				<div class="row">

					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step1 &#8594; <b>Select Value(s)</b>
							</div>
							<div class="panel-body">
								<div class="row ">
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*33.27*/labels/*33.33*/.get(0)),format.raw/*33.40*/(""" """),format.raw/*33.41*/("""</label>  <select required class="form-control" id=""""),_display_(/*33.94*/labels/*33.100*/.get(0)),format.raw/*33.107*/(""""
											name=""""),_display_(/*34.19*/labels/*34.25*/.get(0)),format.raw/*34.32*/("""" >
											<option value="" disabled selected>select """),_display_(/*35.55*/labels/*35.61*/.get(0)),format.raw/*35.68*/("""</option>
										
											"""),_display_(/*37.13*/for(first <- firsts) yield /*37.33*/ {_display_(Seq[Any](format.raw/*37.35*/("""
											"""),format.raw/*38.12*/("""<option value=""""),_display_(/*38.28*/first/*38.33*/.getValue()),format.raw/*38.44*/("""">
												"""),_display_(/*39.14*/first/*39.19*/.getValue()),format.raw/*39.30*/("""</option> """)))}),format.raw/*39.41*/("""
										"""),format.raw/*40.11*/("""</select>
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*43.27*/labels/*43.33*/.get(1)),format.raw/*43.40*/("""
										"""),format.raw/*44.11*/("""</label>  <select required class="form-control" id=""""),_display_(/*44.64*/labels/*44.70*/.get(1)),format.raw/*44.77*/(""""
											name=""""),_display_(/*45.19*/labels/*45.25*/.get(1)),format.raw/*45.32*/("""">
											<option value="" disabled selected>select """),_display_(/*46.55*/labels/*46.61*/.get(1)),format.raw/*46.68*/("""</option>
											"""),_display_(/*47.13*/for(second <- seconds) yield /*47.35*/ {_display_(Seq[Any](format.raw/*47.37*/("""
											"""),format.raw/*48.12*/("""<option value=""""),_display_(/*48.28*/second/*48.34*/.getValue()),format.raw/*48.45*/("""">
												"""),_display_(/*49.14*/second/*49.20*/.getValue()),format.raw/*49.31*/("""</option> """)))}),format.raw/*49.42*/("""
										"""),format.raw/*50.11*/("""</select> 
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*53.27*/labels/*53.33*/.get(2)),format.raw/*53.40*/(""" """),format.raw/*53.41*/("""</label><select required class="form-control" id=""""),_display_(/*53.92*/labels/*53.98*/.get(2)),format.raw/*53.105*/(""""
											name=""""),_display_(/*54.19*/labels/*54.25*/.get(2)),format.raw/*54.32*/("""">
												<option value="" disabled selected>select """),_display_(/*55.56*/labels/*55.62*/.get(2)),format.raw/*55.69*/("""</option>
										
											"""),_display_(/*57.13*/for(third <- thirds) yield /*57.33*/{_display_(Seq[Any](format.raw/*57.34*/("""
											"""),format.raw/*58.12*/("""<option value=""""),_display_(/*58.28*/third/*58.33*/.getValue()),format.raw/*58.44*/("""">"""),_display_(/*58.47*/third/*58.52*/.getValue()),format.raw/*58.63*/("""</option>
											""")))}),format.raw/*59.13*/("""
										"""),format.raw/*60.11*/("""</select>
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										"""),_display_(/*63.12*/if(fourths!=null)/*63.29*/{_display_(Seq[Any](format.raw/*63.30*/("""
										"""),format.raw/*64.11*/("""<label> Select name </label><select required class="form-control" id="name"
											name="name">
											<option value="" disabled selected>select Name</option>
											"""),_display_(/*67.13*/for(fourth <- fourths) yield /*67.35*/{_display_(Seq[Any](format.raw/*67.36*/("""
											"""),format.raw/*68.12*/("""<option value=""""),_display_(/*68.28*/fourth/*68.34*/.getValue()),format.raw/*68.45*/("""">"""),_display_(/*68.48*/fourth/*68.54*/.getValue()),format.raw/*68.65*/("""</option>
											""")))}),format.raw/*69.13*/("""
										"""),format.raw/*70.11*/("""</select>
										""")))}/*71.12*/else/*71.16*/{_display_(Seq[Any](format.raw/*71.17*/("""
											"""),format.raw/*72.12*/("""<label> Enter name </label><input required type="text" class="form-control" id="name"
											name="name"/>
										</select>
										""")))}),format.raw/*75.12*/("""
									"""),format.raw/*76.10*/("""</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			Step 3 &#8594; <b>Select Operation</b>
		</div>
		<div class="panel-body">
			<div class="row" style="margin-bottom: 10px;">
				<div class="col-lg-4">
					<button type="submit" value="continue" name="submitValue1"
						 class="btn btn-primary col-lg-12 btn-block">
						<b>Save and Go Back</b>
					</button>
				</div>
				<div class="col-lg-4">
					<button type="submit" value="more" name="submitValue1"
						 class="btn btn-success col-lg-12 btn-block">
						<b>Save and Add Another Group</b>
					</button>
				</div>
				<div class="col-lg-4">
					<a class="btn btn-danger col-lg-12 btn-block" onClick="location.href = '"""),_display_(/*106.79*/routes/*106.85*/.Application.newFile()),format.raw/*106.107*/("""'">
						<b>Go Back</b>
					</a>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*113.3*/("""
"""),format.raw/*114.1*/("""</div>

""")))}),format.raw/*116.2*/("""
"""))}
  }

  def render(message:String,fileType:String,firsts:List[GraphEntityValue],seconds:List[GraphEntityValue],thirds:List[GraphEntityValue],fourths:List[GraphEntityValue],labels:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(message,fileType,firsts,seconds,thirds,fourths,labels)

  def f:((String,String,List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (message,fileType,firsts,seconds,thirds,fourths,labels) => apply(message,fileType,firsts,seconds,thirds,fourths,labels)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/groupProperty.scala.html
                  HASH: 5ebf743b1ae8fc2b84ffa07534d64af3577f6735
                  MATRIX: 843->1|1142->197|1173->221|1195->234|1234->235|1263->237|1316->264|1330->270|1448->378|1488->379|1518->382|1595->432|1624->440|1938->727|1967->735|1997->737|2396->1109|2411->1115|2439->1122|2468->1123|2548->1176|2564->1182|2593->1189|2641->1210|2656->1216|2684->1223|2770->1282|2785->1288|2813->1295|2875->1330|2911->1350|2951->1352|2992->1365|3035->1381|3049->1386|3081->1397|3125->1414|3139->1419|3171->1430|3213->1441|3253->1453|3396->1569|3411->1575|3439->1582|3479->1594|3559->1647|3574->1653|3602->1660|3650->1681|3665->1687|3693->1694|3778->1752|3793->1758|3821->1765|3871->1788|3909->1810|3949->1812|3990->1825|4033->1841|4048->1847|4080->1858|4124->1875|4139->1881|4171->1892|4213->1903|4253->1915|4397->2032|4412->2038|4440->2045|4469->2046|4547->2097|4562->2103|4591->2110|4639->2131|4654->2137|4682->2144|4768->2203|4783->2209|4811->2216|4873->2251|4909->2271|4948->2272|4989->2285|5032->2301|5046->2306|5078->2317|5108->2320|5122->2325|5154->2336|5208->2359|5248->2371|5376->2472|5402->2489|5441->2490|5481->2502|5690->2684|5728->2706|5767->2707|5808->2720|5851->2736|5866->2742|5898->2753|5928->2756|5943->2762|5975->2773|6029->2796|6069->2808|6110->2830|6123->2834|6162->2835|6203->2848|6379->2993|6418->3004|7279->3837|7295->3843|7340->3865|7453->3947|7483->3949|7525->3960
                  LINES: 26->1|35->7|37->10|37->10|37->10|38->11|39->12|39->12|39->12|39->12|40->13|41->14|41->14|47->20|47->20|47->20|60->33|60->33|60->33|60->33|60->33|60->33|60->33|61->34|61->34|61->34|62->35|62->35|62->35|64->37|64->37|64->37|65->38|65->38|65->38|65->38|66->39|66->39|66->39|66->39|67->40|70->43|70->43|70->43|71->44|71->44|71->44|71->44|72->45|72->45|72->45|73->46|73->46|73->46|74->47|74->47|74->47|75->48|75->48|75->48|75->48|76->49|76->49|76->49|76->49|77->50|80->53|80->53|80->53|80->53|80->53|80->53|80->53|81->54|81->54|81->54|82->55|82->55|82->55|84->57|84->57|84->57|85->58|85->58|85->58|85->58|85->58|85->58|85->58|86->59|87->60|90->63|90->63|90->63|91->64|94->67|94->67|94->67|95->68|95->68|95->68|95->68|95->68|95->68|95->68|96->69|97->70|98->71|98->71|98->71|99->72|102->75|103->76|133->106|133->106|133->106|140->113|141->114|143->116
                  -- GENERATED --
              */
          