
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object groups extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,List[PropertyGroups],Integer,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String, groups: List[PropertyGroups],seedValue:Integer,fileType:String):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*1.81*/(""" 

"""),_display_(/*4.2*/main(title)/*4.13*/{_display_(Seq[Any](format.raw/*4.14*/("""

"""),format.raw/*6.1*/("""<div id="page-wrapper">
<script>
function deleteItem() """),format.raw/*8.23*/("""{"""),format.raw/*8.24*/("""
	"""),format.raw/*9.2*/("""var id = $("#groupId").val();
		if (id != null) """),format.raw/*10.19*/("""{"""),format.raw/*10.20*/("""
			"""),format.raw/*11.4*/("""var option = confirm("Are you sure to delete This Action Type?");

			if (option) """),format.raw/*13.16*/("""{"""),format.raw/*13.17*/("""
				"""),format.raw/*14.5*/("""$.ajax("""),format.raw/*14.12*/("""{"""),format.raw/*14.13*/("""
					"""),format.raw/*15.6*/("""type : "POST",
					url : "/deleteGroup",
					data : """),format.raw/*17.13*/("""{"""),format.raw/*17.14*/("""
						"""),format.raw/*18.7*/(""""groupId" : id,
					"""),format.raw/*19.6*/("""}"""),format.raw/*19.7*/(""",
					success : function(data) """),format.raw/*20.31*/("""{"""),format.raw/*20.32*/("""
						"""),format.raw/*21.7*/("""$('#groupId option[value="'+id+'"]').remove();
						
					"""),format.raw/*23.6*/("""}"""),format.raw/*23.7*/("""
				"""),format.raw/*24.5*/("""}"""),format.raw/*24.6*/(""");
			"""),format.raw/*25.4*/("""}"""),format.raw/*25.5*/("""

		"""),format.raw/*27.3*/("""}"""),format.raw/*27.4*/("""
	"""),format.raw/*28.2*/("""}"""),format.raw/*28.3*/("""
"""),format.raw/*29.1*/("""</script>

	"""),_display_(/*31.3*/helper/*31.9*/.form(action=routes.PropertyFileController.PropertyEdit(),'id->"printGroup",'name->"printGroup")/*31.105*/{_display_(Seq[Any](format.raw/*31.106*/("""
	"""),format.raw/*32.2*/("""<input type="hidden" value=""""),_display_(/*32.31*/fileType),format.raw/*32.39*/("""" name="fileType"/>
			
	"""),_display_(/*34.3*/if(groups.size()!=0)/*34.23*/{_display_(Seq[Any](format.raw/*34.24*/("""
	
	"""),format.raw/*36.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default" style="border: 0; box-shadow: none;">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step 1 &#8594; <b>Select Property Group</b>
							</div>
							<div class="panel-body">
								<div >

									<div class="row">
										<div class="col-lg-12" style="margin-bottom: 10px;">
											<label> Select Group </label> <select class="form-control"
												name="groupId" id="groupId" style="width: 100%;">
												"""),_display_(/*52.14*/for(group <- groups) yield /*52.34*/ {_display_(Seq[Any](format.raw/*52.36*/("""
												"""),format.raw/*53.13*/("""<option style="width: 100%;" value=""""),_display_(/*53.50*/group/*53.55*/.getPropertyGroupId()),format.raw/*53.76*/("""">
													"""),_display_(/*54.15*/group/*54.20*/.getPropertyGroupId()),format.raw/*54.41*/("""</option> """)))}),format.raw/*54.52*/("""
											"""),format.raw/*55.12*/("""</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						Step 2 &#8594; <b>Select Operation</b>
					</div>
					<div class="panel-body">
						<div class="row" style="margin-bottom: 10px;">
							<div class="col-lg-3">
								<button type="submit" value="edit" name="submitValue1"
									id="submitValue1" class="btn btn-primary col-lg-12 btn-block">
									Edit Group</b>
								</button>
							</div>
							<div class="col-lg-3">
								<a  onclick="deleteItem()" class="btn btn-danger col-lg-12 btn-block">
									Delete Group</b>
								</a>
							</div>
							<div class="col-lg-3">
								<button type="submit" value="newGroup" name="submitValue1"
									id="submitValue1" class="btn btn-success col-lg-12 btn-block">
									Add New Group</b>
								</button>
							</div>
							<div class="col-lg-3">
								<button type="submit" value="goBack" name="submitValue1"
									id="submitValue1" class="btn btn-info col-lg-12 btn-block">
									Go Back</b>
								</button>
							</div>
						</div>
					</div>
				</div>
				""")))}/*95.6*/else/*95.10*/{_display_(Seq[Any](format.raw/*95.11*/("""
				"""),format.raw/*96.5*/("""<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading" style="text-align:center">
								<b>No Group Found!!</b>
							</div>
							<div class="panel-body">
								<div class="row">
								
									<div class="col-lg-6" style="margin-bottom: 10px;">
										<a  onClick="location.href = '"""),_display_(/*106.42*/routes/*106.48*/.Application.home()),format.raw/*106.67*/("""'" class="btn btn-primary col-lg-12 btn-block">
										<b>Go Back</b>
										</a>
									</div>
									<div class="col-lg-6" style="margin-bottom: 10px;">
										<button type="submit" value="newGroup" name="submitValue1" class="btn btn-primary col-lg-12 btn-block">
										<b>Add New Group</b>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				""")))}),format.raw/*120.6*/("""
			"""),format.raw/*121.4*/("""</div>
			""")))}),format.raw/*122.5*/("""
			""")))}))}
  }

  def render(title:String,groups:List[PropertyGroups],seedValue:Integer,fileType:String): play.twirl.api.HtmlFormat.Appendable = apply(title,groups,seedValue,fileType)

  def f:((String,List[PropertyGroups],Integer,String) => play.twirl.api.HtmlFormat.Appendable) = (title,groups,seedValue,fileType) => apply(title,groups,seedValue,fileType)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/groups.scala.html
                  HASH: f622d125c0de5aaf7699dc6f33ff66b15ee1e916
                  MATRIX: 760->1|942->80|973->105|992->116|1030->117|1060->121|1144->178|1172->179|1201->182|1278->231|1307->232|1339->237|1451->321|1480->322|1513->328|1548->335|1577->336|1611->343|1695->399|1724->400|1759->408|1808->430|1836->431|1897->464|1926->465|1961->473|2049->534|2077->535|2110->541|2138->542|2172->549|2200->550|2233->556|2261->557|2291->560|2319->561|2348->563|2389->578|2403->584|2509->680|2549->681|2579->684|2635->713|2664->721|2718->749|2747->769|2786->770|2819->776|3455->1385|3491->1405|3531->1407|3573->1421|3637->1458|3651->1463|3693->1484|3738->1502|3752->1507|3794->1528|3836->1539|3877->1552|5123->2780|5136->2784|5175->2785|5208->2791|5609->3164|5625->3170|5666->3189|6127->3619|6160->3624|6203->3636
                  LINES: 26->1|29->1|31->4|31->4|31->4|33->6|35->8|35->8|36->9|37->10|37->10|38->11|40->13|40->13|41->14|41->14|41->14|42->15|44->17|44->17|45->18|46->19|46->19|47->20|47->20|48->21|50->23|50->23|51->24|51->24|52->25|52->25|54->27|54->27|55->28|55->28|56->29|58->31|58->31|58->31|58->31|59->32|59->32|59->32|61->34|61->34|61->34|63->36|79->52|79->52|79->52|80->53|80->53|80->53|80->53|81->54|81->54|81->54|81->54|82->55|122->95|122->95|122->95|123->96|133->106|133->106|133->106|147->120|148->121|149->122
                  -- GENERATED --
              */
          