
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object propertyFiles extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[String,List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/( title: String, types:List[String] ):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*1.39*/(""" 
"""),_display_(/*3.2*/main(title)/*3.13*/{_display_(Seq[Any](format.raw/*3.14*/("""

"""),format.raw/*5.1*/("""<script>
	function updateList(propertyType, fileNameId) """),format.raw/*6.48*/("""{"""),format.raw/*6.49*/("""
		"""),format.raw/*7.3*/("""var propertyType = $("#" + propertyType + " option:selected").val();
		var fileType = $("#propertyType").val();
		$.ajax("""),format.raw/*9.10*/("""{"""),format.raw/*9.11*/("""
					"""),format.raw/*10.6*/("""type : "POST",
					url : "/updatePropertyFiles",
					data : """),format.raw/*12.13*/("""{"""),format.raw/*12.14*/("""
						"""),format.raw/*13.7*/(""""propertyType" : propertyType,
						"propertyType" : propertyType
					"""),format.raw/*15.6*/("""}"""),format.raw/*15.7*/(""",
					success : function(data) """),format.raw/*16.31*/("""{"""),format.raw/*16.32*/("""
						"""),format.raw/*17.7*/("""lst = JSON.parse(data);
						$('#' + fileNameId).empty(); //remove all child nodes
						var defaultOption = $('<option selected="selected" disabled="disabled" value="">Select Property File</option>');
						$('#' + fileNameId).append(defaultOption);
						for (var i = 0; i < lst.length; i++) """),format.raw/*21.44*/("""{"""),format.raw/*21.45*/("""
							"""),format.raw/*22.8*/("""var newOption = $('<option value="'+lst[i]+'">'
									+ lst[i] + '</option>');
							$('#' + fileNameId).append(newOption);
						"""),format.raw/*25.7*/("""}"""),format.raw/*25.8*/("""
						"""),format.raw/*26.7*/("""$('#' + fileNameId).trigger("chosen:updated");
					"""),format.raw/*27.6*/("""}"""),format.raw/*27.7*/("""
				"""),format.raw/*28.5*/("""}"""),format.raw/*28.6*/(""");

	"""),format.raw/*30.2*/("""}"""),format.raw/*30.3*/("""
	"""),format.raw/*31.2*/("""function deleteFile() """),format.raw/*31.24*/("""{"""),format.raw/*31.25*/("""
		"""),format.raw/*32.3*/("""var fileName = $("#propertyFileName").val();
		var fileType = $("#propertyType").val();
		if (fileName != null) """),format.raw/*34.25*/("""{"""),format.raw/*34.26*/("""
			"""),format.raw/*35.4*/("""var option = confirm("Are you sure to delete This File?");

			if (option) """),format.raw/*37.16*/("""{"""),format.raw/*37.17*/("""
				"""),format.raw/*38.5*/("""$.ajax("""),format.raw/*38.12*/("""{"""),format.raw/*38.13*/("""
					"""),format.raw/*39.6*/("""type : "POST",
					url : "/deleteFile",
					data : """),format.raw/*41.13*/("""{"""),format.raw/*41.14*/("""
						"""),format.raw/*42.7*/(""""fileName" : fileName,
						"fileType" : fileType
					"""),format.raw/*44.6*/("""}"""),format.raw/*44.7*/(""",
					success : function(data) """),format.raw/*45.31*/("""{"""),format.raw/*45.32*/("""
						"""),format.raw/*46.7*/("""$('#propertyFileName option[value="' + fileName + '"]')
								.remove();

					"""),format.raw/*49.6*/("""}"""),format.raw/*49.7*/("""
				"""),format.raw/*50.5*/("""}"""),format.raw/*50.6*/(""");
			"""),format.raw/*51.4*/("""}"""),format.raw/*51.5*/("""

		"""),format.raw/*53.3*/("""}"""),format.raw/*53.4*/("""
	"""),format.raw/*54.2*/("""}"""),format.raw/*54.3*/("""
"""),format.raw/*55.1*/("""</script>
<div id="page-wrapper">
	"""),_display_(/*57.3*/if(types.size()!=0)/*57.22*/{_display_(Seq[Any](format.raw/*57.23*/("""
	"""),_display_(/*58.3*/helper/*58.9*/.form(action=routes.Application.submitPropertyFile(),'id->"printPropertyFile",'name->"printPropertyFile")/*58.114*/{_display_(Seq[Any](format.raw/*58.115*/("""

	"""),format.raw/*60.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default" style="border: 0; box-shadow: none;">

				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step 1 &#8594; <b>Select Property File</b>
							</div>
							<div class="panel-body">
								<div id="organization_div">
									<div class="row">
										<div class="col-lg-6" style="margin-bottom: 10px;">
											<label> Select Property File Type </label> <select
												class="form-control" name="propertyType" id="propertyType"
												style="width: 100%;"
												onchange="updateList('propertyType','propertyFileName')">
												<option selected="selected" disabled="disabled" value="">Select
													Property Type</option> """),_display_(/*79.38*/for(typeValue <- types) yield /*79.61*/ {_display_(Seq[Any](format.raw/*79.63*/("""
												"""),format.raw/*80.13*/("""<option style="width: 100%;" value=""""),_display_(/*80.50*/typeValue),format.raw/*80.59*/("""">
													"""),_display_(/*81.15*/typeValue),format.raw/*81.24*/("""</option> """)))}),format.raw/*81.35*/("""
											"""),format.raw/*82.12*/("""</select>
										</div>
										<div class="col-lg-6" style="margin-bottom: 10px;">

											<label> Select Property File </label> <select
												class="form-control" required name="propertyFileName"
												id="propertyFileName" style="width: 100%;">
												<option selected="selected" disabled="disabled" value="">Select
													Property File</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						Step 2 &#8594; <b>Select Operation</b>
					</div>
					<div class="panel-body">
						<div class="row" style="margin-bottom: 10px;">
							<div class="col-lg-4">
								<button type="submit" value="print" name="submitValue1"
									id="submitValue1" class="btn btn-success col-lg-12 btn-block">
									Print File</b>
								</button>
							</div>
							<div class="col-lg-4">
								<a onclick="deleteFile()"
									class="btn btn-danger col-lg-12 btn-block"> Delete File</b>
								</a>
							</div>
							<div class="col-lg-4">
								<a onClick="location.href = '"""),_display_(/*117.39*/routes/*117.45*/.Application.home()),format.raw/*117.64*/("""'"
									class="btn btn-primary col-lg-12 btn-block"> <b>Go Back</b>
								</a>
							</div>
						</div>
					</div>
				</div>
				""")))}),format.raw/*124.6*/(""" """)))}/*124.8*/else/*124.12*/{_display_(Seq[Any](format.raw/*124.13*/("""
				"""),format.raw/*125.5*/("""<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading" style="text-align: center">
								<b>No Property File is in System!!</b>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<button type="submit"
											onClick="location.href = '"""),_display_(/*135.39*/routes/*135.45*/.Application.home()),format.raw/*135.64*/("""'"
											class="btn btn-primary col-lg-12 btn-block">
											<b>Go Back</b>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				""")))}),format.raw/*145.6*/("""
			"""),format.raw/*146.4*/("""</div>
			""")))}))}
  }

  def render(title:String,types:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(title,types)

  def f:((String,List[String]) => play.twirl.api.HtmlFormat.Appendable) = (title,types) => apply(title,types)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:49 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/propertyFiles.scala.html
                  HASH: 6195257742cefa97116171c50ed04fb1a57c67b7
                  MATRIX: 744->1|884->38|913->61|932->72|970->73|1000->77|1084->134|1112->135|1142->139|1292->262|1320->263|1354->270|1446->334|1475->335|1510->343|1611->417|1639->418|1700->451|1729->452|1764->460|2091->759|2120->760|2156->769|2320->906|2348->907|2383->915|2463->968|2491->969|2524->975|2552->976|2586->983|2614->984|2644->987|2694->1009|2723->1010|2754->1014|2896->1128|2925->1129|2957->1134|3062->1211|3091->1212|3124->1218|3159->1225|3188->1226|3222->1233|3305->1288|3334->1289|3369->1297|3454->1355|3482->1356|3543->1389|3572->1390|3607->1398|3718->1482|3746->1483|3779->1489|3807->1490|3841->1497|3869->1498|3902->1504|3930->1505|3960->1508|3988->1509|4017->1511|4081->1549|4109->1568|4148->1569|4178->1573|4192->1579|4307->1684|4347->1685|4379->1690|5241->2525|5280->2548|5320->2550|5362->2564|5426->2601|5456->2610|5501->2628|5531->2637|5573->2648|5614->2661|6838->3857|6854->3863|6895->3882|7073->4029|7094->4031|7108->4035|7148->4036|7182->4042|7620->4452|7636->4458|7677->4477|7909->4678|7942->4683
                  LINES: 26->1|29->1|30->3|30->3|30->3|32->5|33->6|33->6|34->7|36->9|36->9|37->10|39->12|39->12|40->13|42->15|42->15|43->16|43->16|44->17|48->21|48->21|49->22|52->25|52->25|53->26|54->27|54->27|55->28|55->28|57->30|57->30|58->31|58->31|58->31|59->32|61->34|61->34|62->35|64->37|64->37|65->38|65->38|65->38|66->39|68->41|68->41|69->42|71->44|71->44|72->45|72->45|73->46|76->49|76->49|77->50|77->50|78->51|78->51|80->53|80->53|81->54|81->54|82->55|84->57|84->57|84->57|85->58|85->58|85->58|85->58|87->60|106->79|106->79|106->79|107->80|107->80|107->80|108->81|108->81|108->81|109->82|144->117|144->117|144->117|151->124|151->124|151->124|151->124|152->125|162->135|162->135|162->135|172->145|173->146
                  -- GENERATED --
              */
          