
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object groupPropertyEdit extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template9[String,String,List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],Map[String, String],List[String],Long,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(message: String, 
fileType:String,
firsts: List[GraphEntityValue], 
seconds: List[GraphEntityValue], 
thirds: List[GraphEntityValue], 
fourths: List[GraphEntityValue],
answers:Map[String,String],
labels: List[String],
groupId:Long):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*9.14*/("""

"""),_display_(/*12.2*/main(message)/*12.15*/{_display_(Seq[Any](format.raw/*12.16*/("""
"""),format.raw/*13.1*/("""<div id="page-wrapper">
	"""),_display_(/*14.3*/helper/*14.9*/.form(action=routes.GroupController.addGroupPropertyGroup(),'id->"myForm_property",'name->"myForm_property")/*14.117*/{_display_(Seq[Any](format.raw/*14.118*/("""
	"""),format.raw/*15.2*/("""<div class="row">
		<input type="hidden" value=""""),_display_(/*16.32*/groupId),format.raw/*16.39*/("""" name="groupId" id="groupId"/>
		<input type="hidden" value=""""),_display_(/*17.32*/fileType),format.raw/*17.40*/("""" name="fileType" id="fileType"/>
		<div class="col-lg-12">
			<div class="panel panel-default"
				style="border: 0; margin-bottom: 10px; box-shadow: none;">
				<div class="panel-heading"
					style="font-size: 20px; text-align: center; margin-bottom: 10px;">
					<i><b>Create """),_display_(/*23.20*/fileType),format.raw/*23.28*/("""  """),format.raw/*23.30*/("""Group</b> </i>
				</div>

				<div class="row">

					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step1 &#8594; <b>Select Value(s)</b>
							</div>
							<div class="panel-body">
								<div class="row ">
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*36.27*/labels/*36.33*/.get(0)),format.raw/*36.40*/(""" """),format.raw/*36.41*/("""</label>  <select required class="form-control" id=""""),_display_(/*36.94*/labels/*36.100*/.get(0)),format.raw/*36.107*/(""""
											name=""""),_display_(/*37.19*/labels/*37.25*/.get(0)),format.raw/*37.32*/("""" >
											<option value="" disabled >select """),_display_(/*38.47*/labels/*38.53*/.get(0)),format.raw/*38.60*/("""</option>
										
											"""),_display_(/*40.13*/for(first <- firsts) yield /*40.33*/ {_display_(Seq[Any](format.raw/*40.35*/("""
											"""),format.raw/*41.12*/("""<option value=""""),_display_(/*41.28*/first/*41.33*/.getValue()),format.raw/*41.44*/("""" """),_display_(/*41.47*/if(answers.get(labels.get(0)).equals(first.getValue()))/*41.102*/{_display_(Seq[Any](format.raw/*41.103*/("""selected""")))}),format.raw/*41.112*/(""">
												"""),_display_(/*42.14*/first/*42.19*/.getValue()),format.raw/*42.30*/("""</option> """)))}),format.raw/*42.41*/("""
										"""),format.raw/*43.11*/("""</select>
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*46.27*/labels/*46.33*/.get(1)),format.raw/*46.40*/("""
										"""),format.raw/*47.11*/("""</label>  <select required class="form-control" id=""""),_display_(/*47.64*/labels/*47.70*/.get(1)),format.raw/*47.77*/(""""
											name=""""),_display_(/*48.19*/labels/*48.25*/.get(1)),format.raw/*48.32*/("""">
											<option value="" disabled >select """),_display_(/*49.47*/labels/*49.53*/.get(1)),format.raw/*49.60*/("""</option>
											"""),_display_(/*50.13*/for(second <- seconds) yield /*50.35*/ {_display_(Seq[Any](format.raw/*50.37*/("""
											"""),format.raw/*51.12*/("""<option value=""""),_display_(/*51.28*/second/*51.34*/.getValue()),format.raw/*51.45*/("""" """),_display_(/*51.48*/if(answers.get(labels.get(1)).equals(second.getValue()))/*51.104*/{_display_(Seq[Any](format.raw/*51.105*/("""selected""")))}),format.raw/*51.114*/(""">
												"""),_display_(/*52.14*/second/*52.20*/.getValue()),format.raw/*52.31*/("""</option> """)))}),format.raw/*52.42*/("""
										"""),format.raw/*53.11*/("""</select> 
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*56.27*/labels/*56.33*/.get(2)),format.raw/*56.40*/(""" """),format.raw/*56.41*/("""</label><select required class="form-control" id=""""),_display_(/*56.92*/labels/*56.98*/.get(2)),format.raw/*56.105*/(""""
											name=""""),_display_(/*57.19*/labels/*57.25*/.get(2)),format.raw/*57.32*/("""">
												<option value="" disabled >select """),_display_(/*58.48*/labels/*58.54*/.get(2)),format.raw/*58.61*/("""</option>
										
											"""),_display_(/*60.13*/for(third <- thirds) yield /*60.33*/{_display_(Seq[Any](format.raw/*60.34*/("""
											"""),format.raw/*61.12*/("""<option value=""""),_display_(/*61.28*/third/*61.33*/.getValue()),format.raw/*61.44*/("""" """),_display_(/*61.47*/if(answers.get(labels.get(2)).equals(third.getValue()))/*61.102*/{_display_(Seq[Any](format.raw/*61.103*/("""selected""")))}),format.raw/*61.112*/(""">"""),_display_(/*61.114*/third/*61.119*/.getValue()),format.raw/*61.130*/("""</option>
											""")))}),format.raw/*62.13*/("""
										"""),format.raw/*63.11*/("""</select>
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										"""),_display_(/*66.12*/if(fourths!=null)/*66.29*/{_display_(Seq[Any](format.raw/*66.30*/("""
										"""),format.raw/*67.11*/("""<label> Select name </label><select required class="form-control" id="name"
											name="name">
											<option value="" disabled >select Name</option>
											"""),_display_(/*70.13*/for(fourth <- fourths) yield /*70.35*/{_display_(Seq[Any](format.raw/*70.36*/("""
											"""),format.raw/*71.12*/("""<option value=""""),_display_(/*71.28*/fourth/*71.34*/.getValue()),format.raw/*71.45*/("""" """),_display_(/*71.48*/if(answers.get("name").equals(fourth.getValue()))/*71.97*/{_display_(Seq[Any](format.raw/*71.98*/("""selected""")))}),format.raw/*71.107*/(""">"""),_display_(/*71.109*/fourth/*71.115*/.getValue()),format.raw/*71.126*/("""</option>
											""")))}),format.raw/*72.13*/("""
										"""),format.raw/*73.11*/("""</select>
										""")))}/*74.12*/else/*74.16*/{_display_(Seq[Any](format.raw/*74.17*/("""
											"""),format.raw/*75.12*/("""<label> Enter name </label><input required type="text" class="form-control" id="name"
											name="name" value=""""),_display_(/*76.32*/answers/*76.39*/.get("name")),format.raw/*76.51*/(""""/>
										</select>
										""")))}),format.raw/*78.12*/("""
									"""),format.raw/*79.10*/("""</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			Step 3 &#8594; <b>Select Operation</b>
		</div>
		<div class="panel-body">
			<div class="row" style="margin-bottom: 10px;">
				<div class="col-lg-4">
					<button type="submit" value="continue" name="submitValue1"
						 class="btn btn-primary col-lg-12 btn-block">
						<b>Update and Go Back</b>
					</button>
				</div>
				<div class="col-lg-4">
					<button type="submit" value="more" name="submitValue1"
						 class="btn btn-success col-lg-12 btn-block">
						<b>Update and Add Another Group</b>
					</button>
				</div>
				<div class="col-lg-4">
					<a class="btn btn-danger col-lg-12 btn-block" onClick="location.href = '"""),_display_(/*109.79*/routes/*109.85*/.Application.newFile()),format.raw/*109.107*/("""'">
						<b>Go Back</b>
					</a>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*116.3*/("""
"""),format.raw/*117.1*/("""</div>

""")))}),format.raw/*119.2*/("""
"""))}
  }

  def render(message:String,fileType:String,firsts:List[GraphEntityValue],seconds:List[GraphEntityValue],thirds:List[GraphEntityValue],fourths:List[GraphEntityValue],answers:Map[String, String],labels:List[String],groupId:Long): play.twirl.api.HtmlFormat.Appendable = apply(message,fileType,firsts,seconds,thirds,fourths,answers,labels,groupId)

  def f:((String,String,List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],Map[String, String],List[String],Long) => play.twirl.api.HtmlFormat.Appendable) = (message,fileType,firsts,seconds,thirds,fourths,answers,labels,groupId) => apply(message,fileType,firsts,seconds,thirds,fourths,answers,labels,groupId)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/groupPropertyEdit.scala.html
                  HASH: df024b18b46b4b279d79de3e328d6a4a896863b4
                  MATRIX: 872->1|1215->241|1246->265|1268->278|1307->279|1336->281|1389->308|1403->314|1521->422|1561->423|1591->426|1668->476|1696->483|1787->547|1816->555|2130->842|2159->850|2189->852|2588->1224|2603->1230|2631->1237|2660->1238|2740->1291|2756->1297|2785->1304|2833->1325|2848->1331|2876->1338|2954->1389|2969->1395|2997->1402|3059->1437|3095->1457|3135->1459|3176->1472|3219->1488|3233->1493|3265->1504|3295->1507|3360->1562|3400->1563|3441->1572|3484->1588|3498->1593|3530->1604|3572->1615|3612->1627|3755->1743|3770->1749|3798->1756|3838->1768|3918->1821|3933->1827|3961->1834|4009->1855|4024->1861|4052->1868|4129->1918|4144->1924|4172->1931|4222->1954|4260->1976|4300->1978|4341->1991|4384->2007|4399->2013|4431->2024|4461->2027|4527->2083|4567->2084|4608->2093|4651->2109|4666->2115|4698->2126|4740->2137|4780->2149|4924->2266|4939->2272|4967->2279|4996->2280|5074->2331|5089->2337|5118->2344|5166->2365|5181->2371|5209->2378|5287->2429|5302->2435|5330->2442|5392->2477|5428->2497|5467->2498|5508->2511|5551->2527|5565->2532|5597->2543|5627->2546|5692->2601|5732->2602|5773->2611|5803->2613|5818->2618|5851->2629|5905->2652|5945->2664|6073->2765|6099->2782|6138->2783|6178->2795|6379->2969|6417->2991|6456->2992|6497->3005|6540->3021|6555->3027|6587->3038|6617->3041|6675->3090|6714->3091|6755->3100|6785->3102|6801->3108|6834->3119|6888->3142|6928->3154|6969->3176|6982->3180|7021->3181|7062->3194|7207->3312|7223->3319|7256->3331|7324->3368|7363->3379|8228->4216|8244->4222|8289->4244|8402->4326|8432->4328|8474->4339
                  LINES: 26->1|37->9|39->12|39->12|39->12|40->13|41->14|41->14|41->14|41->14|42->15|43->16|43->16|44->17|44->17|50->23|50->23|50->23|63->36|63->36|63->36|63->36|63->36|63->36|63->36|64->37|64->37|64->37|65->38|65->38|65->38|67->40|67->40|67->40|68->41|68->41|68->41|68->41|68->41|68->41|68->41|68->41|69->42|69->42|69->42|69->42|70->43|73->46|73->46|73->46|74->47|74->47|74->47|74->47|75->48|75->48|75->48|76->49|76->49|76->49|77->50|77->50|77->50|78->51|78->51|78->51|78->51|78->51|78->51|78->51|78->51|79->52|79->52|79->52|79->52|80->53|83->56|83->56|83->56|83->56|83->56|83->56|83->56|84->57|84->57|84->57|85->58|85->58|85->58|87->60|87->60|87->60|88->61|88->61|88->61|88->61|88->61|88->61|88->61|88->61|88->61|88->61|88->61|89->62|90->63|93->66|93->66|93->66|94->67|97->70|97->70|97->70|98->71|98->71|98->71|98->71|98->71|98->71|98->71|98->71|98->71|98->71|98->71|99->72|100->73|101->74|101->74|101->74|102->75|103->76|103->76|103->76|105->78|106->79|136->109|136->109|136->109|143->116|144->117|146->119
                  -- GENERATED --
              */
          