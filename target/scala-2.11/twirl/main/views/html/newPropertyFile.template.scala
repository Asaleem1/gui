
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object newPropertyFile extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[String,List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String,types:List[String]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.36*/(""" 

"""),_display_(/*3.2*/main(title)/*3.13*/{_display_(Seq[Any](format.raw/*3.14*/("""
"""),format.raw/*4.1*/("""<div id="page-wrapper">
	
	<script>
	
	
		function updateSeed(option,inputField)"""),format.raw/*9.41*/("""{"""),format.raw/*9.42*/("""
			"""),format.raw/*10.4*/("""var val = $('#'+option).val();
			if(val=="Action"|| val.endsWith('Group'))"""),format.raw/*11.45*/("""{"""),format.raw/*11.46*/("""
				"""),format.raw/*12.5*/("""$('#'+inputField).html("");
			"""),format.raw/*13.4*/("""}"""),format.raw/*13.5*/("""else"""),format.raw/*13.9*/("""{"""),format.raw/*13.10*/("""
				"""),format.raw/*14.5*/("""var html_content="<label style='margin-right: 5px'>Seed</label>";
				html_content+=" <input required class='form-control' placeholder='0' type='number' name='seed' />";
				$('#'+inputField).html(html_content);
						
			"""),format.raw/*18.4*/("""}"""),format.raw/*18.5*/("""
		"""),format.raw/*19.3*/("""}"""),format.raw/*19.4*/("""
		"""),format.raw/*20.3*/("""String.prototype.endsWith = function(pattern) """),format.raw/*20.49*/("""{"""),format.raw/*20.50*/("""
		    """),format.raw/*21.7*/("""var d = this.length - pattern.length;
		    return d >= 0 && this.lastIndexOf(pattern) === d;
		"""),format.raw/*23.3*/("""}"""),format.raw/*23.4*/(""";
	</script>
	"""),_display_(/*25.3*/helper/*25.9*/.form(action=routes.Application.submitMemberFile(),'id->"myForm",'name->"myForm")/*25.90*/{_display_(Seq[Any](format.raw/*25.91*/("""
	"""),format.raw/*26.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default" style="border: 0; box-shadow: none;">
				<div class="panel-heading"
					style="font-size: 20px; text-align: center; margin-bottom: 10px;">
					<i><b>Add New Property File*</b> </i>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<b>Enter Member Property File Detail</b>
							</div>
							<div class="panel-body">
								<div class="row">
									
									<div class="col-lg-6" style="margin-bottom: 10px;">
										<label style="margin-right: 5px">File Type</label> <select
											class="form-control" required id="type" name="type" onchange="updateSeed('type','seedDiv')">
											<option value="" disabled="disabled" selected>Select File Type</option>
											"""),_display_(/*46.13*/for(typeName <- types) yield /*46.35*/ {_display_(Seq[Any](format.raw/*46.37*/("""
											"""),format.raw/*47.12*/("""<option value=""""),_display_(/*47.28*/typeName),format.raw/*47.36*/("""">"""),_display_(/*47.39*/typeName),format.raw/*47.47*/("""</option> """)))}),format.raw/*47.58*/("""
										"""),format.raw/*48.11*/("""</select>
									</div>
									<div class="col-lg-6" style="margin-bottom: 10px;" id="seedDiv">
										
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<button type="submit" value="continue" name="submitValue1"
											id="submitValue1" class="btn btn-success col-lg-12 btn-block">
											<b>Save and Continue</b>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		""")))}),format.raw/*68.4*/("""
	"""),format.raw/*69.2*/("""</div>
	""")))}),format.raw/*70.3*/("""
"""))}
  }

  def render(title:String,types:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(title,types)

  def f:((String,List[String]) => play.twirl.api.HtmlFormat.Appendable) = (title,types) => apply(title,types)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/newPropertyFile.scala.html
                  HASH: 3755d6ec9ea862b004f359b4acc1fb1be8e81ebb
                  MATRIX: 746->1|868->35|899->41|918->52|956->53|984->55|1096->140|1124->141|1156->146|1260->222|1289->223|1322->229|1381->261|1409->262|1440->266|1469->267|1502->273|1755->499|1783->500|1814->504|1842->505|1873->509|1947->555|1976->556|2011->564|2136->662|2164->663|2207->680|2221->686|2311->767|2350->768|2380->771|3288->1652|3326->1674|3366->1676|3407->1689|3450->1705|3479->1713|3509->1716|3538->1724|3580->1735|3620->1747|4202->2299|4232->2302|4272->2312
                  LINES: 26->1|29->1|31->3|31->3|31->3|32->4|37->9|37->9|38->10|39->11|39->11|40->12|41->13|41->13|41->13|41->13|42->14|46->18|46->18|47->19|47->19|48->20|48->20|48->20|49->21|51->23|51->23|53->25|53->25|53->25|53->25|54->26|74->46|74->46|74->46|75->47|75->47|75->47|75->47|75->47|75->47|76->48|96->68|97->69|98->70
                  -- GENERATED --
              */
          