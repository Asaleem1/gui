
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
import helper._
/**/
object graphIntialize extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](_display_(/*2.2*/main("Intialize Graph")/*2.25*/{_display_(Seq[Any](format.raw/*2.26*/("""
"""),format.raw/*3.1*/("""<div id="page-wrapper">

	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">Upload Graph File</div>
				<div class="panel-body">
					<div class="row ">
						<div class="col-lg-12" style="margin-bottom: 10px;">
							"""),_display_(/*12.9*/helper/*12.15*/.form(action = routes.GraphController.upload,
							'method->"POST", 'enctype -> "multipart/form-data")/*13.59*/ {_display_(Seq[Any](format.raw/*13.61*/(""" """),format.raw/*13.62*/("""<input
								id="input-1" name="picture" type="file" class="file"
								data-preview-file-type="text"> """)))}),format.raw/*15.41*/("""
						"""),format.raw/*16.7*/("""</div>

					</div>
					<hr><h3 style="text-align: center;">OR</h3><hr>
					<div class="row ">
						<div class="col-lg-12" style="margin-bottom: 10px;">
							<a href=""""),_display_(/*22.18*/routes/*22.24*/.GraphController.parseGraphFile()),format.raw/*22.57*/(""""	class="btn btn-success col-lg-12 btn-block">
							Process with existing template 
						</a>
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>
</div>
""")))}),format.raw/*34.2*/("""


"""))}
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/graphIntialize.scala.html
                  HASH: 0404d606683c2b9fcdf86aac0c7f6e7d14c1a4d7
                  MATRIX: 822->20|853->43|891->44|919->46|1236->337|1251->343|1365->448|1405->450|1434->451|1575->561|1610->569|1816->748|1831->754|1885->787|2097->969
                  LINES: 29->2|29->2|29->2|30->3|39->12|39->12|40->13|40->13|40->13|42->15|43->16|49->22|49->22|49->22|61->34
                  -- GENERATED --
              */
          