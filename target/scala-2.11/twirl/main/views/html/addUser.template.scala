
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object addUser extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[Form[User],List[Organisation],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.3*/(userForm: Form[User],organisations:List[Organisation]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*1.58*/(""" 
 """),format.raw/*3.1*/(""" 
 """),_display_(/*4.3*/main("SignUp")/*4.17*/{_display_(Seq[Any](format.raw/*4.18*/("""

"""),format.raw/*6.1*/("""<div class="container theme-showcase" role="main">
	"""),_display_(/*7.3*/helper/*7.9*/.form(action=routes.Application.submitUser())/*7.54*/{_display_(Seq[Any](format.raw/*7.55*/("""
	
	
	"""),format.raw/*10.2*/("""<div class="container">
		<div class="row">
			<div class="col-md-12 ">
				<div class="login-panel panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Sign Up</h3>
					</div>

					<div class="panel-body">
						<div class="row">
							"""),_display_(/*20.9*/if(flash.get("message"))/*20.33*/ {_display_(Seq[Any](format.raw/*20.35*/("""
								"""),format.raw/*21.9*/("""<p class="error" style="text-align:center;color:red">"""),_display_(/*21.63*/flash/*21.68*/.get("error")),format.raw/*21.81*/("""</p>
							""")))}),format.raw/*22.9*/("""
							"""),format.raw/*23.8*/("""<div class="col-lg-6" style="margin-bottom: 10px;">
								<label> First Name </label> <input required class="form-control"
									name="firstName" id="firstName" value='"""),_display_(/*25.50*/userForm/*25.58*/.data.get("firstName")),format.raw/*25.80*/("""'/> <label> Last Name </label>
								<input required class="form-control" name="surName" id="surName" value='"""),_display_(/*26.82*/userForm/*26.90*/.data.get("surName")),format.raw/*26.110*/("""'/>
								<label> Contact Number </label> <input required
									class="form-control" name="contactNumber" id="contactNumber" value='"""),_display_(/*28.79*/userForm/*28.87*/.data.get("contactNumber")),format.raw/*28.113*/("""' />
								<label> Email </label> <input required class="form-control"
									name="email" type="email" id="email" value='"""),_display_(/*30.55*/userForm/*30.63*/.data.get("email")),format.raw/*30.81*/("""'/>
							</div>
							<div class="col-lg-6" style="margin-bottom: 10px;">
								<label> UserName </label> <input required class="form-control"
									name="userName" id="userName" value='"""),_display_(/*34.48*/userForm/*34.56*/.data.get("userName")),format.raw/*34.77*/("""'/> <label> Password </label> <input
									required class="form-control" name="password" type="password"
									id="password" /> <label> Type </label> <select required
									class="form-control" name="type" id="type" >
										<option value="admin">admin</option>
										<option value="staff">staff</option>
									</select> 
								<label>
									Organisation </label> <select required class="form-control"
									name="organisationId" id="ogranisationId">
									"""),_display_(/*44.11*/for(org<-organisations) yield /*44.34*/{_display_(Seq[Any](format.raw/*44.35*/("""
										"""),format.raw/*45.11*/("""<option value=""""),_display_(/*45.27*/org/*45.30*/.getOrganisationId()),format.raw/*45.50*/("""">"""),_display_(/*45.53*/org/*45.56*/.getOrganisationName()),format.raw/*45.78*/("""</option>
									""")))}),format.raw/*46.11*/("""
								"""),format.raw/*47.9*/("""</select>
							</div>
							<div class="col-lg-12" style="margin-bottom: 10px;">
								<button type="submit" value="login" name="submitValue"
									id="submitValue" class="btn btn-success col-lg-12 btn-block">
									<b>Log In</b>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*61.3*/("""
"""),format.raw/*62.1*/("""</div>
""")))}),format.raw/*63.2*/("""
"""))}
  }

  def render(userForm:Form[User],organisations:List[Organisation]): play.twirl.api.HtmlFormat.Appendable = apply(userForm,organisations)

  def f:((Form[User],List[Organisation]) => play.twirl.api.HtmlFormat.Appendable) = (userForm,organisations) => apply(userForm,organisations)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:47 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/addUser.scala.html
                  HASH: 8d9eba5174902ed54438abc49c28270f61c7c9af
                  MATRIX: 748->2|906->57|936->80|966->85|988->99|1026->100|1056->104|1135->158|1148->164|1201->209|1239->210|1275->219|1584->502|1617->526|1657->528|1694->538|1775->592|1789->597|1823->610|1867->624|1903->633|2106->809|2123->817|2166->839|2306->952|2323->960|2365->980|2532->1120|2549->1128|2597->1154|2753->1283|2770->1291|2809->1309|3035->1508|3052->1516|3094->1537|3619->2035|3658->2058|3697->2059|3737->2071|3780->2087|3792->2090|3833->2110|3863->2113|3875->2116|3918->2138|3970->2159|4007->2169|4389->2521|4418->2523|4457->2532
                  LINES: 26->1|29->1|30->3|31->4|31->4|31->4|33->6|34->7|34->7|34->7|34->7|37->10|47->20|47->20|47->20|48->21|48->21|48->21|48->21|49->22|50->23|52->25|52->25|52->25|53->26|53->26|53->26|55->28|55->28|55->28|57->30|57->30|57->30|61->34|61->34|61->34|71->44|71->44|71->44|72->45|72->45|72->45|72->45|72->45|72->45|72->45|73->46|74->47|88->61|89->62|90->63
                  -- GENERATED --
              */
          