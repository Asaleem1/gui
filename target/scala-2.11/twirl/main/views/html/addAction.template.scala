
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object addAction extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[String],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(actionTypes:List[String],actionSubTypes:List[String]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*1.56*/(""" 
"""),_display_(/*3.2*/main("Add New Action")/*3.24*/{_display_(Seq[Any](format.raw/*3.25*/("""

"""),format.raw/*5.1*/("""<script>
	$(document).ready(function() """),format.raw/*6.31*/("""{"""),format.raw/*6.32*/("""
		"""),format.raw/*7.3*/("""$("#addActionInput").focus();
	"""),format.raw/*8.2*/("""}"""),format.raw/*8.3*/(""");
</script>
<div id="page-wrapper">

	"""),_display_(/*12.3*/helper/*12.9*/.form(action=routes.ActionController.submitAddAction(),'id->"addActions",'name->"addActions")/*12.102*/{_display_(Seq[Any](format.raw/*12.103*/("""
	"""),format.raw/*13.2*/("""<div id="result"></div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default"
				style="border: 0; margin-bottom: 10px; box-shadow: none;">
				<div class="panel-heading"
					style="font-size: 20px; text-align: center; margin-bottom: 10px;">
					<i><b>Add New Action*</b> </i>
				</div>

				<div class="row">
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step1 &#8594; <b>Select Action Type</b>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label> Action Type </label> <select required
											class="form-control" name="actionType" id="actionTypeOptions">
											<option selected="selected" disabled="disabled" value="">Select
												Action Type</option> """),_display_(/*35.35*/for(actionType <- actionTypes) yield /*35.65*/{_display_(Seq[Any](format.raw/*35.66*/("""
											"""),format.raw/*36.12*/("""<option value=""""),_display_(/*36.28*/actionType),format.raw/*36.38*/("""">"""),_display_(/*36.41*/actionType),format.raw/*36.51*/("""</option> """)))}),format.raw/*36.62*/("""
										"""),format.raw/*37.11*/("""</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- ;;;;;;;;;; -->
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step2 &#8594; <b>Select Action Sub Type</b>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label> Action Sub Type </label> <select required
											class="form-control" name="actionSubType"
											id="actionSubTypeOption">
											<option selected="selected" disabled="disabled" value="">Select
												Action Sub Type</option> """),_display_(/*56.39*/for(actionSubType <- actionSubTypes) yield /*56.75*/{_display_(Seq[Any](format.raw/*56.76*/("""
											"""),format.raw/*57.12*/("""<option value=""""),_display_(/*57.28*/actionSubType),format.raw/*57.41*/("""">"""),_display_(/*57.44*/actionSubType),format.raw/*57.57*/("""</option> """)))}),format.raw/*57.68*/("""
										"""),format.raw/*58.11*/("""</select>
									</div>
									<!-- 
									<div class="col-lg-12" style="text-align: center;">
										<label> OR </label>
									</div>
									<div class="col-lg-12">
										<label>Add New Action Sub Type</label>
										<div class="col-lg-9"
											style="padding-right: 5px; padding-left: 5px">
											<input type="text" class="form-control "
												name="newActionSubType" id='newActionSubType' />
										</div>
										<div class="col-lg-3"
											style="padding-right: 5px; padding-left: 5px">
											<a class="btn btn-success col-lg-12 btn-block"
												onclick="updateField('newActionSubType','actionSubTypeOption')">add</a>
										</div>
									</div>
									-->
								</div>
							</div>
						</div>
					</div>
					<!-- ;;;;;;; -->

					<div class="col-lg-12" style="margin: 0 auto">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step3 &#8594; <b>Select Action Value</b>
							</div>
							<div class="panel-body">
								<div class="row">

									<div class="col-lg-12">
										<label>Add New Action Value</label><span
											style="font-size: 10px;">(multiple actions separated
											by comma)</span> <input required type="text" class="form-control "
											name="newActionValue" id="addActionInput" />
									</div>
								</div>
							</div>
						</div>
					</div>



				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					Step 4 &#8594; <b>Select Operation</b>
				</div>
				<div class="panel-body">
					<div class="row" style="margin-bottom: 10px;">
						<div class="col-lg-4">
							<button type="submit" value="more" name="submitValue1"
								id="submitValue1" class="btn btn-success col-lg-12 btn-block">
								<b>Save and Add More Actions</b>
							</button>
						</div>
						<div class="col-lg-4">
							<button type="submit" value="continue" name="submitValue1"
								id="submitValue1" class="btn btn-primary col-lg-12 btn-block">
								<b>Save and Go Back</b>
							</button>
						</div>
						<div class="col-lg-4">
							<a onClick="location.href = '"""),_display_(/*126.38*/routes/*126.44*/.Application.adminHome()),format.raw/*126.68*/("""'"
								class="btn btn-info col-lg-12 btn-block"> <b>Go Back</b>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*135.3*/("""
"""),format.raw/*136.1*/("""</div>
""")))}),format.raw/*137.2*/("""
"""))}
  }

  def render(actionTypes:List[String],actionSubTypes:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(actionTypes,actionSubTypes)

  def f:((List[String],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (actionTypes,actionSubTypes) => apply(actionTypes,actionSubTypes)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:47 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/addAction.scala.html
                  HASH: cc9818ad05ddd743337a9dddbb93dbfe1db3e050
                  MATRIX: 746->1|903->55|932->77|962->99|1000->100|1030->104|1097->144|1125->145|1155->149|1213->181|1240->182|1310->226|1324->232|1427->325|1467->326|1497->329|2411->1216|2457->1246|2496->1247|2537->1260|2580->1276|2611->1286|2641->1289|2672->1299|2714->1310|2754->1322|3457->1998|3509->2034|3548->2035|3589->2048|3632->2064|3666->2077|3696->2080|3730->2093|3772->2104|3812->2116|6070->4346|6086->4352|6132->4376|6317->4530|6347->4532|6387->4541
                  LINES: 26->1|29->1|30->3|30->3|30->3|32->5|33->6|33->6|34->7|35->8|35->8|39->12|39->12|39->12|39->12|40->13|62->35|62->35|62->35|63->36|63->36|63->36|63->36|63->36|63->36|64->37|83->56|83->56|83->56|84->57|84->57|84->57|84->57|84->57|84->57|85->58|153->126|153->126|153->126|162->135|163->136|164->137
                  -- GENERATED --
              */
          