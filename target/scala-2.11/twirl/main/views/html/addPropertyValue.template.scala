
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object addPropertyValue extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(propertyTypes:List[String]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*1.30*/(""" 
"""),_display_(/*3.2*/main("Add Property Value")/*3.28*/{_display_(Seq[Any](format.raw/*3.29*/("""
"""),format.raw/*4.1*/("""<script>

	$(document).ready(function() """),format.raw/*6.31*/("""{"""),format.raw/*6.32*/("""
		"""),format.raw/*7.3*/("""$("#addSubjectInput").focus();
	"""),format.raw/*8.2*/("""}"""),format.raw/*8.3*/(""");
	function changeType(option,mainType,subTypeOption)"""),format.raw/*9.52*/("""{"""),format.raw/*9.53*/("""
		"""),format.raw/*10.3*/("""var parent = "PARENT",dOption="Main",subType="";
		if(option!='1')"""),format.raw/*11.18*/("""{"""),format.raw/*11.19*/("""
			"""),format.raw/*12.4*/("""parent = $("#"+option+" option:selected").val();	
			dOption="Sub"
		"""),format.raw/*14.3*/("""}"""),format.raw/*14.4*/("""
		"""),format.raw/*15.3*/("""var mainTypeVal = $("#"+mainType+" option:selected").val();
		$.ajax("""),format.raw/*16.10*/("""{"""),format.raw/*16.11*/("""
	        """),format.raw/*17.10*/("""type : "POST",
	        url : "/getMainTypes",
	        data:"""),format.raw/*19.15*/("""{"""),format.raw/*19.16*/(""""mainType":mainTypeVal,"parent":parent"""),format.raw/*19.54*/("""}"""),format.raw/*19.55*/(""",
	        success:function(data)"""),format.raw/*20.32*/("""{"""),format.raw/*20.33*/("""
	        	"""),format.raw/*21.11*/("""lst = JSON.parse(data);
	        	$('#'+subTypeOption).empty(); //remove all child nodes
	        	var defaultOption = $('<option selected="selected" disabled="disabled" value="">Select '+dOption+' Type</option>');
	        	$('#'+subTypeOption).append(defaultOption);
	        	for(var i=0;i<lst.length;i++)"""),format.raw/*25.40*/("""{"""),format.raw/*25.41*/("""
	        		"""),format.raw/*26.12*/("""var newOption = $('<option value="'+lst[i]+'">'+lst[i]+'</option>');
	            	$('#'+subTypeOption).append(newOption);
	        	"""),format.raw/*28.11*/("""}"""),format.raw/*28.12*/("""
	            """),format.raw/*29.14*/("""$('#'+subTypeOption).trigger("chosen:updated");
	            """),format.raw/*30.14*/("""}"""),format.raw/*30.15*/("""
	    """),format.raw/*31.6*/("""}"""),format.raw/*31.7*/(""");
	"""),format.raw/*32.2*/("""}"""),format.raw/*32.3*/("""
"""),format.raw/*33.1*/("""</script>
<div id="page-wrapper">
	"""),_display_(/*35.3*/helper/*35.9*/.form(action=routes.PropertyFileController.submitPropertyValue(),'id->"propertyValueForm",'name->"propertyValueForm")/*35.126*/{_display_(Seq[Any](format.raw/*35.127*/("""
	"""),format.raw/*36.2*/("""<div id="result"></div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default"
				style="border: 0; margin-bottom: 10px; box-shadow: none;">
				<div class="panel-heading"
					style="font-size: 20px; text-align: center; margin-bottom: 10px;">
					<i><b>Add New Property Values*</b> </i>
				</div>

				<div class="row">
					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step1 &#8594; <b>Select Property Type</b>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label> Property Type </label> <select
											onchange="changeType('1','propertyTypeOptions','mainTypeOptions')"
											required class="form-control" name="propertyTypeOptions"
											id="propertyTypeOptions">
											<option selected="selected" disabled="disabled" value="">Select
												Property Type</option> """),_display_(/*60.37*/for(propertyType <- propertyTypes) yield /*60.71*/{_display_(Seq[Any](format.raw/*60.72*/("""
											"""),format.raw/*61.12*/("""<option value=""""),_display_(/*61.28*/propertyType),format.raw/*61.40*/("""">"""),_display_(/*61.43*/propertyType),format.raw/*61.55*/("""</option> """)))}),format.raw/*61.66*/("""
										"""),format.raw/*62.11*/("""</select>
									</div>

								</div>
							</div>
						</div>
					</div>

					<!-- ;;;;;;;;;; -->
					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step2 &#8594; <b>Select Main Type</b>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label> Main Type </label> <select required
											class="form-control" name="mainTypeOptions" id="mainTypeOptions"
											onchange="changeType('propertyTypeOptions','mainTypeOptions','subTypeOptions')">
											<option selected="selected" disabled="disabled" value="">Select
												Main Type</option>
										</select>
									</div>

								</div>
							</div>
						</div>
					</div>

					<!-- ;;;;;;;;;; -->
					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step3 &#8594; <b>Select Sub Type</b>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label> Sub Type </label> <select required
											class="form-control" name="subTypeOptions" id="subTypeOptions">
											<option selected="selected" disabled="disabled" value="">Select
												Sub Type</option>

										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- ;;;;;;; -->

					<div class="col-lg-12" style="margin: 0 auto">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step4 &#8594; <b>Property Value(s)</b>
							</div>
							<div class="panel-body">
								<div class="row">

									<div class="col-lg-12">
										<label>Add New Property Value(s)</label><span
											style="font-size: 10px;">(multiple values separated
											by comma)</span> <input required type="text" class="form-control "
											name="newPropertyValue" id="newPropertyValue" />
									</div>
								</div>
							</div>
						</div>
					</div>



				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					Step5 &#8594; <b>Select Operation</b>
				</div>
				<div class="panel-body">
					<div class="row" style="margin-bottom: 10px;">
						<div class="col-lg-4">
							<button type="submit" value="more" name="submitValue1"
								id="submitValue1" class="btn btn-success col-lg-12 btn-block">
								<b>Save and Add More Values</b>
							</button>
						</div>
						<div class="col-lg-4">
							<button type="submit" value="continue" name="submitValue1"
								id="submitValue1" class="btn btn-primary col-lg-12 btn-block">
								<b>Save and Go Back</b>
							</button>
						</div>
						<div class="col-lg-4">
							<a onClick="location.href = '"""),_display_(/*156.38*/routes/*156.44*/.Application.adminHome()),format.raw/*156.68*/("""'"
								class="btn btn-info col-lg-12 btn-block"> <b>Go Back</b>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*165.3*/("""
"""),format.raw/*166.1*/("""</div>
""")))}),format.raw/*167.2*/("""
"""))}
  }

  def render(propertyTypes:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(propertyTypes)

  def f:((List[String]) => play.twirl.api.HtmlFormat.Appendable) = (propertyTypes) => apply(propertyTypes)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Tue Mar 29 00:25:53 BST 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/addPropertyValue.scala.html
                  HASH: 4c15abf0e29528aa48b263e70a094914bea66f11
                  MATRIX: 740->1|871->29|900->52|934->78|972->79|1000->81|1069->123|1097->124|1127->128|1186->161|1213->162|1295->217|1323->218|1354->222|1449->289|1478->290|1510->295|1608->366|1636->367|1667->371|1765->441|1794->442|1833->453|1924->516|1953->517|2019->555|2048->556|2110->590|2139->591|2179->603|2519->915|2548->916|2589->929|2752->1064|2781->1065|2824->1080|2914->1142|2943->1143|2977->1150|3005->1151|3037->1156|3065->1157|3094->1159|3158->1197|3172->1203|3299->1320|3339->1321|3369->1324|4400->2328|4450->2362|4489->2363|4530->2376|4573->2392|4606->2404|4636->2407|4669->2419|4711->2430|4751->2442|7698->5361|7714->5367|7760->5391|7945->5545|7975->5547|8015->5556
                  LINES: 26->1|29->1|30->3|30->3|30->3|31->4|33->6|33->6|34->7|35->8|35->8|36->9|36->9|37->10|38->11|38->11|39->12|41->14|41->14|42->15|43->16|43->16|44->17|46->19|46->19|46->19|46->19|47->20|47->20|48->21|52->25|52->25|53->26|55->28|55->28|56->29|57->30|57->30|58->31|58->31|59->32|59->32|60->33|62->35|62->35|62->35|62->35|63->36|87->60|87->60|87->60|88->61|88->61|88->61|88->61|88->61|88->61|89->62|183->156|183->156|183->156|192->165|193->166|194->167
                  -- GENERATED --
              */
          