
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object editPolicy extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[String,Policy,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String,policyData: Policy):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.36*/("""

 """),_display_(/*3.3*/main(title)/*3.14*/{_display_(Seq[Any](format.raw/*3.15*/("""
"""),format.raw/*4.1*/("""<div id="page-wrapper">
	"""),_display_(/*5.3*/helper/*5.9*/.form(action=routes.PolicyController.editPolicySubmit(),'id->"myForm",'name->"myForm")/*5.95*/{_display_(Seq[Any](format.raw/*5.96*/("""
	"""),format.raw/*6.2*/("""<div class="row" >
		<div class="col-lg-12">
			<div class="panel panel-default" style="border: 0; box-shadow: none;">
				<div class="panel-heading"
					style="font-size: 20px; text-align: center; margin-bottom: 10px;">
					<i><b>Edit Policy*</b> </i>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<b>Edit Policy Details</b>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label style="margin-right: 5px">Policy Name</label>
										 <input
											required class="form-control" placeholder="Enter Policy Name"
											type="text" name="policyName"  value=""""),_display_(/*25.51*/policyData/*25.61*/.getPolicyName()),format.raw/*25.77*/(""""/>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label style="margin-right: 5px">Policy Description</label>
										<textarea required class="form-control"
											placeholder="Enter Policy Description" type="text"
											name="policyDescription">"""),_display_(/*33.38*/policyData/*33.48*/.getPolicyDescription()),format.raw/*33.71*/("""</textarea>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<button type="submit" value="updatePolicy" name="submitValue1"
											id="submitValue1" class="btn btn-success col-lg-12 btn-block">
											<b>Update</b>
										</button>
									</div>
									<div class="col-lg-6" style="margin-bottom: 10px;">
										<button type="submit" value="addRule" name="submitValue1"
											id="submitValue1" class="btn btn-primary col-lg-12 btn-block">
											<b>Add New Rule</b>
										</button>
									</div>
									<div class="col-lg-6" style="margin-bottom: 10px;">
										<button type="submit" value="combineRules" name="submitValue1"
											id="submitValue1" class="btn btn-primary col-lg-12 btn-block">
											<b>Combine Rules/Groups</b>
										</button>
									</div>
								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" value=""""),_display_(/*63.32*/policyData/*63.42*/.getPolicyId()),format.raw/*63.56*/("""" name="policyId"/>
		""")))}),format.raw/*64.4*/("""
	
	"""),format.raw/*66.2*/("""</div>
	""")))}))}
  }

  def render(title:String,policyData:Policy): play.twirl.api.HtmlFormat.Appendable = apply(title,policyData)

  def f:((String,Policy) => play.twirl.api.HtmlFormat.Appendable) = (title,policyData) => apply(title,policyData)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/editPolicy.scala.html
                  HASH: e0986728538425bf2b25f7bcbe0a38b2d9a4e668
                  MATRIX: 735->1|857->35|888->41|907->52|945->53|973->55|1025->82|1038->88|1132->174|1170->175|1199->178|2010->962|2029->972|2066->988|2443->1338|2462->1348|2506->1371|3579->2417|3598->2427|3633->2441|3687->2465|3720->2471
                  LINES: 26->1|29->1|31->3|31->3|31->3|32->4|33->5|33->5|33->5|33->5|34->6|53->25|53->25|53->25|61->33|61->33|61->33|91->63|91->63|91->63|92->64|94->66
                  -- GENERATED --
              */
          