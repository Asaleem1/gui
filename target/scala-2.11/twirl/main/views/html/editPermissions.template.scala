
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object editPermissions extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[List[Permission],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(permissions:List[Permission]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import java.math.BigInteger; var i=1;var k=0;

Seq[Any](format.raw/*1.32*/("""
"""),_display_(/*3.2*/main("Example")/*3.17*/{_display_(Seq[Any](format.raw/*3.18*/("""
"""),format.raw/*4.1*/("""<script>
String.prototype.capitalizeFirstLetter = function() """),format.raw/*5.53*/("""{"""),format.raw/*5.54*/("""
	"""),format.raw/*6.2*/("""return this.charAt(0).toUpperCase() + this.substring(1).toLowerCase();
"""),format.raw/*7.1*/("""}"""),format.raw/*7.2*/("""

"""),format.raw/*9.1*/("""function deleteItem(rowId, id) """),format.raw/*9.32*/("""{"""),format.raw/*9.33*/("""
	"""),format.raw/*10.2*/("""if (id != null) """),format.raw/*10.18*/("""{"""),format.raw/*10.19*/("""
		"""),format.raw/*11.3*/("""var option = confirm("Are you sure to delete this permission type?");
		if (option) """),format.raw/*12.15*/("""{"""),format.raw/*12.16*/("""
			"""),format.raw/*13.4*/("""$.ajax("""),format.raw/*13.11*/("""{"""),format.raw/*13.12*/("""
				"""),format.raw/*14.5*/("""type : "POST",
				url : "/deletePermission",
				data : """),format.raw/*16.12*/("""{"""),format.raw/*16.13*/("""
					"""),format.raw/*17.6*/(""""attributeId" : id,
				"""),format.raw/*18.5*/("""}"""),format.raw/*18.6*/(""",
				success : function(data) """),format.raw/*19.30*/("""{"""),format.raw/*19.31*/("""
					"""),format.raw/*20.6*/("""$("#"+rowId).remove();
					
				"""),format.raw/*22.5*/("""}"""),format.raw/*22.6*/("""
			"""),format.raw/*23.4*/("""}"""),format.raw/*23.5*/(""");
		"""),format.raw/*24.3*/("""}"""),format.raw/*24.4*/("""

	"""),format.raw/*26.2*/("""}"""),format.raw/*26.3*/("""
"""),format.raw/*27.1*/("""}"""),format.raw/*27.2*/("""

"""),format.raw/*29.1*/("""function updateValue() """),format.raw/*29.24*/("""{"""),format.raw/*29.25*/("""
	"""),format.raw/*30.2*/("""var valueId = $("#valueId").val();
	var editValueName = $("#editValueName").val();
	var rowId = $("#rowId").val();
	$.ajax("""),format.raw/*33.9*/("""{"""),format.raw/*33.10*/("""
		"""),format.raw/*34.3*/("""type : "POST",
		url : "/updatePermission",
		data : """),format.raw/*36.10*/("""{"""),format.raw/*36.11*/("""
			"""),format.raw/*37.4*/(""""valueId" : valueId,
			"valueName" : editValueName
		"""),format.raw/*39.3*/("""}"""),format.raw/*39.4*/(""",
		success : function(data) """),format.raw/*40.28*/("""{"""),format.raw/*40.29*/("""
			"""),format.raw/*41.4*/("""$("#value_" + rowId).html(editValueName);
			$("#myModal").modal('toggle');

		"""),format.raw/*44.3*/("""}"""),format.raw/*44.4*/("""
	"""),format.raw/*45.2*/("""}"""),format.raw/*45.3*/(""");
"""),format.raw/*46.1*/("""}"""),format.raw/*46.2*/("""
"""),format.raw/*47.1*/("""function openEditModal(rowId, id) """),format.raw/*47.35*/("""{"""),format.raw/*47.36*/("""
	"""),format.raw/*48.2*/("""if (id != null) """),format.raw/*48.18*/("""{"""),format.raw/*48.19*/("""
		"""),format.raw/*49.3*/("""var value = $("#value_"+rowId).html();
		$("#editValueName").val(value);
		
		$("#rowId").val(rowId);
		$("#valueId").val(id);
		$("#myModal").modal('show');
	"""),format.raw/*55.2*/("""}"""),format.raw/*55.3*/("""
"""),format.raw/*56.1*/("""}"""),format.raw/*56.2*/("""
"""),format.raw/*57.1*/("""</script>
<div id="page-wrapper">
	"""),_display_(/*59.3*/if(permissions.size()!=0)/*59.28*/{_display_(Seq[Any](format.raw/*59.29*/("""
	"""),format.raw/*60.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">All Permission</div>
				<div class="panel-body" style="overflow: auto; height: 430px;">
					<table class="table table-hover">
						<thead>
							<th>Index</th>
							<th>Permission Name</th>
							<th>Operations</th>
						</thead>
						<tbody>
							"""),_display_(/*72.9*/for(permission <- permissions) yield /*72.39*/{_display_(Seq[Any](format.raw/*72.40*/("""
							"""),format.raw/*73.8*/("""<tr id="tr"""),_display_(/*73.19*/{i}),format.raw/*73.22*/("""">
								<td>"""),_display_(/*74.14*/i),format.raw/*74.15*/("""</td>
								<td id="value_"""),_display_(/*75.24*/i),format.raw/*75.25*/("""">"""),_display_(/*75.28*/permission/*75.38*/.getPermissionValue()),format.raw/*75.59*/("""</td>
								<td>
									<div class="row">
										<div class="col-lg-6">
											<a onclick="openEditModal('"""),_display_(/*79.40*/i),format.raw/*79.41*/("""','"""),_display_(/*79.45*/permission/*79.55*/.getPermissionId()),format.raw/*79.73*/("""')"
												class=" btn btn-info btn-block">Edit</a>
										</div>
										<div class="col-lg-6">
											<a onclick="deleteItem('tr"""),_display_(/*83.39*/i),format.raw/*83.40*/("""','"""),_display_(/*83.44*/permission/*83.54*/.getPermissionId()),format.raw/*83.72*/("""')"
												class=" btn btn-danger btn-block">Delete</a>
										</div>
									</div>
								</td>
							</tr>
							"""),_display_(/*89.9*/{i=i+1}),format.raw/*89.16*/(""" """)))}),format.raw/*89.18*/("""
						"""),format.raw/*90.7*/("""</tbody>
					</table>
					<div class="col-lg-12" style="margin-bottom: 10px;">
						<button type="submit"
							onClick="location.href = '"""),_display_(/*94.35*/routes/*94.41*/.Application.adminHome()),format.raw/*94.65*/("""'"
							class="btn btn-primary btn-block">
							<b>Go Back</b>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modalDiv">
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
			<input type="hidden" name="valueId" id="valueId" />
			<input type="hidden" name="rowId" id="rowId" />
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">Edit Permission</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							
							<div class="col-lg-12">
								<label>Edit Permission Value</label> <input type="text"
									class="form-control " name="editValueName"
									id='editValueName' />
							</div>
						</div>

					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary" onclick="updateValue()">Update
							Permission</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	

	""")))}/*140.3*/else/*140.7*/{_display_(Seq[Any](format.raw/*140.8*/("""
	"""),format.raw/*141.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading" style="text-align: center">
					<b>No Permissions in DomainModel yet!!</b>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12" style="margin-bottom: 10px;">
							<button type="submit"
								onClick="location.href = '"""),_display_(/*151.36*/routes/*151.42*/.Application.adminHome()),format.raw/*151.66*/("""'"
								class="btn btn-primary col-lg-12 btn-block">
								<b>Go Back</b>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*161.3*/("""
"""),format.raw/*162.1*/("""</div>


""")))}),format.raw/*165.2*/("""
"""))}
  }

  def render(permissions:List[Permission]): play.twirl.api.HtmlFormat.Appendable = apply(permissions)

  def f:((List[Permission]) => play.twirl.api.HtmlFormat.Appendable) = (permissions) => apply(permissions)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/editPermissions.scala.html
                  HASH: bb39a0afb2825f0075bcf0a889727fc1708e951a
                  MATRIX: 743->1|906->31|934->82|957->97|995->98|1023->100|1112->162|1140->163|1169->166|1267->238|1294->239|1324->243|1382->274|1410->275|1440->278|1484->294|1513->295|1544->299|1657->384|1686->385|1718->390|1753->397|1782->398|1815->404|1902->463|1931->464|1965->471|2017->496|2045->497|2105->529|2134->530|2168->537|2230->572|2258->573|2290->578|2318->579|2351->585|2379->586|2411->591|2439->592|2468->594|2496->595|2527->599|2578->622|2607->623|2637->626|2790->752|2819->753|2850->757|2933->812|2962->813|2994->818|3077->874|3105->875|3163->905|3192->906|3224->911|3333->993|3361->994|3391->997|3419->998|3450->1002|3478->1003|3507->1005|3569->1039|3598->1040|3628->1043|3672->1059|3701->1060|3732->1064|3924->1229|3952->1230|3981->1232|4009->1233|4038->1235|4102->1273|4136->1298|4175->1299|4205->1302|4615->1686|4661->1716|4700->1717|4736->1726|4774->1737|4798->1740|4842->1757|4864->1758|4921->1788|4943->1789|4973->1792|4992->1802|5034->1823|5183->1945|5205->1946|5236->1950|5255->1960|5294->1978|5470->2127|5492->2128|5523->2132|5542->2142|5581->2160|5742->2295|5770->2302|5803->2304|5838->2312|6012->2459|6027->2465|6072->2489|7413->3811|7426->3815|7465->3816|7496->3819|7908->4203|7924->4209|7970->4233|8172->4404|8202->4406|8246->4419
                  LINES: 26->1|29->1|30->3|30->3|30->3|31->4|32->5|32->5|33->6|34->7|34->7|36->9|36->9|36->9|37->10|37->10|37->10|38->11|39->12|39->12|40->13|40->13|40->13|41->14|43->16|43->16|44->17|45->18|45->18|46->19|46->19|47->20|49->22|49->22|50->23|50->23|51->24|51->24|53->26|53->26|54->27|54->27|56->29|56->29|56->29|57->30|60->33|60->33|61->34|63->36|63->36|64->37|66->39|66->39|67->40|67->40|68->41|71->44|71->44|72->45|72->45|73->46|73->46|74->47|74->47|74->47|75->48|75->48|75->48|76->49|82->55|82->55|83->56|83->56|84->57|86->59|86->59|86->59|87->60|99->72|99->72|99->72|100->73|100->73|100->73|101->74|101->74|102->75|102->75|102->75|102->75|102->75|106->79|106->79|106->79|106->79|106->79|110->83|110->83|110->83|110->83|110->83|116->89|116->89|116->89|117->90|121->94|121->94|121->94|167->140|167->140|167->140|168->141|178->151|178->151|178->151|188->161|189->162|192->165
                  -- GENERATED --
              */
          