
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object memberProperty extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[String,List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(message: String, 
firsts: List[GraphEntityValue], 
seconds: List[GraphEntityValue], 
thirds: List[GraphEntityValue],
labels: List[String]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*5.22*/("""

"""),_display_(/*8.2*/main(message)/*8.15*/{_display_(Seq[Any](format.raw/*8.16*/("""
"""),format.raw/*9.1*/("""<div id="page-wrapper">
	"""),_display_(/*10.3*/helper/*10.9*/.form(action=routes.PropertyFileController.addMemberPropertyGroup(),'id->"myForm_property",'name->"myForm_property")/*10.125*/{_display_(Seq[Any](format.raw/*10.126*/("""
	"""),format.raw/*11.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default"
				style="border: 0; margin-bottom: 10px; box-shadow: none;">
				<div class="panel-heading"
					style="font-size: 20px; text-align: center; margin-bottom: 10px;">
					<i><b>Create Member Property Group</b> </i>
				</div>

				<div class="row">

					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step1 &#8594; <b>Select Value(s)</b>
							</div>
							<div class="panel-body">
								<div class="row ">
									<div class="col-lg-4" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*30.27*/labels/*30.33*/.get(0)),format.raw/*30.40*/("""s </label> <label
											class="checkbox-inline"> <input type="checkbox"
											value=""""),_display_(/*32.20*/labels/*32.26*/.get(0)),format.raw/*32.33*/("""" name="firstOption"
											onchange="changeOption('firstIds','firstOption'); changeDivVisibility('firstChoiceSpan');">
											In Use
										</label> <select class="form-control" id="firstIds"
											disabled="disabled" name="firstIds[]" multiple>
											"""),_display_(/*37.13*/for(first <- firsts) yield /*37.33*/ {_display_(Seq[Any](format.raw/*37.35*/("""
											"""),format.raw/*38.12*/("""<option value=""""),_display_(/*38.28*/first/*38.33*/.getValue()),format.raw/*38.44*/("""">
												"""),_display_(/*39.14*/first/*39.19*/.getValue()),format.raw/*39.30*/("""</option> """)))}),format.raw/*39.41*/("""
										"""),format.raw/*40.11*/("""</select> <span id="firstChoiceSpan"
											style="display: none; font-size: 12px; font-weight: 400 !important;">
											<label style="font-size: 12px; font-weight: 400;">
												Option for Selected """),_display_(/*43.34*/labels/*43.40*/.get(0)),format.raw/*43.47*/(""" """),format.raw/*43.48*/("""</label> <label
											class="radio-in-line"><input type="radio"
												name="firstChoice" id="firstChoice" value="fixed"
												checked> Fixed </label> <label class="radio-in-line"><input
												type="radio" name="firstChoice" id="firstChoice"
												value="omit" checked> Omit </label>
										</span>
									</div>
									<div class="col-lg-4" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*52.27*/labels/*52.33*/.get(1)),format.raw/*52.40*/("""s
										</label> <label
											class="checkbox-inline"> <input type="checkbox"
											value=""""),_display_(/*55.20*/labels/*55.26*/.get(1)),format.raw/*55.33*/("""" name="secondChoiceOption"
											onchange="changeOption('secondChoiceId','secondChoiceOption'); changeDivVisibility('secondChoiceSpan');">
											In Use
										</label> <select class="form-control" id="secondChoiceId"
											disabled="disabed" name="secondChoiceId[]" multiple>
											"""),_display_(/*60.13*/for(second <- seconds) yield /*60.35*/ {_display_(Seq[Any](format.raw/*60.37*/("""
											"""),format.raw/*61.12*/("""<option value=""""),_display_(/*61.28*/second/*61.34*/.getValue()),format.raw/*61.45*/("""">
												"""),_display_(/*62.14*/second/*62.20*/.getValue()),format.raw/*62.31*/("""</option> """)))}),format.raw/*62.42*/("""
										"""),format.raw/*63.11*/("""</select> <span id="secondChoiceSpan"
											style="display: none; font-size: 12px; font-weight: 400 !important;">
											<label style="font-size: 12px; font-weight: 400;">
												Option for Selected """),_display_(/*66.34*/labels/*66.40*/.get(1)),format.raw/*66.47*/(""" """),format.raw/*66.48*/("""</label> <label class="radio-in-line"><input
												type="radio" name="secondChoice" id="secondChoice"
												value="fixed" checked> Fixed </label> <label
											class="radio-in-line"><input type="radio"
												name="secondChoice" id="secondChoice" value="omit" checked>
												Omit </label>
										</span>
									</div>
									<div class="col-lg-4" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*75.27*/labels/*75.33*/.get(2)),format.raw/*75.40*/("""s </label> <label class="checkbox-inline">
											<input type="checkbox" value=""""),_display_(/*76.43*/labels/*76.49*/.get(2)),format.raw/*76.56*/("""" name="thirdChoiceOption"
											onchange="changeOption('thirdChoiceId','thirdChoiceOption'); changeDivVisibility('thirdChoiceSpan');">
											In Use
										</label> <select class="form-control" id="thirdChoiceId"
											disabled="disabled" name="thirdChoiceId[]" multiple>
											"""),_display_(/*81.13*/for(third <- thirds) yield /*81.33*/{_display_(Seq[Any](format.raw/*81.34*/("""
											"""),format.raw/*82.12*/("""<option value=""""),_display_(/*82.28*/third/*82.33*/.getValue()),format.raw/*82.44*/("""">"""),_display_(/*82.47*/third/*82.52*/.getValue()),format.raw/*82.63*/("""</option>
											""")))}),format.raw/*83.13*/("""
										"""),format.raw/*84.11*/("""</select> <span id="thirdChoiceSpan"
											style="display: none; font-size: 12px; font-weight: 400 !important;">
											<label style="font-size: 12px; font-weight: 400;">
												Option for Selected """),_display_(/*87.34*/labels/*87.40*/.get(2)),format.raw/*87.47*/(""" """),format.raw/*87.48*/("""</label> <label class="radio-in-line"><input
												type="radio" name="thirdChoice" id="thirdChoice" value="fixed"
												checked> Fixed </label> <label class="radio-in-line"><input
												type="radio" name="thirdChoice" id="thirdChoice" value="omit"
												checked> Omit </label>
										</span>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step2 &#8594; <b>Set Instance Count</b>
							</div>
							<div class="panel-body">
								<div class="row ">
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Attribute Combination Count </label> <input
											type="number" placeholder="0" min="0" class="form-control"
											required name="attributeCombinationCount">
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Small </label> <input type="number" placeholder="0"
											min="0" class="form-control" required
											name="smallInstanceCount">
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Medium </label> <input type="number" placeholder="0"
											min="0" class="form-control" required
											name="mediumInstanceCount">
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Large </label> <input type="number" placeholder="0"
											min="0" class="form-control" required
											name="largeInstanceCount">
									</div>
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label>
										Prefixes for Member<i><span style="font-size:10px;">(Comma seperated prefixes)</span></i>
										</label>
										<input required class="form-control"
											placeholder="Enter Name PreFixes" type="text"
											name="namePrefixes" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			Step 3 &#8594; <b>Select Operation</b>
		</div>
		<div class="panel-body">
			<div class="row" style="margin-bottom: 10px;">
				<div class="col-lg-4">
					<button type="submit" value="continue" name="submitValue1"
						 class="btn btn-primary col-lg-12 btn-block">
						<b>Save and Go Back</b>
					</button>
				</div>
				<div class="col-lg-4">
					<button type="submit" value="more" name="submitValue1"
						 class="btn btn-success col-lg-12 btn-block">
						<b>Save and Add Another Group</b>
					</button>
				</div>
				<div class="col-lg-4">
					<button type="submit" value="back" name="submitValue1"
						 class="btn btn-danger col-lg-12 btn-block">
						<b>Go Back</b>
					</button>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*170.3*/("""
"""),format.raw/*171.1*/("""</div>

""")))}),format.raw/*173.2*/("""
"""))}
  }

  def render(message:String,firsts:List[GraphEntityValue],seconds:List[GraphEntityValue],thirds:List[GraphEntityValue],labels:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(message,firsts,seconds,thirds,labels)

  def f:((String,List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (message,firsts,seconds,thirds,labels) => apply(message,firsts,seconds,thirds,labels)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/memberProperty.scala.html
                  HASH: 3e27373bee5ba5e7cdee469c894131a619a71626
                  MATRIX: 814->1|1060->144|1090->168|1111->181|1149->182|1177->184|1230->211|1244->217|1370->333|1410->334|1440->337|2125->995|2140->1001|2168->1008|2293->1106|2308->1112|2336->1119|2643->1399|2679->1419|2719->1421|2760->1434|2803->1450|2817->1455|2849->1466|2893->1483|2907->1488|2939->1499|2981->1510|3021->1522|3264->1738|3279->1744|3307->1751|3336->1752|3805->2194|3820->2200|3848->2207|3984->2316|3999->2322|4027->2329|4366->2641|4404->2663|4444->2665|4485->2678|4528->2694|4543->2700|4575->2711|4619->2728|4634->2734|4666->2745|4708->2756|4748->2768|4992->2985|5007->2991|5035->2998|5064->2999|5537->3445|5552->3451|5580->3458|5693->3544|5708->3550|5736->3557|6070->3864|6106->3884|6145->3885|6186->3898|6229->3914|6243->3919|6275->3930|6305->3933|6319->3938|6351->3949|6405->3972|6445->3984|6688->4200|6703->4206|6731->4213|6760->4214|9713->7136|9743->7138|9785->7149
                  LINES: 26->1|33->5|35->8|35->8|35->8|36->9|37->10|37->10|37->10|37->10|38->11|57->30|57->30|57->30|59->32|59->32|59->32|64->37|64->37|64->37|65->38|65->38|65->38|65->38|66->39|66->39|66->39|66->39|67->40|70->43|70->43|70->43|70->43|79->52|79->52|79->52|82->55|82->55|82->55|87->60|87->60|87->60|88->61|88->61|88->61|88->61|89->62|89->62|89->62|89->62|90->63|93->66|93->66|93->66|93->66|102->75|102->75|102->75|103->76|103->76|103->76|108->81|108->81|108->81|109->82|109->82|109->82|109->82|109->82|109->82|109->82|110->83|111->84|114->87|114->87|114->87|114->87|197->170|198->171|200->173
                  -- GENERATED --
              */
          