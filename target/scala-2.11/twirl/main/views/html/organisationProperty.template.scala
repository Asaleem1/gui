
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object organisationProperty extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[String,List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(message: String, 
firsts: List[GraphEntityValue], 
seconds: List[GraphEntityValue], 
thirds: List[GraphEntityValue],
labels: List[String]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*5.22*/("""

"""),_display_(/*8.2*/main(message)/*8.15*/{_display_(Seq[Any](format.raw/*8.16*/("""
"""),format.raw/*9.1*/("""<div id="page-wrapper">
	"""),_display_(/*10.3*/helper/*10.9*/.form(action=routes.PropertyFileController.addMemberPropertyGroup(),'id->"myForm_property",'name->"myForm_property")/*10.125*/{_display_(Seq[Any](format.raw/*10.126*/("""
	"""),format.raw/*11.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default"
				style="border: 0; margin-bottom: 10px; box-shadow: none;">
				<div class="panel-heading"
					style="font-size: 20px; text-align: center; margin-bottom: 10px;">
					<i><b>Create Organisation Property Group</b> </i>
				</div>

				<div class="row">

					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step1 &#8594; <b>Select Value(s)</b>
							</div>
							<div class="panel-body">
								<div class="row ">
									<div class="col-lg-4" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*30.27*/labels/*30.33*/.get(0)),format.raw/*30.40*/("""s </label> <label
											class="checkbox-inline"> <input type="checkbox"
											value=""""),_display_(/*32.20*/labels/*32.26*/.get(0)),format.raw/*32.33*/("""" name="firstOption"
											onchange="changeOption('firstIds','firstOption'); changeDivVisibility('firstChoiceSpan');">
											In Use
										</label> <select class="form-control" id="firstIds"
											disabled="disabled" name="firstIds[]" multiple>
											"""),_display_(/*37.13*/for(first <- firsts) yield /*37.33*/ {_display_(Seq[Any](format.raw/*37.35*/("""
											"""),format.raw/*38.12*/("""<option value=""""),_display_(/*38.28*/first/*38.33*/.getValue()),format.raw/*38.44*/("""">
												"""),_display_(/*39.14*/first/*39.19*/.getValue()),format.raw/*39.30*/("""</option> """)))}),format.raw/*39.41*/("""
										"""),format.raw/*40.11*/("""</select> <span id="firstChoiceSpan"
											style="display: none; font-size: 12px; font-weight: 400 !important;">
											<label style="font-size: 12px; font-weight: 400;">
												Option for Selected """),_display_(/*43.34*/labels/*43.40*/.get(0)),format.raw/*43.47*/(""" """),format.raw/*43.48*/("""</label> <label
											class="radio-in-line"><input type="radio"
												name="firstChoice" id="firstChoice" value="fixed"
												checked> Fixed </label> <label class="radio-in-line"><input
												type="radio" name="firstChoice" id="firstChoice"
												value="omit" checked> Omit </label>
										</span>
									</div>
									<div class="col-lg-4" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*52.27*/labels/*52.33*/.get(1)),format.raw/*52.40*/("""s
										</label> <label
											class="checkbox-inline"> <input type="checkbox"
											value=""""),_display_(/*55.20*/labels/*55.26*/.get(1)),format.raw/*55.33*/("""" name="secondChoiceOption"
											onchange="changeOption('secondChoiceId','secondChoiceOption'); changeDivVisibility('secondChoiceSpan');">
											In Use
										</label> <select class="form-control" id="secondChoiceId"
											disabled="disabed" name="secondChoiceId[]" multiple>
											"""),_display_(/*60.13*/for(second <- seconds) yield /*60.35*/ {_display_(Seq[Any](format.raw/*60.37*/("""
											"""),format.raw/*61.12*/("""<option value=""""),_display_(/*61.28*/second/*61.34*/.getValue()),format.raw/*61.45*/("""">
												"""),_display_(/*62.14*/second/*62.20*/.getValue()),format.raw/*62.31*/("""</option> """)))}),format.raw/*62.42*/("""
										"""),format.raw/*63.11*/("""</select> <span id="secondChoiceSpan"
											style="display: none; font-size: 12px; font-weight: 400 !important;">
											<label style="font-size: 12px; font-weight: 400;">
												Option for Selected """),_display_(/*66.34*/labels/*66.40*/.get(1)),format.raw/*66.47*/(""" """),format.raw/*66.48*/("""</label> <label class="radio-in-line"><input
												type="radio" name="secondChoice" id="secondChoice"
												value="fixed" checked> Fixed </label> <label
											class="radio-in-line"><input type="radio"
												name="secondChoice" id="secondChoice" value="omit" checked>
												Omit </label>
										</span>
									</div>
									<div class="col-lg-4" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*75.27*/labels/*75.33*/.get(2)),format.raw/*75.40*/("""s </label> <label class="checkbox-inline">
											<input type="checkbox" value=""""),_display_(/*76.43*/labels/*76.49*/.get(2)),format.raw/*76.56*/("""" name="thirdChoiceOption"
											onchange="changeOption('thirdChoiceId','thirdChoiceOption'); changeDivVisibility('thirdChoiceSpan');">
											In Use
										</label> <select class="form-control" id="thirdChoiceId"
											disabled="disabled" name="thirdChoiceId[]" multiple>
											"""),_display_(/*81.13*/for(third <- thirds) yield /*81.33*/{_display_(Seq[Any](format.raw/*81.34*/("""
											"""),format.raw/*82.12*/("""<option value=""""),_display_(/*82.28*/third/*82.33*/.getValue()),format.raw/*82.44*/("""">"""),_display_(/*82.47*/third/*82.52*/.getValue()),format.raw/*82.63*/("""</option>
											""")))}),format.raw/*83.13*/("""
										"""),format.raw/*84.11*/("""</select> <span id="thirdChoiceSpan"
											style="display: none; font-size: 12px; font-weight: 400 !important;">
											<label style="font-size: 12px; font-weight: 400;">
												Option for Selected """),_display_(/*87.34*/labels/*87.40*/.get(2)),format.raw/*87.47*/(""" """),format.raw/*87.48*/("""</label> <label class="radio-in-line"><input
												type="radio" name="thirdChoice" id="thirdChoice" value="fixed"
												checked> Fixed </label> <label class="radio-in-line"><input
												type="radio" name="thirdChoice" id="thirdChoice" value="omit"
												checked> Omit </label>
										</span>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step2 &#8594; <b>Set Instance Count</b>
							</div>
							<div class="panel-body">
								<div class="row ">
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Attribute Combination Count </label> <input
											type="number" placeholder="0" min="0" class="form-control"
											required name="attributeCombinationCount">
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Small </label> <input type="number" placeholder="0"
											min="0" class="form-control" required
											name="smallInstanceCount">
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Medium </label> <input type="number" placeholder="0"
											min="0" class="form-control" required
											name="mediumInstanceCount">
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Large </label> <input type="number" placeholder="0"
											min="0" class="form-control" required
											name="largeInstanceCount">
									</div>
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label>
										Prefixes for Member<i><span style="font-size:10px;">(Comma seperated prefixes)</span></i>
										</label>
										<input required class="form-control"
											placeholder="Enter Name PreFixes" type="text"
											name="namePrefixes" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			Step 3 &#8594; <b>Select Operation</b>
		</div>
		<div class="panel-body">
			<div class="row" style="margin-bottom: 10px;">
				<div class="col-lg-4">
					<button type="submit" value="continue" name="submitValue1"
						 class="btn btn-primary col-lg-12 btn-block">
						<b>Save and Go Back</b>
					</button>
				</div>
				<div class="col-lg-4">
					<button type="submit" value="more" name="submitValue1"
						 class="btn btn-success col-lg-12 btn-block">
						<b>Save and Add Another Group</b>
					</button>
				</div>
				<div class="col-lg-4">
					<a onClick="location.href = '"""),_display_(/*162.36*/routes/*162.42*/.Application.newFile()),format.raw/*162.64*/("""'" value="back" name="submitValue1"
						 class="btn btn-danger col-lg-12 btn-block">
						<b>Go Back</b>
					</a>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*170.3*/("""
"""),format.raw/*171.1*/("""</div>

""")))}),format.raw/*173.2*/("""
"""))}
  }

  def render(message:String,firsts:List[GraphEntityValue],seconds:List[GraphEntityValue],thirds:List[GraphEntityValue],labels:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(message,firsts,seconds,thirds,labels)

  def f:((String,List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (message,firsts,seconds,thirds,labels) => apply(message,firsts,seconds,thirds,labels)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/organisationProperty.scala.html
                  HASH: 4bf8013d6ad1a12554d40f66c6a9dc3a3192e5d5
                  MATRIX: 820->1|1066->144|1096->168|1117->181|1155->182|1183->184|1236->211|1250->217|1376->333|1416->334|1446->337|2137->1001|2152->1007|2180->1014|2305->1112|2320->1118|2348->1125|2655->1405|2691->1425|2731->1427|2772->1440|2815->1456|2829->1461|2861->1472|2905->1489|2919->1494|2951->1505|2993->1516|3033->1528|3276->1744|3291->1750|3319->1757|3348->1758|3817->2200|3832->2206|3860->2213|3996->2322|4011->2328|4039->2335|4378->2647|4416->2669|4456->2671|4497->2684|4540->2700|4555->2706|4587->2717|4631->2734|4646->2740|4678->2751|4720->2762|4760->2774|5004->2991|5019->2997|5047->3004|5076->3005|5549->3451|5564->3457|5592->3464|5705->3550|5720->3556|5748->3563|6082->3870|6118->3890|6157->3891|6198->3904|6241->3920|6255->3925|6287->3936|6317->3939|6331->3944|6363->3955|6417->3978|6457->3990|6700->4206|6715->4212|6743->4219|6772->4220|9562->6982|9578->6988|9622->7010|9819->7176|9849->7178|9891->7189
                  LINES: 26->1|33->5|35->8|35->8|35->8|36->9|37->10|37->10|37->10|37->10|38->11|57->30|57->30|57->30|59->32|59->32|59->32|64->37|64->37|64->37|65->38|65->38|65->38|65->38|66->39|66->39|66->39|66->39|67->40|70->43|70->43|70->43|70->43|79->52|79->52|79->52|82->55|82->55|82->55|87->60|87->60|87->60|88->61|88->61|88->61|88->61|89->62|89->62|89->62|89->62|90->63|93->66|93->66|93->66|93->66|102->75|102->75|102->75|103->76|103->76|103->76|108->81|108->81|108->81|109->82|109->82|109->82|109->82|109->82|109->82|109->82|110->83|111->84|114->87|114->87|114->87|114->87|189->162|189->162|189->162|197->170|198->171|200->173
                  -- GENERATED --
              */
          