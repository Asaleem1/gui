
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object actionProperty extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,List[GraphEntityValue],List[ActionPropertyFile],String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(message: String, firsts: List[GraphEntityValue], typeValues:
List[ActionPropertyFile], status: String):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*2.42*/(""" """),_display_(/*3.2*/main(message)/*3.15*/{_display_(Seq[Any](format.raw/*3.16*/("""
"""),format.raw/*4.1*/("""<div id="page-wrapper">
	"""),_display_(/*5.3*/helper/*5.9*/.form(action=routes.GroupController.addActionPropertyGroup(),'id->"myForm_property",'name->"myForm_property")/*5.118*/{_display_(Seq[Any](format.raw/*5.119*/("""
	"""),format.raw/*6.2*/("""<!-- ------ -->
	"""),_display_(/*7.3*/if(typeValues!=null && typeValues.size()!=0)/*7.47*/{_display_(Seq[Any](format.raw/*7.48*/("""
	"""),format.raw/*8.2*/("""<div class="row">
		<div class='col-lg-12'>
			<div class='panel panel-default'>
				<div class='panel-heading'>All Action(s)</div>
				<div class='panel-body' style='overflow: auto; height: 400px;'>
					<table class='table table-hover' id="actionTable">
						<thead>
							<th>Action Type</th>
							<th>Action Value</th>
							<th>Operations</th>
						</thead>
						<tbody>
							"""),_display_(/*20.9*/for(index <- 0 until typeValues.size) yield /*20.46*/{_display_(Seq[Any](format.raw/*20.47*/("""
							"""),format.raw/*21.8*/("""<tr id="tr"""),_display_(/*21.19*/index),format.raw/*21.24*/("""">
								<td>"""),_display_(/*22.14*/typeValues/*22.24*/.get(index).getActionType()),format.raw/*22.51*/("""</td>
								<td id="value_"""),_display_(/*23.24*/index),format.raw/*23.29*/("""">"""),_display_(/*23.32*/typeValues/*23.42*/.get(index).getActionValue()),format.raw/*23.70*/("""</td>
								<td>
									<div class='row'>
										<div class='col-lg-6'>
											<a
												onclick="openEditModal('"""),_display_(/*28.38*/{index}),format.raw/*28.45*/("""','"""),_display_(/*28.49*/typeValues/*28.59*/.get(index).getActionId()),format.raw/*28.84*/("""')"
												class=' btn btn-info btn-block'>Edit</a>
										</div>
										<div class='col-lg-6'>
											<a
												onclick="deleteItem('tr"""),_display_(/*33.37*/{index}),format.raw/*33.44*/("""','"""),_display_(/*33.48*/typeValues/*33.58*/.get(index).getActionId()),format.raw/*33.83*/("""')"
												class=' btn btn-danger btn-block'>Delete</a>
										</div>
									</div>
								</td>
							</tr>
							""")))}),format.raw/*39.9*/("""
						"""),format.raw/*40.7*/("""</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- ------ -->
	""")))}),format.raw/*47.3*/("""
	"""),format.raw/*48.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default"
				style="border: 0; margin-bottom: 10px; box-shadow: none;">
				<div class="panel-heading"
					style="font-size: 20px; text-align: center; margin-bottom: 10px;">
					<i><b>"""),_display_(/*54.13*/if(status=="update")/*54.33*/{_display_(Seq[Any](format.raw/*54.34*/("""Add More""")))}/*54.43*/else/*54.47*/{_display_(Seq[Any](format.raw/*54.48*/("""Create""")))}),format.raw/*54.55*/(""" """),format.raw/*54.56*/("""Action
							Property Group</b> </i>
				</div>

				<div class="row">

					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step1 &#8594; <b>Enter Value(s)&nbsp<span
									style="font-size: 10px; font-weight: normal"><i>(seperated
											by comma for multiple values)</i></span></b>
							</div>
							<div class="panel-body">
								"""),_display_(/*68.10*/for(first <- firsts) yield /*68.30*/ {_display_(Seq[Any](format.raw/*68.32*/("""

								"""),format.raw/*70.9*/("""<div class="row ">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label> Enter values for """),_display_(/*72.37*/first/*72.42*/.getValue()),format.raw/*72.53*/(""" """),format.raw/*72.54*/("""</label> <input
											type="text" class="form-control "
											name="value_"""),_display_(/*74.25*/first/*74.30*/.getValue()),format.raw/*74.41*/("""" id="value_"""),_display_(/*74.54*/first/*74.59*/.getValue()),format.raw/*74.70*/("""" />
									</div>
								</div>
								""")))}),format.raw/*77.10*/("""
							"""),format.raw/*78.8*/("""</div>
						</div>
					</div>
					<input type="hidden" name="status" value=""""),_display_(/*81.49*/status),format.raw/*81.55*/("""" />
				</div>
			</div>

		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			Step 2 &#8594; <b>Select Operation</b>
		</div>
		<div class="panel-body">
			<div class="row" style="margin-bottom: 10px;">
				<div class="col-lg-6">
					<button type="submit" value="continue" name="submitValue1"
						class="btn btn-primary col-lg-12 btn-block">
						<b>Save and Go Back</b>
					</button>
				</div>
				<div class="col-lg-6">
					<a type="submit"
						onclick="location.href = '"""),_display_(/*102.34*/routes/*102.40*/.Application.home()),format.raw/*102.59*/("""'"
						value="back" name="submitValue1"
						class="btn btn-danger col-lg-12 btn-block"> <b>Go Back</b>
					</a>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*110.3*/("""
"""),format.raw/*111.1*/("""</div>
<div id="modalDiv">
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Edit Action</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-12">
							<label>New Action Value</label> <input type="text"
								class="form-control " name="actionValue" id='actionValue' />
						</div>
					</div>
				</div>
				<input type="hidden" name="actionId" id="actionId" /> 
				<input type="hidden" name="rowId" id="rowId" />
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-primary"
						onclick="updateAction()">Update Action</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function updateAction() """),format.raw/*145.26*/("""{"""),format.raw/*145.27*/("""
		"""),format.raw/*146.3*/("""var actionId = $("#actionId").val();
		var actionValue = $("#actionValue").val();
		var rowId = $("#rowId").val();
		$.ajax("""),format.raw/*149.10*/("""{"""),format.raw/*149.11*/("""
			"""),format.raw/*150.4*/("""type : "POST",
			url : "/updateActionValue",
			data : """),format.raw/*152.11*/("""{"""),format.raw/*152.12*/("""
				"""),format.raw/*153.5*/(""""actionId" : actionId,
				"actionValue" : actionValue
			"""),format.raw/*155.4*/("""}"""),format.raw/*155.5*/(""",
			success : function(data) """),format.raw/*156.29*/("""{"""),format.raw/*156.30*/("""
				"""),format.raw/*157.5*/("""$("#value_" + rowId).html(actionValue);
				$("#myModal").modal('toggle');

			"""),format.raw/*160.4*/("""}"""),format.raw/*160.5*/("""
		"""),format.raw/*161.3*/("""}"""),format.raw/*161.4*/(""");
	"""),format.raw/*162.2*/("""}"""),format.raw/*162.3*/("""

	"""),format.raw/*164.2*/("""function openEditModal(rowId, id) """),format.raw/*164.36*/("""{"""),format.raw/*164.37*/("""
		"""),format.raw/*165.3*/("""if (id != null) """),format.raw/*165.19*/("""{"""),format.raw/*165.20*/("""
			"""),format.raw/*166.4*/("""$("#rowId").val(rowId);
			$("#actionId").val(id);
			$("#actionValue").val();
			$("#myModal").modal('show');
		"""),format.raw/*170.3*/("""}"""),format.raw/*170.4*/("""

	"""),format.raw/*172.2*/("""}"""),format.raw/*172.3*/("""
	"""),format.raw/*173.2*/("""function deleteItem(rowId,id) """),format.raw/*173.32*/("""{"""),format.raw/*173.33*/("""
		"""),format.raw/*174.3*/("""if (id != null) """),format.raw/*174.19*/("""{"""),format.raw/*174.20*/("""
			"""),format.raw/*175.4*/("""var option =  confirm("Are you sure to delete This Action Type?");
			
			if(option)"""),format.raw/*177.14*/("""{"""),format.raw/*177.15*/("""
				"""),format.raw/*178.5*/("""$.ajax("""),format.raw/*178.12*/("""{"""),format.raw/*178.13*/("""
					"""),format.raw/*179.6*/("""type : "POST",
					url : "/deleteActionValue",
					data : """),format.raw/*181.13*/("""{"""),format.raw/*181.14*/("""
						"""),format.raw/*182.7*/(""""actionId" : id,
					"""),format.raw/*183.6*/("""}"""),format.raw/*183.7*/(""",
					success : function(data) """),format.raw/*184.31*/("""{"""),format.raw/*184.32*/("""
						"""),format.raw/*185.7*/("""$("#"+rowId).remove();
						document.getElementById("actionTable").deleteRow(rowId);
					"""),format.raw/*187.6*/("""}"""),format.raw/*187.7*/("""
				"""),format.raw/*188.5*/("""}"""),format.raw/*188.6*/(""");		
			"""),format.raw/*189.4*/("""}"""),format.raw/*189.5*/("""
		
		"""),format.raw/*191.3*/("""}"""),format.raw/*191.4*/("""
	"""),format.raw/*192.2*/("""}"""),format.raw/*192.3*/("""
"""),format.raw/*193.1*/("""</script>

""")))}),format.raw/*195.2*/("""
"""))}
  }

  def render(message:String,firsts:List[GraphEntityValue],typeValues:List[ActionPropertyFile],status:String): play.twirl.api.HtmlFormat.Appendable = apply(message,firsts,typeValues,status)

  def f:((String,List[GraphEntityValue],List[ActionPropertyFile],String) => play.twirl.api.HtmlFormat.Appendable) = (message,firsts,typeValues,status) => apply(message,firsts,typeValues,status)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:47 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/actionProperty.scala.html
                  HASH: 61ae53910d10bd1145032b805df804308bc04837
                  MATRIX: 787->1|994->105|1021->125|1042->138|1080->139|1108->141|1160->168|1173->174|1291->283|1330->284|1359->287|1403->306|1455->350|1493->351|1522->354|1951->757|2004->794|2043->795|2079->804|2117->815|2143->820|2187->837|2206->847|2254->874|2311->904|2337->909|2367->912|2386->922|2435->950|2597->1085|2625->1092|2656->1096|2675->1106|2721->1131|2910->1293|2938->1300|2969->1304|2988->1314|3034->1339|3199->1474|3234->1482|3351->1569|3381->1572|3672->1836|3701->1856|3740->1857|3768->1866|3781->1870|3820->1871|3858->1878|3887->1879|4334->2299|4370->2319|4410->2321|4449->2333|4595->2452|4609->2457|4641->2468|4670->2469|4784->2556|4798->2561|4830->2572|4870->2585|4884->2590|4916->2601|4995->2649|5031->2658|5141->2741|5168->2747|5735->3286|5751->3292|5792->3311|5988->3476|6018->3478|7202->4633|7232->4634|7264->4638|7420->4765|7450->4766|7483->4771|7570->4829|7600->4830|7634->4836|7722->4896|7751->4897|7811->4928|7841->4929|7875->4935|7985->5017|8014->5018|8046->5022|8075->5023|8108->5028|8137->5029|8170->5034|8233->5068|8263->5069|8295->5073|8340->5089|8370->5090|8403->5095|8548->5212|8577->5213|8610->5218|8639->5219|8670->5222|8729->5252|8759->5253|8791->5257|8836->5273|8866->5274|8899->5279|9014->5365|9044->5366|9078->5372|9114->5379|9144->5380|9179->5387|9270->5449|9300->5450|9336->5458|9387->5481|9416->5482|9478->5515|9508->5516|9544->5524|9665->5617|9694->5618|9728->5624|9757->5625|9794->5634|9823->5635|9859->5643|9888->5644|9919->5647|9948->5648|9978->5650|10023->5664
                  LINES: 26->1|30->2|30->3|30->3|30->3|31->4|32->5|32->5|32->5|32->5|33->6|34->7|34->7|34->7|35->8|47->20|47->20|47->20|48->21|48->21|48->21|49->22|49->22|49->22|50->23|50->23|50->23|50->23|50->23|55->28|55->28|55->28|55->28|55->28|60->33|60->33|60->33|60->33|60->33|66->39|67->40|74->47|75->48|81->54|81->54|81->54|81->54|81->54|81->54|81->54|81->54|95->68|95->68|95->68|97->70|99->72|99->72|99->72|99->72|101->74|101->74|101->74|101->74|101->74|101->74|104->77|105->78|108->81|108->81|129->102|129->102|129->102|137->110|138->111|172->145|172->145|173->146|176->149|176->149|177->150|179->152|179->152|180->153|182->155|182->155|183->156|183->156|184->157|187->160|187->160|188->161|188->161|189->162|189->162|191->164|191->164|191->164|192->165|192->165|192->165|193->166|197->170|197->170|199->172|199->172|200->173|200->173|200->173|201->174|201->174|201->174|202->175|204->177|204->177|205->178|205->178|205->178|206->179|208->181|208->181|209->182|210->183|210->183|211->184|211->184|212->185|214->187|214->187|215->188|215->188|216->189|216->189|218->191|218->191|219->192|219->192|220->193|222->195
                  -- GENERATED --
              */
          