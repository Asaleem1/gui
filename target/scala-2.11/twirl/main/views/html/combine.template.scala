
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object combine extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template15[String,List[Rule],List[RuleGroup],List[GraphEntityValue],List[Permission],List[CombiningAlgorithm],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String, rules: List[Rule], groups: List[RuleGroup],
actionTypes: List[GraphEntityValue], permissions: List[Permission],
combiningAlgorithms:List[CombiningAlgorithm],
assetConfidentialities:List[GraphEntityValue],
assetIntegrities:List[GraphEntityValue],
assetTypes:List[GraphEntityValue],
memberFunctions:List[GraphEntityValue],
memberLevels:List[GraphEntityValue], memberRoles:List[GraphEntityValue],
organisations:List[GraphEntityValue],
organisationRoles:List[GraphEntityValue],
organisationSectors:List[GraphEntityValue] ):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*11.45*/(""" 

"""),_display_(/*14.2*/main(title)/*14.13*/{_display_(Seq[Any](format.raw/*14.14*/("""

"""),format.raw/*16.1*/("""<div id="page-wrapper">
	"""),_display_(/*17.3*/helper/*17.9*/.form(action=routes.RuleController.submitCombineRule(),'id->"combineForm",'name->"combineForm")/*17.104*/{_display_(Seq[Any](format.raw/*17.105*/("""

	"""),format.raw/*19.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default" style="border: 0; box-shadow: none;">
				<div class="panel-heading"
					style="font-size: 20px; text-align: center; margin-bottom: 10px;">
					<i><b>Add New Group</b></i>(<span
						style="font-size: 12px; color: #428bca"> <label
						class="checkbox-inline"><input type="checkbox"
							value="true" name="isBaseGroup">Mark as <b>BaseGroup?</b>
					</label>
					</span> ) <input class="form-control" placeholder="Enter Group Name"
						type="text" name="ruleGroupName" />
				</div>
				<div class="row">
					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step1 &#8594; <b>Select Rules / Group</b>
							</div>
							<div class="panel-body">
								<div class="row ">
									<div class="col-lg-12">
										<label> Choose Rules To Combine </label> <select
											class="form-control" name="ruleId[]" id="ruleId" multiple>
											"""),_display_(/*43.13*/for(rule <- rules) yield /*43.31*/ {_display_(Seq[Any](format.raw/*43.33*/("""
											"""),format.raw/*44.12*/("""<option value=""""),_display_(/*44.28*/rule/*44.32*/.getruleId()),format.raw/*44.44*/("""">
												"""),_display_(/*45.14*/rule/*45.18*/.getruleName()),format.raw/*45.32*/("""</option> """)))}),format.raw/*45.43*/("""
										"""),format.raw/*46.11*/("""</select>
									</div>
									<div class="col-lg-12">
										<label> Choose Groups To Combine </label> <select
											class="form-control" name="groupId[]" id="groupId" multiple>
											"""),_display_(/*51.13*/for(group <- groups) yield /*51.33*/ {_display_(Seq[Any](format.raw/*51.35*/("""
											"""),format.raw/*52.12*/("""<option value=""""),_display_(/*52.28*/group/*52.33*/.getRuleGroupId()),format.raw/*52.50*/("""">
												"""),_display_(/*53.14*/group/*53.19*/.getRuleGroupName()),format.raw/*53.38*/("""</option> """)))}),format.raw/*53.49*/("""
										"""),format.raw/*54.11*/("""</select>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								Step2 &#8594; <b>Select Combining Algorithm</b>
							</div>
							<div class="panel-body">
								<div class="row ">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label> Select Algorithm </label> <select class="form-control"
											name="combiningAlgorithm" id="combiningAlgorithm">
											"""),_display_(/*68.13*/for(combiningAlgorithm <- combiningAlgorithms) yield /*68.59*/ {_display_(Seq[Any](format.raw/*68.61*/("""
											"""),format.raw/*69.12*/("""<option value=""""),_display_(/*69.28*/combiningAlgorithm/*69.46*/.getCombiningAlgorithmId()),format.raw/*69.72*/("""">
												"""),_display_(/*70.14*/combiningAlgorithm/*70.32*/.getCombiningAlgorithmName()),format.raw/*70.60*/("""</option> """)))}),format.raw/*70.71*/("""
										"""),format.raw/*71.11*/("""</select>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								Step3 &#8594; <b>Select Action</b> <label
									class="checkbox-inline"> <input type="checkbox"
									value="inuse" name="actionTypeOption"
									onchange="changeDivVisibility('actionDiv')"> In Use
								</label>
							</div>
							<div class="panel-body" id="actionDiv" style="display: none;">
								<div class="row ">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label> Select Action Type </label> <label
											class="checkbox-inline"> <input type="checkbox"
											value="inuse" name="actionTypeOption"
											onchange="changeOption('actionType','actionTypeOption'); checkOption('actionTypeOption','actionValuesDiv')">
											In Use
										</label> <select onchange="fetchRows('actionType','actionValuesDiv')"
											style="margin-bottom: 10px;" class="form-control"
											name="actionType" disabled="disabled" id="actionType">
											<option value="" disabled="disabled" selected>Choose Action Value</option>
											"""),_display_(/*96.13*/for(actionType<- actionTypes) yield /*96.42*/{_display_(Seq[Any](format.raw/*96.43*/("""
											"""),format.raw/*97.12*/("""<option value=""""),_display_(/*97.28*/actionType/*97.38*/.getValue()),format.raw/*97.49*/("""">"""),_display_(/*97.52*/actionType/*97.62*/.getValue()),format.raw/*97.73*/("""</option>
											""")))}),format.raw/*98.13*/("""
										"""),format.raw/*99.11*/("""</select>
										<div id="actionValuesDiv" style="display: none;"></div>
									</div>
								</div>
							</div>

						</div>


					</div>
					<div class="col-lg-8">
						<div class="col-lg-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									Step4 &#8594; <b>Create Subject Combination</b> <label
										class="checkbox-inline"><input type="checkbox"
										value="inuse" name="subjectOption"
										onchange="changeDivVisibility('subjDiv')">In Use</label>
								</div>
								<div class="panel-body" style="display: none;" id="subjDiv">
									<div class="row">
										<div class="col-lg-12" style="margin-bottom: 10px;">
											<label style="margin-right: 5px">Category</label> <label
												class="radio-inline"><input type="radio" checked
												value="organisation" name="subjectType"
												onChange="changeSubject('organisation');">Organisation</label>
											<label class="radio-inline"><input type="radio"
												value="member" name="subjectType"
												onChange="changeSubject('member');">Member</label>
										</div>
									</div>

									<div id="organisation_div">
										<div class="row">
											<div class="col-lg-12" style="margin-bottom: 10px;">
												<label> Select Organisation </label> <label
													class="checkbox-inline"><input type="checkbox"
													value="inuse" name="organisationOption"
													onchange="changeOption('organisationId','organisationOption')">In
													Use</label> <select disabled class="form-control" multiple
													name="organisationId[]" id="organisationId">
													"""),_display_(/*140.15*/for(organisation <- organisations) yield /*140.49*/ {_display_(Seq[Any](format.raw/*140.51*/("""
													"""),format.raw/*141.14*/("""<option value=""""),_display_(/*141.30*/organisation/*141.42*/.getGraphEntityId()),format.raw/*141.61*/("""">
														"""),_display_(/*142.16*/organisation/*142.28*/.getValue()),format.raw/*142.39*/("""</option> """)))}),format.raw/*142.50*/("""
												"""),format.raw/*143.13*/("""</select>
											</div>
										</div>

										<div class="row">
											<div class="col-lg-12" style="margin-bottom: 10px;">
												<label> Select Organisation Role </label> <label
													class="checkbox-inline"><input type="checkbox"
													value="inuse" name="organisationRoleOption"
													onchange="changeOption('organisationRoleId','organisationRoleOption')">In
													Use</label> <select class="form-control" multiple
													name="organisationRoleId[]" id="organisationRoleId"
													disabled="disabled"> """),_display_(/*155.36*/for(organisationRole <-
													organisationRoles) yield /*156.32*/ {_display_(Seq[Any](format.raw/*156.34*/("""
													"""),format.raw/*157.14*/("""<option value=""""),_display_(/*157.30*/organisationRole/*157.46*/.getGraphEntityId()),format.raw/*157.65*/("""">
														"""),_display_(/*158.16*/organisationRole/*158.32*/.getValue()),format.raw/*158.43*/("""</option> """)))}),format.raw/*158.54*/("""
												"""),format.raw/*159.13*/("""</select>
											</div>
										</div>

										<div class="row ">
											<div class="col-lg-12" style="margin-bottom: 10px;">
												<label> Select Organisation Sector </label> <label
													class="checkbox-inline"><input type="checkbox"
													value="inuse" name="organisationSecOption"
													onchange="changeOption('organisationSectorId','organisationSecOption')">In
													Use</label> <select class="form-control"
													name="organisationSectorId[]" multiple
													id="organisationSectorId" disabled="disabled">
													"""),_display_(/*172.15*/for(organisationSector <- organisationSectors) yield /*172.61*/ {_display_(Seq[Any](format.raw/*172.63*/("""
													"""),format.raw/*173.14*/("""<option value=""""),_display_(/*173.30*/organisationSector/*173.48*/.getGraphEntityId()),format.raw/*173.67*/("""">
														"""),_display_(/*174.16*/organisationSector/*174.34*/.getValue()),format.raw/*174.45*/("""</option> """)))}),format.raw/*174.56*/("""
												"""),format.raw/*175.13*/("""</select>
											</div>
										</div>

									</div>

									<div id="member_div" style="display: none;">
										<div class="row ">
											<div class="col-lg-12" style="margin-bottom: 10px;">
												<label> Select Member Function </label> <label
													class="checkbox-inline"><input type="checkbox"
													value="inuse" name="memberFunctionOption"
													onchange="changeOption('memberFunctionId','memberFunctionOption')">In
													Use</label> <select disabled="disabled" class="form-control"
													name="memberFunctionId[]" id="memberFunctionId" multiple>
													"""),_display_(/*190.15*/for(memberFunction <- memberFunctions) yield /*190.53*/ {_display_(Seq[Any](format.raw/*190.55*/("""
													"""),format.raw/*191.14*/("""<option value=""""),_display_(/*191.30*/memberFunction/*191.44*/.getGraphEntityId()),format.raw/*191.63*/("""">
														"""),_display_(/*192.16*/memberFunction/*192.30*/.getValue()),format.raw/*192.41*/("""</option> """)))}),format.raw/*192.52*/("""
												"""),format.raw/*193.13*/("""</select>
											</div>
										</div>
										<div class="row ">
											<div class="col-lg-12" style="margin-bottom: 10px;">
												<label> Select Member Role </label> <label
													class="checkbox-inline"><input type="checkbox"
													value="inuse" name="memberRoleOption"
													onchange="changeOption('memberRoleId','memberRoleOption')">In
													Use</label> <select class="form-control" name="memberRoleId[]"
													id="memberRoleId" disabled="disabled" multiple>
													"""),_display_(/*204.15*/for(memberRole <- memberRoles) yield /*204.45*/ {_display_(Seq[Any](format.raw/*204.47*/("""
													"""),format.raw/*205.14*/("""<option value=""""),_display_(/*205.30*/memberRole/*205.40*/.getGraphEntityId()),format.raw/*205.59*/("""">
														"""),_display_(/*206.16*/memberRole/*206.26*/.getValue()),format.raw/*206.37*/("""</option> """)))}),format.raw/*206.48*/("""
												"""),format.raw/*207.13*/("""</select>
											</div>
										</div>
										<div class="row ">
											<div class="col-lg-12" style="margin-bottom: 10px;">
												<label> Select Member Level </label> <label
													class="checkbox-inline"><input type="checkbox"
													value="inuse" name="memberLevelOption"
													onchange="changeOption('memberLevelId','memberLevelOption')">In
													Use</label> <select class="form-control" name="memberLevelId[]"
													id="memberLevelId" disabled="disabled" multiple>
													"""),_display_(/*218.15*/for(memberLevel <- memberLevels) yield /*218.47*/ {_display_(Seq[Any](format.raw/*218.49*/("""
													"""),format.raw/*219.14*/("""<option value=""""),_display_(/*219.30*/memberLevel/*219.41*/.getGraphEntityId()),format.raw/*219.60*/("""">
														"""),_display_(/*220.16*/memberLevel/*220.27*/.getValue()),format.raw/*220.38*/("""</option> """)))}),format.raw/*220.49*/("""
												"""),format.raw/*221.13*/("""</select>
											</div>
										</div>
									</div>

								</div>

							</div>
						</div>
						<div class="col-lg-6">
							<div class="panel panel-default">
								<div class="panel-heading"
									style="padding-right: 3px; padding-left: 6px;">
									Step5 &#8594; <b>Create Resource Combination</b> <label
										class="checkbox-inline"><input type="checkbox"
										value="inuse" name="resourceOption"
										onchange="changeDivVisibility('resDiv')">In Use</label>
								</div>
								<div class="panel-body"
									style="display: none; padding-bottom: 50px;" id="resDiv">

									<div class="row ">
										<div class="col-lg-12" style="margin-bottom: 10px;">
											<label> Select Asset Confidentiality </label> <label
												class="checkbox-inline"> <input type="checkbox"
												value="inuse" name="assetConfidentialityOption"
												onchange="changeOption('assetConfidentialityId','assetConfidentialityOption')">
												In Use
											</label> <select class="form-control" id="assetConfidentialityId"
												disabled="disabled" name="assetConfidentialityId[]" multiple>
												"""),_display_(/*251.14*/for(assetConfidentiality <- assetConfidentialities) yield /*251.65*/ {_display_(Seq[Any](format.raw/*251.67*/("""
												"""),format.raw/*252.13*/("""<option value=""""),_display_(/*252.29*/assetConfidentiality/*252.49*/.getGraphEntityId()),format.raw/*252.68*/("""">
													"""),_display_(/*253.15*/assetConfidentiality/*253.35*/.getValue()),format.raw/*253.46*/("""</option> """)))}),format.raw/*253.57*/("""
											"""),format.raw/*254.12*/("""</select>
										</div>
									</div>
									<div class="row ">
										<div class="col-lg-12" style="margin-bottom: 10px;">
											<label> Select Asset Integrity </label> <label
												class="checkbox-inline"> <input type="checkbox"
												value="inuse" name="assetIntegrityOption"
												onchange="changeOption('assetIntegrityId','assetIntegrityOption')">
												In Use
											</label> <select class="form-control" id="assetIntegrityId"
												disabled="disabed" name="assetIntegrityId[]" multiple>
												"""),_display_(/*266.14*/for(assetIntegrity <- assetIntegrities) yield /*266.53*/ {_display_(Seq[Any](format.raw/*266.55*/("""
												"""),format.raw/*267.13*/("""<option value=""""),_display_(/*267.29*/assetIntegrity/*267.43*/.getGraphEntityId()),format.raw/*267.62*/("""">
													"""),_display_(/*268.15*/assetIntegrity/*268.29*/.getValue()),format.raw/*268.40*/("""</option> """)))}),format.raw/*268.51*/("""
											"""),format.raw/*269.12*/("""</select>
										</div>
									</div>
									<div class="row ">
										<div class="col-lg-12" style="margin-bottom: 10px;">
											<label> Select Asset Type </label> <label
												class="checkbox-inline"> <input type="checkbox"
												value="inuse" name="assetTypeOption"
												onchange="changeOption('assetTypeId','assetTypeOption')">
												In Use
											</label> <select class="form-control" id="assetTypeId"
												disabled="disabled" name="assetTypeId[]" multiple>
												"""),_display_(/*281.14*/for(assetType <- assetTypes) yield /*281.42*/{_display_(Seq[Any](format.raw/*281.43*/("""
												"""),format.raw/*282.13*/("""<option value=""""),_display_(/*282.29*/assetType/*282.38*/.getGraphEntityId()),format.raw/*282.57*/("""">"""),_display_(/*282.60*/assetType/*282.69*/.getValue()),format.raw/*282.80*/("""</option>
												""")))}),format.raw/*283.14*/("""
											"""),format.raw/*284.12*/("""</select>
										</div>
									</div>

								</div>

							</div>

						</div>
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									Step6 &#8594; <b>Group Description(Optional)</b>
								</div>
								<div class="panel-body" style="padding-bottom: 35px;">
									<div class="row ">
										<div class="col-lg-12" style="margin-bottom: 10px;">
											<label> Group Description </label>
											<textarea class="form-control" name="groupDescription">
								</textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- col8 ends here -->

				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					Step 7 &#8594; <b>Select Operation</b>
				</div>
				<div class="panel-body">
					<div class="row" style="margin-bottom: 10px;">
						<div class="col-lg-4">
							<button type="submit" value="continue" name="submitValue1"
								id="submitValue1" class="btn btn-primary col-lg-12 btn-block">
								<b>Save and Continue</b>
							</button>
						</div>
						<div class="col-lg-4">
							<button type="submit" value="more" name="submitValue2"
								id="submitValue2" class="btn btn-success col-lg-12 btn-block">
								<b>Save and Add Another Group</b>
							</button>
						</div>
						<div class="col-lg-4">
							<a onclick="location.href = '"""),_display_(/*333.38*/routes/*333.44*/.Application.home()),format.raw/*333.63*/("""'"
								class="btn btn-danger col-lg-12 btn-block"> <b>Go Back</b>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		""")))}),format.raw/*342.4*/("""
	"""),format.raw/*343.2*/("""</div>
	<script>
		function fetchRows(selectId, divId) """),format.raw/*345.39*/("""{"""),format.raw/*345.40*/("""

			"""),format.raw/*347.4*/("""var actionType = $("#" + selectId + " option:selected").val();
			if (actionType != "") """),format.raw/*348.26*/("""{"""),format.raw/*348.27*/("""
				"""),format.raw/*349.5*/("""$
						.ajax("""),format.raw/*350.13*/("""{"""),format.raw/*350.14*/("""
							"""),format.raw/*351.8*/("""type : "POST",
							url : "/fetchActionValues",
							data : """),format.raw/*353.15*/("""{"""),format.raw/*353.16*/("""
								"""),format.raw/*354.9*/(""""actionType" : actionType
							"""),format.raw/*355.8*/("""}"""),format.raw/*355.9*/(""",
							success : function(data) """),format.raw/*356.33*/("""{"""),format.raw/*356.34*/("""
								"""),format.raw/*357.9*/("""values = JSON.parse(data);
								var html_content = "<select class='form-control' name='actionValueId[]'";
					html_content +=	" id='actionValueId' multiple>";
								for (var i = 0; i < values.length; i++) """),format.raw/*360.49*/("""{"""),format.raw/*360.50*/("""
									"""),format.raw/*361.10*/("""html_content += "<option value='"+values[i].actionId+"'>";
									html_content += values[i].actionValue
											+ "</option> ";
								"""),format.raw/*364.9*/("""}"""),format.raw/*364.10*/("""

								"""),format.raw/*366.9*/("""html_content += "</select>";
								$("#" + divId).html(html_content);
								$("#" + divId).show(400);
							"""),format.raw/*369.8*/("""}"""),format.raw/*369.9*/("""
						"""),format.raw/*370.7*/("""}"""),format.raw/*370.8*/(""");
			"""),format.raw/*371.4*/("""}"""),format.raw/*371.5*/("""
		"""),format.raw/*372.3*/("""}"""),format.raw/*372.4*/("""
	"""),format.raw/*373.2*/("""</script>
	""")))}))}
  }

  def render(title:String,rules:List[Rule],groups:List[RuleGroup],actionTypes:List[GraphEntityValue],permissions:List[Permission],combiningAlgorithms:List[CombiningAlgorithm],assetConfidentialities:List[GraphEntityValue],assetIntegrities:List[GraphEntityValue],assetTypes:List[GraphEntityValue],memberFunctions:List[GraphEntityValue],memberLevels:List[GraphEntityValue],memberRoles:List[GraphEntityValue],organisations:List[GraphEntityValue],organisationRoles:List[GraphEntityValue],organisationSectors:List[GraphEntityValue]): play.twirl.api.HtmlFormat.Appendable = apply(title,rules,groups,actionTypes,permissions,combiningAlgorithms,assetConfidentialities,assetIntegrities,assetTypes,memberFunctions,memberLevels,memberRoles,organisations,organisationRoles,organisationSectors)

  def f:((String,List[Rule],List[RuleGroup],List[GraphEntityValue],List[Permission],List[CombiningAlgorithm],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue]) => play.twirl.api.HtmlFormat.Appendable) = (title,rules,groups,actionTypes,permissions,combiningAlgorithms,assetConfidentialities,assetIntegrities,assetTypes,memberFunctions,memberLevels,memberRoles,organisations,organisationRoles,organisationSectors) => apply(title,rules,groups,actionTypes,permissions,combiningAlgorithms,assetConfidentialities,assetIntegrities,assetTypes,memberFunctions,memberLevels,memberRoles,organisations,organisationRoles,organisationSectors)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:47 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/combine.scala.html
                  HASH: 32b07317e2f58ed453fa55de297f345ccc7168bc
                  MATRIX: 1025->1|1673->545|1705->569|1725->580|1764->581|1795->585|1848->612|1862->618|1967->713|2007->714|2039->719|3085->1738|3119->1756|3159->1758|3200->1771|3243->1787|3256->1791|3289->1803|3333->1820|3346->1824|3381->1838|3423->1849|3463->1861|3698->2069|3734->2089|3774->2091|3815->2104|3858->2120|3872->2125|3910->2142|3954->2159|3968->2164|4008->2183|4050->2194|4090->2206|4612->2701|4674->2747|4714->2749|4755->2762|4798->2778|4825->2796|4872->2822|4916->2839|4943->2857|4992->2885|5034->2896|5074->2908|6278->4085|6323->4114|6362->4115|6403->4128|6446->4144|6465->4154|6497->4165|6527->4168|6546->4178|6578->4189|6632->4212|6672->4224|8410->5934|8461->5968|8502->5970|8546->5985|8590->6001|8612->6013|8653->6032|8700->6051|8722->6063|8755->6074|8798->6085|8841->6099|9447->6677|9520->6733|9561->6735|9605->6750|9649->6766|9675->6782|9716->6801|9763->6820|9789->6836|9822->6847|9865->6858|9908->6872|10535->7471|10598->7517|10639->7519|10683->7534|10727->7550|10755->7568|10796->7587|10843->7606|10871->7624|10904->7635|10947->7646|10990->7660|11659->8301|11714->8339|11755->8341|11799->8356|11843->8372|11867->8386|11908->8405|11955->8424|11979->8438|12012->8449|12055->8460|12098->8474|12667->9015|12714->9045|12755->9047|12799->9062|12843->9078|12863->9088|12904->9107|12951->9126|12971->9136|13004->9147|13047->9158|13090->9172|13665->9719|13714->9751|13755->9753|13799->9768|13843->9784|13864->9795|13905->9814|13952->9833|13973->9844|14006->9855|14049->9866|14092->9880|15316->11076|15384->11127|15425->11129|15468->11143|15512->11159|15542->11179|15583->11198|15629->11216|15659->11236|15692->11247|15735->11258|15777->11271|16373->11839|16429->11878|16470->11880|16513->11894|16557->11910|16581->11924|16622->11943|16668->11961|16692->11975|16725->11986|16768->11997|16810->12010|17377->12549|17422->12577|17462->12578|17505->12592|17549->12608|17568->12617|17609->12636|17640->12639|17659->12648|17692->12659|17748->12683|17790->12696|19294->14172|19310->14178|19351->14197|19532->14347|19563->14350|19649->14407|19679->14408|19714->14415|19832->14504|19862->14505|19896->14511|19940->14526|19970->14527|20007->14536|20102->14602|20132->14603|20170->14613|20232->14647|20261->14648|20325->14683|20355->14684|20393->14694|20636->14908|20666->14909|20706->14920|20878->15064|20908->15065|20948->15077|21092->15193|21121->15194|21157->15202|21186->15203|21221->15210|21250->15211|21282->15215|21311->15216|21342->15219
                  LINES: 26->1|39->11|41->14|41->14|41->14|43->16|44->17|44->17|44->17|44->17|46->19|70->43|70->43|70->43|71->44|71->44|71->44|71->44|72->45|72->45|72->45|72->45|73->46|78->51|78->51|78->51|79->52|79->52|79->52|79->52|80->53|80->53|80->53|80->53|81->54|95->68|95->68|95->68|96->69|96->69|96->69|96->69|97->70|97->70|97->70|97->70|98->71|123->96|123->96|123->96|124->97|124->97|124->97|124->97|124->97|124->97|124->97|125->98|126->99|167->140|167->140|167->140|168->141|168->141|168->141|168->141|169->142|169->142|169->142|169->142|170->143|182->155|183->156|183->156|184->157|184->157|184->157|184->157|185->158|185->158|185->158|185->158|186->159|199->172|199->172|199->172|200->173|200->173|200->173|200->173|201->174|201->174|201->174|201->174|202->175|217->190|217->190|217->190|218->191|218->191|218->191|218->191|219->192|219->192|219->192|219->192|220->193|231->204|231->204|231->204|232->205|232->205|232->205|232->205|233->206|233->206|233->206|233->206|234->207|245->218|245->218|245->218|246->219|246->219|246->219|246->219|247->220|247->220|247->220|247->220|248->221|278->251|278->251|278->251|279->252|279->252|279->252|279->252|280->253|280->253|280->253|280->253|281->254|293->266|293->266|293->266|294->267|294->267|294->267|294->267|295->268|295->268|295->268|295->268|296->269|308->281|308->281|308->281|309->282|309->282|309->282|309->282|309->282|309->282|309->282|310->283|311->284|360->333|360->333|360->333|369->342|370->343|372->345|372->345|374->347|375->348|375->348|376->349|377->350|377->350|378->351|380->353|380->353|381->354|382->355|382->355|383->356|383->356|384->357|387->360|387->360|388->361|391->364|391->364|393->366|396->369|396->369|397->370|397->370|398->371|398->371|399->372|399->372|400->373
                  -- GENERATED --
              */
          