
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object addRules extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template12[String,List[Permission],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(message: String, 
permissions: List[Permission], 
actionTypes:List[GraphEntityValue], 
assetConfidentialities: List[GraphEntityValue],
assetIntegrities: List[GraphEntityValue], 
assetTypes:List[GraphEntityValue], 
memberFunctions: List[GraphEntityValue],
memberLevels: List[GraphEntityValue], 
memberRoles:List[GraphEntityValue], 
organisations: List[GraphEntityValue],
organisationRoles: List[GraphEntityValue], 
organisationSectors:List[GraphEntityValue]
):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*13.2*/(""" 

"""),_display_(/*16.2*/main(message)/*16.15*/{_display_(Seq[Any](format.raw/*16.16*/("""
"""),format.raw/*17.1*/("""<div id="page-wrapper">
	"""),_display_(/*18.3*/helper/*18.9*/.form(action=
	routes.RuleController.addRule(),'id->"myForm",'name->"myForm")/*19.64*/{_display_(Seq[Any](format.raw/*19.65*/("""

	"""),format.raw/*21.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default"
				style="border: 0; margin-bottom: 10px; box-shadow: none;">
				<div class="panel-heading"
					style="font-size: 20px; text-align: center; margin-bottom: 10px;">
					<i><b>Add New Rule*</b> <input required class="form-control"
						placeholder="Enter Rule Name" type="text" name="ruleName" /> </i>
				</div>

				<div class="row">
					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step1 &#8594; <b>Create Subject Combination</b>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label style="margin-right: 5px">Category</label> <label
											class="radio-inline"><input type="radio" checked
											value="organisation" name="subjectType"
											onChange="changeSubject('organisation');">Organisation</label>
										<label class="radio-inline"><input type="radio"
											value="member" name="subjectType"
											onChange="changeSubject('member');">Member</label>
									</div>
								</div>
								<div id="organisation_div">

									<div class="row">
										<div class="col-lg-12" style="margin-bottom: 10px;">
											<label> Select Organisation </label> <label
												class="checkbox-inline"><input type="checkbox"
												value="inuse" name="organisationOption"
												onchange="changeOption('organisationId','organisationOption')">In
												Use</label> <select disabled class="form-control" multiple
												name="organisationId[]" id="organisationId">
												"""),_display_(/*59.14*/for(organisation <- organisations) yield /*59.48*/ {_display_(Seq[Any](format.raw/*59.50*/("""
												"""),format.raw/*60.13*/("""<option value=""""),_display_(/*60.29*/organisation/*60.41*/.getGraphEntityId()),format.raw/*60.60*/("""">
													"""),_display_(/*61.15*/organisation/*61.27*/.getValue()),format.raw/*61.38*/("""</option> """)))}),format.raw/*61.49*/("""
											"""),format.raw/*62.12*/("""</select>
										</div>
									</div>

									<div class="row">
										<div class="col-lg-12" style="margin-bottom: 10px;">
											<label> Select Organisation Role </label> <label
												class="checkbox-inline"><input type="checkbox"
												value="inuse" name="organisationRoleOption"
												onchange="changeOption('organisationRoleId','organisationRoleOption')">In
												Use</label> <select class="form-control" multiple
												name="organisationRoleId[]" id="organisationRoleId"
												disabled="disabled"> """),_display_(/*74.35*/for(organisationRole <-
												organisationRoles) yield /*75.31*/ {_display_(Seq[Any](format.raw/*75.33*/("""
												"""),format.raw/*76.13*/("""<option value=""""),_display_(/*76.29*/organisationRole/*76.45*/.getGraphEntityId()),format.raw/*76.64*/("""">
													"""),_display_(/*77.15*/organisationRole/*77.31*/.getValue()),format.raw/*77.42*/("""</option> """)))}),format.raw/*77.53*/("""
											"""),format.raw/*78.12*/("""</select>
										</div>
									</div>

									<div class="row ">
										<div class="col-lg-12" style="margin-bottom: 10px;">
											<label> Select Organisation Sector </label> <label
												class="checkbox-inline"><input type="checkbox"
												value="inuse" name="organisationSecOption"
												onchange="changeOption('organisationSectorId','organisationSecOption')">In
												Use</label> <select class="form-control"
												name="organisationSectorId[]" multiple
												id="organisationSectorId" disabled="disabled">
												"""),_display_(/*91.14*/for(organisationSector <- organisationSectors) yield /*91.60*/ {_display_(Seq[Any](format.raw/*91.62*/("""
												"""),format.raw/*92.13*/("""<option value=""""),_display_(/*92.29*/organisationSector/*92.47*/.getGraphEntityId()),format.raw/*92.66*/("""">
													"""),_display_(/*93.15*/organisationSector/*93.33*/.getValue()),format.raw/*93.44*/("""</option> """)))}),format.raw/*93.55*/("""
											"""),format.raw/*94.12*/("""</select>
										</div>
									</div>

								</div>

								<div id="member_div" style="display: none;">
									<div class="row ">
										<div class="col-lg-12" style="margin-bottom: 10px;">
											<label> Select Member Function </label> <label
												class="checkbox-inline"><input type="checkbox"
												value="inuse" name="memberFunctionOption"
												onchange="changeOption('memberFunctionId','memberFunctionOption')">In
												Use</label> <select disabled="disabled" class="form-control"
												name="memberFunctionId[]" id="memberFunctionId" multiple>
												"""),_display_(/*109.14*/for(memberFunction <- memberFunctions) yield /*109.52*/ {_display_(Seq[Any](format.raw/*109.54*/("""
												"""),format.raw/*110.13*/("""<option value=""""),_display_(/*110.29*/memberFunction/*110.43*/.getGraphEntityId()),format.raw/*110.62*/("""">
													"""),_display_(/*111.15*/memberFunction/*111.29*/.getValue()),format.raw/*111.40*/("""</option> """)))}),format.raw/*111.51*/("""
											"""),format.raw/*112.12*/("""</select>
										</div>
									</div>
									<div class="row ">
										<div class="col-lg-12" style="margin-bottom: 10px;">
											<label> Select Member Role </label> <label
												class="checkbox-inline"><input type="checkbox"
												value="inuse" name="memberRoleOption"
												onchange="changeOption('memberRoleId','memberRoleOption')">In
												Use</label> <select class="form-control" name="memberRoleId[]"
												id="memberRoleId" disabled="disabled" multiple>
												"""),_display_(/*123.14*/for(memberRole <- memberRoles) yield /*123.44*/ {_display_(Seq[Any](format.raw/*123.46*/("""
												"""),format.raw/*124.13*/("""<option value=""""),_display_(/*124.29*/memberRole/*124.39*/.getGraphEntityId()),format.raw/*124.58*/("""">
													"""),_display_(/*125.15*/memberRole/*125.25*/.getValue()),format.raw/*125.36*/("""</option> """)))}),format.raw/*125.47*/("""
											"""),format.raw/*126.12*/("""</select>
										</div>
									</div>
									<div class="row ">
										<div class="col-lg-12" style="margin-bottom: 10px;">
											<label> Select Member Level </label> <label
												class="checkbox-inline"><input type="checkbox"
												value="inuse" name="memberLevelOption"
												onchange="changeOption('memberLevelId','memberLevelOption')">In
												Use</label> <select class="form-control" name="memberLevelId[]"
												id="memberLevelId" disabled="disabled" multiple>
												"""),_display_(/*137.14*/for(memberLevel <- memberLevels) yield /*137.46*/ {_display_(Seq[Any](format.raw/*137.48*/("""
												"""),format.raw/*138.13*/("""<option value=""""),_display_(/*138.29*/memberLevel/*138.40*/.getGraphEntityId()),format.raw/*138.59*/("""">
													"""),_display_(/*139.15*/memberLevel/*139.26*/.getValue()),format.raw/*139.37*/("""</option> """)))}),format.raw/*139.48*/("""
											"""),format.raw/*140.12*/("""</select>
										</div>
									</div>
								</div>

							</div>

						</div>
					</div>
					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step2 &#8594; <b>Create Resource Combination</b>
							</div>
							<div class="panel-body">
								<!-- 
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label> Name of Resource </label> <input class="form-control"
											placeholder="Enter Resource Name" type="text"
											name="resourceName">
									</div>
								</div>
						 	-->
								<div class="row ">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label> Select Asset Confidentiality </label> <label
											class="checkbox-inline"> <input type="checkbox"
											value="inuse" name="assetConfidentialityOption"
											onchange="changeOption('assetConfidentialityId','assetConfidentialityOption')">
											In Use
										</label> <select class="form-control" id="assetConfidentialityId"
											disabled="disabled" name="assetConfidentialityId[]" multiple>
											"""),_display_(/*173.13*/for(assetConfidentiality <- assetConfidentialities) yield /*173.64*/ {_display_(Seq[Any](format.raw/*173.66*/("""
											"""),format.raw/*174.12*/("""<option value=""""),_display_(/*174.28*/assetConfidentiality/*174.48*/.getGraphEntityId()),format.raw/*174.67*/("""">
												"""),_display_(/*175.14*/assetConfidentiality/*175.34*/.getValue()),format.raw/*175.45*/("""</option> """)))}),format.raw/*175.56*/("""
										"""),format.raw/*176.11*/("""</select>
									</div>
								</div>
								<div class="row ">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label> Select Asset Integrity </label> <label
											class="checkbox-inline"> <input type="checkbox"
											value="inuse" name="assetIntegrityOption"
											onchange="changeOption('assetIntegrityId','assetIntegrityOption')">
											In Use
										</label> <select class="form-control" id="assetIntegrityId"
											disabled="disabed" name="assetIntegrityId[]" multiple>
											"""),_display_(/*188.13*/for(assetIntegrity <- assetIntegrities) yield /*188.52*/ {_display_(Seq[Any](format.raw/*188.54*/("""
											"""),format.raw/*189.12*/("""<option value=""""),_display_(/*189.28*/assetIntegrity/*189.42*/.getGraphEntityId()),format.raw/*189.61*/("""">
												"""),_display_(/*190.14*/assetIntegrity/*190.28*/.getValue()),format.raw/*190.39*/("""</option> """)))}),format.raw/*190.50*/("""
										"""),format.raw/*191.11*/("""</select>
									</div>
								</div>
								<div class="row ">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label> Select Asset Type </label> <label
											class="checkbox-inline"> <input type="checkbox"
											value="inuse" name="assetTypeOption"
											onchange="changeOption('assetTypeId','assetTypeOption')">
											In Use
										</label> <select class="form-control" id="assetTypeId"
											disabled="disabled" name="assetTypeId[]" multiple>
											"""),_display_(/*203.13*/for(assetType <- assetTypes) yield /*203.41*/{_display_(Seq[Any](format.raw/*203.42*/("""
											"""),format.raw/*204.12*/("""<option value=""""),_display_(/*204.28*/assetType/*204.37*/.getGraphEntityId()),format.raw/*204.56*/("""">"""),_display_(/*204.59*/assetType/*204.68*/.getValue()),format.raw/*204.79*/("""</option>
											""")))}),format.raw/*205.13*/("""
										"""),format.raw/*206.11*/("""</select>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step3 &#8594; <b>Select Action</b>
							</div>
							<div class="panel-body">
								<div class="row ">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label> Select Action Type </label> <label
											class="checkbox-inline"> <input type="checkbox"
											value="inuse" name="actionTypeOption"
											onchange="changeOption('actionType','actionTypeOption'); checkOption('actionTypeOption','actionValuesDiv')">
											In Use
										</label> <select onchange="fetchRows('actionType','actionValuesDiv')" style="margin-bottom: 10px;" class="form-control"
											name="actionType" disabled="disabled" id="actionType">
											<option value="" disabled="disabled" selected>Choose Action Value</option>
											
											"""),_display_(/*230.13*/for(actionType<- actionTypes) yield /*230.42*/{_display_(Seq[Any](format.raw/*230.43*/("""
											"""),format.raw/*231.12*/("""<option 
												value=""""),_display_(/*232.21*/actionType/*232.31*/.getValue()),format.raw/*232.42*/("""">"""),_display_(/*232.45*/actionType/*232.55*/.getValue()),format.raw/*232.66*/("""</option>
											""")))}),format.raw/*233.13*/("""
										"""),format.raw/*234.11*/("""</select>
										<div id="actionValuesDiv" style="display:none;"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								Step4 &#8594; <b>Select Decision</b>
							</div>
							<div class="panel-body">
								<div class="row ">
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label> Select Decision </label> <select class="form-control"
											name="decision" id="decision"> """),_display_(/*248.44*/for(permission <-
											permissions) yield /*249.24*/ {_display_(Seq[Any](format.raw/*249.26*/("""
											"""),format.raw/*250.12*/("""<option value=""""),_display_(/*250.28*/permission/*250.38*/.getPermissionId()),format.raw/*250.56*/("""">
												"""),_display_(/*251.14*/permission/*251.24*/.getPermissionValue()),format.raw/*251.45*/("""</option> """)))}),format.raw/*251.56*/("""
										"""),format.raw/*252.11*/("""</select>
									</div>
								</div>
							</div>
						</div>


					</div>

				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						Step5 &#8594; <b>Rule Description(Optional)</b>
					</div>
					<div class="panel-body">
						<div class="row ">
							<div class="col-lg-12" style="margin-bottom: 10px;">
								<label> Rule Description </label>
								<textarea class="form-control" name="ruleDescription">
								</textarea>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			Step 6 &#8594; <b>Select Operation</b>
		</div>
		<div class="panel-body">
			<div class="row" style="margin-bottom: 10px;">
				<div class="col-lg-4">
					<button type="submit" value="continue" name="submitValue1"
						id="submitValue1" class="btn btn-primary col-lg-12 btn-block">
						<b>Save and Add Groups</b>
					</button>
				</div>
				<div class="col-lg-4">
					<button type="submit" value="more" name="submitValue2"
						id="submitValue2" class="btn btn-success col-lg-12 btn-block">
						<b>Save and Add Another Rule</b>
					</button>
				</div>
				<div class="col-lg-4">
					<button type="submit" value="back" name="submitValue3"
						id="submitValue3" class="btn btn-danger col-lg-12 btn-block">
						<b>Save and Go Back</b>
					</button>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*307.3*/("""
"""),format.raw/*308.1*/("""</div>
<script>
function fetchRows(selectId,divId) """),format.raw/*310.36*/("""{"""),format.raw/*310.37*/("""
	
	"""),format.raw/*312.2*/("""var actionType = $("#" + selectId + " option:selected").val();
	if (actionType != "") """),format.raw/*313.24*/("""{"""),format.raw/*313.25*/("""
		"""),format.raw/*314.3*/("""$
				.ajax("""),format.raw/*315.11*/("""{"""),format.raw/*315.12*/("""
					"""),format.raw/*316.6*/("""type : "POST",
					url : "/fetchActionValues",
					data : """),format.raw/*318.13*/("""{"""),format.raw/*318.14*/("""
						"""),format.raw/*319.7*/(""""actionType" : actionType
					"""),format.raw/*320.6*/("""}"""),format.raw/*320.7*/(""",
					success : function(data) """),format.raw/*321.31*/("""{"""),format.raw/*321.32*/("""
					"""),format.raw/*322.6*/("""values = JSON.parse(data);
					var html_content= "<select class='form-control' name='actionValueId[]'";
					html_content +=	" id='actionValueId' multiple>";
					for (var i = 0; i < values.length; i++) """),format.raw/*325.46*/("""{"""),format.raw/*325.47*/("""
						"""),format.raw/*326.7*/("""html_content +=	"<option value='"+values[i].actionId+"'>";
						html_content +=	values[i].actionValue+"</option> ";
					"""),format.raw/*328.6*/("""}"""),format.raw/*328.7*/("""
					
					"""),format.raw/*330.6*/("""html_content += "</select>";
					$("#"+divId).html(html_content);
					$("#"+divId).show(400);
"""),format.raw/*333.1*/("""}"""),format.raw/*333.2*/("""
				"""),format.raw/*334.5*/("""}"""),format.raw/*334.6*/(""");
	"""),format.raw/*335.2*/("""}"""),format.raw/*335.3*/("""
"""),format.raw/*336.1*/("""}"""),format.raw/*336.2*/("""
"""),format.raw/*337.1*/("""</script>
""")))}),format.raw/*338.2*/("""
"""))}
  }

  def render(message:String,permissions:List[Permission],actionTypes:List[GraphEntityValue],assetConfidentialities:List[GraphEntityValue],assetIntegrities:List[GraphEntityValue],assetTypes:List[GraphEntityValue],memberFunctions:List[GraphEntityValue],memberLevels:List[GraphEntityValue],memberRoles:List[GraphEntityValue],organisations:List[GraphEntityValue],organisationRoles:List[GraphEntityValue],organisationSectors:List[GraphEntityValue]): play.twirl.api.HtmlFormat.Appendable = apply(message,permissions,actionTypes,assetConfidentialities,assetIntegrities,assetTypes,memberFunctions,memberLevels,memberRoles,organisations,organisationRoles,organisationSectors)

  def f:((String,List[Permission],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue]) => play.twirl.api.HtmlFormat.Appendable) = (message,permissions,actionTypes,assetConfidentialities,assetIntegrities,assetTypes,memberFunctions,memberLevels,memberRoles,organisations,organisationRoles,organisationSectors) => apply(message,permissions,actionTypes,assetConfidentialities,assetIntegrities,assetTypes,memberFunctions,memberLevels,memberRoles,organisations,organisationRoles,organisationSectors)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:47 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/addRules.scala.html
                  HASH: cf8106f18d270b6738ba73aba106d099142b64f1
                  MATRIX: 974->1|1548->472|1580->497|1602->510|1641->511|1670->513|1723->540|1737->546|1824->624|1863->625|1895->630|3631->2339|3681->2373|3721->2375|3763->2389|3806->2405|3827->2417|3867->2436|3912->2454|3933->2466|3965->2477|4007->2488|4048->2501|4642->3068|4713->3123|4753->3125|4795->3139|4838->3155|4863->3171|4903->3190|4948->3208|4973->3224|5005->3235|5047->3246|5088->3259|5702->3846|5764->3892|5804->3894|5846->3908|5889->3924|5916->3942|5956->3961|6001->3979|6028->3997|6060->4008|6102->4019|6143->4032|6799->4660|6854->4698|6895->4700|6938->4714|6982->4730|7006->4744|7047->4763|7093->4781|7117->4795|7150->4806|7193->4817|7235->4830|7793->5360|7840->5390|7881->5392|7924->5406|7968->5422|7988->5432|8029->5451|8075->5469|8095->5479|8128->5490|8171->5501|8213->5514|8777->6050|8826->6082|8867->6084|8910->6098|8954->6114|8975->6125|9016->6144|9062->6162|9083->6173|9116->6184|9159->6195|9201->6208|10413->7392|10481->7443|10522->7445|10564->7458|10608->7474|10638->7494|10679->7513|10724->7530|10754->7550|10787->7561|10830->7572|10871->7584|11455->8140|11511->8179|11552->8181|11594->8194|11638->8210|11662->8224|11703->8243|11748->8260|11772->8274|11805->8285|11848->8296|11889->8308|12444->8835|12489->8863|12529->8864|12571->8877|12615->8893|12634->8902|12675->8921|12706->8924|12725->8933|12758->8944|12813->8967|12854->8979|13873->9970|13919->9999|13959->10000|14001->10013|14059->10043|14079->10053|14112->10064|14143->10067|14163->10077|14196->10088|14251->10111|14292->10123|14837->10640|14896->10682|14937->10684|14979->10697|15023->10713|15043->10723|15083->10741|15128->10758|15148->10768|15191->10789|15234->10800|15275->10812|16783->12289|16813->12291|16895->12344|16925->12345|16959->12351|17075->12438|17105->12439|17137->12443|17179->12456|17209->12457|17244->12464|17335->12526|17365->12527|17401->12535|17461->12567|17490->12568|17552->12601|17582->12602|17617->12609|17853->12816|17883->12817|17919->12825|18071->12949|18100->12950|18142->12964|18269->13063|18298->13064|18332->13070|18361->13071|18394->13076|18423->13077|18453->13079|18482->13080|18512->13082|18555->13094
                  LINES: 26->1|41->13|43->16|43->16|43->16|44->17|45->18|45->18|46->19|46->19|48->21|86->59|86->59|86->59|87->60|87->60|87->60|87->60|88->61|88->61|88->61|88->61|89->62|101->74|102->75|102->75|103->76|103->76|103->76|103->76|104->77|104->77|104->77|104->77|105->78|118->91|118->91|118->91|119->92|119->92|119->92|119->92|120->93|120->93|120->93|120->93|121->94|136->109|136->109|136->109|137->110|137->110|137->110|137->110|138->111|138->111|138->111|138->111|139->112|150->123|150->123|150->123|151->124|151->124|151->124|151->124|152->125|152->125|152->125|152->125|153->126|164->137|164->137|164->137|165->138|165->138|165->138|165->138|166->139|166->139|166->139|166->139|167->140|200->173|200->173|200->173|201->174|201->174|201->174|201->174|202->175|202->175|202->175|202->175|203->176|215->188|215->188|215->188|216->189|216->189|216->189|216->189|217->190|217->190|217->190|217->190|218->191|230->203|230->203|230->203|231->204|231->204|231->204|231->204|231->204|231->204|231->204|232->205|233->206|257->230|257->230|257->230|258->231|259->232|259->232|259->232|259->232|259->232|259->232|260->233|261->234|275->248|276->249|276->249|277->250|277->250|277->250|277->250|278->251|278->251|278->251|278->251|279->252|334->307|335->308|337->310|337->310|339->312|340->313|340->313|341->314|342->315|342->315|343->316|345->318|345->318|346->319|347->320|347->320|348->321|348->321|349->322|352->325|352->325|353->326|355->328|355->328|357->330|360->333|360->333|361->334|361->334|362->335|362->335|363->336|363->336|364->337|365->338
                  -- GENERATED --
              */
          