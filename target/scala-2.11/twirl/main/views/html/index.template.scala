
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object index extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(user: User):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*1.14*/(""" 

"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/main("ATLAS Application")/*5.27*/{_display_(Seq[Any](format.raw/*5.28*/("""
"""),format.raw/*6.1*/("""<div>
	<h1>
		Hello,
		<a>"""),_display_(/*9.7*/if(user==null)/*9.21*/{_display_(Seq[Any](format.raw/*9.22*/("""
		"""),format.raw/*10.3*/("""Guest
		""")))}/*11.4*/else/*11.8*/{_display_(Seq[Any](format.raw/*11.9*/("""
			"""),_display_(/*12.5*/user/*12.9*/.getFirstName()),format.raw/*12.24*/(""" """),_display_(/*12.26*/user/*12.30*/.getSurName()),format.raw/*12.43*/("""
		""")))}),format.raw/*13.4*/("""</a>
	</h1>
</div>
""")))}),format.raw/*16.2*/("""

"""))}
  }

  def render(user:User): play.twirl.api.HtmlFormat.Appendable = apply(user)

  def f:((User) => play.twirl.api.HtmlFormat.Appendable) = (user) => apply(user)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/index.scala.html
                  HASH: e6d8d1b277c50256a15773f5224fbae35daeb53d
                  MATRIX: 721->1|836->13|867->37|895->40|928->65|966->66|994->68|1049->98|1071->112|1109->113|1140->117|1168->127|1180->131|1218->132|1250->138|1262->142|1298->157|1327->159|1340->163|1374->176|1409->181|1462->204
                  LINES: 26->1|29->1|31->4|32->5|32->5|32->5|33->6|36->9|36->9|36->9|37->10|38->11|38->11|38->11|39->12|39->12|39->12|39->12|39->12|39->12|40->13|43->16
                  -- GENERATED --
              */
          