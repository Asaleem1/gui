
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object organisationPropertyEdit extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template11[String,List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[String],PropertyGroups,List[String],List[String],List[String],List[String],String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(message: String, 
firsts: List[GraphEntityValue], 
seconds: List[GraphEntityValue], 
thirds: List[GraphEntityValue], 
labels: List[String],
groupDetail: PropertyGroups,
firstValues: List[String],
secondValues: List[String],
thirdValues: List[String],
optionList:List[String],
groupId:String):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*11.16*/("""

"""),_display_(/*14.2*/main(message)/*14.15*/{_display_(Seq[Any](format.raw/*14.16*/("""
"""),format.raw/*15.1*/("""<div id="page-wrapper">
	"""),_display_(/*16.3*/helper/*16.9*/.form(action=routes.PropertyFileController.addMemberPropertyGroup(),'id->"myForm_property",'name->"myForm_property")/*16.125*/{_display_(Seq[Any](format.raw/*16.126*/("""
	"""),format.raw/*17.2*/("""<input type="hidden" value=""""),_display_(/*17.31*/groupId),format.raw/*17.38*/("""" name="groupId"/>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default"
				style="border: 0; margin-bottom: 10px; box-shadow: none;">
				<div class="panel-heading"
					style="font-size: 20px; text-align: center; margin-bottom: 10px;">
					<i><b>Update Organisation Property Group</b> </i>
				</div>

				<div class="row">

					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step1 &#8594; <b>Select Value(s)</b>
							</div>
							<div class="panel-body">
								<div class="row ">
									<div class="col-lg-4" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*37.27*/labels/*37.33*/.get(0)),format.raw/*37.40*/("""s </label> <label
											class="checkbox-inline"> <input
											"""),_display_(/*39.13*/if(firstValues.size()!=0)/*39.38*/{_display_(Seq[Any](format.raw/*39.39*/("""
											"""),format.raw/*40.12*/("""checked
										""")))}),format.raw/*41.12*/("""
											 """),format.raw/*42.13*/("""type="checkbox"
											value=""""),_display_(/*43.20*/labels/*43.26*/.get(0)),format.raw/*43.33*/("""" name="firstOption"
											onchange="changeOption('firstIds','firstOption'); changeDivVisibility('firstChoiceSpan');">
											In Use
										</label> <select """),_display_(/*46.29*/if(firstValues.size()==0)/*46.54*/{_display_(Seq[Any](format.raw/*46.55*/("""
											"""),format.raw/*47.12*/("""disabled="disabled"
										""")))}),format.raw/*48.12*/(""" """),format.raw/*48.13*/("""class="form-control" id="firstIds"
											 name="firstIds[]" multiple>
											"""),_display_(/*50.13*/for(first <- firsts) yield /*50.33*/ {_display_(Seq[Any](format.raw/*50.35*/("""
											"""),format.raw/*51.12*/("""<option """),_display_(/*51.21*/if(firstValues.contains(first.getValue()))/*51.63*/{_display_(Seq[Any](format.raw/*51.64*/(""" 
														"""),format.raw/*52.15*/("""selected
											 """)))}),format.raw/*53.14*/("""
											 """),format.raw/*54.13*/("""value=""""),_display_(/*54.21*/first/*54.26*/.getValue()),format.raw/*54.37*/("""">
												"""),_display_(/*55.14*/first/*55.19*/.getValue()),format.raw/*55.30*/("""</option> """)))}),format.raw/*55.41*/("""
										"""),format.raw/*56.11*/("""</select> <span id="firstChoiceSpan"
											style=""""),_display_(/*57.20*/if(firstValues.size()==0)/*57.45*/{_display_(Seq[Any](format.raw/*57.46*/("""display: none;""")))}),format.raw/*57.61*/("""  """),format.raw/*57.63*/("""font-size: 12px; font-weight: 400 !important;">
											<label style="font-size: 12px; font-weight: 400;">
												Option for  """),_display_(/*59.26*/labels/*59.32*/.get(0)),format.raw/*59.39*/(""" """),format.raw/*59.40*/("""</label> <label
											class="radio-in-line"><input type="radio"
												name="firstChoice" id="firstChoice" value="fixed"
												"""),_display_(/*62.14*/if(optionList.get(0)=="fixed")/*62.44*/{_display_(Seq[Any](format.raw/*62.45*/("""checked""")))}),format.raw/*62.53*/("""> Fixed </label> <label class="radio-in-line"><input
												type="radio" name="firstChoice" id="firstChoice"
												value="omit" """),_display_(/*64.27*/if(optionList.get(0)=="omit")/*64.56*/{_display_(Seq[Any](format.raw/*64.57*/("""checked""")))}),format.raw/*64.65*/("""> Omit </label>
										</span>
									</div>
									<div class="col-lg-4" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*68.27*/labels/*68.33*/.get(1)),format.raw/*68.40*/("""s
										</label> <label
											class="checkbox-inline"> <input """),_display_(/*70.45*/if(secondValues.size()!=0)/*70.71*/{_display_(Seq[Any](format.raw/*70.72*/("""
											"""),format.raw/*71.12*/("""checked
										""")))}),format.raw/*72.12*/(""" """),format.raw/*72.13*/("""type="checkbox"
											value=""""),_display_(/*73.20*/labels/*73.26*/.get(1)),format.raw/*73.33*/("""" name="secondChoiceOption"
											onchange="changeOption('secondChoiceId','secondChoiceOption'); changeDivVisibility('secondChoiceSpan');">
											In Use
										</label> <select class="form-control" id="secondChoiceId"
											"""),_display_(/*77.13*/if(secondValues.size()==0)/*77.39*/{_display_(Seq[Any](format.raw/*77.40*/("""
											"""),format.raw/*78.12*/("""disabled="disabled"
										""")))}),format.raw/*79.12*/(""" """),format.raw/*79.13*/("""name="secondChoiceId[]" multiple>
											"""),_display_(/*80.13*/for(second <- seconds) yield /*80.35*/ {_display_(Seq[Any](format.raw/*80.37*/("""
											"""),format.raw/*81.12*/("""<option value=""""),_display_(/*81.28*/second/*81.34*/.getValue()),format.raw/*81.45*/(""""
											"""),_display_(/*82.13*/if(secondValues.contains(second.getValue()))/*82.57*/{_display_(Seq[Any](format.raw/*82.58*/(""" 
														"""),format.raw/*83.15*/("""selected
											 """)))}),format.raw/*84.14*/("""
											"""),format.raw/*85.12*/(""">
												"""),_display_(/*86.14*/second/*86.20*/.getValue()),format.raw/*86.31*/("""</option> """)))}),format.raw/*86.42*/("""
										"""),format.raw/*87.11*/("""</select> <span id="secondChoiceSpan"
											style=""""),_display_(/*88.20*/if(secondValues.size()==0)/*88.46*/{_display_(Seq[Any](format.raw/*88.47*/("""display: none;""")))}),format.raw/*88.62*/("""  """),format.raw/*88.64*/("""font-size: 12px; font-weight: 400 !important;">
											<label style="font-size: 12px; font-weight: 400;">
												Option for  """),_display_(/*90.26*/labels/*90.32*/.get(1)),format.raw/*90.39*/(""" """),format.raw/*90.40*/("""</label> <label class="radio-in-line"><input
												type="radio" name="secondChoice" id="secondChoice"
												value="fixed" """),_display_(/*92.28*/if(optionList.get(1)=="fixed")/*92.58*/{_display_(Seq[Any](format.raw/*92.59*/("""checked""")))}),format.raw/*92.67*/("""> Fixed </label> <label
											class="radio-in-line"><input type="radio"
												name="secondChoice" id="secondChoice" value="omit" """),_display_(/*94.65*/if(optionList.get(1)=="omit")/*94.94*/{_display_(Seq[Any](format.raw/*94.95*/("""checked""")))}),format.raw/*94.103*/(""">
												Omit </label>
										</span>
									</div>
									<div class="col-lg-4" style="margin-bottom: 10px;">
										<label> Select """),_display_(/*99.27*/labels/*99.33*/.get(2)),format.raw/*99.40*/("""s </label> <label class="checkbox-inline">
											<input """),_display_(/*100.20*/if(thirdValues.size()!=0)/*100.45*/{_display_(Seq[Any](format.raw/*100.46*/("""
											"""),format.raw/*101.12*/("""checked
										""")))}),format.raw/*102.12*/(""" """),format.raw/*102.13*/("""type="checkbox" value=""""),_display_(/*102.37*/labels/*102.43*/.get(2)),format.raw/*102.50*/("""" name="thirdChoiceOption"
											onchange="changeOption('thirdChoiceId','thirdChoiceOption'); changeDivVisibility('thirdChoiceSpan');">
											In Use
										</label> <select class="form-control" id="thirdChoiceId"
											"""),_display_(/*106.13*/if(thirdValues.size()==0)/*106.38*/{_display_(Seq[Any](format.raw/*106.39*/("""
											"""),format.raw/*107.12*/("""disabled="disabled"
										""")))}),format.raw/*108.12*/(""" """),format.raw/*108.13*/("""name="thirdChoiceId[]" multiple>
											"""),_display_(/*109.13*/for(third <- thirds) yield /*109.33*/{_display_(Seq[Any](format.raw/*109.34*/("""
											"""),format.raw/*110.12*/("""<option value=""""),_display_(/*110.28*/third/*110.33*/.getValue()),format.raw/*110.44*/(""""
											"""),_display_(/*111.13*/if(thirdValues.contains(third.getValue()))/*111.55*/{_display_(Seq[Any](format.raw/*111.56*/(""" 
														"""),format.raw/*112.15*/("""selected
											 """)))}),format.raw/*113.14*/("""
											"""),format.raw/*114.12*/(""">"""),_display_(/*114.14*/third/*114.19*/.getValue()),format.raw/*114.30*/("""</option>
											""")))}),format.raw/*115.13*/("""
										"""),format.raw/*116.11*/("""</select> <span id="thirdChoiceSpan"
											style=""""),_display_(/*117.20*/if(thirdValues.size()==0)/*117.45*/{_display_(Seq[Any](format.raw/*117.46*/("""display: none;""")))}),format.raw/*117.61*/("""  """),format.raw/*117.63*/("""font-size: 12px; font-weight: 400 !important;">
											<label style="font-size: 12px; font-weight: 400;">
												Option for  """),_display_(/*119.26*/labels/*119.32*/.get(2)),format.raw/*119.39*/(""" """),format.raw/*119.40*/("""</label> <label class="radio-in-line"><input
												type="radio" name="thirdChoice" id="thirdChoice" value="fixed"
												"""),_display_(/*121.14*/if(optionList.get(2)=="fixed")/*121.44*/{_display_(Seq[Any](format.raw/*121.45*/("""checked""")))}),format.raw/*121.53*/("""> Fixed </label> <label class="radio-in-line"><input
												type="radio" name="thirdChoice" id="thirdChoice" value="omit"
												"""),_display_(/*123.14*/if(optionList.get(2)=="omit")/*123.43*/{_display_(Seq[Any](format.raw/*123.44*/("""checked""")))}),format.raw/*123.52*/("""> Omit </label>
										</span>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								Step2 &#8594; <b>Set Instance Count</b>
							</div>
							<div class="panel-body">
								<div class="row ">
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Attribute Combination Count </label> <input
											type="number" placeholder="0" min="0" class="form-control"
											required name="attributeCombinationCount" value=""""),_display_(/*141.62*/groupDetail/*141.73*/.getCombinationCount()),format.raw/*141.95*/("""">
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Small </label> <input type="number" placeholder="0"
											min="0" class="form-control" required
											name="smallInstanceCount" value=""""),_display_(/*146.46*/groupDetail/*146.57*/.getSmallInstanceCount()),format.raw/*146.81*/("""">
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Medium </label> <input type="number" placeholder="0"
											min="0" class="form-control" required
											name="mediumInstanceCount" value=""""),_display_(/*151.47*/groupDetail/*151.58*/.getMediumInstanceCount()),format.raw/*151.83*/("""">
									</div>
									<div class="col-lg-3" style="margin-bottom: 10px;">
										<label> Large </label> <input type="number" placeholder="0"
											min="0" class="form-control" required
											name="largeInstanceCount" value=""""),_display_(/*156.46*/groupDetail/*156.57*/.getLargeInstanceCount()),format.raw/*156.81*/("""">
									</div>
									<div class="col-lg-12" style="margin-bottom: 10px;">
										<label>
										Prefixes for Member<i><span style="font-size:10px;">(Comma seperated prefixes)</span></i>
										</label>
										<input required class="form-control"
											placeholder="Enter Name PreFixes" type="text"
											name="namePrefixes" value=""""),_display_(/*164.40*/groupDetail/*164.51*/.getNamePrefix()),format.raw/*164.67*/(""""/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			Step 3 &#8594; <b>Select Operation</b>
		</div>
		<div class="panel-body">
			<div class="row" style="margin-bottom: 10px;">
				<div class="col-lg-6">
					<button type="submit" value="continue" name="submitValue1"
						 class="btn btn-primary col-lg-12 btn-block">
						<b>Update and Go Back</b>
					</button>
				</div>
				<div class="col-lg-6">
					<button type="submit" value="back" name="submitValue3"
						 class="btn btn-danger col-lg-12 btn-block">
						<b>Cancel and Go Back</b>
					</button>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*196.3*/("""
"""),format.raw/*197.1*/("""</div>

""")))}),format.raw/*199.2*/("""
"""))}
  }

  def render(message:String,firsts:List[GraphEntityValue],seconds:List[GraphEntityValue],thirds:List[GraphEntityValue],labels:List[String],groupDetail:PropertyGroups,firstValues:List[String],secondValues:List[String],thirdValues:List[String],optionList:List[String],groupId:String): play.twirl.api.HtmlFormat.Appendable = apply(message,firsts,seconds,thirds,labels,groupDetail,firstValues,secondValues,thirdValues,optionList,groupId)

  def f:((String,List[GraphEntityValue],List[GraphEntityValue],List[GraphEntityValue],List[String],PropertyGroups,List[String],List[String],List[String],List[String],String) => play.twirl.api.HtmlFormat.Appendable) = (message,firsts,seconds,thirds,labels,groupDetail,firstValues,secondValues,thirdValues,optionList,groupId) => apply(message,firsts,seconds,thirds,labels,groupDetail,firstValues,secondValues,thirdValues,optionList,groupId)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/organisationPropertyEdit.scala.html
                  HASH: 063799e1e00664d49861376f1c00c9f8b2509bec
                  MATRIX: 899->1|1305->303|1336->327|1358->340|1397->341|1426->343|1479->370|1493->376|1619->492|1659->493|1689->496|1745->525|1773->532|2485->1217|2500->1223|2528->1230|2630->1305|2664->1330|2703->1331|2744->1344|2795->1364|2837->1378|2900->1414|2915->1420|2943->1427|3143->1600|3177->1625|3216->1626|3257->1639|3320->1671|3349->1672|3465->1761|3501->1781|3541->1783|3582->1796|3618->1805|3669->1847|3708->1848|3753->1865|3807->1888|3849->1902|3884->1910|3898->1915|3930->1926|3974->1943|3988->1948|4020->1959|4062->1970|4102->1982|4186->2039|4220->2064|4259->2065|4305->2080|4335->2082|4499->2219|4514->2225|4542->2232|4571->2233|4745->2380|4784->2410|4823->2411|4862->2419|5031->2561|5069->2590|5108->2591|5147->2599|5315->2740|5330->2746|5358->2753|5459->2827|5494->2853|5533->2854|5574->2867|5625->2887|5654->2888|5717->2924|5732->2930|5760->2937|6034->3184|6069->3210|6108->3211|6149->3224|6212->3256|6241->3257|6315->3304|6353->3326|6393->3328|6434->3341|6477->3357|6492->3363|6524->3374|6566->3389|6619->3433|6658->3434|6703->3451|6757->3474|6798->3487|6841->3503|6856->3509|6888->3520|6930->3531|6970->3543|7055->3601|7090->3627|7129->3628|7175->3643|7205->3645|7369->3782|7384->3788|7412->3795|7441->3796|7605->3933|7644->3963|7683->3964|7722->3972|7892->4115|7930->4144|7969->4145|8009->4153|8190->4307|8205->4313|8233->4320|8324->4383|8359->4408|8399->4409|8441->4422|8493->4442|8523->4443|8575->4467|8591->4473|8620->4480|8890->4722|8925->4747|8965->4748|9007->4761|9071->4793|9101->4794|9175->4840|9212->4860|9252->4861|9294->4874|9338->4890|9353->4895|9386->4906|9429->4921|9481->4963|9521->4964|9567->4981|9622->5004|9664->5017|9694->5019|9709->5024|9742->5035|9797->5058|9838->5070|9923->5127|9958->5152|9998->5153|10045->5168|10076->5170|10241->5307|10257->5313|10286->5320|10316->5321|10479->5456|10519->5486|10559->5487|10599->5495|10769->5637|10808->5666|10848->5667|10888->5675|11518->6277|11539->6288|11583->6310|11860->6559|11881->6570|11927->6594|12206->6845|12227->6856|12274->6881|12551->7130|12572->7141|12618->7165|13015->7534|13036->7545|13074->7561|13869->8325|13899->8327|13941->8338
                  LINES: 26->1|39->11|41->14|41->14|41->14|42->15|43->16|43->16|43->16|43->16|44->17|44->17|44->17|64->37|64->37|64->37|66->39|66->39|66->39|67->40|68->41|69->42|70->43|70->43|70->43|73->46|73->46|73->46|74->47|75->48|75->48|77->50|77->50|77->50|78->51|78->51|78->51|78->51|79->52|80->53|81->54|81->54|81->54|81->54|82->55|82->55|82->55|82->55|83->56|84->57|84->57|84->57|84->57|84->57|86->59|86->59|86->59|86->59|89->62|89->62|89->62|89->62|91->64|91->64|91->64|91->64|95->68|95->68|95->68|97->70|97->70|97->70|98->71|99->72|99->72|100->73|100->73|100->73|104->77|104->77|104->77|105->78|106->79|106->79|107->80|107->80|107->80|108->81|108->81|108->81|108->81|109->82|109->82|109->82|110->83|111->84|112->85|113->86|113->86|113->86|113->86|114->87|115->88|115->88|115->88|115->88|115->88|117->90|117->90|117->90|117->90|119->92|119->92|119->92|119->92|121->94|121->94|121->94|121->94|126->99|126->99|126->99|127->100|127->100|127->100|128->101|129->102|129->102|129->102|129->102|129->102|133->106|133->106|133->106|134->107|135->108|135->108|136->109|136->109|136->109|137->110|137->110|137->110|137->110|138->111|138->111|138->111|139->112|140->113|141->114|141->114|141->114|141->114|142->115|143->116|144->117|144->117|144->117|144->117|144->117|146->119|146->119|146->119|146->119|148->121|148->121|148->121|148->121|150->123|150->123|150->123|150->123|168->141|168->141|168->141|173->146|173->146|173->146|178->151|178->151|178->151|183->156|183->156|183->156|191->164|191->164|191->164|223->196|224->197|226->199
                  -- GENERATED --
              */
          