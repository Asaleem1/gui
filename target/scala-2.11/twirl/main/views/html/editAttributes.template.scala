
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object editAttributes extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[String],List[GraphEntityValue],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(mainTypes: List[String], attributeValues:List[GraphEntityValue]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import java.math.BigInteger; var i=1;var k=0;

Seq[Any](format.raw/*1.67*/("""
"""),_display_(/*3.2*/main("Example")/*3.17*/{_display_(Seq[Any](format.raw/*3.18*/("""
"""),format.raw/*4.1*/("""<script>
	String.prototype.capitalizeFirstLetter = function() """),format.raw/*5.54*/("""{"""),format.raw/*5.55*/("""
		"""),format.raw/*6.3*/("""return this.charAt(0).toUpperCase() + this.substring(1).toLowerCase();
	"""),format.raw/*7.2*/("""}"""),format.raw/*7.3*/("""

	"""),format.raw/*9.2*/("""function deleteItem(rowId, id) """),format.raw/*9.33*/("""{"""),format.raw/*9.34*/("""
		"""),format.raw/*10.3*/("""if (id != null) """),format.raw/*10.19*/("""{"""),format.raw/*10.20*/("""
			"""),format.raw/*11.4*/("""var option = confirm("Are you sure to delete This Action Type?");

			if (option) """),format.raw/*13.16*/("""{"""),format.raw/*13.17*/("""
				"""),format.raw/*14.5*/("""$.ajax("""),format.raw/*14.12*/("""{"""),format.raw/*14.13*/("""
					"""),format.raw/*15.6*/("""type : "POST",
					url : "/deleteAttributeValue",
					data : """),format.raw/*17.13*/("""{"""),format.raw/*17.14*/("""
						"""),format.raw/*18.7*/(""""attributeId" : id,
					"""),format.raw/*19.6*/("""}"""),format.raw/*19.7*/(""",
					success : function(data) """),format.raw/*20.31*/("""{"""),format.raw/*20.32*/("""
						"""),format.raw/*21.7*/("""$("#"+rowId).remove();
						
					"""),format.raw/*23.6*/("""}"""),format.raw/*23.7*/("""
				"""),format.raw/*24.5*/("""}"""),format.raw/*24.6*/(""");
			"""),format.raw/*25.4*/("""}"""),format.raw/*25.5*/("""

		"""),format.raw/*27.3*/("""}"""),format.raw/*27.4*/("""
	"""),format.raw/*28.2*/("""}"""),format.raw/*28.3*/("""

	"""),format.raw/*30.2*/("""function updateValue() """),format.raw/*30.25*/("""{"""),format.raw/*30.26*/("""
		"""),format.raw/*31.3*/("""var valueId = $("#valueId").val();
		var editValueName = $("#editValueName").val();
		var rowId = $("#rowId").val();
		$.ajax("""),format.raw/*34.10*/("""{"""),format.raw/*34.11*/("""
			"""),format.raw/*35.4*/("""type : "POST",
			url : "/updateAttributeValue",
			data : """),format.raw/*37.11*/("""{"""),format.raw/*37.12*/("""
				"""),format.raw/*38.5*/(""""valueId" : valueId,
				"valueName" : editValueName
			"""),format.raw/*40.4*/("""}"""),format.raw/*40.5*/(""",
			success : function(data) """),format.raw/*41.29*/("""{"""),format.raw/*41.30*/("""
				"""),format.raw/*42.5*/("""$("#value_" + rowId).html(editValueName);
				$("#myModal").modal('toggle');

			"""),format.raw/*45.4*/("""}"""),format.raw/*45.5*/("""
		"""),format.raw/*46.3*/("""}"""),format.raw/*46.4*/(""");
	"""),format.raw/*47.2*/("""}"""),format.raw/*47.3*/("""

	"""),format.raw/*49.2*/("""function fetchRows(id) """),format.raw/*49.25*/("""{"""),format.raw/*49.26*/("""
		"""),format.raw/*50.3*/("""var attributeType = $("#" + id + " option:selected").val();
		if (attributeType != "") """),format.raw/*51.28*/("""{"""),format.raw/*51.29*/("""
			"""),format.raw/*52.4*/("""$
					.ajax("""),format.raw/*53.12*/("""{"""),format.raw/*53.13*/("""
						"""),format.raw/*54.7*/("""type : "POST",
						url : "/fetchAllValues",
						data : """),format.raw/*56.14*/("""{"""),format.raw/*56.15*/("""
							"""),format.raw/*57.8*/(""""attributeType" : attributeType
						"""),format.raw/*58.7*/("""}"""),format.raw/*58.8*/(""",
						success : function(data) """),format.raw/*59.32*/("""{"""),format.raw/*59.33*/("""
							"""),format.raw/*60.8*/("""values = JSON.parse(data);
							var html_content = "<div class='col-lg-12'>";
							html_content += "<div class='panel panel-default'>";
							html_content += "<div class='panel-heading'>All "
									+ attributeType + "(S)  </div>";
							html_content += "<div class='panel-body' style='overflow: auto; height: 430px;'>";
							html_content += "<table id='attributeTable' class='table table-hover'>";
							html_content += "<thead>";
							html_content += "<th>Index</th>";
							html_content += "<th>"
									+ attributeType.capitalizeFirstLetter()
									+ " Type</th>";
							html_content += "<th>"
									+ attributeType.capitalizeFirstLetter()
									+ " Sub Type</th>";
							html_content += "<th>"
									+ attributeType.capitalizeFirstLetter()
									+ " Name</th>";
							html_content += "<th>Operations</th>";
							html_content += "</thead>";
							html_content += "<tbody>";
							for (var i = 0; i < values.length; i++) """),format.raw/*81.48*/("""{"""),format.raw/*81.49*/("""
								"""),format.raw/*82.9*/("""html_content += "<tr id='tr"+i+"'>";
								html_content += "<td>" + (i + 1) + "</td>";
								html_content += "<td>" + values[i].mainType
										+ "</td>";
								html_content += "<td>" + values[i].subType
										+ "</td>";
								html_content += "<td id='value_"+i+"'>"
										+ values[i].value + "</td>";
								html_content += "<td>";
								html_content += "<div class='row'>";
								html_content += "<div class='col-lg-6'>";
								html_content += "<a onclick=\"openEditModal('"
										+ i + "','" + values[i].graphEntityId
										+ "')\"";
								html_content += "class=' btn btn-info btn-block'>Edit</a>";
								html_content += "</div>";
								html_content += "<div class='col-lg-6'>";
								html_content += "<a class=' btn btn-danger btn-block' href='#' onclick=\"deleteItem('tr"
										+ (i)
										+ "', '"
										+ values[i].graphEntityId
										+ "')\">Delete</a>";
								html_content += "</div>";
								html_content += "</div>";
								html_content += "</td>";
								html_content += "</tr>";
							"""),format.raw/*108.8*/("""}"""),format.raw/*108.9*/("""
							"""),format.raw/*109.8*/("""html_content += "</tbody>";
							html_content += "</table>";
							html_content += "</div>";
							html_content += "</div>";
							html_content += "</div>";
							$("#attribute_div").html(html_content);
						"""),format.raw/*115.7*/("""}"""),format.raw/*115.8*/("""
					"""),format.raw/*116.6*/("""}"""),format.raw/*116.7*/(""");
		"""),format.raw/*117.3*/("""}"""),format.raw/*117.4*/("""

	"""),format.raw/*119.2*/("""}"""),format.raw/*119.3*/("""
	"""),format.raw/*120.2*/("""function openEditModal(rowId, id) """),format.raw/*120.36*/("""{"""),format.raw/*120.37*/("""
		"""),format.raw/*121.3*/("""if (id != null) """),format.raw/*121.19*/("""{"""),format.raw/*121.20*/("""
			"""),format.raw/*122.4*/("""var value = $("#value_"+rowId).html();
			$("#editValueName").val(value);
			
			$("#rowId").val(rowId);
			$("#valueId").val(id);
			$("#myModal").modal('show');
		"""),format.raw/*128.3*/("""}"""),format.raw/*128.4*/("""

	"""),format.raw/*130.2*/("""}"""),format.raw/*130.3*/("""
"""),format.raw/*131.1*/("""</script>
<div id="page-wrapper">
	"""),_display_(/*133.3*/if(attributeValues.size()!=0)/*133.32*/{_display_(Seq[Any](format.raw/*133.33*/("""
	"""),format.raw/*134.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Step1 &#8594; <b>Select Attribute Type</b>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-6" style="margin-bottom: 10px;">
							<select required class="form-control" name="actionType"
								id="actionTypeOptions">
								<option selected="selected" disabled="disabled" value="">Select
									Attribute Type</option> """),_display_(/*146.35*/for(mainType <- mainTypes) yield /*146.61*/{_display_(Seq[Any](format.raw/*146.62*/("""
								"""),format.raw/*147.9*/("""<option value=""""),_display_(/*147.25*/mainType),format.raw/*147.33*/("""">"""),_display_(/*147.36*/mainType),format.raw/*147.44*/("""</option> """)))}),format.raw/*147.55*/("""
							"""),format.raw/*148.8*/("""</select>
						</div>
						<div class="col-lg-3">
							<button onclick="fetchRows('actionTypeOptions')"
								class="btn btn-primary btn-block">View Attributes</button>
						</div>
						<div class="col-lg-3" style="margin-bottom: 10px;">
							<button type="submit"
								onClick="location.href = '"""),_display_(/*156.36*/routes/*156.42*/.Application.adminHome()),format.raw/*156.66*/("""'"
								class="btn btn-danger btn-block">
								<b>Go Back</b>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row" id="attribute_div"></div>
	<div id="modalDiv">
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">Edit Value</h4>
					</div>
					<div class="modal-body">
						<input type="hidden" name="valueId" id="valueId" /> <input
							type="hidden" name="rowId" id="rowId" />

						<div class="row">
							<div class="col-lg-12">
								<label>Edit Name Value</label> <input type="text"
									class="form-control " name="editValueName" id='editValueName' />
							</div>
						</div>

					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary"
							onclick="updateValue()">Update Value</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	""")))}/*202.3*/else/*202.7*/{_display_(Seq[Any](format.raw/*202.8*/("""
	"""),format.raw/*203.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading" style="text-align: center">
					<b>No Actions in DomainModel yet!!</b>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12" style="margin-bottom: 10px;">
							<button type="submit"
								onClick="location.href = '"""),_display_(/*213.36*/routes/*213.42*/.Application.adminHome()),format.raw/*213.66*/("""'"
								class="btn btn-primary col-lg-12 btn-block">
								<b>Go Back</b>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*223.3*/("""
"""),format.raw/*224.1*/("""</div>


""")))}),format.raw/*227.2*/("""
"""))}
  }

  def render(mainTypes:List[String],attributeValues:List[GraphEntityValue]): play.twirl.api.HtmlFormat.Appendable = apply(mainTypes,attributeValues)

  def f:((List[String],List[GraphEntityValue]) => play.twirl.api.HtmlFormat.Appendable) = (mainTypes,attributeValues) => apply(mainTypes,attributeValues)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/editAttributes.scala.html
                  HASH: f3b7a6d3f49a5614d1bd84a09505375a8bb28f90
                  MATRIX: 761->1|959->66|987->118|1010->133|1048->134|1076->136|1166->199|1194->200|1224->204|1323->277|1350->278|1381->283|1439->314|1467->315|1498->319|1542->335|1571->336|1603->341|1715->425|1744->426|1777->432|1812->439|1841->440|1875->447|1968->512|1997->513|2032->521|2085->547|2113->548|2174->581|2203->582|2238->590|2302->627|2330->628|2363->634|2391->635|2425->642|2453->643|2486->649|2514->650|2544->653|2572->654|2604->659|2655->682|2684->683|2715->687|2872->816|2901->817|2933->822|3022->883|3051->884|3084->890|3169->948|3197->949|3256->980|3285->981|3318->987|3429->1071|3457->1072|3488->1076|3516->1077|3548->1082|3576->1083|3608->1088|3659->1111|3688->1112|3719->1116|3835->1204|3864->1205|3896->1210|3938->1224|3967->1225|4002->1233|4091->1294|4120->1295|4156->1304|4222->1343|4250->1344|4312->1378|4341->1379|4377->1388|5388->2371|5417->2372|5454->2382|6571->3471|6600->3472|6637->3481|6886->3702|6915->3703|6950->3710|6979->3711|7013->3717|7042->3718|7075->3723|7104->3724|7135->3727|7198->3761|7228->3762|7260->3766|7305->3782|7335->3783|7368->3788|7567->3959|7596->3960|7629->3965|7658->3966|7688->3968|7753->4006|7792->4035|7832->4036|7863->4039|8386->4534|8429->4560|8469->4561|8507->4571|8551->4587|8581->4595|8612->4598|8642->4606|8685->4617|8722->4626|9068->4944|9084->4950|9130->4974|10503->6328|10516->6332|10555->6333|10586->6336|10994->6716|11010->6722|11056->6746|11258->6917|11288->6919|11332->6932
                  LINES: 26->1|29->1|30->3|30->3|30->3|31->4|32->5|32->5|33->6|34->7|34->7|36->9|36->9|36->9|37->10|37->10|37->10|38->11|40->13|40->13|41->14|41->14|41->14|42->15|44->17|44->17|45->18|46->19|46->19|47->20|47->20|48->21|50->23|50->23|51->24|51->24|52->25|52->25|54->27|54->27|55->28|55->28|57->30|57->30|57->30|58->31|61->34|61->34|62->35|64->37|64->37|65->38|67->40|67->40|68->41|68->41|69->42|72->45|72->45|73->46|73->46|74->47|74->47|76->49|76->49|76->49|77->50|78->51|78->51|79->52|80->53|80->53|81->54|83->56|83->56|84->57|85->58|85->58|86->59|86->59|87->60|108->81|108->81|109->82|135->108|135->108|136->109|142->115|142->115|143->116|143->116|144->117|144->117|146->119|146->119|147->120|147->120|147->120|148->121|148->121|148->121|149->122|155->128|155->128|157->130|157->130|158->131|160->133|160->133|160->133|161->134|173->146|173->146|173->146|174->147|174->147|174->147|174->147|174->147|174->147|175->148|183->156|183->156|183->156|229->202|229->202|229->202|230->203|240->213|240->213|240->213|250->223|251->224|254->227
                  -- GENERATED --
              */
          