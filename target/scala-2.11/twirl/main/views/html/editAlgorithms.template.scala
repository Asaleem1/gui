
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object editAlgorithms extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[List[CombiningAlgorithm],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(algorithms:List[CombiningAlgorithm]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import java.math.BigInteger; var i=1;var k=0;

Seq[Any](format.raw/*1.39*/("""
"""),_display_(/*3.2*/main("Combining Algorithm")/*3.29*/{_display_(Seq[Any](format.raw/*3.30*/("""
"""),format.raw/*4.1*/("""<script>
String.prototype.capitalizeFirstLetter = function() """),format.raw/*5.53*/("""{"""),format.raw/*5.54*/("""
	"""),format.raw/*6.2*/("""return this.charAt(0).toUpperCase() + this.substring(1).toLowerCase();
"""),format.raw/*7.1*/("""}"""),format.raw/*7.2*/("""

"""),format.raw/*9.1*/("""function deleteItem(rowId, id) """),format.raw/*9.32*/("""{"""),format.raw/*9.33*/("""
	"""),format.raw/*10.2*/("""if (id != null) """),format.raw/*10.18*/("""{"""),format.raw/*10.19*/("""
		"""),format.raw/*11.3*/("""var option = confirm("Are you sure to delete this combining algorithm?");
		if (option) """),format.raw/*12.15*/("""{"""),format.raw/*12.16*/("""
			"""),format.raw/*13.4*/("""$.ajax("""),format.raw/*13.11*/("""{"""),format.raw/*13.12*/("""
				"""),format.raw/*14.5*/("""type : "POST",
				url : "/deleteAlgorithm",
				data : """),format.raw/*16.12*/("""{"""),format.raw/*16.13*/("""
					"""),format.raw/*17.6*/(""""attributeId" : id,
				"""),format.raw/*18.5*/("""}"""),format.raw/*18.6*/(""",
				success : function(data) """),format.raw/*19.30*/("""{"""),format.raw/*19.31*/("""
					"""),format.raw/*20.6*/("""$("#"+rowId).remove();
					
				"""),format.raw/*22.5*/("""}"""),format.raw/*22.6*/("""
			"""),format.raw/*23.4*/("""}"""),format.raw/*23.5*/(""");
		"""),format.raw/*24.3*/("""}"""),format.raw/*24.4*/("""

	"""),format.raw/*26.2*/("""}"""),format.raw/*26.3*/("""
"""),format.raw/*27.1*/("""}"""),format.raw/*27.2*/("""

"""),format.raw/*29.1*/("""function updateValue() """),format.raw/*29.24*/("""{"""),format.raw/*29.25*/("""
	"""),format.raw/*30.2*/("""var valueId = $("#valueId").val();
	var editValueName = $("#editValueName").val();
	var rowId = $("#rowId").val();
	$.ajax("""),format.raw/*33.9*/("""{"""),format.raw/*33.10*/("""
		"""),format.raw/*34.3*/("""type : "POST",
		url : "/updateAlgorithm",
		data : """),format.raw/*36.10*/("""{"""),format.raw/*36.11*/("""
			"""),format.raw/*37.4*/(""""valueId" : valueId,
			"valueName" : editValueName
		"""),format.raw/*39.3*/("""}"""),format.raw/*39.4*/(""",
		success : function(data) """),format.raw/*40.28*/("""{"""),format.raw/*40.29*/("""
			"""),format.raw/*41.4*/("""$("#value_" + rowId).html(editValueName);
			$("#myModal").modal('toggle');

		"""),format.raw/*44.3*/("""}"""),format.raw/*44.4*/("""
	"""),format.raw/*45.2*/("""}"""),format.raw/*45.3*/(""");
"""),format.raw/*46.1*/("""}"""),format.raw/*46.2*/("""
"""),format.raw/*47.1*/("""function openEditModal(rowId, id) """),format.raw/*47.35*/("""{"""),format.raw/*47.36*/("""
	"""),format.raw/*48.2*/("""if (id != null) """),format.raw/*48.18*/("""{"""),format.raw/*48.19*/("""
		"""),format.raw/*49.3*/("""var value = $("#value_"+rowId).html();
		$("#editValueName").val(value);
		
		$("#rowId").val(rowId);
		$("#valueId").val(id);
		$("#myModal").modal('show');
	"""),format.raw/*55.2*/("""}"""),format.raw/*55.3*/("""
"""),format.raw/*56.1*/("""}"""),format.raw/*56.2*/("""
"""),format.raw/*57.1*/("""</script>
<div id="page-wrapper">
"""),_display_(/*59.2*/if(algorithms.size()!=0)/*59.26*/{_display_(Seq[Any](format.raw/*59.27*/("""
	"""),format.raw/*60.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">All Combining Algorithms</div>
				<div class="panel-body" style="overflow: auto; height: 430px;">
					<table class="table table-hover">
						<thead>
							<th>Index</th>
							<th>Algorithm Name</th>
							<th>Operations</th>
						</thead>
						<tbody>
							"""),_display_(/*72.9*/for(algorithm <- algorithms) yield /*72.37*/{_display_(Seq[Any](format.raw/*72.38*/("""
							"""),format.raw/*73.8*/("""<tr id="tr"""),_display_(/*73.19*/{i}),format.raw/*73.22*/("""">
								<td>"""),_display_(/*74.14*/i),format.raw/*74.15*/("""</td>
								<td id="value_"""),_display_(/*75.24*/i),format.raw/*75.25*/("""">"""),_display_(/*75.28*/algorithm/*75.37*/.getCombiningAlgorithmName()),format.raw/*75.65*/("""</td>
								<td>
									<div class="row">
										<div class="col-lg-6">
											<a onclick="openEditModal('"""),_display_(/*79.40*/i),format.raw/*79.41*/("""','"""),_display_(/*79.45*/algorithm/*79.54*/.getCombiningAlgorithmId()),format.raw/*79.80*/("""')"
												class=" btn btn-info btn-block">Edit</a>
										</div>
										<div class="col-lg-6">
											<a onclick="deleteItem('tr"""),_display_(/*83.39*/i),format.raw/*83.40*/("""','"""),_display_(/*83.44*/algorithm/*83.53*/.getCombiningAlgorithmId()),format.raw/*83.79*/("""')"
												class=" btn btn-danger btn-block">Delete</a>
										</div>
									</div>
								</td>
							</tr>
							"""),_display_(/*89.9*/{i=i+1}),format.raw/*89.16*/(""" """)))}),format.raw/*89.18*/("""
						"""),format.raw/*90.7*/("""</tbody>
					</table>
					<div class="col-lg-12" style="margin-bottom: 10px;">
							<button type="submit"
								onClick="location.href = '"""),_display_(/*94.36*/routes/*94.42*/.Application.adminHome()),format.raw/*94.66*/("""'"
								class="btn btn-primary btn-block">
								<b>Go Back</b>
							</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modalDiv">
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<input type="hidden" name="valueId" id="valueId" />
			<input type="hidden" name="rowId" id="rowId" />
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">Edit Algorithm</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-12">
							<label>Edit Algorithm Name</label>
								<input type="text" class="form-control "
										name="editValueName" id='editValueName' />
							</div>
						</div>
						
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary" onclick="updateValue()">Update CombiningAlgorithm </button>
					</div>
				</div>
			</div>
		</div>
	</div>
	""")))}/*136.3*/else/*136.7*/{_display_(Seq[Any](format.raw/*136.8*/("""
	"""),format.raw/*137.2*/("""<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading" style="text-align: center">
					<b>No Algorithm in DomainModel yet!!</b>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12" style="margin-bottom: 10px;">
							<button type="submit"
								onClick="location.href = '"""),_display_(/*147.36*/routes/*147.42*/.Application.adminHome()),format.raw/*147.66*/("""'"
								class="btn btn-primary col-lg-12 btn-block">
								<b>Go Back</b>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	""")))}),format.raw/*157.3*/("""
"""),format.raw/*158.1*/("""</div>

""")))}),format.raw/*160.2*/("""
"""))}
  }

  def render(algorithms:List[CombiningAlgorithm]): play.twirl.api.HtmlFormat.Appendable = apply(algorithms)

  def f:((List[CombiningAlgorithm]) => play.twirl.api.HtmlFormat.Appendable) = (algorithms) => apply(algorithms)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Mon Mar 21 16:33:48 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/editAlgorithms.scala.html
                  HASH: c2f01497f9f0d84d3372e3595faba82555113ae3
                  MATRIX: 750->1|920->38|948->89|983->116|1021->117|1049->119|1138->181|1166->182|1195->185|1293->257|1320->258|1350->262|1408->293|1436->294|1466->297|1510->313|1539->314|1570->318|1687->407|1716->408|1748->413|1783->420|1812->421|1845->427|1931->485|1960->486|1994->493|2046->518|2074->519|2134->551|2163->552|2197->559|2259->594|2287->595|2319->600|2347->601|2380->607|2408->608|2440->613|2468->614|2497->616|2525->617|2556->621|2607->644|2636->645|2666->648|2819->774|2848->775|2879->779|2961->833|2990->834|3022->839|3105->895|3133->896|3191->926|3220->927|3252->932|3361->1014|3389->1015|3419->1018|3447->1019|3478->1023|3506->1024|3535->1026|3597->1060|3626->1061|3656->1064|3700->1080|3729->1081|3760->1085|3952->1250|3980->1251|4009->1253|4037->1254|4066->1256|4129->1293|4162->1317|4201->1318|4231->1321|4650->1714|4694->1742|4733->1743|4769->1752|4807->1763|4831->1766|4875->1783|4897->1784|4954->1814|4976->1815|5006->1818|5024->1827|5073->1855|5222->1977|5244->1978|5275->1982|5293->1991|5340->2017|5516->2166|5538->2167|5569->2171|5587->2180|5634->2206|5795->2341|5823->2348|5856->2350|5891->2358|6067->2507|6082->2513|6127->2537|7465->3856|7478->3860|7517->3861|7548->3864|7958->4246|7974->4252|8020->4276|8222->4447|8252->4449|8294->4460
                  LINES: 26->1|29->1|30->3|30->3|30->3|31->4|32->5|32->5|33->6|34->7|34->7|36->9|36->9|36->9|37->10|37->10|37->10|38->11|39->12|39->12|40->13|40->13|40->13|41->14|43->16|43->16|44->17|45->18|45->18|46->19|46->19|47->20|49->22|49->22|50->23|50->23|51->24|51->24|53->26|53->26|54->27|54->27|56->29|56->29|56->29|57->30|60->33|60->33|61->34|63->36|63->36|64->37|66->39|66->39|67->40|67->40|68->41|71->44|71->44|72->45|72->45|73->46|73->46|74->47|74->47|74->47|75->48|75->48|75->48|76->49|82->55|82->55|83->56|83->56|84->57|86->59|86->59|86->59|87->60|99->72|99->72|99->72|100->73|100->73|100->73|101->74|101->74|102->75|102->75|102->75|102->75|102->75|106->79|106->79|106->79|106->79|106->79|110->83|110->83|110->83|110->83|110->83|116->89|116->89|116->89|117->90|121->94|121->94|121->94|163->136|163->136|163->136|164->137|174->147|174->147|174->147|184->157|185->158|187->160
                  -- GENERATED --
              */
          