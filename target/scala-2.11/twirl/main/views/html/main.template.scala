
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object main extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String)(content: Html):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.32*/("""

"""),format.raw/*3.1*/("""<!DOCTYPE html>

<html>
<head>

<title>"""),_display_(/*8.9*/title),format.raw/*8.14*/("""</title>
<link rel="stylesheet" media="screen" href=""""),_display_(/*9.46*/routes/*9.52*/.Assets.at("stylesheets/main.css")),format.raw/*9.86*/("""">
<link rel="stylesheet" media="screen" href=""""),_display_(/*10.46*/routes/*10.52*/.Assets.at("stylesheets/bootstrap.min.css")),format.raw/*10.95*/("""">
<link rel="shortcut icon" type="image/png" href=""""),_display_(/*11.51*/routes/*11.57*/.Assets.at("images/favicon.png")),format.raw/*11.89*/("""">
<link rel="shortcut icon" type="image/png" href=""""),_display_(/*12.51*/routes/*12.57*/.Assets.at("stylesheets/plugins/metisMenu/metisMenu.min.css")),format.raw/*12.118*/("""">
<link rel="shortcut icon" type="image/png" href=""""),_display_(/*13.51*/routes/*13.57*/.Assets.at("font-awesome-4.1.0/css/font-awesome.min.css")),format.raw/*13.114*/("""">

<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
<link href=""""),_display_(/*16.14*/routes/*16.20*/.Assets.at("stylesheets/fileinput.min.css")),format.raw/*16.63*/("""" media="all" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src=""""),_display_(/*18.15*/routes/*18.21*/.Assets.at("javascripts/fileinput.min.js")),format.raw/*18.63*/(""""></script>
<script type="text/javascript" src='"""),_display_(/*19.38*/routes/*19.44*/.Application.javascriptRoutes()),format.raw/*19.75*/("""'></script>

<script src=""""),_display_(/*21.15*/routes/*21.21*/.Assets.at("javascripts/hello.js")),format.raw/*21.55*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*22.15*/routes/*22.21*/.Assets.at("javascripts/jquery.js")),format.raw/*22.56*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*23.15*/routes/*23.21*/.Assets.at("javascripts/bootstrap.min.js")),format.raw/*23.63*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*24.15*/routes/*24.21*/.Assets.at("javascripts/myCustom.js")),format.raw/*24.58*/("""" type="text/javascript"></script>
<script src=""""),_display_(/*25.15*/routes/*25.21*/.Assets.at("javascripts/plugins/metisMenu/metisMenu.min.js")),format.raw/*25.81*/("""" type="text/javascript"></script>
<style> option[default] """),format.raw/*26.25*/("""{"""),format.raw/*26.26*/(""" """),format.raw/*26.27*/("""display:none; """),format.raw/*26.41*/("""}"""),format.raw/*26.42*/("""
"""),format.raw/*27.1*/("""div.navbar-collapse li.selected a """),format.raw/*27.35*/("""{"""),format.raw/*27.36*/(""" """),format.raw/*27.37*/("""color: #FF0000; """),format.raw/*27.53*/("""}"""),format.raw/*27.54*/("""
 """),format.raw/*28.2*/("""</style>

</head>
<body>
 <script>
 $(document).ready(function() """),format.raw/*33.31*/("""{"""),format.raw/*33.32*/("""
"""),format.raw/*34.1*/("""// alert(location.pathname.substring(1));
 
  $('li').removeClass('active');
  $("#"+location.pathname.substring(1)).addClass('active');
"""),format.raw/*38.1*/("""}"""),format.raw/*38.2*/(""");
;
 

</script>
	<!-- Header Start -->
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href=""""),_display_(/*53.36*/routes/*53.42*/.Application.index()),format.raw/*53.62*/("""">ATLAS Application</a>
			</div>
			

			<div class="navbar-collapse collapse" >
				<ul class="nav navbar-nav left menu" >
					<li id="home"><a href=""""),_display_(/*59.30*/routes/*59.36*/.Application.index()),format.raw/*59.56*/("""">Home</a></li>
					
					"""),_display_(/*61.7*/if(session.get("type")!=null && session.get("type").equals("admin"))/*61.75*/{_display_(Seq[Any](format.raw/*61.76*/("""
						"""),format.raw/*62.7*/("""<li id="adminHome"><a href=""""),_display_(/*62.36*/routes/*62.42*/.Application.adminHome()),format.raw/*62.66*/("""">Admin Panel</a></li>
					""")))}),format.raw/*63.7*/("""
					"""),format.raw/*64.6*/("""<li id="aboutUs"><a href=""""),_display_(/*64.33*/routes/*64.39*/.Application.about()),format.raw/*64.59*/("""">About</a></li>
					<li id="contact"><a href=""""),_display_(/*65.33*/routes/*65.39*/.Application.contact()),format.raw/*65.61*/("""">Contact</a></li>
				</ul>
				<ul class="nav navbar-nav " style="float: right;">
				"""),_display_(/*68.6*/if(session.get("email")!=null)/*68.36*/{_display_(Seq[Any](format.raw/*68.37*/("""
					
					"""),format.raw/*70.6*/("""<li><a href=""""),_display_(/*70.20*/routes/*70.26*/.Application.logout()),format.raw/*70.47*/("""">Logout</a></li>
				""")))}/*71.6*/else/*71.10*/{_display_(Seq[Any](format.raw/*71.11*/("""
					"""),format.raw/*72.6*/("""<li><a href=""""),_display_(/*72.20*/routes/*72.26*/.Application.login()),format.raw/*72.46*/("""">Login</a></li>
					<li><a href=""""),_display_(/*73.20*/routes/*73.26*/.Application.addUser()),format.raw/*73.48*/("""">SignUp</a></li>
					
				
				""")))}),format.raw/*76.6*/("""
				"""),format.raw/*77.5*/("""</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>
	<!-- Header Ends -->
	<div class="container">"""),_display_(/*83.26*/content),format.raw/*83.33*/("""</div>
<!--  
	<footer class="footer navbar-fixed-bottom" style="background-color: silver;font-size:16px;">
		<div class="container">
			Footer-Details
		</div>
	</footer>
	-->
	

</body>

</html>




"""))}
  }

  def render(title:String,content:Html): play.twirl.api.HtmlFormat.Appendable = apply(title)(content)

  def f:((String) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title) => (content) => apply(title)(content)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Wed Mar 23 18:57:22 GMT 2016
                  SOURCE: C:/Users/HP/Documents/gui/app/views/main.scala.html
                  HASH: 1b7a6fe375e134a85f4f1a9e50e3ea62b2159346
                  MATRIX: 727->1|845->31|875->35|945->80|970->85|1051->140|1065->146|1119->180|1195->229|1210->235|1274->278|1355->332|1370->338|1423->370|1504->424|1519->430|1602->491|1683->545|1698->551|1777->608|1924->728|1939->734|2003->777|2184->931|2199->937|2262->979|2339->1029|2354->1035|2406->1066|2462->1095|2477->1101|2532->1135|2609->1185|2624->1191|2680->1226|2757->1276|2772->1282|2835->1324|2912->1374|2927->1380|2985->1417|3062->1467|3077->1473|3158->1533|3246->1593|3275->1594|3304->1595|3346->1609|3375->1610|3404->1612|3466->1646|3495->1647|3524->1648|3568->1664|3597->1665|3627->1668|3725->1738|3754->1739|3783->1741|3951->1882|3979->1883|4480->2357|4495->2363|4536->2383|4723->2543|4738->2549|4779->2569|4835->2599|4912->2667|4951->2668|4986->2676|5042->2705|5057->2711|5102->2735|5162->2765|5196->2772|5250->2799|5265->2805|5306->2825|5383->2875|5398->2881|5441->2903|5559->2995|5598->3025|5637->3026|5678->3040|5719->3054|5734->3060|5776->3081|5818->3105|5831->3109|5870->3110|5904->3117|5945->3131|5960->3137|6001->3157|6065->3194|6080->3200|6123->3222|6190->3259|6223->3265|6362->3377|6390->3384
                  LINES: 26->1|29->1|31->3|36->8|36->8|37->9|37->9|37->9|38->10|38->10|38->10|39->11|39->11|39->11|40->12|40->12|40->12|41->13|41->13|41->13|44->16|44->16|44->16|46->18|46->18|46->18|47->19|47->19|47->19|49->21|49->21|49->21|50->22|50->22|50->22|51->23|51->23|51->23|52->24|52->24|52->24|53->25|53->25|53->25|54->26|54->26|54->26|54->26|54->26|55->27|55->27|55->27|55->27|55->27|55->27|56->28|61->33|61->33|62->34|66->38|66->38|81->53|81->53|81->53|87->59|87->59|87->59|89->61|89->61|89->61|90->62|90->62|90->62|90->62|91->63|92->64|92->64|92->64|92->64|93->65|93->65|93->65|96->68|96->68|96->68|98->70|98->70|98->70|98->70|99->71|99->71|99->71|100->72|100->72|100->72|100->72|101->73|101->73|101->73|104->76|105->77|111->83|111->83
                  -- GENERATED --
              */
          