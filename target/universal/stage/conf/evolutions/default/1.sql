# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table action (
  action_id                 bigint auto_increment not null,
  action_name               varchar(255),
  action_in_use             varchar(255),
  constraint pk_action primary key (action_id))
;

create table action_in_use (
  action_in_use_id          bigint auto_increment not null,
  action_id                 bigint,
  action_value_id           bigint,
  constraint pk_action_in_use primary key (action_in_use_id))
;

create table action_value (
  action_value_id           bigint auto_increment not null,
  action_type_name          varchar(255),
  action_sub_type_name      varchar(255),
  action_value              varchar(255),
  constraint pk_action_value primary key (action_value_id))
;

create table combining_algorithm (
  combining_algorithm_id    bigint auto_increment not null,
  combining_algorithm       varchar(255),
  constraint pk_combining_algorithm primary key (combining_algorithm_id))
;

create table group_in_use (
  group_in_use_id           bigint auto_increment not null,
  policy_id                 bigint,
  owned_by_rule_group_id    bigint,
  used_rule_group_id        bigint,
  constraint pk_group_in_use primary key (group_in_use_id))
;

create table permission (
  permission_id             bigint auto_increment not null,
  permission_value          varchar(255),
  constraint pk_permission primary key (permission_id))
;

create table policy (
  policy_id                 bigint auto_increment not null,
  policy_name               varchar(255),
  policy_description        varchar(255),
  user_id                   bigint,
  constraint pk_policy primary key (policy_id))
;

create table resource (
  resource_id               bigint auto_increment not null,
  resource_name             varchar(255),
  asset_confidentiality_use varchar(255),
  asset_integrity_use       varchar(255),
  asset_type_use            varchar(255),
  constraint pk_resource primary key (resource_id))
;

create table resource_in_use (
  resource_in_use_id        bigint auto_increment not null,
  resource_id               bigint,
  resource_value_id         bigint,
  constraint pk_resource_in_use primary key (resource_in_use_id))
;

create table resource_value (
  resource_value_id         bigint auto_increment not null,
  resource_type             varchar(255),
  resource_sub_type         varchar(255),
  resource_value            varchar(255),
  constraint pk_resource_value primary key (resource_value_id))
;

create table rule (
  rule_id                   bigint auto_increment not null,
  rule_name                 varchar(255),
  rule_description          varchar(255),
  subject_id                bigint,
  resource_id               bigint,
  action_id                 bigint,
  permission_id             bigint,
  user_id                   bigint,
  policy_id                 bigint,
  constraint pk_rule primary key (rule_id))
;

create table rule_group (
  rule_group_id             bigint auto_increment not null,
  rule_group_name           varchar(255),
  combining_algorithm_id    bigint,
  group_description         varchar(255),
  subject_id                bigint,
  resource_id               bigint,
  action_id                 bigint,
  policy_id                 bigint,
  user_id                   bigint,
  is_base_group             tinyint(1) default 0,
  constraint pk_rule_group primary key (rule_group_id))
;

create table rule_in_use (
  rule_in_use_id            bigint auto_increment not null,
  rule_group_id             bigint,
  rule_id                   bigint,
  policy_id                 bigint,
  constraint pk_rule_in_use primary key (rule_in_use_id))
;

create table subject (
  subject_id                bigint auto_increment not null,
  subject_name              varchar(255),
  subject_type              varchar(255),
  organization_use          varchar(255),
  organization_role_use     varchar(255),
  organization_sector_use   varchar(255),
  member_function_use       varchar(255),
  member_Role_use           varchar(255),
  member_level_use          varchar(255),
  constraint pk_subject primary key (subject_id))
;

create table subject_in_use (
  subject_in_use_id         bigint auto_increment not null,
  subject_id                bigint,
  subject_value_id          bigint,
  constraint pk_subject_in_use primary key (subject_in_use_id))
;

create table subject_value (
  subject_value_id          bigint auto_increment not null,
  subject_type              varchar(255),
  subject_sub_type          varchar(255),
  subject_value             varchar(255),
  constraint pk_subject_value primary key (subject_value_id))
;

create table user (
  user_id                   bigint auto_increment not null,
  email                     varchar(255),
  contact_number            varchar(255),
  first_name                varchar(255),
  sur_name                  varchar(255),
  date_joining              datetime,
  username                  varchar(255),
  password                  varchar(255),
  constraint pk_user primary key (user_id))
;

alter table action_in_use add constraint fk_action_in_use_actionId_1 foreign key (action_id) references action (action_id) on delete restrict on update restrict;
create index ix_action_in_use_actionId_1 on action_in_use (action_id);
alter table action_in_use add constraint fk_action_in_use_actionValueId_2 foreign key (action_value_id) references action_value (action_value_id) on delete restrict on update restrict;
create index ix_action_in_use_actionValueId_2 on action_in_use (action_value_id);
alter table group_in_use add constraint fk_group_in_use_policyId_3 foreign key (policy_id) references policy (policy_id) on delete restrict on update restrict;
create index ix_group_in_use_policyId_3 on group_in_use (policy_id);
alter table group_in_use add constraint fk_group_in_use_ownedByRuleGroupId_4 foreign key (owned_by_rule_group_id) references rule_group (rule_group_id) on delete restrict on update restrict;
create index ix_group_in_use_ownedByRuleGroupId_4 on group_in_use (owned_by_rule_group_id);
alter table group_in_use add constraint fk_group_in_use_usedRuleGroupId_5 foreign key (used_rule_group_id) references rule_group (rule_group_id) on delete restrict on update restrict;
create index ix_group_in_use_usedRuleGroupId_5 on group_in_use (used_rule_group_id);
alter table policy add constraint fk_policy_userId_6 foreign key (user_id) references user (user_id) on delete restrict on update restrict;
create index ix_policy_userId_6 on policy (user_id);
alter table resource_in_use add constraint fk_resource_in_use_resourceId_7 foreign key (resource_id) references resource (resource_id) on delete restrict on update restrict;
create index ix_resource_in_use_resourceId_7 on resource_in_use (resource_id);
alter table resource_in_use add constraint fk_resource_in_use_resourceValueId_8 foreign key (resource_value_id) references resource_value (resource_value_id) on delete restrict on update restrict;
create index ix_resource_in_use_resourceValueId_8 on resource_in_use (resource_value_id);
alter table rule add constraint fk_rule_subjectId_9 foreign key (subject_id) references subject (subject_id) on delete restrict on update restrict;
create index ix_rule_subjectId_9 on rule (subject_id);
alter table rule add constraint fk_rule_resourceId_10 foreign key (resource_id) references resource (resource_id) on delete restrict on update restrict;
create index ix_rule_resourceId_10 on rule (resource_id);
alter table rule add constraint fk_rule_actionId_11 foreign key (action_id) references action (action_id) on delete restrict on update restrict;
create index ix_rule_actionId_11 on rule (action_id);
alter table rule add constraint fk_rule_permissionId_12 foreign key (permission_id) references permission (permission_id) on delete restrict on update restrict;
create index ix_rule_permissionId_12 on rule (permission_id);
alter table rule add constraint fk_rule_userId_13 foreign key (user_id) references user (user_id) on delete restrict on update restrict;
create index ix_rule_userId_13 on rule (user_id);
alter table rule add constraint fk_rule_policyId_14 foreign key (policy_id) references policy (policy_id) on delete restrict on update restrict;
create index ix_rule_policyId_14 on rule (policy_id);
alter table rule_group add constraint fk_rule_group_combiningAlgorithmId_15 foreign key (combining_algorithm_id) references combining_algorithm (combining_algorithm_id) on delete restrict on update restrict;
create index ix_rule_group_combiningAlgorithmId_15 on rule_group (combining_algorithm_id);
alter table rule_group add constraint fk_rule_group_subjectId_16 foreign key (subject_id) references subject (subject_id) on delete restrict on update restrict;
create index ix_rule_group_subjectId_16 on rule_group (subject_id);
alter table rule_group add constraint fk_rule_group_resourceId_17 foreign key (resource_id) references resource (resource_id) on delete restrict on update restrict;
create index ix_rule_group_resourceId_17 on rule_group (resource_id);
alter table rule_group add constraint fk_rule_group_actionId_18 foreign key (action_id) references action (action_id) on delete restrict on update restrict;
create index ix_rule_group_actionId_18 on rule_group (action_id);
alter table rule_group add constraint fk_rule_group_policyId_19 foreign key (policy_id) references policy (policy_id) on delete restrict on update restrict;
create index ix_rule_group_policyId_19 on rule_group (policy_id);
alter table rule_group add constraint fk_rule_group_userId_20 foreign key (user_id) references user (user_id) on delete restrict on update restrict;
create index ix_rule_group_userId_20 on rule_group (user_id);
alter table rule_in_use add constraint fk_rule_in_use_ruleGroupId_21 foreign key (rule_group_id) references rule_group (rule_group_id) on delete restrict on update restrict;
create index ix_rule_in_use_ruleGroupId_21 on rule_in_use (rule_group_id);
alter table rule_in_use add constraint fk_rule_in_use_ruleId_22 foreign key (rule_id) references rule (rule_id) on delete restrict on update restrict;
create index ix_rule_in_use_ruleId_22 on rule_in_use (rule_id);
alter table rule_in_use add constraint fk_rule_in_use_policyId_23 foreign key (policy_id) references policy (policy_id) on delete restrict on update restrict;
create index ix_rule_in_use_policyId_23 on rule_in_use (policy_id);
alter table subject_in_use add constraint fk_subject_in_use_subjectId_24 foreign key (subject_id) references subject (subject_id) on delete restrict on update restrict;
create index ix_subject_in_use_subjectId_24 on subject_in_use (subject_id);
alter table subject_in_use add constraint fk_subject_in_use_subjectValueId_25 foreign key (subject_value_id) references subject_value (subject_value_id) on delete restrict on update restrict;
create index ix_subject_in_use_subjectValueId_25 on subject_in_use (subject_value_id);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table action;

drop table action_in_use;

drop table action_value;

drop table combining_algorithm;

drop table group_in_use;

drop table permission;

drop table policy;

drop table resource;

drop table resource_in_use;

drop table resource_value;

drop table rule;

drop table rule_group;

drop table rule_in_use;

drop table subject;

drop table subject_in_use;

drop table subject_value;

drop table user;

SET FOREIGN_KEY_CHECKS=1;

