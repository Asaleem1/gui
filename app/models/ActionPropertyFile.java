package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class ActionPropertyFile extends Model {

	/**
	 * 
	 */
	private static long serialVersionUID = 1L;

	@Id
	@Column(name = "action_id")
	private Long actionId;

	@Column(name = "action_type")
	private String actionType;

	@Column(name = "action_value")
	private String actionValue;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "organisation_id", referencedColumnName = "organisation_id")
	private Organisation organisationId;

	/*
	 * @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	 * "actionValueId") public List<ActionInUse> actionInUse = new
	 * ArrayList<ActionInUse>();
	 */

	private static Finder<Integer, ActionPropertyFile> find = new Model.Finder<>(
			Integer.class, ActionPropertyFile.class);

	/**
	 * @param actionType
	 * @param actionValue
	 * @param organisationId
	 */
	public ActionPropertyFile(String actionType, String actionValue,
			Organisation organisationId) {
		super();
		this.actionType = actionType;
		this.actionValue = actionValue;
		this.organisationId = organisationId;
	}

	/**
	 * @return the actionId
	 */
	public Long getActionId() {
		return actionId;
	}

	/**
	 * @param actionId
	 *            the actionId to set
	 */
	public void setActionId(Long actionId) {
		this.actionId = actionId;
	}

	/**
	 * @return the actionType
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * @param actionType
	 *            the actionType to set
	 */
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	/**
	 * @return the actionValue
	 */
	public String getActionValue() {
		return actionValue;
	}

	/**
	 * @param actionValue
	 *            the actionValue to set
	 */
	public void setActionValue(String actionValue) {
		this.actionValue = actionValue;
	}

	/**
	 * @return the organisationId
	 */
	public Organisation getOrganisationId() {
		return organisationId;
	}

	/**
	 * @param organisationId
	 *            the organisationId to set
	 */
	public void setOrganisationId(Organisation organisationId) {
		this.organisationId = organisationId;
	}

	/**
	 * @return the find
	 */
	public static Finder<Integer, ActionPropertyFile> getFind() {
		return find;
	}

	/**
	 * @param find
	 *            the find to set
	 */
	public static void setFind(Finder<Integer, ActionPropertyFile> find) {
		ActionPropertyFile.find = find;
	}

	public static ActionPropertyFile findById(int id) {
		return find.byId(id);
	}

	public static List<ActionPropertyFile> findByOrganisationId(
			int organisationId) {
		return find.where().eq("organisation_id", organisationId).findList();
	}

	public static List<ActionPropertyFile> findByTypeOrganisationId(
			String type, String organisationId) {
		return find.where().eq("organisation_id", organisationId)
				.eq("action_type", type).findList();
	}

	public static List<ActionPropertyFile> findAll() {
		return find.all();
	}

	public static List<ActionPropertyFile> findByActionType(String actionType,String organisationId) {
		return find.where().eq("action_type", actionType).eq("organisation_id", organisationId).findList();
	}

	public static List<String> findDistisnctActionTypes(Long organisationId) {

		String sql = "select DISTINCT action_type from action_property_file where organisation_id= '"
				+ organisationId + "'";
		SqlQuery sqlQuery = Ebean.createSqlQuery(sql);

		List<SqlRow> list = sqlQuery.findList();
		List<String> actionTypes = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++) {
			actionTypes.add(list.get(i).getString("action_type"));
		}

		return actionTypes;
	}
	public static boolean isActionsAvailable(String organisationId){
		String sql="select count(*) as rows from action_property_file where organisation_id = '"+organisationId+"'";
		SqlQuery sqlQuery = Ebean.createSqlQuery(sql);
		List<SqlRow> list = sqlQuery.findList();
		if(Integer.valueOf(list.get(0).getString("rows"))!=0){
			return true;
		}
		return false;
	}
	public boolean deleteActionPropertyFile() {

		if (ActionInUse.deleteAllActionsByAPF(this.getActionId())) {
			this.delete();
		} else {
			return false;
		}
		return true;
	}
}
