package models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class ResourceInUse extends Model{
	
	/**
	 * 
	 */
	private static long serialVersionUID = 1L;

	@Id
	@Column(name="resource_in_use_id")
	private Long resourceInUseId;
	
	@ManyToOne
	@JoinColumn(name="resource_id", referencedColumnName="resource_id")
    private Resource resourceId;

	@ManyToOne
	@JoinColumn(name="resource_value_id", referencedColumnName="graph_entity_value_id")
    private GraphEntityValue resourceValueId;
	
	private static Finder<Integer, ResourceInUse> find = new Model.Finder<>(Integer.class, ResourceInUse.class);


	/**
	 * @param resourceId
	 * @param resourceValueId
	 */
	public ResourceInUse(Resource resourceId, GraphEntityValue resourceValueId) {
		super();
		this.resourceId = resourceId;
		this.resourceValueId = resourceValueId;
	}


	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	/**
	 * @param serialversionuid the serialversionuid to set
	 */
	public static void setSerialversionuid(long serialversionuid) {
		serialVersionUID = serialversionuid;
	}


	/**
	 * @return the resourceInUseId
	 */
	public Long getResourceInUseId() {
		return resourceInUseId;
	}


	/**
	 * @param resourceInUseId the resourceInUseId to set
	 */
	public void setResourceInUseId(Long resourceInUseId) {
		this.resourceInUseId = resourceInUseId;
	}


	/**
	 * @return the resourceId
	 */
	public Resource getResourceId() {
		return resourceId;
	}


	/**
	 * @param resourceId the resourceId to set
	 */
	public void setResourceId(Resource resourceId) {
		this.resourceId = resourceId;
	}


	/**
	 * @return the resourceValueId
	 */
	public GraphEntityValue getResourceValueId() {
		return resourceValueId;
	}


	/**
	 * @param resourceValueId the resourceValueId to set
	 */
	public void setResourceValueId(GraphEntityValue resourceValueId) {
		this.resourceValueId = resourceValueId;
	}


	/**
	 * @return the find
	 */
	public static Finder<Integer, ResourceInUse> getFind() {
		return find;
	}


	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Integer, ResourceInUse> find) {
		ResourceInUse.find = find;
	}

	public static List<ResourceInUse> findAllResource(Long resourceId){
		return find.where().eq("resource_id", resourceId).findList();
	}
	
	public static List<ResourceInUse> findAllResourceByValueId(Long resourceValueId){
		return find.where().eq("resource_value_id", resourceValueId).findList();
	}
	
}
