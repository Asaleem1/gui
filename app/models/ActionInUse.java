package models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class ActionInUse extends Model{
	
	/**
	 * 
	 */
	private static long serialVersionUID = 1L;

	@Id
	@Column(name="action_in_use_id")
	private Long actionInUseId;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="action_id", referencedColumnName="action_id")
    private Action actionId;

	@JoinColumn(name="action_value_id", referencedColumnName="action_id")
	@ManyToOne(fetch=FetchType.LAZY)
	private ActionPropertyFile actionValueId;

	
	private static Finder<Integer, ActionInUse> find = new Model.Finder<>(Integer.class, ActionInUse.class);
	

	/**
	 * @param actionId
	 * @param actionValueId
	 */
	public ActionInUse(Action actionId, ActionPropertyFile actionValueId) {
		super();
		this.actionId = actionId;
		this.actionValueId = actionValueId;
		
	}


	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	/**
	 * @param serialversionuid the serialversionuid to set
	 */
	public static void setSerialversionuid(long serialversionuid) {
		serialVersionUID = serialversionuid;
	}


	/**
	 * @return the actionInUseId
	 */
	public Long getActionInUseId() {
		return actionInUseId;
	}


	/**
	 * @param actionInUseId the actionInUseId to set
	 */
	public void setActionInUseId(Long actionInUseId) {
		this.actionInUseId = actionInUseId;
	}


	/**
	 * @return the actionId
	 */
	public Action getActionId() {
		return actionId;
	}


	/**
	 * @param actionId the actionId to set
	 */
	public void setActionId(Action actionId) {
		this.actionId = actionId;
	}


	/**
	 * @return the actionValueId
	 */
	public ActionPropertyFile getActionValueId() {
		return actionValueId;
	}


	/**
	 * @param actionValueId the actionValueId to set
	 */
	public void setActionValueId(ActionPropertyFile actionValueId) {
		this.actionValueId = actionValueId;
	}


	/**
	 * @return the find
	 */
	public static Finder<Integer, ActionInUse> getFind() {
		return find;
	}


	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Integer, ActionInUse> find) {
		ActionInUse.find = find;
	}

	public static List<ActionInUse> findAllAction(Long actionId){
		return find.where().eq("action_id", actionId).findList();
	}
	
	public static boolean deleteAllActionsByAPF(Long actionValueId){
		boolean noError = true;
		List<ActionInUse> actionInUseList =  find.where().eq("action_value_id",actionValueId).findList();
		for(ActionInUse actionInUse: actionInUseList){
			try{
				actionInUse.delete();
			}catch(Exception e){
				noError = false;
			}
		} 
		return noError;
	}
	
}
