package models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class RuleInUse extends Model{
	
	/**
	 * 
	 */
	private static long serialVersionUID = 1L;

	@Id
	@Column(name="rule_in_use_id")
	private Long ruleInUseId;
	
	@ManyToOne
	@JoinColumn(name="rule_group_id", referencedColumnName="rule_group_id")
    private RuleGroup ruleGroupId;

	@ManyToOne
	@JoinColumn(name="rule_id", referencedColumnName="rule_id")
    private Rule ruleId;

	@ManyToOne
	@JoinColumn(name="policy_id",referencedColumnName="policy_id")
	private Policy policyId;
	
	private static Finder<Integer, RuleInUse> find = new Model.Finder<>(Integer.class, RuleInUse.class);


	/**
	 * @return the ruleInUseId
	 */
	public Long getRuleInUseId() {
		return ruleInUseId;
	}


	/**
	 * @param ruleInUseId the ruleInUseId to set
	 */
	public void setRuleInUseId(Long ruleInUseId) {
		this.ruleInUseId = ruleInUseId;
	}


	/**
	 * @return the groupId
	 */
	public RuleGroup getRuleGroupId() {
		return ruleGroupId;
	}


	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(RuleGroup ruleGroupId) {
		this.ruleGroupId = ruleGroupId;
	}


	/**
	 * @return the ruleId
	 */
	public Rule getRuleId() {
		return ruleId;
	}


	/**
	 * @param ruleId the ruleId to set
	 */
	public void setRuleId(Rule ruleId) {
		this.ruleId = ruleId;
	}


	/**
	 * @return the find
	 */
	public static Finder<Integer, RuleInUse> getFind() {
		return find;
	}


	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Integer, RuleInUse> find) {
		RuleInUse.find = find;
	}

	
	/**
	 * @param ruleGroupId
	 * @param ruleId
	 * @param policyId
	 */
	public RuleInUse(RuleGroup ruleGroupId, Rule ruleId, Policy policyId) {
		super();
		this.ruleGroupId = ruleGroupId;
		this.ruleId = ruleId;
		this.policyId = policyId;
	}


	/**
	 * @return the policyId
	 */
	public Policy getPolicyId() {
		return policyId;
	}


	/**
	 * @param policyId the policyId to set
	 */
	public void setPolicyId(Policy policyId) {
		this.policyId = policyId;
	}


	/**
	 * @param ruleGroupId the ruleGroupId to set
	 */
	public void setRuleGroupId(RuleGroup ruleGroupId) {
		this.ruleGroupId = ruleGroupId;
	}


	public static List<RuleInUse> findAllRule(Long ruleGroupId){
		return find.where().eq("rule_group_id", ruleGroupId).findList();
	}
	
	public static List<RuleInUse> findAllRuleByPolicy(Long policyId){
		return find.where().eq("policy_id",policyId).findList();
	}
	

}
