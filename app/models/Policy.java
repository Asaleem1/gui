package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class Policy extends Model{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="policy_id")
	private Long policyId;
	
	@Column(name="policy_name")
	private String policyName;
	
	@Column(name="policy_description")
	private String policyDescription;
	
	@ManyToOne
    @JoinColumn(name="organisation_id", referencedColumnName="organisation_id")
    private Organisation organisationId;
	
	
	private static Finder<Integer, Policy> find = new Model.Finder<>(Integer.class, Policy.class);
	
	/**
	 * @return the find
	 */
	public static Finder<Integer, Policy> getFind() {
		return find;
	}

	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Integer, Policy> find) {
		Policy.find = find;
	}

	public static List<Policy> findAll(String organisationId){
		return find.where().eq("organisation_id", organisationId).findList();
	}
	
	public static Policy findById(int id){
		return find.byId(id);
	}

	/**
	 * @return the policyId
	 */
	public Long getPolicyId() {
		return policyId;
	}

	/**
	 * @param policyId the policyId to set
	 */
	public void setPolicyId(Long policyId) {
		this.policyId = policyId;
	}

	/**
	 * @return the policyName
	 */
	public String getPolicyName() {
		return policyName;
	}

	/**
	 * @param policyName the policyName to set
	 */
	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}

	/**
	 * @return the policyDescription
	 */
	public String getPolicyDescription() {
		return policyDescription;
	}

	/**
	 * @param policyDescription the policyDescription to set
	 */
	public void setPolicyDescription(String policyDescription) {
		this.policyDescription = policyDescription;
	}

	/**
	 * @return the organisationId
	 */
	public Organisation getOrganisationId() {
		return organisationId;
	}

	/**
	 * @param organisationId the organisationId to set
	 */
	public void setOrganisationId(Organisation organisationId) {
		this.organisationId = organisationId;
	}

	/**
	 * @param policyName
	 * @param policyDescription
	 * @param organisationId
	 */
	public Policy(String policyName, String policyDescription, Organisation organisationId) {
		super();
		this.policyName = policyName;
		this.policyDescription = policyDescription;
		this.organisationId = organisationId;
	}
	
	
	
}

