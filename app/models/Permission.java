package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class Permission extends Model{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="permission_id")
	private Long permissionId;
	
	@Column(name="permission_value")
	private String permissionValue;

	@ManyToOne
    @JoinColumn(name="organisation_id", referencedColumnName="organisation_id")
    private Organisation organisationId;
	
	/*
		@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "permissionId")
		public List<Rule> RulePermission = new ArrayList<Rule>();
	*/
	/**
	 * @return the organisationId
	 */
	public Organisation getOrganisationId() {
		return organisationId;
	}

	/**
	 * @param organisationId the organisationId to set
	 */
	public void setOrganisationId(Organisation organisationId) {
		this.organisationId = organisationId;
	}
	
	private static Finder<Integer, Permission> find = new Model.Finder<>(Integer.class, Permission.class);

	/**
	 * @param permissionValue
	 */
	public Permission(String permissionValue,Organisation organisation) {
		super();
		this.permissionValue = permissionValue;
		this.organisationId = organisation;
	}

	/**
	 * @return the permissionId
	 */
	public Long getPermissionId() {
		return permissionId;
	}

	/**
	 * @param permissionId the permissionId to set
	 */
	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}

	/**
	 * @return the permissionValue
	 */
	public String getPermissionValue() {
		return permissionValue;
	}

	/**
	 * @param permissionValue the permissionValue to set
	 */
	public void setPermissionValue(String permissionValue) {
		this.permissionValue = permissionValue;
	}

	/**
	 * @return the find
	 */
	public static Finder<Integer, Permission> getFind() {
		return find;
	}

	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Integer, Permission> find) {
		Permission.find = find;
	}
	
	public static List<Permission> findAll(String organisationId){
		return find.where().eq("organisation_id", organisationId).findList();
	}
	
	public static Permission findById(int id){
		return find.byId(id);
	}

}
