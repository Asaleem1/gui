package models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import play.db.ebean.Model;

@Entity
public class CombiningAlgorithm extends Model{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Id
	@Column(name="combining_algorithm_id")
	private Long CombiningAlgorithmId;
	
	@Column(name="combining_algorithm")
	private String CombiningAlgorithmName;

	@ManyToOne
    @JoinColumn(name="organisation_id", referencedColumnName="organisation_id")
    private Organisation organisationId;
	
	/**
	 * @return the organisationId
	 */
	public Organisation getOrganisationId() {
		return organisationId;
	}


	/**
	 * @param organisationId the organisationId to set
	 */
	public void setOrganisationId(Organisation organisationId) {
		this.organisationId = organisationId;
	}

	public static Finder<Integer, CombiningAlgorithm> find = new Model.Finder<>(Integer.class, CombiningAlgorithm.class);

	
	/**
	 * 
	 * @param combiningAlgorithm
	 */
	public CombiningAlgorithm(
			String combiningAlgorithmName,Organisation organisation) {
		super();
		CombiningAlgorithmName = combiningAlgorithmName;
		organisationId = organisation;
	}


	/**
	 * @return the combiningAlgorithmId
	 */
	public Long getCombiningAlgorithmId() {
		return CombiningAlgorithmId;
	}


	/**
	 * @param combiningAlgorithmId the combiningAlgorithmId to set
	 */
	public void setCombiningAlgorithmId(Long combiningAlgorithmId) {
		CombiningAlgorithmId = combiningAlgorithmId;
	}


	/**
	 * @return the combiningAlgorithm
	 */
	public String getCombiningAlgorithmName() {
		return CombiningAlgorithmName;
	}


	/**
	 * @param combiningAlgorithm the combiningAlgorithm to set
	 */
	public void setCombiningAlgorithmName(String combiningAlgorithmName) {
		CombiningAlgorithmName = combiningAlgorithmName;
	}


	/**
	 * @return the find
	 */
	public static Finder<Integer, CombiningAlgorithm> getFind() {
		return find;
	}

	public static CombiningAlgorithm findById(int id){
		return find.byId(id);
	}
	public static List<CombiningAlgorithm> findAllCombiningAlgorithms(String organisationId){
		return find.where().eq("organisation_id", organisationId).findList();
	}


	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Integer, CombiningAlgorithm> find) {
		CombiningAlgorithm.find = find;
	}
	
	public static List<CombiningAlgorithm> findAll(String orgId){
		return find.where().eq("organisation_id", orgId).findList();
	}
	
}
