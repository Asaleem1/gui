package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class JoinGroups extends Model{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="join_group_id")
	private Long joinGroupId;
	
	
	
	@ManyToOne
    @JoinColumn(name="join_file_id", referencedColumnName="join_file_id")
    private JoinFile joinFileId;

	
	
	private static Finder<Long, JoinGroups> find = new Model.Finder<>(Long.class, JoinGroups.class);
	
	
	
	/**
	 * @return the joinGroupId
	 */
	public Long getJoinGroupId() {
		return joinGroupId;
	}


	/**
	 * @param joingroupId the joingroupId to set
	 */
	public void setJoinGroupId(Long joinGroupId) {
		this.joinGroupId = joinGroupId;
	}

	/**
	 * @return the joinFileId
	 */
	public JoinFile getJoinFileId() {
		return joinFileId;
	}


	/**
	 * @param joinFileId the joinFileId to set
	 */
	public void setJoinFileId(JoinFile joinFileId) {
		this.joinFileId = joinFileId;
	}
	/**
	 * @param joinFileId
	 */
	public JoinGroups( JoinFile joinFileId) {
		super();
		this.joinFileId = joinFileId;
		}


	/**
	 * @return the find
	 */
	public static Finder<Long, JoinGroups> getFind() {
		return find;
	}


	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Long, JoinGroups> find) {
		JoinGroups.find = find;
	}

	public static List<JoinGroups> findAll(){
		return find.all();
	}
	
	public static JoinGroups findById(Long id){
		return find.byId(id);
	}
	
	public static List<JoinGroups> findByOrganisationId(String organisationId){
		return find.where().eq("organisation_id",organisationId).findList();
	}
	
	public static List<JoinGroups> findByJoinFileId(Long joinFileId){
		return find.where().eq("join_file_id",joinFileId).findList();
	}
	public void deleteGroup(){
		List<JoinGroupValues> values = JoinGroupValues.findByGroupId(this.getJoinGroupId());
		for(JoinGroupValues value:values){
			value.delete();
		}
		this.delete();
	}
	
}

