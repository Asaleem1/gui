package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class PropertyGroups extends Model{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="property_group_id")
	private Long propertyGroupId;
	
	@Required
	@Column(name="name_prefix")
	private String namePrefix;
	
	@Required
	@Column(name="combination_count")
	private Long combinationCount;
	
	@Required
	@Column(name="small_instance_count")
	private Long smallInstanceCount;
	
	@Required
	@Column(name="medium_instance_count")
	private Long mediumInstanceCount;
	
	@Required
	@Column(name="large_instance_count")
	private Long largeInstanceCount;
	
	@ManyToOne
    @JoinColumn(name="property_file_id", referencedColumnName="property_file_id")
    private PropertiesFile propertyFileId;
	
	/*@ManyToOne
    @JoinColumn(name="organisation_id", referencedColumnName="organisation_id")
    private Organisation organisationId;
	*/
	
	
	private static Finder<Long, PropertyGroups> find = new Model.Finder<>(Long.class, PropertyGroups.class);
	
	
	
	/**
	 * @return the propertyGroupId
	 */
	public Long getPropertyGroupId() {
		return propertyGroupId;
	}


	/**
	 * @param propertygroupId the propertygroupId to set
	 */
	public void setPropertyGroupId(Long propertyGroupId) {
		this.propertyGroupId = propertyGroupId;
	}


	/**
	 * @return the namePrefix
	 */
	public String getNamePrefix() {
		return namePrefix;
	}


	/**
	 * @param namePrefix the namePrefix to set
	 */
	public void setNamePrefix(String namePrefix) {
		this.namePrefix = namePrefix;
	}


	/**
	 * @return the combinationCount
	 */
	public Long getCombinationCount() {
		return combinationCount;
	}


	/**
	 * @param combinationCount the combinationCount to set
	 */
	public void setCombinationCount(Long combinationCount) {
		this.combinationCount = combinationCount;
	}


	/**
	 * @return the smallInstanceCount
	 */
	public Long getSmallInstanceCount() {
		return smallInstanceCount;
	}


	/**
	 * @param smallInstanceCount the smallInstanceCount to set
	 */
	public void setSmallInstanceCount(Long smallInstanceCount) {
		this.smallInstanceCount = smallInstanceCount;
	}


	/**
	 * @return the mediumInstanceCount
	 */
	public Long getMediumInstanceCount() {
		return mediumInstanceCount;
	}


	/**
	 * @param mediumInstanceCount the mediumInstanceCount to set
	 */
	public void setMediumInstanceCount(Long mediumInstanceCount) {
		this.mediumInstanceCount = mediumInstanceCount;
	}


	/**
	 * @return the largeInstanceCount
	 */
	public Long getLargeInstanceCount() {
		return largeInstanceCount;
	}


	/**
	 * @param largeInstanceCount the largeInstanceCount to set
	 */
	public void setLargeInstanceCount(Long largeInstanceCount) {
		this.largeInstanceCount = largeInstanceCount;
	}


	/**
	 * @return the propertyFileId
	 */
	public PropertiesFile getPropertyFileId() {
		return propertyFileId;
	}


	/**
	 * @param propertyFileId the propertyFileId to set
	 */
	public void setPropertyFileId(PropertiesFile propertyFileId) {
		this.propertyFileId = propertyFileId;
	}


	/**
	 * @return the organisationId
	 */
	/*public Organisation getOrganisationId() {
		return organisationId;
	}
*/

	/**
	 * @param organisationId the organisationId to set
	 */
	/*public void setOrganisationId(Organisation organisationId) {
		this.organisationId = organisationId;
	}
*/

	/**
	 * @param namePrefix
	 * @param combinationCount
	 * @param smallInstanceCount
	 * @param mediumInstanceCount
	 * @param largeInstanceCount
	 * @param propertyFileId
	 */
	public PropertyGroups(String namePrefix, Long combinationCount,
			Long smallInstanceCount, Long mediumInstanceCount,
			Long largeInstanceCount, PropertiesFile propertyFileId
			/*Organisation organisationId*/) {
		super();
		this.namePrefix = namePrefix;
		this.combinationCount = combinationCount;
		this.smallInstanceCount = smallInstanceCount;
		this.mediumInstanceCount = mediumInstanceCount;
		this.largeInstanceCount = largeInstanceCount;
		this.propertyFileId = propertyFileId;
		//this.organisationId = organisationId;
	}


	/**
	 * @return the find
	 */
	public static Finder<Long, PropertyGroups> getFind() {
		return find;
	}


	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Long, PropertyGroups> find) {
		PropertyGroups.find = find;
	}

	public static List<PropertyGroups> findAll(){
		return find.all();
	}
	
	public static PropertyGroups findById(Long id){
		return find.byId(id);
	}
	
	public static List<PropertyGroups> findByOrganisationId(String organisationId){
		return find.where().eq("organisation_id",organisationId).findList();
	}
	
	public static List<PropertyGroups> findByPropertyFileId(Long propertyFileId){
		return find.where().eq("property_file_id",propertyFileId).findList();
	}
	public void deleteGroup(){
		List<PropertyGroupValues> values = PropertyGroupValues.findByGroupId(this.getPropertyGroupId());
		for(PropertyGroupValues value:values){
			value.delete();
		}
		this.delete();
	}
	
}

