package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class GraphEntityValue extends Model {

	@Required
	@Id
	@Column(name = "graph_entity_value_id")
	private Long graphEntityId;

	@Required
	@Column(name = "main_type")
	private String mainType;

	@Required
	@Column(name = "sub_type")
	private String subType;

	@Required
	@Column(name = "value")
	private String value;

	private static long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "organisation_id", referencedColumnName = "organisation_id")
	private Organisation organisationId;
	/*
		@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "resourceValueId")
		public List<ResourceInUse> ResourcesInUse = new ArrayList<ResourceInUse>();
		
		@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "subjectValueId")
		public List<SubjectInUse> SubjectInUse = new ArrayList<SubjectInUse>();
	*/
	
	/**
	 * @return the graphEntityId
	 */
	public Long getGraphEntityId() {
		return graphEntityId;
	}

	/**
	 * @param graphEntityId the graphEntityId to set
	 */
	public void setGraphEntityId(Long graphEntityId) {
		this.graphEntityId = graphEntityId;
	}

	/**
	 * @return the mainType
	 */
	public String getMainType() {
		return mainType;
	}

	/**
	 * @param mainType the mainType to set
	 */
	public void setMainType(String mainType) {
		this.mainType = mainType;
	}

	/**
	 * @return the subType
	 */
	public String getSubType() {
		return subType;
	}

	/**
	 * @param subType the subType to set
	 */
	public void setSubType(String subType) {
		this.subType = subType;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @param serialversionuid the serialversionuid to set
	 */
	public static void setSerialversionuid(long serialversionuid) {
		serialVersionUID = serialversionuid;
	}

	/**
	 * @return the organisationId
	 */
	public Organisation getOrganisationId() {
		return organisationId;
	}

	/**
	 * @param organisationId the organisationId to set
	 */
	public void setOrganisationId(Organisation organisationId) {
		this.organisationId = organisationId;
	}

	
	/**
	 * @param mainType
	 * @param subType
	 * @param value
	 * @param organisationId
	 */
	public GraphEntityValue(String mainType, String subType, String value,
			Organisation organisationId) {
		super();
		this.mainType = mainType;
		this.subType = subType;
		this.value = value;
		this.organisationId = organisationId;
	}


	private static Finder<Integer, GraphEntityValue> find = new Model.Finder<>(
			Integer.class, GraphEntityValue.class);

	/**
	 * @return the find
	 */
	public static Finder<Integer, GraphEntityValue> getFind() {
		return find;
	}

	/**
	 * @param find
	 *            the find to set
	 */
	public static void setFind(Finder<Integer, GraphEntityValue> find) {
		GraphEntityValue.find = find;
	}

	public static List<GraphEntityValue> findAll(String organisationId) {
		return find.where().eq("organisation_id", organisationId).findList();
	}

	public static GraphEntityValue findById(int id) {
		return find.byId(id);
	}

	public static List<GraphEntityValue> findByMainTypeOrganisationId(String mainType,String organisationId) {
		
		return find.where().eq("main_type", mainType).eq("organisation_id", organisationId).findList();
	}

	public static List<GraphEntityValue> findBySubType(String subType) {
		return find.where().eq("sub_type", subType).findList();
	}

	public static List<GraphEntityValue> findByMainSubTypes(String mainType,
			String subType,String organisationId) {
		return find.where().eq("main_type", mainType).eq("sub_type", subType).eq("organisation_id",organisationId)
				.findList();
	}

	public static List<String> findDistinctMainTypes(String mainType,String organisationId) {

		String sql = "select DISTINCT sub_type from graph_entity where organisation_id = '"+organisationId+"' and main_type = '"
				+ mainType + "'";
		SqlQuery sqlQuery = Ebean.createSqlQuery(sql);

		List<SqlRow> list = sqlQuery.findList();
		List<String> subTypes = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++) {
			subTypes.add(list.get(i).getString("sub_type"));
		}

		return subTypes;
	}

	public static List<String> findDistinctSubTypesValues(String mainType,
			String subType) {

		String sql = "select DISTINCT value from graph_entity where main_type = '"
				+ mainType + "' and sub_type ='" + subType + "'";
		SqlQuery sqlQuery = Ebean.createSqlQuery(sql);

		List<SqlRow> list = sqlQuery.findList();
		List<String> subTypes = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++) {
			subTypes.add(list.get(i).getString("value"));
		}

		return subTypes;
	}
	public void deleteEntity(String organisationId){
		if (this.getMainType().equalsIgnoreCase("action")
				&& this.getSubType().equalsIgnoreCase("type")) {
			List<ActionPropertyFile> actionValues = ActionPropertyFile.findByActionType(this.getValue(),organisationId);
			for(ActionPropertyFile actionValue:actionValues){
				actionValue.deleteActionPropertyFile();
			}
		}else{
			if(this.getMainType().equalsIgnoreCase("asset")){
				List<ResourceInUse> resourcesInUse = ResourceInUse.findAllResourceByValueId(this.getGraphEntityId());
				for(ResourceInUse resourceInUse:resourcesInUse){
					resourceInUse.delete();
				}
			}else{//this is for subject
				List<SubjectInUse> subjectsInUse = SubjectInUse.findAllSubjectByValueId(this.getGraphEntityId());
				for(SubjectInUse subjectInUse:subjectsInUse){
					subjectInUse.delete();
				}
			}
		}
		this.delete();
	}
}
