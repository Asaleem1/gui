package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import play.data.format.*;
import play.data.validation.*;
import play.db.ebean.*;

@Entity
public class User extends Model{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="user_id")
    private Long userId;
	
	@Constraints.Required
	@Column(name="email",unique=true)
    private String email;
	
	@Constraints.Required
	@Column(name="contact_number")
    private String contactNumber;
	
	@Constraints.Required
	@Column(name="first_name")
    private String firstName;
	
	@Constraints.Required
	@Column(name="sur_name")
    private String surName;
	
	@Constraints.Required
	@Formats.DateTime(pattern="dd-MM-yyyy")
	@Column(name="date_joining")
    public Date dateJoining;
	
	@Constraints.Required
	@Column(name="username",unique=true)
    public String username;
	
	@Constraints.Required
	@Column(name="password")
    public String password;
	
	@Column(name="type")
    public String type;
	
	@ManyToOne
	@JoinColumn(name="organisation_id", referencedColumnName="organisation_id")
    private Organisation organisationId;

	
	
	public static User authenticate(String email, String password) {
        return find.where().eq("email", email)
            .eq("password", password).findUnique();
    }
	
	
	
	
	
	/**
	 * @param email
	 * @param contactNumber
	 * @param firstName
	 * @param surName
	 * @param dateJoining
	 * @param username
	 * @param password
	 * @param type
	 * @param organisationId
	 */
	public User(String email, String contactNumber, String firstName,
			String surName, Date dateJoining, String username, String password,
			String type, Organisation organisationId) {
		super();
		this.email = email;
		this.contactNumber = contactNumber;
		this.firstName = firstName;
		this.surName = surName;
		this.dateJoining = dateJoining;
		this.username = username;
		this.password = password;
		this.type = type;
		this.organisationId = organisationId;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the organisationId
	 */
	public Organisation getOrganisationId() {
		return organisationId;
	}
	/**
	 * @param organisationId the organisationId to set
	 */
	public void setOrganisationId(Organisation organisationId) {
		this.organisationId = organisationId;
	}


	private static Finder<Integer, User> find = new Model.Finder<>(Integer.class, User.class);
	
	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the contactNumber
	 */
	public String getContactNumber() {
		return contactNumber;
	}
	/**
	 * @param contactNumber the contactNumber to set
	 */
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the surName
	 */
	public String getSurName() {
		return surName;
	}
	/**
	 * @param surName the surName to set
	 */
	public void setSurName(String surName) {
		this.surName = surName;
	}
	
	/**
	 * @return the dateJoining
	 */
	public Date getDateJoining() {
		return dateJoining;
	}
	/**
	 * @param dateJoining the dateJoining to set
	 */
	public void setDateJoining(Date dateJoining) {
		this.dateJoining = dateJoining;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public static List<User> getAllUsers(){
		List<User> allUsers =null;
		try{
			allUsers = find.all();
		}catch(Exception e){
			allUsers = null;
		}
		return allUsers;
	}
	/**
	 * @return the find
	 */
	public static Finder<Integer, User> getFind() {
		return find;
	}
	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Integer, User> find) {
		User.find = find;
	}
	
	public static User findUserById(int userId){
		return find.byId(userId);
	}
	public static User findUserByEmail(String email){
		return find.where().eq("email",email).findUnique();
	}
	

	
	
}
