package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class JoinFile extends Model {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "join_file_id")
	private Long joinFileId;

	@Required
	@Column(name = "join_file_name")
	private String joinFileName;


	@Required
	@Column(name = "join_type")
	private String joinType;

	@ManyToOne
	@JoinColumn(name = "organisation_id", referencedColumnName = "organisation_id")
	private Organisation organisationId;
	
	
	
	private static Finder<Integer, JoinFile> find = new Model.Finder<>(
			Integer.class, JoinFile.class);

	
	/**
	 * @return the joinFileId
	 */
	public Long getJoinFileId() {
		return joinFileId;
	}

	/**
	 * @param joinFileId
	 *            the joinFileId to set
	 */
	public void setJoinFileId(Long joinFileId) {
		this.joinFileId = joinFileId;
	}

	

	/**
	 * @return the joinFileName
	 */
	public String getJoinFileName() {
		return joinFileName;
	}

	/**
	 * @param joinFileName
	 *            the joinFileName to set
	 */
	public void setJoinFileName(String joinFileName) {
		this.joinFileName = joinFileName;
	}

	/**
	 * @return the joinType
	 */
	public String getJoinType() {
		return joinType;
	}

	/**
	 * @param joinType
	 *            the joinType to set
	 */
	public void setJoinType(String joinType) {
		this.joinType = joinType;
	}

	/**
	 * @return the organisationId
	 */
	public Organisation getOrganisationId() {
		return organisationId;
	}

	/**
	 * @param organisationId
	 *            the organisationId to set
	 */
	public void setOrganisationId(Organisation organisationId) {
		this.organisationId = organisationId;
	}

	/**
	 * @param joinFileName
	 * @param joinType
	 * @param organisationId
	 */
	public JoinFile(String joinFileName,
			String joinType, Organisation organisationId) {
		super();
		this.joinFileName = joinFileName;
		this.joinType = joinType;
		this.organisationId = organisationId;
	}

	/**
	 * @return the find
	 */
	public static Finder<Integer, JoinFile> getFind() {
		return find;
	}

	/**
	 * @param find
	 *            the find to set
	 */
	public static void setFind(Finder<Integer, JoinFile> find) {
		JoinFile.find = find;
	}

	public static List<JoinFile> findAll() {
		return find.all();
	}

	public static JoinFile findById(int id) {
		return find.byId(id);
	}

	public static List<JoinFile> findByOrganisationId(String organisationId) {
		return find.where().eq("organisation_id", organisationId).findList();
	}

	public static List<JoinFile> findByJoinType(String joinType) {
		return find.where().eq("join_type", joinType).findList();
	}

	public static List<String> getDistinctTypes(String organisationId) {

		String sql = "select DISTINCT join_type from properties_file where organisation_id = '"+organisationId+"'" ;
		SqlQuery sqlQuery = Ebean.createSqlQuery(sql);

		List<SqlRow> list = sqlQuery.findList();
		List<String> type = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++) {
			type.add(list.get(i).getString("join_type"));
		}

		return type;
	}

	public static JoinFile findByTypeOrganisation(String type, Long organisationId) {
		List<JoinFile> result = find.where().eq("join_type", type)
				.eq("organisation_id", organisationId).findList();
		if (!result.isEmpty()) {
			return result.get(0);
		} else {
			return null;
		}
	}
	
	
	public void deleteFile(){
		List<JoinGroups> groups = JoinGroups.findByJoinFileId(this.joinFileId);
		for(JoinGroups group:groups){
			JoinGroupValues.deleteGroupValues(group.getJoinGroupId());
			group.delete();
		}
		this.delete();
	}
	public static JoinFile findByJoinFileName(String joinFileName,Long organisationId) {
		List<JoinFile> result =find.where().eq("join_file_name", joinFileName).eq("organisation_id", organisationId).findList();
		if(result.size()!=0){
			return result.get(0);
		}
		return null;
	}

}
