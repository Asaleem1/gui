package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class Subject extends Model{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="subject_id")
	private Long subjectId;
	
	
	@Required
	@Column(name="subject_type")
	private String subjectType;
	
	@Column(name="organization_use")
	private String organizationUse;
	
	@Column(name="organization_role_use")
	private String organizationRoleUse;
	
	@Column(name="organization_sector_use")
	private String organizationSectorUse;
	

	@Column(name="member_function_use")
	private String memberFunctionUse;

	@Column(name="member_role_use")
	private String memberRoleUse;

	@Column(name="member_level_use")
	private String memberLevelUse;

	private static Finder<Integer, Subject> find = new Model.Finder<>(Integer.class, Subject.class);

	
	/**
	 * @return the subjectId
	 */
	public Long getSubjectId() {
		return subjectId;
	}

	/**
	 * @param subjectId the subjectId to set
	 */
	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}


	/**
	 * @return the subjectType
	 */
	public String getSubjectType() {
		return subjectType;
	}



	/**
	 * @param subjectType the subjectType to set
	 */
	public void setSubjectType(String subjectType) {
		this.subjectType = subjectType;
	}



	/**
	 * @return the organizationUse
	 */
	public String getOrganizationUse() {
		return organizationUse;
	}



	/**
	 * @param organizationUse the organizationUse to set
	 */
	public void setOrganizationUse(String organizationUse) {
		this.organizationUse = organizationUse;
	}



	/**
	 * @return the organizationRoleUse
	 */
	public String getOrganizationRoleUse() {
		return organizationRoleUse;
	}



	/**
	 * @param organizationRoleUse the organizationRoleUse to set
	 */
	public void setOrganizationRoleUse(String organizationRoleUse) {
		this.organizationRoleUse = organizationRoleUse;
	}



	/**
	 * @return the organizationSectorUse
	 */
	public String getOrganizationSectorUse() {
		return organizationSectorUse;
	}



	/**
	 * @param organizationSectorUse the organizationSectorUse to set
	 */
	public void setOrganizationSectorUse(String organizationSectorUse) {
		this.organizationSectorUse = organizationSectorUse;
	}



	/**
	 * @return the memberFunctionUse
	 */
	public String getMemberFunctionUse() {
		return memberFunctionUse;
	}



	/**
	 * @param memberFunctionUse the memberFunctionUse to set
	 */
	public void setMemberFunctionUse(String memberFunctionUse) {
		this.memberFunctionUse = memberFunctionUse;
	}



	/**
	 * @return the memberRoleUse
	 */
	public String getMemberRoleUse() {
		return memberRoleUse;
	}



	/**
	 * @param memberRoleUse the memberRoleUse to set
	 */
	public void setMemberRoleUse(String memberRoleUse) {
		this.memberRoleUse = memberRoleUse;
	}



	/**
	 * @return the memberLevelUse
	 */
	public String getMemberLevelUse() {
		return memberLevelUse;
	}



	/**
	 * @param memberLevelUse the memberLevelUse to set
	 */
	public void setMemberLevelUse(String memberLevelUse) {
		this.memberLevelUse = memberLevelUse;
	}



	/**
	 * @return the find
	 */
	public static Finder<Integer, Subject> getFind() {
		return find;
	}



	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Integer, Subject> find) {
		Subject.find = find;
	}



	public static List<Subject> findAllSubject(){
		return find.all();
	}

	/**
	 * @param subjectType
	 * @param organizationUse
	 * @param organizationRoleUse
	 * @param organizationSectorUse
	 * @param memberFunctionUse
	 * @param memberRoleUse
	 * @param memberLevelUse
	 */
	public Subject(String subjectType,
			String organizationUse, String organizationRoleUse,
			String organizationSectorUse, String memberFunctionUse,
			String memberRoleUse, String memberLevelUse) {
		super();
		this.subjectType = subjectType;
		this.organizationUse = organizationUse;
		this.organizationRoleUse = organizationRoleUse;
		this.organizationSectorUse = organizationSectorUse;
		this.memberFunctionUse = memberFunctionUse;
		this.memberRoleUse = memberRoleUse;
		this.memberLevelUse = memberLevelUse;
	}
	
	public String toString(){
		String str="";
		
		return str;
	}
		

}
