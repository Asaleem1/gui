package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import play.data.format.*;
import play.data.validation.*;
import play.db.ebean.*;

@Entity
public class Organisation extends Model{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="organisation_id")
    private Long organisationId;
	
	@Constraints.Required
	@Column(name="organisation_name")
    private String organisationName;
	
	@Column(name="organisation_address")
    private String organisationAddress;
	
	@Column(name="organisation_type")
    private String organisationType;
	
	@Column(name="contact_number")
    private String contactNumber;
	
	@Column(name="email")
    private String email;
	
	
	@Formats.DateTime(pattern="dd-MM-yyyy")
	@Column(name="date_joining")
    public Date dateJoining;
	
	
	
	private static Finder<Integer, Organisation> find = new Model.Finder<>(Integer.class, Organisation.class);
	
	
	
	/**
	 * @param organisationName
	 * @param organisationAddress
	 * @param organisationType
	 * @param contactNumber
	 * @param email
	 * @param dateJoining
	 */
	public Organisation(String organisationName, String organisationAddress,
			String organisationType, String contactNumber, String email,
			Date dateJoining) {
		super();
		this.organisationName = organisationName;
		this.organisationAddress = organisationAddress;
		this.organisationType = organisationType;
		this.contactNumber = contactNumber;
		this.email = email;
		this.dateJoining = dateJoining;
	}
	/**
	 * @return the organisationId
	 */
	public Long getOrganisationId() {
		return organisationId;
	}
	/**
	 * @param organisationId the organisationId to set
	 */
	public void setOrganisationId(Long organisationId) {
		this.organisationId = organisationId;
	}
	/**
	 * @return the organisationName
	 */
	public String getOrganisationName() {
		return organisationName;
	}
	/**
	 * @param organisationName the organisationName to set
	 */
	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}
	/**
	 * @return the organisationAddress
	 */
	public String getOrganisationAddress() {
		return organisationAddress;
	}
	/**
	 * @param organisationAddress the organisationAddress to set
	 */
	public void setOrganisationAddress(String organisationAddress) {
		this.organisationAddress = organisationAddress;
	}
	/**
	 * @return the organisationType
	 */
	public String getOrganisationType() {
		return organisationType;
	}
	/**
	 * @param organisationType the organisationType to set
	 */
	public void setOrganisationType(String organisationType) {
		this.organisationType = organisationType;
	}
	/**
	 * @return the contactNumber
	 */
	public String getContactNumber() {
		return contactNumber;
	}
	/**
	 * @param contactNumber the contactNumber to set
	 */
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the dateJoining
	 */
	public Date getDateJoining() {
		return dateJoining;
	}
	/**
	 * @param dateJoining the dateJoining to set
	 */
	public void setDateJoining(Date dateJoining) {
		this.dateJoining = dateJoining;
	}
	public static List<Organisation> getAllOrganisations(){
		List<Organisation> allOrganisations =null;
		try{
			allOrganisations = find.all();
		}catch(Exception e){
			allOrganisations = null;
		}
		return allOrganisations;
	}
	/**
	 * @return the find
	 */
	public static Finder<Integer, Organisation> getFind() {
		return find;
	}
	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Integer, Organisation> find) {
		Organisation.find = find;
	}
	
	public static Organisation findOrganisationById(int organisationId){
		return find.byId(organisationId);
	}
	public static Organisation findOrganisationByEmail(String email){
		return find.where().eq("email",email).findUnique();
	}
	 
	public static List<Organisation> findAll(){
		return find.all();
	}

	
	
}
