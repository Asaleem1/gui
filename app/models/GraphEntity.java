package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = {
		"main_type", "sub_type",
		"value", "organisation_id" }) })
public class GraphEntity extends Model {
	

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "graph_id")
	public Long graphId;
	
	@Required
	@Column(name = "main_type",nullable=false)
	private String mainType;

	@Required
	@Column(name = "sub_type",nullable=false)
	private String subType;

	@Required
	@Column(name = "value",nullable=false)
	private String value;
	
	@Required
	@ManyToOne
	@JoinColumn(name = "organisation_id", referencedColumnName = "organisation_id",nullable=false)
	private Organisation organisationId;

	public Organisation getOrganisationId() {
		return organisationId;
	}

	/**
	 * @param organisationId
	 *            the organisationId to set
	 */
	public void setOrganisationId(Organisation organisationId) {
		this.organisationId = organisationId;
		// graphId.setOrganisationId(String.valueOf(organisationId.getOrganisationId()));
	}

	/**
	 * @return the graphId
	 */
	public Long getGraphId() {
		return graphId;
	}

	/**
	 * @param graphId
	 *            the graphId to set
	 */
	public void setGraphId(Long graphId) {
		this.graphId = graphId;
	}

	private static Finder<Integer, GraphEntity> find = new Model.Finder<>(
			Integer.class, GraphEntity.class);

	public GraphEntity(String mainType, String subType, String value,
			Organisation organisationId) {
		super();
		this.setMainType(mainType);
		this.setSubType(subType);
		this.setValue(value);
		this.organisationId = organisationId;
	}
	
	/**
	 * @return the mainType
	 */
	public String getMainType() {
		return mainType;
	}

	/**
	 * @param mainType
	 *            the mainType to set
	 */
	public void setMainType(String mainType) {
		this.mainType = mainType;
	}

	/**
	 * @return the Value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the subType
	 */
	public String getSubType() {
		return subType;
	}

	/**
	 * @param subType
	 *            the subType to set
	 */
	public void setSubType(String subType) {
		this.subType = subType;
	}

	/**
	 * @return the find
	 */
	public static Finder<Integer, GraphEntity> getFind() {
		return find;
	}

	/**
	 * @param find
	 *            the find to set
	 */
	public static void setFind(Finder<Integer, GraphEntity> find) {
		GraphEntity.find = find;
	}

	public static List<GraphEntity> findAll(String organisationId) {
		return find.where().eq("organisation_id", organisationId).findList();
	}

	public static GraphEntity findById(int id) {
		return find.byId(id);
	}

	public static List<GraphEntity> findByMainTypeOrganisationId(
			String mainType, String organisationId) {
		return find.where().eq("main_type", mainType)
				.eq("organisation_id", organisationId).findList();
	}

	public static List<GraphEntity> findBySubTypeOrgnisationId(String subType,
			String organisationId) {
		return find.where().eq("sub_type", subType)
				.eq("organisation_id", organisationId).findList();
	}

	public static List<String> findByMainSubTypesOrganisationId(
			String mainType, String subType, String org_id) {
		String sql = "select value from graph_entity where organisation_id = '"
				+ org_id + "' and main_type = '" + mainType
				+ "' and sub_type = '" + subType + "'";
		SqlQuery sqlQuery = Ebean.createSqlQuery(sql);

		List<SqlRow> list = sqlQuery.findList();
		List<String> subTypes = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++) {
			subTypes.add(list.get(i).getString("value"));
		}

		return subTypes;

	}

	public static List<String> findDistinctMainTypesOrganisationId(
			String mainType, String orgId) {

		String sql = "select DISTINCT sub_type from graph_entity where organisation_id = '"
				+ orgId + "' and main_type = '" + mainType + "'";
		SqlQuery sqlQuery = Ebean.createSqlQuery(sql);

		List<SqlRow> list = sqlQuery.findList();
		List<String> subTypes = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++) {
			if (!(list.get(i).getString("sub_type")
					.equalsIgnoreCase("environment")))
				subTypes.add(list.get(i).getString("sub_type"));
		}

		return subTypes;
	}

	public static List<String> findDistinctSubTypesValuesOrganisationId(
			String mainType, String subType, String orgId) {

		String sql = "select DISTINCT value from graph_entity where organisation_id = '"
				+ orgId
				+ "' and main_type = '"
				+ mainType
				+ "' and sub_type ='" + subType + "'";
		SqlQuery sqlQuery = Ebean.createSqlQuery(sql);

		List<SqlRow> list = sqlQuery.findList();
		List<String> subTypes = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++) {
			subTypes.add(list.get(i).getString("value"));
		}

		return subTypes;
	}

	public static List<String> findDistinctSubTypesOrganisationId(String orgId) {

		String sql = "select DISTINCT sub_type from graph_entity where organisation_id = '"
				+ orgId + "' and main_type != 'PARENT'";
		SqlQuery sqlQuery = Ebean.createSqlQuery(sql);

		List<SqlRow> list = sqlQuery.findList();
		List<String> subTypes = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++) {
			subTypes.add(list.get(i).getString("sub_type"));
		}

		return subTypes;
	}

	public static List<String> findGroupTypes(String organisationId) {

		String sql = "select DISTINCT value from graph_entity where organisation_id = '"
				+ organisationId
				+ "' and  main_type = 'parent' and sub_type = 'GROUP'";
		SqlQuery sqlQuery = Ebean.createSqlQuery(sql);

		List<SqlRow> list = sqlQuery.findList();
		List<String> subTypes = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++) {
			subTypes.add(list.get(i).getString("value"));
		}

		return subTypes;
	}

	public static List<String> findValueBySubTypes(String subType,
			String organisationId) {

		String sql = "select  value from graph_entity where  organisation_id = '"
				+ organisationId + "' and sub_type = '" + subType + "'";
		SqlQuery sqlQuery = Ebean.createSqlQuery(sql);

		List<SqlRow> list = sqlQuery.findList();
		List<String> subTypes = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++) {
			subTypes.add(list.get(i).getString("value"));
		}
		return subTypes;
	}
	
	public static int findCount(String organisationId){
		String sql = "select count(*) as rows from graph_entity where organisation_id = '"+organisationId+"'";
		
		SqlQuery sqlQuery = Ebean.createSqlQuery(sql);
		List<SqlRow> list = sqlQuery.findList();
		
		return Integer.valueOf(list.get(0).getString("rows"));
		
	}
}
