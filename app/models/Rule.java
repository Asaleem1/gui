package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.avaje.ebean.Expr;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class Rule extends Model{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="rule_id")
	private Long ruleId;
	
	@Required
	@Column(name="rule_name")
	private String ruleName;
	
	@Required
	@Column(name="rule_description")
	private String ruleDescription;
	
	@Column(name="subject_id")
	@ManyToOne
	@JoinColumn(name="subject_id", referencedColumnName="subject_id")
	private Subject subjectId;
	
	@Column(name="resource_id")
	@ManyToOne
	@JoinColumn(name="resource_id", referencedColumnName="resource_id")
	private Resource resourceId;
	
	@Column(name="action_id")
	@ManyToOne
	@JoinColumn(name="action_id", referencedColumnName="action_id")
	private Action actionId;

	
	@Column(name="permission_id")
	@ManyToOne
	@JoinColumn(name="permission_id", referencedColumnName="permission_id")
	private Permission permissionId;

	/*@ManyToOne
    @JoinColumn(name="organisation_id", referencedColumnName="organisation_id")
    private Organisation organisationId;
	 */
	@ManyToOne
	@JoinColumn(name="policy_id",referencedColumnName="policy_id")
	private Policy policyId;
	
	
	private static Finder<Integer, Rule> find = new Model.Finder<>(Integer.class, Rule.class);


	/**
	 * @return the policyId
	 */
	public Policy getPolicyId() {
		return policyId;
	}


	/**
	 * @param policyId the policyId to set
	 */
	public void setPolicyId(Policy policyId) {
		this.policyId = policyId;
	}


	/**
	 * @return the organisationId
	 */
	/*public Organisation getOrganisationId() {
		return organisationId;
	}


	/**
	 * @param organisationId the organisationId to set
	 
	public void setOrganisationId(Organisation organisationId) {
		this.organisationId = organisationId;
	}

*/
	/**
	 * @return the ruleId
	 */
	public Long getruleId() {
		return ruleId;
	}


	/**
	 * @param ruleId the ruleId to set
	 */
	public void setruleId(Long ruleId) {
		this.ruleId = ruleId;
	}


	/**
	 * @return the ruleName
	 */
	public String getruleName() {
		return ruleName;
	}


	/**
	 * @param ruleName the ruleName to set
	 */
	public void setruleName(String ruleName) {
		this.ruleName = ruleName;
	}


	/**
	 * @return the ruleDescription
	 */
	public String getruleDescription() {
		return ruleDescription;
	}


	/**
	 * @param ruleDescription the ruleDescription to set
	 */
	public void setruleDescription(String ruleDescription) {
		this.ruleDescription = ruleDescription;
	}


	/**
	 * @return the subjectId
	 */
	public Subject getSubjectId() {
		return subjectId;
	}


	/**
	 * @param subjectId the subjectId to set
	 */
	public void setSubjectId(Subject subjectId) {
		this.subjectId = subjectId;
	}


	/**
	 * @return the resourceId
	 */
	public Resource getResourceId() {
		return resourceId;
	}


	/**
	 * @param resourceId the resourceId to set
	 */
	public void setResourceId(Resource resourceId) {
		this.resourceId = resourceId;
	}


	/**
	 * @return the actionId
	 */
	public Action getActionId() {
		return actionId;
	}


	/**
	 * @param actionId the actionId to set
	 */
	public void setActionId(Action actionId) {
		this.actionId = actionId;
	}


	/**
	 * @return the permissionId
	 */
	public Permission getPermissionId() {
		return permissionId;
	}


	/**
	 * @param permissionId the permissionId to set
	 */
	public void setPermissionId(Permission permissionId) {
		this.permissionId = permissionId;
	}


	/**
	 * @return the find
	 */
	public static Finder<Integer, Rule> getFind() {
		return find;
	}


	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Integer, Rule> find) {
		Rule.find = find;
	}


	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @param ruleName
	 * @param ruleDescription
	 * @param subjectId
	 * @param resourceId
	 * @param actionId
	 * @param permissionId
	 * @param organisationId
	 * @param policyId
	 */
	public Rule(String ruleName, String ruleDescription, Subject subjectId,
			Resource resourceId, Action actionId, Permission permissionId,
			Organisation organisationId, Policy policyId) {
		super();
		this.ruleName = ruleName;
		this.ruleDescription = ruleDescription;
		this.subjectId = subjectId;
		this.resourceId = resourceId;
		this.actionId = actionId;
		this.permissionId = permissionId;
		this.policyId = policyId;
	}


	public static List<Rule> findAll(String organisationId){
		return find.where().eq("organisation_id", organisationId).findList();
	}
	
	public static Rule findById(int id){
		return find.byId(id);
	}
	public static List<Rule> findByPolicyId(Long policyId){
		return find.where().eq("policy_id", policyId).findList();
	}
}

