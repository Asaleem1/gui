package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class GPropertyGroups extends Model{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="g_property_group_id")
	private Long gPropertyGroupId;
	
	@ManyToOne
    @JoinColumn(name="g_property_file_id", referencedColumnName="g_property_file_id")
    private GPropertyFile gPropertyFileId;
	
	private static Finder<Long, GPropertyGroups> find = new Model.Finder<>(Long.class, GPropertyGroups.class);
	
	
	
	
	/**
	 * @return the gPropertyGroupId
	 */
	public Long getgPropertyGroupId() {
		return gPropertyGroupId;
	}


	/**
	 * @param gPropertyGroupId the gPropertyGroupId to set
	 */
	public void setgPropertyGroupId(Long gPropertyGroupId) {
		this.gPropertyGroupId = gPropertyGroupId;
	}


	/**
	 * @return the gPropertyFileId
	 */
	public GPropertyFile getgPropertyFileId() {
		return gPropertyFileId;
	}


	/**
	 * @param gPropertyFileId the gPropertyFileId to set
	 */
	public void setgPropertyFileId(GPropertyFile gPropertyFileId) {
		this.gPropertyFileId = gPropertyFileId;
	}


	/**
	 * @param gPropertyFileId
	 */
	public GPropertyGroups(GPropertyFile gPropertyFileId) {
		super();
		this.gPropertyFileId = gPropertyFileId;
	}


	/**
	 * @return the find
	 */
	public static Finder<Long, GPropertyGroups> getFind() {
		return find;
	}


	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Long, GPropertyGroups> find) {
		GPropertyGroups.find = find;
	}

	public static List<GPropertyGroups> findAll(){
		return find.all();
	}
	
	public static GPropertyGroups findById(Long id){
		return find.byId(id);
	}
	
	
	
	public static List<GPropertyGroups> findByPropertyFileId(Long gPropertyFileId){
		return find.where().eq("g_property_file_id",gPropertyFileId).findList();
	}
	public void deleteGroup(){
		List<GPropertyGroupValues> values = GPropertyGroupValues.findByGroupId(this.getgPropertyGroupId());
		for(GPropertyGroupValues value:values){
			value.delete();
		}
		this.delete();
	}
	
}

