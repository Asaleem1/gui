package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;
import views.html.newPolicy;

import com.avaje.ebean.Expr;

@Entity
public class RuleGroup extends Model {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "rule_group_id")
	private Long ruleGroupId;

	@Column(name = "rule_group_name")
	private String ruleGroupName;

	@Required
	@ManyToOne
	@Column(name = "combining_algorithm")
	@JoinColumn(name = "combining_algorithm_id", referencedColumnName = "combining_algorithm_id")
	private CombiningAlgorithm combiningAlgorithmId;

	@Column(name = "group_description")
	private String groupDescription;

	@Column(name = "subject_id")
	@ManyToOne
	@JoinColumn(name = "subject_id", referencedColumnName = "subject_id")
	private Subject subjectId;

	@Column(name = "resource_id")
	@ManyToOne
	@JoinColumn(name = "resource_id", referencedColumnName = "resource_id")
	private Resource resourceId;

	@Column(name = "action_id")
	@ManyToOne
	@JoinColumn(name = "action_id", referencedColumnName = "action_id")
	private Action actionId;

	@Column(name = "policy_id")
	@ManyToOne
	@JoinColumn(name = "policy_id", referencedColumnName = "policy_id")
	private Policy policyId;

	/*
	@ManyToOne
	@JoinColumn(name = "organisation_id", referencedColumnName = "organisation_id")
	private Organisation organisationId;
	*/
	@Column(name = "is_base_group")
	private Boolean isBaseGroup;
	
	
	private static Finder<Integer, RuleGroup> find = new Model.Finder<>(
			Integer.class, RuleGroup.class);

	/**
	 * @return the isBaseGroup
	 */
	public Boolean getIsBaseGroup() {
		return isBaseGroup;
	}

	/**
	 * @param isBaseGroup
	 *            the isBaseGroup to set
	 */
	public void setIsBaseGroup(Boolean isBaseGroup) {
		this.isBaseGroup = isBaseGroup;
	}

	/**
	 * @return the organisationId
	 */
	/*
	public Organisation getOrganisationId() {
		return organisationId;
	}

	/**
	 * @param organisationId
	 *            the organisationId to set
	 */
	/*public void setOrganisationId(Organisation organisationId) {
		this.organisationId = organisationId;
	}
*/
	/**
	 * @return the ruleGroupId
	 */
	public Long getRuleGroupId() {
		return ruleGroupId;
	}

	/**
	 * @param ruleGroupId
	 *            the GroupId to set
	 */
	public void setRuleGroupId(Long ruleGroupId) {
		this.ruleGroupId = ruleGroupId;
	}

	/**
	 * @return the combiningAlgorithm
	 */
	public CombiningAlgorithm getCombiningAlgorithmId() {
		return combiningAlgorithmId;
	}

	/**
	 * @param combiningAlgorithm
	 *            the combiningAlgorithm to set
	 */
	public void setCombiningAlgorithm(CombiningAlgorithm combiningAlgorithmId) {
		this.combiningAlgorithmId = combiningAlgorithmId;
	}

	/**
	 * @return the find
	 */
	public static Finder<Integer, RuleGroup> getFind() {
		return find;
	}

	/**
	 * @param find
	 *            the find to set
	 */
	public static void setFind(Finder<Integer, RuleGroup> find) {
		RuleGroup.find = find;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public static List<RuleGroup> findAll() {
		return find.all();
	}

	public static RuleGroup findById(int id) {
		return find.byId(id);
	}

	/**
	 * @param ruleGroupName
	 * @param combiningAlgorithmId
	 * @param groupDescription
	 * @param subjectId
	 * @param resourceId
	 * @param actionId
	 * @param policyId
	 * @param organisationId
	 * @param isBaseGroup
	 */
	public RuleGroup(String ruleGroupName,
			CombiningAlgorithm combiningAlgorithmId, String groupDescription,
			Subject subjectId, Resource resourceId, Action actionId,
			Policy policyId, Organisation organisationId, Boolean isBaseGroup) {
		super();
		this.ruleGroupName = ruleGroupName;
		this.combiningAlgorithmId = combiningAlgorithmId;
		this.groupDescription = groupDescription;
		this.subjectId = subjectId;
		this.resourceId = resourceId;
		this.actionId = actionId;
		this.policyId = policyId;
		//this.organisationId = organisationId;
		this.isBaseGroup = isBaseGroup;
	}

	/**
	 * @return the ruleGroupName
	 */
	public String getRuleGroupName() {
		return ruleGroupName;
	}

	/**
	 * @param ruleGroupName
	 *            the ruleGroupName to set
	 */
	public void setRuleGroupName(String ruleGroupName) {
		this.ruleGroupName = ruleGroupName;
	}

	/**
	 * @return the actionId
	 */
	public Action getActionId() {
		return actionId;
	}

	/**
	 * @param actionId
	 *            the actionId to set
	 */
	public void setActionId(Action actionId) {
		this.actionId = actionId;
	}

	/**
	 * @return the policyId
	 */
	public Policy getPolicyId() {
		return policyId;
	}

	/**
	 * @param policyId
	 *            the policyId to set
	 */
	public void setPolicyId(Policy policyId) {
		this.policyId = policyId;
	}

	/**
	 * @param combiningAlgorithmId
	 *            the combiningAlgorithmId to set
	 */
	public void setCombiningAlgorithmId(CombiningAlgorithm combiningAlgorithmId) {
		this.combiningAlgorithmId = combiningAlgorithmId;
	}

	/**
	 * @return the groupDescription
	 */
	public String getGroupDescription() {
		return groupDescription;
	}

	/**
	 * @param groupDescription
	 *            the groupDescription to set
	 */
	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	/**
	 * @return the subjectId
	 */
	public Subject getSubjectId() {
		return subjectId;
	}

	/**
	 * @param subjectId
	 *            the subjectId to set
	 */
	public void setSubjectId(Subject subjectId) {
		this.subjectId = subjectId;
	}

	/**
	 * @return the resourceId
	 */
	public Resource getResourceId() {
		return resourceId;
	}

	/**
	 * @param resourceId
	 *            the resourceId to set
	 */
	public void setResourceId(Resource resourceId) {
		this.resourceId = resourceId;
	}

	public static List<RuleGroup> findAllGroups(Long policyId) {
		return find.where().eq("policy_id", policyId).findList();
	}

	public static List<RuleGroup> findAllNonBaseGroups(Long policyId) {

		return find
				.where()
				.and(Expr.eq("policy_id", policyId),
						Expr.eq("is_base_group", "0")).findList();
	}

}
