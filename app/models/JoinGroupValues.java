package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class JoinGroupValues extends Model{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="property_group_value_id")
	private Long propertyGroupValueId;
	
	@Required
	@Column(name="sub_type")
	private String subType;
	
	@Required
	@Column(name="value")
	private String value;
	
	
	
	@ManyToOne
    @JoinColumn(name="join_group_id", referencedColumnName="join_group_id")
    private JoinGroups joinGroupId;
	
	
	
	/**
	 * @param subType
	 * @param value
	 * @param joinGroupId
	 */
	public JoinGroupValues(String subType, String value,
			JoinGroups joinGroupId) {
		super();
		this.subType = subType;
		this.value = value;
		this.joinGroupId = joinGroupId;
	}

	/**
	 * @return the propertyGroupValueId
	 */
	public Long getPropertyGroupValueId() {
		return propertyGroupValueId;
	}

	/**
	 * @param propertyGroupValueId the propertyGroupValueId to set
	 */
	public void setPropertyGroupValueId(Long propertyGroupValueId) {
		this.propertyGroupValueId = propertyGroupValueId;
	}

	/**
	 * @return the subType
	 */
	public String getSubType() {
		return subType;
	}

	/**
	 * @param subType the subType to set
	 */
	public void setSubType(String subType) {
		this.subType = subType;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	

	/**
	 * @return the joinGroupId
	 */
	public JoinGroups getJoinGroupId() {
		return joinGroupId;
	}

	/**
	 * @param joinGroupId the joinGroupId to set
	 */
	public void setJoinGroupId(JoinGroups joinGroupId) {
		this.joinGroupId = joinGroupId;
	}

	private static Finder<Integer, JoinGroupValues> find = new Model.Finder<>(Integer.class, JoinGroupValues.class);
	
	/**
	 * @return the find
	 */
	public static Finder<Integer, JoinGroupValues> getFind() {
		return find;
	}

	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Integer, JoinGroupValues> find) {
		JoinGroupValues.find = find;
	}

	public static List<JoinGroupValues> findAll(){
		return find.all();
	}
	
	public static JoinGroupValues findById(int id){
		return find.byId(id);
	}
	
	public static List<JoinGroupValues> findByGroupId(Long groupId){
		return find.where().eq("join_group_id",groupId).findList();
	}
	
	public static List<JoinGroupValues> findBySubType(String subType){
		return find.where().eq("sub_type",subType).findList();
	}
	
	public static List<JoinGroupValues> findByGroupIdSubType(String groupId, String subType){
		return  find.where().eq("join_group_id", groupId).eq("sub_type",subType).findList();
		/*ArrayList<String> values = new ArrayList<String>();
		for(PropertyGroupValues groupValue:groupValues){
			values.add(groupValue.getValue());
		}
		return values;*/
	}
	public static void deleteGroupValues(Long groupId){
		for(JoinGroupValues values:find.where().eq("join_group_id", groupId).findList()){
			values.delete();
		}
	}
	
	
	
}

