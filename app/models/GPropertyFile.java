package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class GPropertyFile extends Model{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="g_property_file_id")
	private Long gPropertyFileId;
	
	@Required
	@Column(name = "g_property_file_name")
	private String gPropertyFileName;
	
	@Required
	@Column(name = "g_property_type")
	private String gPropertyType;
	
	@ManyToOne
    @JoinColumn(name="organisation_id", referencedColumnName="organisation_id")
    private Organisation organisationId;
  
	private static Finder<Integer, GPropertyFile> find = new Model.Finder<>(Integer.class, GPropertyFile.class);
	
	
	
	
	/**
	 * @return the gPropertyFileId
	 */
	public Long getGPropertyFileId() {
		return gPropertyFileId;
	}


	/**
	 * @param gPropertyFileName
	 * @param gPropertyType
	 * @param organisationId
	 */
	public GPropertyFile(String gPropertyFileName, String gPropertyType,
			Organisation organisationId) {
		super();
		this.gPropertyFileName = gPropertyFileName;
		this.gPropertyType = gPropertyType;
		this.organisationId = organisationId;
	}


	/**
	 * @return the gPropertyFileName
	 */
	public String getgPropertyFileName() {
		return gPropertyFileName;
	}


	/**
	 * @param gPropertyFileName the gPropertyFileName to set
	 */
	public void setgPropertyFileName(String gPropertyFileName) {
		this.gPropertyFileName = gPropertyFileName;
	}


	/**
	 * @return the gPropertyType
	 */
	public String getgPropertyType() {
		return gPropertyType;
	}


	/**
	 * @param gPropertyType the gPropertyType to set
	 */
	public void setgPropertyType(String gPropertyType) {
		this.gPropertyType = gPropertyType;
	}


	/**
	 * @param propertyFileId the propertyFileId to set
	 */
	public void setGPropertyFileId(Long gPropertyFileId) {
		this.gPropertyFileId = gPropertyFileId;
	}

	/**
	 * @return the organisationId
	 */
	public Organisation getOrganisationId() {
		return organisationId;
	}


	/**
	 * @param organisationId the organisationId to set
	 */
	public void setOrganisationId(Organisation organisationId) {
		this.organisationId = organisationId;
	}


	/**
	 * @param organisationId
	 */
	public GPropertyFile(Organisation organisationId) {
		super();
		this.organisationId = organisationId;
	}


	/**
	 * @return the find
	 */
	public static Finder<Integer, GPropertyFile> getFind() {
		return find;
	}


	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Integer, GPropertyFile> find) {
		GPropertyFile.find = find;
	}

	public static List<GPropertyFile> findAll(String organisationId){
		return find.where().eq("organisation_id", organisationId).findList();
	}
	
	public static GPropertyFile findById(int id){
		return find.byId(id);
	}
	
	public static List<GPropertyFile> findByOrganisationId(String organisationId){
		return find.where().eq("organisation_id",organisationId).findList();
	}
	
	public static GPropertyFile findByTypeOrganisation(String type, Long organisationId) {
		List<GPropertyFile> result = find.where().eq("g_property_type", type)
				.eq("organisation_id", organisationId).findList();
		if (!result.isEmpty()) {
			return result.get(0);
		} else {
			return null;
		}
	}
	public static GPropertyFile findByPropertyType(String propertyType,Long organisationId) {
		List<GPropertyFile> result =find.where().eq("g_property_type", propertyType).eq("organisation_id", organisationId).findList();
		
		if(result.size()!=0){
			return result.get(0);
		}
		return null;
		
	}
	public static GPropertyFile findByPropertyFileName(String propertyFileName,Long organisationId) {
		List<GPropertyFile> result =find.where().eq("g_property_file_name", propertyFileName).eq("organisation_id", organisationId).findList();
		if(result.size()!=0){
			return result.get(0);
		}
		return null;
	}
	
	public void deleteFile(){
		List<GPropertyGroups> groups = GPropertyGroups.findByPropertyFileId(this.gPropertyFileId);
		for(GPropertyGroups group:groups){
			GPropertyGroupValues.deleteGroupValues(group.getgPropertyGroupId());
			group.delete();
		}
		this.delete();
	}
	
	
}

