package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class PropertiesFile extends Model {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "property_file_id")
	private Long propertyFileId;

	@Required
	@Column(name = "property_file_name")
	private String propertyFileName;

	@Required
	@Column(name = "seed")
	private Integer seed;

	@Required
	@Column(name = "property_type")
	private String propertyType;

	@ManyToOne
	@JoinColumn(name = "organisation_id", referencedColumnName = "organisation_id")
	private Organisation organisationId;
	
	
	private static Finder<Integer, PropertiesFile> find = new Model.Finder<>(
			Integer.class, PropertiesFile.class);

	
	/**
	 * @return the propertyFileId
	 */
	public Long getPropertyFileId() {
		return propertyFileId;
	}

	/**
	 * @param propertyFileId
	 *            the propertyFileId to set
	 */
	public void setPropertyFileId(Long propertyFileId) {
		this.propertyFileId = propertyFileId;
	}

	/**
	 * @return the seed
	 */
	public Integer getSeed() {
		return seed;
	}

	/**
	 * @param seed
	 *            the seed to set
	 */
	public void setSeed(Integer seed) {
		this.seed = seed;
	}

	/**
	 * @return the propertyFileName
	 */
	public String getPropertyFileName() {
		return propertyFileName;
	}

	/**
	 * @param propertyFileName
	 *            the propertyFileName to set
	 */
	public void setPropertyFileName(String propertyFileName) {
		this.propertyFileName = propertyFileName;
	}

	/**
	 * @return the propertyType
	 */
	public String getPropertyType() {
		return propertyType;
	}

	/**
	 * @param propertyType
	 *            the propertyType to set
	 */
	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}

	/**
	 * @return the organisationId
	 */
	public Organisation getOrganisationId() {
		return organisationId;
	}

	/**
	 * @param organisationId
	 *            the organisationId to set
	 */
	public void setOrganisationId(Organisation organisationId) {
		this.organisationId = organisationId;
	}

	/**
	 * @param seed
	 * @param propertyFileName
	 * @param propertyType
	 * @param organisationId
	 */
	public PropertiesFile(Integer seed, String propertyFileName,
			String propertyType, Organisation organisationId) {
		super();
		this.seed = seed;
		this.propertyFileName = propertyFileName;
		this.propertyType = propertyType;
		this.organisationId = organisationId;
	}

	/**
	 * @return the find
	 */
	public static Finder<Integer, PropertiesFile> getFind() {
		return find;
	}

	/**
	 * @param find
	 *            the find to set
	 */
	public static void setFind(Finder<Integer, PropertiesFile> find) {
		PropertiesFile.find = find;
	}

	public static List<PropertiesFile> findAll(String organisationId) {
		return find.where().eq("organisation_id", organisationId).findList();
	}

	public static PropertiesFile findById(int id) {
		return find.byId(id);
	}

	public static List<PropertiesFile> findByOrganisationId(String organisationId) {
		return find.where().eq("organisation_id", organisationId).findList();
	}

	public static List<PropertiesFile> findByPropertyType(String propertyType,String organisationId) {
		return find.where().eq("property_type", propertyType).eq("organisation_id", organisationId).findList();
	}

	public static List<String> getDistinctTypes(String organisationId) {

		String sql = "select DISTINCT property_type from properties_file where organisation_id = '"+organisationId+"' ";
		SqlQuery sqlQuery = Ebean.createSqlQuery(sql);

		List<SqlRow> list = sqlQuery.findList();
		List<String> type = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++) {
			type.add(list.get(i).getString("property_type"));
		}

		return type;
	}

	public static PropertiesFile findByTypeOrganisation(String type, Long organisationId) {
		List<PropertiesFile> result = find.where().eq("property_type", type)
				.eq("organisation_id", organisationId).findList();
		if (!result.isEmpty()) {
			return result.get(0);
		} else {
			return null;
		}
	}
	public void deleteFile(){
		if(this.getPropertyType().equalsIgnoreCase("action")){
			deleteActionFile();
		}else{
			deleteOtherFiles();
		}
	}
	public void deleteActionFile(){
		Long organisationId = this.getOrganisationId().getOrganisationId();
		List<ActionPropertyFile> actions = ActionPropertyFile.findByOrganisationId(organisationId.intValue());
		for(ActionPropertyFile action:actions){
			ActionInUse.deleteAllActionsByAPF(action.getActionId());
			action.delete();
		}
		this.delete();
	}
	public void deleteOtherFiles(){
		List<PropertyGroups> groups = PropertyGroups.findByPropertyFileId(this.propertyFileId);
		for(PropertyGroups group:groups){
			PropertyGroupValues.deleteGroupValues(group.getPropertyGroupId());
			group.delete();
		}
		this.delete();
	}
	public static PropertiesFile findByPropertyFileName(String propertyFileName,Long organisationId) {
		List<PropertiesFile> result =find.where().eq("property_file_name", propertyFileName).eq("organisation_id", organisationId).findList();
		if(result.size()!=0){
			return result.get(0);
		}
		return null;
	}

}
