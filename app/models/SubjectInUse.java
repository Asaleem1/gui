package models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class SubjectInUse extends Model{
	
	/**
	 * 
	 */
	private static long serialVersionUID = 1L;

	@Id
	@Column(name="subject_in_use_id")
	private Long subjectInUseId;
	
	
	@ManyToOne
	@JoinColumn(name="subject_id", referencedColumnName="subject_id")
    private Subject subjectId;
	

	@ManyToOne
	@JoinColumn(name="subject_value_id", referencedColumnName="graph_entity_value_id")
    private GraphEntityValue subjectValueId;

	
	private static Finder<Integer, SubjectInUse> find = new Model.Finder<>(Integer.class, SubjectInUse.class);


	/**
	 * @param subjectId
	 * @param subjectValueId
	 */
	public SubjectInUse(Subject subjectId, GraphEntityValue subjectValueId) {
		super();
		this.subjectId = subjectId;
		this.subjectValueId = subjectValueId;
	}


	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	/**
	 * @param serialversionuid the serialversionuid to set
	 */
	public static void setSerialversionuid(long serialversionuid) {
		serialVersionUID = serialversionuid;
	}


	/**
	 * @return the subjectInUseId
	 */
	public Long getSubjectInUseId() {
		return subjectInUseId;
	}


	/**
	 * @param subjectInUseId the subjectInUseId to set
	 */
	public void setSubjectInUseId(Long subjectInUseId) {
		this.subjectInUseId = subjectInUseId;
	}


	/**
	 * @return the subjectId
	 */
	public Subject getSubjectId() {
		return subjectId;
	}


	/**
	 * @param subjectId the subjectId to set
	 */
	public void setSubjectId(Subject subjectId) {
		this.subjectId = subjectId;
	}


	/**
	 * @return the subjectValueId
	 */
	public GraphEntityValue getSubjectValueId() {
		return subjectValueId;
	}


	/**
	 * @param subjectValueId the subjectValueId to set
	 */
	public void setSubjectValueId(GraphEntityValue subjectValueId) {
		this.subjectValueId = subjectValueId;
	}


	/**
	 * @return the find
	 */
	public static Finder<Integer, SubjectInUse> getFind() {
		return find;
	}


	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Integer, SubjectInUse> find) {
		SubjectInUse.find = find;
	}

	public static List<SubjectInUse> findAllSubject(Long subjectId){
		return find.where().eq("subject_id", subjectId).findList();
	}
	
	public static List<SubjectInUse> findAllSubjectByValueId(Long subjectValueId){
		return find.where().eq("subject_value_id", subjectValueId).findList();
	}
	
}
