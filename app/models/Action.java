package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class Action extends Model{
	
	/**
	 * 
	 */
	private static long serialVersionUID = 1L;

	@Id
	@Column(name="action_id")
	private Long actionId;
	
	@Column(name="action_name")
	private String actionName;

	@Column(name="action_in_use")
	private String actionInUse;
	
	
	private static Finder<Integer, Action> find = new Model.Finder<>(Integer.class, Action.class);

	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="organisation_id", referencedColumnName="organisation_id")
    private Organisation organisationId;
	
	
	
	/**
	 * @return the organisationId
	 */
	public Organisation getOrganisationId() {
		return organisationId;
	}

	/**
	 * @param organisationId the organisationId to set
	 */
	public void setOrganisationId(Organisation organisationId) {
		this.organisationId = organisationId;
	}

	/**
	 * @param actionName
	 * @param actionInUse
	 * @param organisationId
	 */
	public Action(String actionName, String actionInUse,Organisation organisationId) {
		super();
		this.actionName = actionName;
		this.actionInUse = actionInUse;
		this.organisationId = organisationId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	/**
	 * @param serialversionuid the serialversionuid to set
	 */
	public static void setSerialversionuid(long serialversionuid) {
		serialVersionUID = serialversionuid;
	}



	/**
	 * @return the actionId
	 */
	public Long getActionId() {
		return actionId;
	}



	/**
	 * @param actionId the actionId to set
	 */
	public void setActionId(Long actionId) {
		this.actionId = actionId;
	}



	/**
	 * @return the actionName
	 */
	public String getActionName() {
		return actionName;
	}


	/**
	 * @param actionName the actionName to set
	 */
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}



	/**
	 * @return the actionInUse
	 */
	public String getActionInUse() {
		return actionInUse;
	}



	/**
	 * @param actionInUse the actionInUse to set
	 */
	public void setActionInUse(String actionInUse) {
		this.actionInUse = actionInUse;
	}



	/**
	 * @return the find
	 */
	public static Finder<Integer, Action> getFind() {
		return find;
	}



	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Integer, Action> find) {
		Action.find = find;
	}



	public static Action findById(int id){
		return find.byId(id);
	}
	
	public static List<Action> findAll(String organisationId){
		return find.where().eq("organisation_id",organisationId).findList();
	}
}
