package models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.avaje.ebean.Expr;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class GroupInUse extends Model {

	/**
	 * 
	 */

	@Id
	@Column(name = "group_in_use_id")
	private Long groupInUseId;

	@ManyToOne
	@JoinColumn(name = "policy_id", referencedColumnName = "policy_id")
	private Policy policyId;

	@ManyToOne
	@JoinColumn(name = "owned_by_rule_group_id", referencedColumnName = "rule_group_id")
	private RuleGroup ownedByRuleGroupId;

	@ManyToOne
	@JoinColumn(name = "used_rule_group_id", referencedColumnName = "rule_group_id")
	private RuleGroup usedRuleGroupId;

	/**
	 * @return the groupInUseId
	 */
	public Long getGroupInUseId() {
		return groupInUseId;
	}

	/**
	 * @param groupInUseId
	 *            the groupInUseId to set
	 */
	public void setGroupInUseId(Long groupInUseId) {
		this.groupInUseId = groupInUseId;
	}

	/**
	 * @return the policyId
	 */
	public Policy getPolicyId() {
		return policyId;
	}

	/**
	 * @param policyId
	 *            the policyId to set
	 */
	public void setPolicyId(Policy policyId) {
		this.policyId = policyId;
	}

	/**
	 * @param policyId
	 * @param ownedByRuleGroupId
	 * @param usedRuleGroupId
	 */
	public GroupInUse(Policy policyId, RuleGroup ownedByRuleGroupId,
			RuleGroup usedRuleGroupId) {
		super();
		this.policyId = policyId;
		this.ownedByRuleGroupId = ownedByRuleGroupId;
		this.usedRuleGroupId = usedRuleGroupId;
	}

	/**
	 * @return the ownedByRuleGroupId
	 */
	public RuleGroup getOwnedByRuleGroupId() {
		return ownedByRuleGroupId;
	}

	/**
	 * @param ownedByRuleGroupId
	 *            the ownedByRuleGroupId to set
	 */
	public void setOwnedByRuleGroupId(RuleGroup ownedByRuleGroupId) {
		this.ownedByRuleGroupId = ownedByRuleGroupId;
	}

	/**
	 * @return the usedRuleGroupId
	 */
	public RuleGroup getUsedRuleGroupId() {
		return usedRuleGroupId;
	}

	/**
	 * @param usedRuleGroupId
	 *            the usedRuleGroupId to set
	 */
	public void setUsedRuleGroupId(RuleGroup usedRuleGroupId) {
		this.usedRuleGroupId = usedRuleGroupId;
	}

	private static Finder<Integer, GroupInUse> find = new Model.Finder<>(
			Integer.class, GroupInUse.class);

	/**
	 * @return the find
	 */
	public static Finder<Integer, GroupInUse> getFind() {
		return find;
	}

	/**
	 * @param find
	 *            the find to set
	 */
	public static void setFind(Finder<Integer, GroupInUse> find) {
		GroupInUse.find = find;
	}

	public static List<GroupInUse> findAllGroups(Long policyId) {
		return find.where().eq("policy_id", policyId).findList();
	}
	
	public static List<GroupInUse> findAllGroupsByParent(Long policyId,Long parentId){
		System.out.println("PARENT ID: "+parentId);
		System.out.println("Policy ID: "+policyId);
		
		return find.where().and(Expr.eq("policy_id", policyId), Expr.eq("owned_by_rule_group_id", parentId)).findList();
	}

}
