package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class Resource extends Model{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="resource_id")
	private Long resourceId;
	
	@Column(name="asset_confidentiality_use")
	private String assetConfidentialityUse;
	
	@Column(name="asset_integrity_use")
	private String assetIntegrityUse;
	
	@Column(name="asset_type_use")
	private String assetTypeUse;
	
	private static Finder<Integer, Resource> find = new Model.Finder<>(Integer.class, Resource.class);

	
	public static List<Resource> findAllResource(){
		return find.all();
	}


	/**
	 * @param assetConfidentialityUse
	 * @param assetIntegrityUse
	 * @param assetTypeUse
	 */
	public Resource(String assetConfidentialityUse,
			String assetIntegrityUse, String assetTypeUse) {
		super();
		this.assetConfidentialityUse = assetConfidentialityUse;
		this.assetIntegrityUse = assetIntegrityUse;
		this.assetTypeUse = assetTypeUse;
	}


	/**
	 * @return the resourceId
	 */
	public Long getResourceId() {
		return resourceId;
	}


	/**
	 * @param resourceId the resourceId to set
	 */
	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}


	/**
	 * @return the assetConfidentialityUse
	 */
	public String getAssetConfidentialityUse() {
		return assetConfidentialityUse;
	}


	/**
	 * @param assetConfidentialityUse the assetConfidentialityUse to set
	 */
	public void setAssetConfidentialityUse(String assetConfidentialityUse) {
		this.assetConfidentialityUse = assetConfidentialityUse;
	}


	/**
	 * @return the assetIntegrityUse
	 */
	public String getAssetIntegrityUse() {
		return assetIntegrityUse;
	}


	/**
	 * @param assetIntegrityUse the assetIntegrityUse to set
	 */
	public void setAssetIntegrityUse(String assetIntegrityUse) {
		this.assetIntegrityUse = assetIntegrityUse;
	}


	/**
	 * @return the assetTypeUse
	 */
	public String getAssetTypeUse() {
		return assetTypeUse;
	}


	/**
	 * @param assetTypeUse the assetTypeUse to set
	 */
	public void setAssetTypeUse(String assetTypeUse) {
		this.assetTypeUse = assetTypeUse;
	}


	/**
	 * @return the find
	 */
	public static Finder<Integer, Resource> getFind() {
		return find;
	}


	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Integer, Resource> find) {
		Resource.find = find;
	}


	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
