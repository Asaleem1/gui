package models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class GPropertyGroupValues extends Model{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="g_property_group_value_id")
	private Long gPropertyGroupValueId;
	
	@Required
	@Column(name="sub_type")
	private String subType;
	
	@Required
	@Column(name="value")
	private String value;
	
	
	@ManyToOne
    @JoinColumn(name="g_property_group_id", referencedColumnName="g_property_group_id")
    private GPropertyGroups gPropertyGroupId;
	
	
	
	/**
	 * @return the gPropertyGroupValueId
	 */
	public Long getgPropertyGroupValueId() {
		return gPropertyGroupValueId;
	}

	/**
	 * @param gPropertyGroupValueId the gPropertyGroupValueId to set
	 */
	public void setgPropertyGroupValueId(Long gPropertyGroupValueId) {
		this.gPropertyGroupValueId = gPropertyGroupValueId;
	}

	/**
	 * @return the subType
	 */
	public String getSubType() {
		return subType;
	}

	/**
	 * @param subType the subType to set
	 */
	public void setSubType(String subType) {
		this.subType = subType;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the gPropertyGroupId
	 */
	public GPropertyGroups getgPropertyGroupId() {
		return gPropertyGroupId;
	}

	/**
	 * @param gPropertyGroupId the gPropertyGroupId to set
	 */
	public void setgPropertyFileId(GPropertyGroups gPropertyGroupId) {
		this.gPropertyGroupId = gPropertyGroupId;
	}

	
	/**
	 * @param subType
	 * @param value
	 * @param gPropertyGroupId
	 */
	public GPropertyGroupValues(String subType, String value,
			GPropertyGroups gPropertyGroupId) {
		super();
		this.subType = subType;
		this.value = value;
		this.gPropertyGroupId = gPropertyGroupId;
	}


	private static Finder<Integer, GPropertyGroupValues> find = new Model.Finder<>(Integer.class, GPropertyGroupValues.class);
	
	/**
	 * @return the find
	 */
	public static Finder<Integer, GPropertyGroupValues> getFind() {
		return find;
	}

	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Integer, GPropertyGroupValues> find) {
		GPropertyGroupValues.find = find;
	}

	public static List<GPropertyGroupValues> findAll(){
		return find.all();
	}
	
	public static GPropertyGroupValues findById(int id){
		return find.byId(id);
	}
	
	public static List<GPropertyGroupValues> findByGroupId(Long groupId){
		return find.where().eq("g_property_group_id",groupId).findList();
	}
	
	public static List<GPropertyGroupValues> findBySubType(String subType){
		return find.where().eq("sub_type",subType).findList();
	}
	public static void deleteGroupValues(Long groupId){
		for(GPropertyGroupValues values:find.where().eq("g_property_group_id", groupId).findList()){
			values.delete();
		}
	}
	
}

