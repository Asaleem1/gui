package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class PropertyGroupValues extends Model{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="property_group_value_id")
	private Long propertyGroupValueId;
	
	@Required
	@Column(name="sub_type")
	private String subType;
	
	@Required
	@Column(name="value")
	private String value;
	
	@Required
	@Column(name="selected_option")
	private String selectedOption;
	
	@ManyToOne
    @JoinColumn(name="property_group_id", referencedColumnName="property_group_id")
    private PropertyGroups propertyGroupId;
	
	
	
	/**
	 * @param subType
	 * @param value
	 * @param selectedOption
	 * @param propertyGroupId
	 */
	public PropertyGroupValues(String subType, String value, String selectedOption,
			PropertyGroups propertyGroupId) {
		super();
		this.subType = subType;
		this.value = value;
		this.selectedOption = selectedOption;
		this.propertyGroupId = propertyGroupId;
	}

	/**
	 * @return the propertyGroupValueId
	 */
	public Long getPropertyGroupValueId() {
		return propertyGroupValueId;
	}

	/**
	 * @param propertyGroupValueId the propertyGroupValueId to set
	 */
	public void setPropertyGroupValueId(Long propertyGroupValueId) {
		this.propertyGroupValueId = propertyGroupValueId;
	}

	/**
	 * @return the subType
	 */
	public String getSubType() {
		return subType;
	}

	/**
	 * @param subType the subType to set
	 */
	public void setSubType(String subType) {
		this.subType = subType;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the option
	 */
	public String getOption() {
		return selectedOption;
	}

	/**
	 * @param option the option to set
	 */
	public void setOption(String selectedOption) {
		this.selectedOption = selectedOption;
	}

	/**
	 * @return the propertyGroupId
	 */
	public PropertyGroups getPropertyGroupId() {
		return propertyGroupId;
	}

	/**
	 * @param propertyGroupId the propertyGroupId to set
	 */
	public void setPropertyGroupId(PropertyGroups propertyGroupId) {
		this.propertyGroupId = propertyGroupId;
	}

	private static Finder<Integer, PropertyGroupValues> find = new Model.Finder<>(Integer.class, PropertyGroupValues.class);
	
	/**
	 * @return the find
	 */
	public static Finder<Integer, PropertyGroupValues> getFind() {
		return find;
	}

	/**
	 * @param find the find to set
	 */
	public static void setFind(Finder<Integer, PropertyGroupValues> find) {
		PropertyGroupValues.find = find;
	}

	public static List<PropertyGroupValues> findAll(){
		return find.all();
	}
	
	public static PropertyGroupValues findById(int id){
		return find.byId(id);
	}
	
	public static List<PropertyGroupValues> findByGroupId(Long groupId){
		return find.where().eq("property_group_id",groupId).findList();
	}
	
	public static List<PropertyGroupValues> findBySubType(String subType){
		return find.where().eq("sub_type",subType).findList();
	}
	
	public static List<PropertyGroupValues> findByGroupIdSubType(String groupId, String subType){
		return  find.where().eq("property_group_id", groupId).eq("sub_type",subType).findList();
		/*ArrayList<String> values = new ArrayList<String>();
		for(PropertyGroupValues groupValue:groupValues){
			values.add(groupValue.getValue());
		}
		return values;*/
	}
	public static void deleteGroupValues(Long groupId){
		for(PropertyGroupValues values:find.where().eq("property_group_id", groupId).findList()){
			values.delete();
		}
	}
	
	
	
}

