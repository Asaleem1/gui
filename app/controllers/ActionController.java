package controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import models.GraphEntity;
import models.GraphEntityValue;
import models.User;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

public class ActionController extends Controller {

	public static Result addActions() {
		String email = session("email");
		User user = User.findUserByEmail(email);
		if (user != null) {
			String organisationId = session("organisationId");
			List<String> actionTypes = GraphEntity
					.findDistinctMainTypesOrganisationId("ACTION", organisationId);
			List<String> actionSubTypes = new ArrayList<String>();
			for (String actionType : actionTypes) {
				actionSubTypes.addAll(GraphEntity.findDistinctSubTypesValuesOrganisationId("ACTION", actionType, organisationId));
			}
			return ok(views.html.addAction.render(actionTypes, actionSubTypes));
		}
		return redirect(routes.Application.index());
	}

	public static Result submitAddAction() {

		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		System.out.println(data.toString());
		String choice = data.get("submitValue1").trim();
		String actionTypeName = data.get("actionType").toLowerCase();
		String actionSubTypeName = data.get("actionSubType").toLowerCase();
		String[] actionValues = data.get("newActionValue").toLowerCase().trim()
				.split(",");

		for (int i = 0; i < actionValues.length; i++) {
			GraphEntityValue newActionValue = new GraphEntityValue(
					actionTypeName, actionSubTypeName, actionValues[i], null);
			newActionValue.save();
		}

		if (choice.compareToIgnoreCase("more") == 0)
			return redirect(routes.ActionController.addActions());
		else
			return redirect("/adminHome");
	}

}
