package controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import models.GraphEntity;
import models.GraphEntityValue;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

public class ResourceController extends Controller {
	public static Result addResources() {
		List<String> resourceTypes = GraphEntity.findDistinctMainTypesOrganisationId("RESOURCE", session("organisationId"));
		List<String> resourceSubTypes = new ArrayList<String>();
		for(String resourceType: resourceTypes){
			resourceSubTypes.addAll(GraphEntity.findDistinctSubTypesValuesOrganisationId("RESOURCE", resourceType, session("organisationId")));
		}
		return ok(views.html.addResource
				.render(resourceTypes, resourceSubTypes));
	}

	public static Result editResources() {
		List<String> resourceTypes = GraphEntity.findDistinctMainTypesOrganisationId("RESOURCE", session("organisationId"));
		List<GraphEntityValue> resourceValues = new ArrayList<GraphEntityValue>();
		for(String resourceType:resourceTypes ){
			resourceValues.addAll(GraphEntityValue.findByMainTypeOrganisationId(resourceType,session("organisationId")));
		}return ok(views.html.editResources.render(resourceValues));
	}

	public static Result submitAddResource() {

		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		System.out.println(data.toString());
		String choice = data.get("submitValue1").trim();
		String resourceTypeName = data.get("resourceType").toLowerCase();
		String resourceSubTypeName = data.get("resourceSubType").toLowerCase();
		String[] resourceValues = data.get("newResourceValue").toLowerCase().trim()
				.split(",");

		for (int i = 0; i < resourceValues.length; i++) {
			GraphEntityValue newresourceValue = new GraphEntityValue(resourceTypeName,
					resourceSubTypeName, resourceValues[i],null);
			newresourceValue.save();
		}

		if (choice.compareToIgnoreCase("more") == 0)
			return redirect(routes.ResourceController.addResources());
		else
			return redirect("/adminHome");
	}
}
