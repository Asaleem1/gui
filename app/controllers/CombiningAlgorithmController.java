package controllers;

import java.util.List;
import java.util.Map;

import models.CombiningAlgorithm;
import models.Organisation;
import models.User;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

public class CombiningAlgorithmController extends Controller {

	public static Result submitAddCombiningAlgorithm() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		System.out.println(data.toString());
		String choice = data.get("submitValue1").trim();
		String[] algorithmValues = data.get("newCombiningAlgorithmValue")
				.toLowerCase().trim().split(",");
		Organisation org = Organisation.findOrganisationById(Integer.valueOf(session("organisationId")));
		for (int i = 0; i < algorithmValues.length; i++) {
			CombiningAlgorithm newCombiningAlgorithm = new CombiningAlgorithm(
					algorithmValues[i],org);
			newCombiningAlgorithm.save();
		}

		if (choice.compareToIgnoreCase("more") == 0)
			return redirect(routes.CombiningAlgorithmController
					.addCombiningAlgorithm());
		else
			return redirect("/adminHome");

	}

	public static Result addCombiningAlgorithm() {
		String email = session("email");
		User user = User.findUserByEmail(email);
		if (user != null && "admin".equalsIgnoreCase(user.getType())) {
			return ok(views.html.addCombiningAlgorithm.render());
		}
		return redirect(routes.Application.index());
	}

	public static Result editCombiningAlgorithm() {
		String email = session("email");
		User user = User.findUserByEmail(email);
		if (user != null) {
			System.out.println(session("organisationId"));
		List<CombiningAlgorithm> combiningAlgorithms = CombiningAlgorithm
				.findAll(session("organisationId"));
		return ok(views.html.editAlgorithms.render(combiningAlgorithms));
		}
		return redirect(routes.Application.index());
	}
}
