package controllers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.databind.ser.PropertyFilter;

import models.*;
import play.Logger;
import play.Play;
import play.Routes;
import play.data.*;
import play.mvc.*;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.libs.Json;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.nio.file.StandardCopyOption.COPY_ATTRIBUTES;

public class GroupController extends Controller {

	// GroupFile function starts from here ...
	public static Result groupFileGroups(String type) {
		String email = session("email");
		User user = User.findUserByEmail(email);
		if (user != null) {
			Long organisationId = user.getOrganisationId().getOrganisationId();
			GPropertyFile gPropertyFile = GPropertyFile.findByTypeOrganisation(
					type, organisationId);
			if (gPropertyFile != null) {
				List<GPropertyGroups> propertyFileGroups = GPropertyGroups
						.findByPropertyFileId(gPropertyFile
								.getGPropertyFileId());
				return ok(views.html.groupFileGroups.render("Select Group",
						propertyFileGroups, type));
			}
			return redirect(routes.Application.home());
		}
		return redirect(routes.Application.index());

	}

	public static Result groupProperty(String type) {
		String email = session("email");
		User user = User.findUserByEmail(email);
		if (user != null) {
			
		
		List<String> labels = new ArrayList<String>();
		for (String value : GraphEntity.findByMainSubTypesOrganisationId("GROUP", type, session("organisationId"))) {
			labels.add(value);
		}
		List<GraphEntityValue> names = null;
		if (type.equalsIgnoreCase("assetGroup")) {
			names = GraphEntityValue.findByMainSubTypes("Action", "type",session("organisationId"));
		}
		if (labels.isEmpty())
			return redirect(routes.Application.home());

		return ok(views.html.groupProperty.render("New " + type + " Group",
				type, GraphEntityValue.findByMainSubTypes(type, labels.get(0),session("organisationId")),
				GraphEntityValue.findByMainSubTypes(type, labels.get(1),session("organisationId")),
				GraphEntityValue.findByMainSubTypes(type, labels.get(2),session("organisationId")),
				names, labels));
		}
		return redirect(routes.Application.index());
	}

	public static Result GPropertyEdit() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		System.out.println(data.toString());
		String type = data.get("fileType");
		if (data.get("submitValue1").equalsIgnoreCase("newGroup")) {
			session("gFileStatus", "newGroup");
			return redirect(routes.GroupController.groupProperty(type));
		} else if (data.get("submitValue1").equalsIgnoreCase("edit")) {
			return redirect(routes.GroupController.editGroupPropertyGroup(type,
					Long.parseLong(data.get("groupId"))));
		}

		return ok();
	}

	public static Result editGroupPropertyGroup(String type, Long groupId) {

		System.out.println("Aya Group Edit me");
		List<String> labels = new ArrayList<String>();
		for (String value : GraphEntity.findByMainSubTypesOrganisationId("GROUP", type, session("ogranisationId"))) {
			labels.add(value);
		}
		List<GraphEntityValue> names = null;
		if (type.equalsIgnoreCase("assetGroup")) {
			names = GraphEntityValue.findByMainSubTypes("Action", "type",session("organisationId"));
		}
		if (labels.isEmpty())
			return redirect(routes.Application.home());
		HashMap<String, String> answers = new HashMap<String, String>();
		List<GPropertyGroupValues> groupValues = GPropertyGroupValues
				.findByGroupId(groupId);
		for (GPropertyGroupValues groupValue : groupValues) {
			answers.put(groupValue.getSubType(), groupValue.getValue());
		}
		System.out.println(answers.toString());

		return ok(views.html.groupPropertyEdit.render("New " + type + " Group",
				type, GraphEntityValue.findByMainSubTypes(type, labels.get(0),session("organisationId")),
				GraphEntityValue.findByMainSubTypes(type, labels.get(1),session("organisationId")),
				GraphEntityValue.findByMainSubTypes(type, labels.get(2),session("organisationId")),
				names, answers, labels, groupId));

	}

	public static Result addActionPropertyGroup() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String[]> map_values = request().body().asFormUrlEncoded();
		Map<String, String> data = requestData.data();
	
		Organisation organisation = Organisation.findOrganisationById(Integer.valueOf(session("organisationId")));
		for (Map.Entry<String, String> keyValue : data.entrySet()) {

			if (keyValue.getKey().startsWith("value_")) {
				List<String> values = Arrays.asList(keyValue.getValue().trim()
						.split(","));
				for (String value : values) {
					if (value != "") {
						ActionPropertyFile actionPropertyFile = new ActionPropertyFile(
								keyValue.getKey().substring(6), value,
								organisation);
						actionPropertyFile.save();
					}
				}
			}
		}
		PropertyFileController.printPropertyFile(
				PropertiesFile.findByTypeOrganisation("Action",
						organisation.getOrganisationId()).getPropertyFileId(),
				"action");
		return redirect(routes.Application.home());
	}

	public static Result submitGroupFile(String type) {

		int seed = 0;
		Organisation organisation = Organisation
				.findOrganisationById(Integer.valueOf(session("organisationId")));
		Long organisationId = organisation.getOrganisationId();
		
		String propertyFileName = type + ".properties";
		GPropertyFile updateFile = GPropertyFile.findByTypeOrganisation(type,
				organisationId);

		if (updateFile != null) {
			session("gFileStatus", "update");
			System.out.println("Aaya hai idhr," + updateFile);

			session("newGPropertyFile",
					String.valueOf(updateFile.getGPropertyFileId()));
			return redirect(routes.GroupController.groupFileGroups(type));

		} else {
			session("gFileStatus", "new");
			GPropertyFile new_group_file = new GPropertyFile(propertyFileName,
					type, organisation);
			new_group_file.save();
			System.out.println("NewMemberFileID : "
					+ String.valueOf(new_group_file.getGPropertyFileId()));
			session("newGPropertyFile",
					String.valueOf(new_group_file.getGPropertyFileId()));
		}

		session("newGPropertyFileType", type);
		List<String> labels = new ArrayList<String>();
		for (String value : GraphEntity.findByMainSubTypesOrganisationId("GROUP", type,session("organisationId"))) {
			labels.add(value);
		}
		List<GraphEntityValue> names = null;
		if (type.equalsIgnoreCase("assetGroup")) {
			names = GraphEntityValue.findByMainSubTypes("Action", "type",session("organisationId"));
		}
		if (labels.isEmpty())
			return redirect(routes.Application.home());

		return ok(views.html.groupProperty.render("New " + type + " Group",
				type, GraphEntityValue.findByMainSubTypes(type, labels.get(0),session("organisationId")),
				GraphEntityValue.findByMainSubTypes(type, labels.get(1),session("organisationId")),
				GraphEntityValue.findByMainSubTypes(type, labels.get(2),session("organisationId")),
				names, labels));

	}

	public static Result addGroupPropertyGroup() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String fileType = data.get("fileType"), option = data
				.get("submitValue1"), groupId = data.get("groupId");
		if (option.equalsIgnoreCase("back")) {
			return redirect(routes.Application.home());
		}
		if (groupId != null) {
			GPropertyGroups.findById(Long.parseLong(groupId)).deleteGroup();
		}
		Organisation organisation = Organisation.findOrganisationById(Integer.valueOf(session("organisationId")));
		GPropertyFile gPropertyFile = GPropertyFile.findByTypeOrganisation(
				fileType, organisation
						.getOrganisationId());

		GPropertyGroups gPropertyGroup = new GPropertyGroups(gPropertyFile);
		gPropertyGroup.save();

		for (Entry<String, String> keyValue : data.entrySet()) {

			if (!(keyValue.getKey().equalsIgnoreCase("submitValue1"))
					&& (!keyValue.getKey().equalsIgnoreCase("fileType"))
					&& (!keyValue.getKey().equalsIgnoreCase("groupId"))) {

				GPropertyGroupValues newValues = new GPropertyGroupValues(
						keyValue.getKey(), keyValue.getValue(), gPropertyGroup);
				newValues.save();
			}
			System.out.println(keyValue.getKey() + "::" + keyValue.getValue());
		}
		if (option.equalsIgnoreCase("more")) {
			return redirect(routes.GroupController.groupProperty(fileType));
			// return redirect(routes.Application.submitGroupFile(fileType));
		} else if (option.equalsIgnoreCase("continue")) {
			printGroupPropertyFile(gPropertyFile.getGPropertyFileId());
			return redirect(routes.Application.home());
		} else {
			printGroupPropertyFile(gPropertyFile.getGPropertyFileId());
			return redirect(routes.Application.home());
		}
	}

	public static Result printGroupPropertyFile(Long groupFileId) {

		GPropertyFile gPropertyFile = GPropertyFile.findById(groupFileId
				.intValue());
		List<GPropertyGroups> groupList = GPropertyGroups
				.findByPropertyFileId(groupFileId);
		Organisation organisation = Organisation.findOrganisationById(Integer.valueOf(session("organisationId")));
		String keys = "", values = "";
		int index = 0;
		FileWriter out = null;
		try {
			File theDir = new File(organisation.getOrganisationName());
			if(!theDir.exists()){
				theDir.mkdirs();
			}
			out = new FileWriter(new File(theDir,gPropertyFile.getgPropertyFileName()));
			out.write("nGroups = " + groupList.size() + "\n");
			// 0.category.term.type.name = Project, Long, Action-type, Person
			for (GPropertyGroups group : groupList) {
				List<GPropertyGroupValues> groupValues = GPropertyGroupValues
						.findByGroupId(group.getgPropertyGroupId());
				keys = String.valueOf(index);
				values = "";
				index++;
				for (GPropertyGroupValues groupValue : groupValues) {
					keys += "." + groupValue.getSubType();
					values += groupValue.getValue() + ",";
				}
				out.write(keys + " = "
						+ values.trim().substring(0, values.length() - 1)
						+ "\n");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return ok();

	}

	public static Result deleteGPropertyGroup() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String groupId = data.get("groupId");
		GPropertyGroups propertyGroup = GPropertyGroups.findById(Long
				.parseLong(groupId));
		propertyGroup.deleteGroup();

		return ok(Json.toJson(groupId).toString());
	}
	// GroupFile functions ends here ...
}