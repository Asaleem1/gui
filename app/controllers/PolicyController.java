package controllers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import models.ActionInUse;
import models.ActionPropertyFile;
import models.GraphEntityValue;
import models.GroupInUse;
import models.Organisation;
import models.Policy;
import models.ResourceInUse;
import models.Rule;
import models.RuleGroup;
import models.RuleInUse;
import models.SubjectInUse;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

public class PolicyController extends Controller {
	public static Result newPolicy() {

		return ok(views.html.newPolicy.render("New Policy"));

	}

	public static Result editPolicy() {

		String policyId = session("editPolicy");
		session("purpose","edit");
		if (policyId != null) {
			Policy policy = Policy.findById(Integer.parseInt(policyId));
			return ok(views.html.editPolicy.render("Edit Policy", policy));
		} else {
			return ok(views.html.home.render());
		}

	}

	public static Result editPolicySubmit() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String option = data.get("submitValue1");
		String policyId = data.get("policyId");
		if (option.compareToIgnoreCase("updatePolicy") == 0) {

			System.out.println(data.toString());
			Policy updatePolicy = Policy.findById(Integer.parseInt(data
					.get("policyId")));
			updatePolicy.setPolicyDescription(data.get("policyDescription"));
			updatePolicy.setPolicyName(data.get("policyName"));
			updatePolicy.update();
			return redirect(routes.PolicyController.editPolicy());
		} else if (option.compareToIgnoreCase("addRule") == 0) {
			session("purpose","edit");
			session("editRulePolicy", policyId);
			return redirect(routes.RuleController.addRules());

		} else if (option.compareToIgnoreCase("combineRules") == 0) {
			session("editRulePolicy", policyId);
			session("purpose","edit");
			return redirect(routes.RuleController.combineRule());
		}

		return redirect(routes.Application.home());
	}

	public static Result exportPolicy(int id) throws IOException {

		Policy policyToPrint = Policy.findById(id);
		List<RuleGroup> groupsInPolicy = RuleGroup.findAllGroups(policyToPrint
				.getPolicyId());
		LinkedHashMap<String, RuleInUse> rulesInUse = new LinkedHashMap<String, RuleInUse>();

		List<RuleInUse> tempRulesList = RuleInUse
				.findAllRuleByPolicy(policyToPrint.getPolicyId());

		for (RuleInUse ruleInUse : tempRulesList) {
			if (!rulesInUse.containsKey(ruleInUse.getRuleId().getruleId()
					.toString())) {
				rulesInUse.put(ruleInUse.getRuleId().getruleId().toString(),
						ruleInUse);
			}
		}

		

		int subjectIndex = 0, resourceIndex = 0, actionIndex = 0, groupIndex = 0;
		String allRules = "Rules=";
		FileWriter out = null;
		Organisation organisation = Organisation.findOrganisationById(Integer.valueOf(session("organisationId")));
		LinkedHashMap<String, String> uniqueDeclarations = new LinkedHashMap<String, String>();
		LinkedHashMap<String, String> rulesDict = new LinkedHashMap<String, String>();
		LinkedHashMap<String, String> groupsMaping = new LinkedHashMap<String, String>();
		try {
			File theDir = new File(organisation.getOrganisationName());
			if(!theDir.exists()){
				theDir.mkdirs();
			}
			out = new FileWriter(new File(theDir,policyToPrint.getPolicyName()));
			
			int ruleIndex = 0;
			for (RuleInUse ruleInUse : rulesInUse.values()) {

				Rule rule = ruleInUse.getRuleId();
				rulesDict.put(makeRuleKey(ruleInUse),
						"R" + String.valueOf(ruleIndex));
				allRules += "R" + String.valueOf(ruleIndex++) + ",";

				subjectIndex = updateUniqueSubjects(uniqueDeclarations,
						SubjectInUse.findAllSubject(rule.getSubjectId()
								.getSubjectId()), subjectIndex);
				actionIndex = updateUniqueActions(uniqueDeclarations,
						ActionInUse.findAllAction(rule.getActionId()
								.getActionId()), actionIndex);
				resourceIndex = updateUniqueResources(uniqueDeclarations,
						ResourceInUse.findAllResource(rule.getResourceId()
								.getResourceId()), resourceIndex);

			}

			// for group of policies.
			String allGroups = "Groups=";
			String baseGroups = "baseGroup=";
			groupIndex = 0;
			for (RuleGroup groupInUse : groupsInPolicy) {
				groupsMaping.put(groupInUse.getRuleGroupId().toString(), "G"
						+ String.valueOf(groupIndex));
				if (groupInUse.getIsBaseGroup())
					baseGroups += "G" + String.valueOf(groupIndex) + ",";
				allGroups += "G" + String.valueOf(groupIndex++) + ",";
				subjectIndex = updateUniqueSubjects(uniqueDeclarations,
						SubjectInUse.findAllSubject(groupInUse.getSubjectId()
								.getSubjectId()), subjectIndex);

				actionIndex = updateUniqueActions(uniqueDeclarations,
						ActionInUse.findAllAction(groupInUse.getActionId()
								.getActionId()), actionIndex);

				resourceIndex = updateUniqueResources(uniqueDeclarations,
						ResourceInUse.findAllResource(groupInUse
								.getResourceId().getResourceId()),
						resourceIndex);

			}
			groupIndex = 0;
			// used for sorting map of declartions..
			List<Map.Entry<String, String>> entries = new ArrayList<Map.Entry<String, String>>(
					uniqueDeclarations.entrySet());

			Collections.sort(entries,
					new Comparator<Map.Entry<String, String>>() {
						public int compare(Map.Entry<String, String> a,
								Map.Entry<String, String> b) {
							return a.getValue().compareTo(b.getValue());
						}
					});
			Map<String, String> sortedMap = new LinkedHashMap<String, String>();
			for (Map.Entry<String, String> entry : entries) {
				sortedMap.put(entry.getKey(), entry.getValue());
			}
			// sorting ends here..

			for (Map.Entry<String, String> entry : sortedMap.entrySet()) {
				String key = entry.getKey();
				Object value = entry.getValue();
				out.write(value + "=" + key + "\n");
			}
			out.write("\n");
			allRules = allRules.substring(0, allRules.length() - 1);
			out.write(allRules + "\n");
			if (!groupsInPolicy.isEmpty()) {
				out.write(baseGroups.trim().substring(0,
						baseGroups.length() - 1)
						+ "\n");
				out.write(allGroups.trim().substring(0, allGroups.length() - 1));
			}
			out.write("\n\n");

			ruleIndex = 0;
			for (RuleInUse ruleInUse : rulesInUse.values()) {
				Rule rule = ruleInUse.getRuleId();
				out.write("R" + ruleIndex + ".Desc="
						+ rule.getruleDescription().trim() + "\n");

				ruleIndex = printSubjects("R", rule.getSubjectId()
						.getSubjectId(), ruleIndex, uniqueDeclarations, out);
				ruleIndex = printActions("R", rule.getActionId().getActionId(),
						ruleIndex, uniqueDeclarations, out);
				ruleIndex = printResources("R", rule.getResourceId()
						.getResourceId(), ruleIndex, uniqueDeclarations, out);
				out.write("R" + ruleIndex + ".Decisn="
						+ rule.getPermissionId().getPermissionValue() + "\n");
				out.write("\n");
				ruleIndex += 1;
			}
			String containRules = "";
			for (RuleGroup groupInUse : groupsInPolicy) {
				containRules = "G" + String.valueOf(groupIndex) + ".Cntains=";

				List<RuleInUse> rulesInGroup = RuleInUse.findAllRule(groupInUse
						.getRuleGroupId());
				for (RuleInUse ruleInUse : rulesInGroup) {
					containRules += rulesDict.get(makeRuleKey(ruleInUse)) + ",";
				}
				List<GroupInUse> groupsInGroup = GroupInUse
						.findAllGroupsByParent(policyToPrint.getPolicyId(),
								groupInUse.getRuleGroupId());
				// System.out.println("SEIZE :: "+groupsInGroup.get(0).toString());
				for (int i = 0; i < groupsInGroup.size(); i++) {
					containRules += groupsMaping.get(groupsInGroup.get(i)
							.getUsedRuleGroupId().getRuleGroupId().toString())
							+ ",";
				}
				out.write(containRules.trim().substring(0,
						containRules.length() - 1)
						+ "\n");
				out.write("G" + groupIndex + ".Desc="
						+ groupInUse.getGroupDescription().trim() + "\n");

				groupIndex = printSubjects("G", groupInUse.getSubjectId()
						.getSubjectId(), groupIndex, uniqueDeclarations, out);
				groupIndex = printActions("G", groupInUse.getActionId()
						.getActionId(), groupIndex, uniqueDeclarations, out);

				groupIndex = printResources("G", groupInUse.getResourceId()
						.getResourceId(), groupIndex, uniqueDeclarations, out);
				out.write("G"
						+ groupIndex
						+ ".CmbnAlg="
						+ groupInUse.getCombiningAlgorithmId()
								.getCombiningAlgorithmName() + "\n");
				out.write("\n");
				groupIndex += 1;
			}

		} catch (Exception e) {
			System.out.println("Error Below");
			System.out.print(e.getMessage());
		} finally {
			if (out != null) {
				out.close();
			}
		}

		return redirect(routes.Application.home());
	}

	public static int printActions(String choice, Long actionId, int ruleIndex,
			HashMap<String, String> uniqueDeclarations, FileWriter out)
			throws IOException {
		List<ActionInUse> actionInUse = ActionInUse.findAllAction(actionId);
		int actionIndex = 0;
		for (ActionInUse actionInUse2 : actionInUse) {
			String key = makeActionKey(actionInUse2.getActionValueId());
			out.write(choice + ruleIndex + ".Action." + actionIndex + "="
					+ uniqueDeclarations.get(key) + "\n");
			actionIndex++;
		}
		return ruleIndex;
	}

	public static int updateUniqueSubjects(
			HashMap<String, String> uniqueDeclarations,
			List<SubjectInUse> Subjects, int index) {
		for (SubjectInUse subjectInUse : Subjects) {
			String key = makeKey(subjectInUse.getSubjectValueId());
			if (uniqueDeclarations.containsKey(key) == false) {
				uniqueDeclarations.put(key, "SUBJECT." + String.valueOf(index));
				index++;
			}
		}
		return index;
	}

	public static String makeRuleKey(RuleInUse rule) {
		// SubjectValue subjectvalue = subject.getSubjectValueId();
		String key = rule.getRuleId().getruleName()
				+ rule.getRuleId().getruleDescription();// ;
		return key;
	}

	

	public static int updateUniqueResources(
			HashMap<String, String> uniqueDeclarations,
			List<ResourceInUse> Resources, int index) {
		for (ResourceInUse resourceInUse : Resources) {
			String key = makeKey(resourceInUse.getResourceValueId());
			if (uniqueDeclarations.containsKey(key) == false) {
				uniqueDeclarations
						.put(key, "RESOURCE." + String.valueOf(index));
				index++;
			}
		}
		return index;
	}

	public static String makeResourceKey(GraphEntityValue resourceValue) {
		String key = resourceValue.getMainType() + "."
				+ resourceValue.getSubType() + ",equals,"
				+ resourceValue.getValue();
		return key;
	}

	public static int updateUniqueActions(
			HashMap<String, String> uniqueDeclarations,
			List<ActionInUse> Actions, int index) {
		for (ActionInUse actionInUse : Actions) {
			String key = makeActionKey(actionInUse.getActionValueId());
			if (uniqueDeclarations.containsKey(key) == false) {
				uniqueDeclarations.put(key, "ACTION." + String.valueOf(index));
				index++;
			}
		}
		return index;
	}

	public static String makeKey(GraphEntityValue actionValue) {
		String key = actionValue.getMainType() + "."
				+ actionValue.getSubType() + ",equals,"
				+ actionValue.getValue();
		return key;
	}
	public static String makeActionKey(ActionPropertyFile actionValue) {
		String key = "Action.name,equals,"
				+ actionValue.getActionValue();
		return key;
	}

	public static int printResources(String choice, Long resourceId,
			int ruleIndex, HashMap<String, String> uniqueDeclarations,
			FileWriter out) throws IOException {
		List<ResourceInUse> resourceInUse = ResourceInUse
				.findAllResource(resourceId);

		List<GraphEntityValue> resIds = new ArrayList<GraphEntityValue>();
		for (ResourceInUse resInUse : resourceInUse) {
			resIds.add(resInUse.getResourceValueId());
		}
		HashMap<String, ArrayList<String>> resCategories = new HashMap<String, ArrayList<String>>();
		String key = null;
		ArrayList<String> indexingArray = new ArrayList<String>();

		// classifying into groups
		for (int i = 0; i < resIds.size(); i++) {
			key = resIds.get(i).getMainType()
					+ resIds.get(i).getSubType();
			if (resCategories.containsKey(key) == false) {
				ArrayList<String> arr = new ArrayList<String>();

				arr.add(makeResourceKey(resIds.get(i)));
				resCategories.put(key, arr);
				indexingArray.add(key);
			} else {
				resCategories.get(key).add(makeResourceKey(resIds.get(i)));
				resCategories.put(key, resCategories.get(key));
			}
		}
		int subIndex = 0;

		// 3 cases to print AND condition ...
		// case1:
		if (resCategories.size() == 1) {
			System.out.println("1 me aya");
			for (Map.Entry<String, ArrayList<String>> entry : resCategories
					.entrySet()) {
				key = entry.getKey();
				ArrayList<String> value = entry.getValue();
				int index = 0;
				for (String string : value) {
					out.write(choice + ruleIndex + ".Resrce." + index + "="
							+ uniqueDeclarations.get(string) + "\n");
					index++;
				}
			}
		} else if (resCategories.size() == 2) { // case:2
			ArrayList<String> firstArr = resCategories
					.get(indexingArray.get(0));
			ArrayList<String> secondArr = resCategories.get(indexingArray
					.get(1));

			for (int i = 0; i < firstArr.size(); i++) {
				for (int j = 0; j < secondArr.size(); j++) {

					out.write(choice + ruleIndex + ".Resrce." + subIndex + "="
							+ uniqueDeclarations.get(firstArr.get(i)) + "\n");
					out.write(choice + ruleIndex + ".Resrce." + subIndex + "="
							+ uniqueDeclarations.get(secondArr.get(j)) + "\n");
					subIndex++;
				}
			}

		} else if (resCategories.size() == 3) {// case3:
			System.out.println("idhar aya 3");
			ArrayList<String> firstArr = resCategories
					.get(indexingArray.get(0));
			ArrayList<String> secondArr = resCategories.get(indexingArray
					.get(1));
			ArrayList<String> thirdArr = resCategories
					.get(indexingArray.get(2));

			for (int i = 0; i < firstArr.size(); i++) {
				for (int j = 0; j < secondArr.size(); j++) {
					for (int k = 0; k < thirdArr.size(); k++) {

						out.write(choice + ruleIndex + ".Resrce." + subIndex
								+ "=" + uniqueDeclarations.get(firstArr.get(i))
								+ "\n");
						out.write(choice + ruleIndex + ".Resrce." + subIndex
								+ "="
								+ uniqueDeclarations.get(secondArr.get(j))
								+ "\n");
						out.write(choice + ruleIndex + ".Resrce." + subIndex
								+ "=" + uniqueDeclarations.get(thirdArr.get(k))
								+ "\n");
						subIndex++;
					}
				}
			}
		}
		return ruleIndex;
	}

	public static int printSubjects(String choice, Long subjectId,
			int ruleIndex, HashMap<String, String> uniqueDeclarations,
			FileWriter out) throws IOException {
		List<SubjectInUse> subjectInUse = SubjectInUse
				.findAllSubject(subjectId);

		List<GraphEntityValue> subjIds = new ArrayList<GraphEntityValue>();
		for (SubjectInUse subInUse : subjectInUse) {
			subjIds.add(subInUse.getSubjectValueId());
		}
		HashMap<String, ArrayList<String>> subjCategories = new HashMap<String, ArrayList<String>>();
		String key = null;
		ArrayList<String> indexingArray = new ArrayList<String>();

		// classifying into groups
		for (int i = 0; i < subjIds.size(); i++) {
			key = subjIds.get(i).getMainType()
					+ subjIds.get(i).getSubType();
			if (subjCategories.containsKey(key) == false) {
				ArrayList<String> arr = new ArrayList<String>();

				arr.add(makeKey(subjIds.get(i)));
				subjCategories.put(key, arr);
				indexingArray.add(key);
			} else {
				subjCategories.get(key).add(makeKey(subjIds.get(i)));
				subjCategories.put(key, subjCategories.get(key));
			}
		}
		int subIndex = 0;

		// 3 cases to print AND condition ...
		// case1:
		if (subjCategories.size() == 1) {
			for (Map.Entry<String, ArrayList<String>> entry : subjCategories
					.entrySet()) {
				key = entry.getKey();
				ArrayList<String> value = entry.getValue();
				int index = 0;
				for (String string : value) {
					out.write(choice + ruleIndex + ".Subjct." + index + "="
							+ uniqueDeclarations.get(string) + "\n");
					index++;
				}
			}
		} else if (subjCategories.size() == 2) { // case:2
			ArrayList<String> firstArr = subjCategories.get(indexingArray
					.get(0));
			ArrayList<String> secondArr = subjCategories.get(indexingArray
					.get(1));

			for (int i = 0; i < firstArr.size(); i++) {
				for (int j = 0; j < secondArr.size(); j++) {
					out.write(choice + ruleIndex + ".Subjct." + subIndex + "="
							+ uniqueDeclarations.get(firstArr.get(i)) + "\n");
					out.write(choice + ruleIndex + ".Subjct." + subIndex + "="
							+ uniqueDeclarations.get(secondArr.get(j)) + "\n");
					subIndex++;
				}
			}

		} else if (subjCategories.size() == 3) {// case3:
			ArrayList<String> firstArr = subjCategories.get(indexingArray
					.get(0));
			ArrayList<String> secondArr = subjCategories.get(indexingArray
					.get(1));
			ArrayList<String> thirdArr = subjCategories.get(indexingArray
					.get(2));

			for (int i = 0; i < firstArr.size(); i++) {
				for (int j = 0; j < secondArr.size(); j++) {
					for (int k = 0; k < thirdArr.size(); k++) {

						out.write(choice + ruleIndex + ".Subjct." + subIndex
								+ "=" + uniqueDeclarations.get(firstArr.get(i))
								+ "\n");
						out.write(choice + ruleIndex + ".Subjct." + subIndex
								+ "="
								+ uniqueDeclarations.get(secondArr.get(j))
								+ "\n");
						out.write(choice + ruleIndex + ".Subjct." + subIndex
								+ "=" + uniqueDeclarations.get(thirdArr.get(k))
								+ "\n");
						subIndex++;
					}
				}
			}
		}
		return ruleIndex;
	}

	public static Result submitActionPolicy() throws NumberFormatException,
			IOException {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String choice = data.get("submitValue1").trim();
		String policyId = data.get("policyId");
		System.out.println(choice);
		if (choice.compareToIgnoreCase("print") == 0) {
			System.out.println("print policy Number:" + choice);
			exportPolicy(Integer.parseInt(policyId));

		} else if (choice.compareToIgnoreCase("edit") == 0) {
			System.out.print("Edit policy Number:" + policyId);
			session("editPolicy", policyId);
			session("purpose","edit");
			return redirect(routes.PolicyController.editPolicy());
		} else if (choice.compareToIgnoreCase("delete") == 0) {
			System.out.print("Delete policy Number:" + policyId);

		}
		return redirect(routes.Application.home());
	}
	
	

}
