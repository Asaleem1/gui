package controllers;

import static java.nio.file.StandardCopyOption.COPY_ATTRIBUTES;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import models.GraphEntity;
import models.Organisation;
import play.Play;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;

public class GraphController extends Controller {

	public static Result graphIntialize() {
		return ok(views.html.graphIntialize.render());
	}

	public static Result upload() {
		MultipartFormData body = request().body().asMultipartFormData();
		System.out.println(body.toString());
		FilePart picture = body.getFile("picture");
		String fileName = "GraphEntity";
		String message="";
		Organisation org = Organisation.findOrganisationById(Integer.valueOf(session("organisationId")));
		if (picture != null) {
			String subKey, parent;
			File file = picture.getFile();
			File newFile = new File(Play.application().path().getAbsolutePath()
					+ "/conf/"+org.getOrganisationName(), fileName);
			System.out.println(Play.application().path().getAbsolutePath());
			try {
				if (!newFile.exists()) {
					newFile.mkdirs();
				}
				Files.copy(file.toPath(), newFile.toPath(), REPLACE_EXISTING,
						COPY_ATTRIBUTES);
				
				HashMap<String, HashMap<String, List<String>>> typeSubTypes = ParseGraphEntityFile(newFile);

				for (Map.Entry<String, HashMap<String, List<String>>> keySet : typeSubTypes
						.entrySet()) {
					parent = keySet.getKey();
					for (Map.Entry<String, List<String>> subSet : keySet.getValue()
							.entrySet()) {
						subKey = subSet.getKey();
						if (subSet.getValue() != null) {
							for (String value : subSet.getValue()) {
								GraphEntity gEntity = new GraphEntity(parent,
										subKey, value, org);
								try {
									gEntity.save();
								} catch (Exception e) {
									System.out.println("InsertionERROR"
											+ e.getMessage());
								}
							}
						}
					}
				}
				message="DomainModel initialise successfully";
			} catch (IOException e) {
				// TODO Auto-generated catch block
				message="Error in Creating Directory";
				System.out.println("Uploading ERROR:" + e.getMessage());
				e.printStackTrace();
			}
			
			flash("message",message);
			return redirect(routes.Application.adminHome());
		} else {
			message="Missing file error";
			flash("message", message);
			return redirect(routes.Application.adminHome());
		}
	}
	
	public static Result parseGraphFile(){
		Organisation org = Organisation.findOrganisationById(Integer.valueOf(session("organisationId")));
		String fileName = "GraphEntity";
		File file = new File(Play.application().path().getAbsolutePath()
					+ "/conf/"+org.getOrganisationName(),fileName);
		if (file != null && file.exists()) {
			String subKey, parent;
			
			HashMap<String, HashMap<String, List<String>>> typeSubTypes = ParseGraphEntityFile(file);

			for (Map.Entry<String, HashMap<String, List<String>>> keySet : typeSubTypes
					.entrySet()) {
				parent = keySet.getKey();
				for (Map.Entry<String, List<String>> subSet : keySet.getValue()
						.entrySet()) {
					subKey = subSet.getKey();
					if (subSet.getValue() != null) {
						for (String value : subSet.getValue()) {
							
							GraphEntity gEntity = new GraphEntity(parent,
									subKey, value, org);
							try {
								gEntity.save();
							} catch (Exception e) {
								System.out.println("InsertionERROR"
										+ e.getMessage());
							}
						}
					}
				}
			}
			return redirect(routes.Application.adminHome());
		} else {
			flash("message", "error:Missing file");
			System.out.println("Error:File Not Found");
			return redirect(routes.Application.adminHome());
		}
	}
	
	public static HashMap<String, HashMap<String, List<String>>> ParseGraphEntityFile(
			File f) {
		Charset charset = Charset.forName("ISO-8859-1");
		HashMap<String, List<String>> relatedLines = new HashMap<String, List<String>>();
		HashMap<String, List<String>> subTypes = new HashMap<String, List<String>>();
		HashMap<String, HashMap<String, List<String>>> treeStructure = new HashMap<String, HashMap<String, List<String>>>();
		List<String> tempList = null, tempValues = null, keyValue = null;

		String subType = new String(), key = new String(), parent = new String();
		int i = 0;
		try {
			for (String line : Files.readAllLines(f.toPath(), charset)) {
				if (line.startsWith("luMain.")) {
					if (!relatedLines.containsKey("luMain.")) {
						List<String> newList = new ArrayList<String>();
						newList.add(line.substring(7).trim());
						relatedLines.put("luMain.", newList);
					} else {
						tempList = relatedLines.get("luMain.");
						tempList.add(line.substring(7).trim());
						relatedLines.put("luMain.", tempList);
					}
				} else if (line.startsWith("luSub.")) {
					if (!relatedLines.containsKey("luSub.")) {
						List<String> newList = new ArrayList<String>();
						newList.add(line.substring(6).trim());
						relatedLines.put("luSub.", newList);
					} else {
						tempList = relatedLines.get("luSub.");
						tempList.add(line.substring(6).trim());
						relatedLines.put("luSub.", tempList);
					}
				}
			}
			tempList = relatedLines.get("luSub.");
			for (String line : tempList) {
				keyValue = Arrays.asList(line.split("="));
				parent = keyValue.get(0).trim();
				System.out.println(keyValue);
				tempValues = Arrays.asList(keyValue.get(1).replaceAll(" ", "")
						.split(","));
				subTypes.put(parent, tempValues);
				//Note: To ignore groups uncomment below code section.
				/*if (parent.endsWith("Group")) {
					if (!treeStructure.containsKey("parent")) {
						HashMap<String, List<String>> tempTree = new HashMap<String, List<String>>();
						tempTree.put(parent, tempValues);
						treeStructure.put("parent", tempTree);
					} else {
						treeStructure.get("parent").put(parent, tempValues);
					}
				} else {
					subTypes.put(parent, tempValues);
				}*/

			}

			tempList = relatedLines.get("luMain.");
			for (String line : tempList) {
				keyValue = Arrays.asList(line.split("="));
				parent = keyValue.get(0).trim();
				tempValues = Arrays.asList(keyValue.get(1).replaceAll(" ", "")
						.split(","));
				System.out.println(parent + "::" + tempValues.toString());
				subTypes.put(parent, tempValues);
				for (String value : tempValues) {
					if (!treeStructure.containsKey(parent)) {
						HashMap<String, List<String>> tempTree = new HashMap<String, List<String>>();
						tempTree.put(value, subTypes.get(value));
						treeStructure.put(parent, tempTree);
					} else {
						treeStructure.get(parent).put(value,
								subTypes.get(value));
					}
				}
				if (!treeStructure.containsKey("parent")) {
					HashMap<String, List<String>> tempTree = new HashMap<String, List<String>>();
					tempTree.put(parent, tempValues);
					treeStructure.put("parent", tempTree);
				} else {
					treeStructure.get("parent").put(parent, tempValues);
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return treeStructure;
	}
}
