package controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import models.GraphEntity;
import models.GraphEntityValue;
import play.Routes;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

public class SubjectController  extends Controller{
	
	public static Result addSubjects() {
		List<String> subjectTypes = GraphEntity.findDistinctMainTypesOrganisationId("SUBJECT", session("organisationId"));
		List<String> subjectSubTypes = new ArrayList<String>();
		for(String subjectType: subjectTypes){
			subjectSubTypes.addAll(GraphEntity.findDistinctSubTypesValuesOrganisationId("SUBJECT",subjectType,session("organisationId")));
		}
		 
		return ok(views.html.addSubject.render(subjectTypes, subjectSubTypes));
	}
	
	public static Result submitAddSubject() {

		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		System.out.println(data.toString());
		String choice = data.get("submitValue1").trim();
		String subjectTypeName = data.get("subjectType").toLowerCase();
		String subjectSubTypeName = data.get("subjectSubType").toLowerCase();
		String[] subjectValues = data.get("newSubjectValue").toLowerCase().trim()
				.split(",");

		for (int i = 0; i < subjectValues.length; i++) {
			GraphEntityValue newsubjectValue = new GraphEntityValue(subjectTypeName,
					subjectSubTypeName, subjectValues[i],null);
			newsubjectValue.save();
		}

		if (choice.compareToIgnoreCase("more") == 0)
			return redirect(routes.SubjectController.addSubjects());
		else
			return redirect("/adminHome");
	}
	public static Result editSubjects() {
		List<String> subjectTypes = GraphEntity.findDistinctMainTypesOrganisationId("SUBJECT", session("organisationId"));
		List<GraphEntityValue> subjectValues = new ArrayList<GraphEntityValue>();
		for(String subjectType:subjectTypes){
			subjectValues.addAll(GraphEntityValue.findByMainTypeOrganisationId(subjectType,session("organisationId")));
		}
		return ok(views.html.editSubjects.render(subjectValues));
	}
	
	public static Result updateSubType(String MainType){
		
		return ok();
	}

}
