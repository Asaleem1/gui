package controllers;

import java.util.List;
import java.util.Map;

import models.Action;
import models.ActionInUse;
import models.ActionPropertyFile;
import models.CombiningAlgorithm;
import models.GraphEntityValue;
import models.GroupInUse;
import models.Organisation;
import models.Permission;
import models.Policy;
import models.Resource;
import models.ResourceInUse;
import models.Rule;
import models.RuleGroup;
import models.RuleInUse;
import models.Subject;
import models.SubjectInUse;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

public class RuleController extends Controller {
	public static Result addNewRule() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		Organisation org = Organisation.findOrganisationById(Integer.valueOf(session("organisationId")));
		Policy new_policy = new Policy(data.get("policyName"),
				data.get("policyDescription"), org);
		new_policy.save();

		System.out.println("NewPolicy ID : "
				+ String.valueOf(new_policy.getPolicyId()));
		session("purpose", "new");
		session("newPolicy", String.valueOf(new_policy.getPolicyId()));
		return redirect(routes.RuleController.addRules());
	}

	public static Result addRules() {
		if (session("email") != null) {
			if ((session("editRulePolicy") == null)
					&& (session("newPolicy") == null)) {
				return redirect(routes.Application.home());
			}
			if(!ActionPropertyFile.isActionsAvailable(session("organisationId"))){
				flash("message", "Please ask administrator to add action first");
				System.out.println("Please ask administrator to add action first");
				return redirect(routes.Application.home());
			}
			return ok(views.html.addRules.render("New RuleGroup", Permission
					.findAll(session("organisationId")), GraphEntityValue.findByMainSubTypes("Action",
					"type",session("organisationId")), GraphEntityValue.findByMainSubTypes("Asset",
					"Confidentiality",session("organisationId")), GraphEntityValue.findByMainSubTypes(
					"Asset", "Integrity",session("organisationId")), GraphEntityValue.findByMainSubTypes(
					"Asset", "Type",session("organisationId")), GraphEntityValue.findByMainSubTypes(
					"Member", "function",session("organisationId")), GraphEntityValue.findByMainSubTypes(
					"Member", "Level",session("organisationId")), GraphEntityValue.findByMainSubTypes(
					"Member", "Role",session("organisationId")), GraphEntityValue.findByMainSubTypes(
					"Organisation", "headquartered",session("organisationId")), GraphEntityValue
					.findByMainSubTypes("Organisation", "type",session("organisationId")),
					GraphEntityValue.findByMainSubTypes("Organisation",
							"sector",session("organisationId"))));
		}
		return redirect(routes.Application.index());
	}

	public static Result addRule() {

		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String[]> map_values = request().body().asFormUrlEncoded();
		Map<String, String> data = requestData.data();
		Organisation org = Organisation.findOrganisationById(Integer.valueOf(session("organisationId")));
		Subject new_sub = new Subject(data.get("subjectType"),
				data.get("organisationOption"),
				data.get("organisationRoleOption"),
				data.get("organisationSecOption"),
				data.get("memberFunctionOption"), data.get("memberRoleOption"),
				data.get("memberLevelOption"));
		new_sub.save();

		if (data.get("subjectType").compareToIgnoreCase("Member") == 0) {
			if (data.get("memberFunctionOption") != null) {
				String[] checkedVal = map_values.get("memberFunctionId[]");

				for (int i = 0; i < checkedVal.length; i++) {
					SubjectInUse SIU = new SubjectInUse(new_sub,
							GraphEntityValue.findById(Integer
									.parseInt(checkedVal[i])));
					SIU.save();
				}
			}
			if (data.get("memberRoleOption") != null) {
				String[] checkedVal = map_values.get("memberRoleId[]");
				for (int i = 0; i < checkedVal.length; i++) {
					SubjectInUse SIU = new SubjectInUse(new_sub,
							GraphEntityValue.findById(Integer
									.parseInt(checkedVal[i])));
					SIU.save();
				}
			}
			if (data.get("memberLevelOption") != null) {
				String[] checkedVal = map_values.get("memberLevelId[]");
				for (int i = 0; i < checkedVal.length; i++) {
					SubjectInUse SIU = new SubjectInUse(new_sub,
							GraphEntityValue.findById(Integer
									.parseInt(checkedVal[i])));
					SIU.save();
				}
			}
		} else if (data.get("subjectType").compareToIgnoreCase("Organisation") == 0) {
			if (data.get("organisationOption") != null) {
				String[] checkedVal = map_values.get("organisationId[]"); // get
				for (int i = 0; i < checkedVal.length; i++) {
					SubjectInUse SIU = new SubjectInUse(new_sub,
							GraphEntityValue.findById(Integer
									.parseInt(checkedVal[i])));
					SIU.save();
				}
			}
			if (data.get("organisationSecOption") != null) {
				String[] checkedVal = map_values.get("organisationSectorId[]"); // get
				for (int i = 0; i < checkedVal.length; i++) {
					SubjectInUse SIU = new SubjectInUse(new_sub,
							GraphEntityValue.findById(Integer
									.parseInt(checkedVal[i])));
					SIU.save();
				}
			}
			if (data.get("organisationRoleOption") != null) {
				String[] checkedVal = map_values.get("organisationRoleId[]"); // get
				for (int i = 0; i < checkedVal.length; i++) {
					SubjectInUse SIU = new SubjectInUse(new_sub,
							GraphEntityValue.findById(Integer
									.parseInt(checkedVal[i])));
					SIU.save();
				}
			}
		}
		Resource new_res = new Resource(data.get("assetConfidentialityOption"),
				data.get("assetIntegrityOption"), data.get("assetTypeOption"));
		new_res.save();

		if (data.get("assetConfidentialityOption") != null) {
			String[] checkedVal = map_values.get("assetConfidentialityId[]"); // get
			for (int i = 0; i < checkedVal.length; i++) {
				ResourceInUse RIU = new ResourceInUse(new_res,
						GraphEntityValue.findById(Integer
								.parseInt(checkedVal[i])));
				RIU.save();
			}
		}
		if (data.get("assetIntegrityOption") != null) {
			String[] checkedVal = map_values.get("assetIntegrityId[]"); // get
			for (int i = 0; i < checkedVal.length; i++) {
				ResourceInUse RIU = new ResourceInUse(new_res,
						GraphEntityValue.findById(Integer
								.parseInt(checkedVal[i])));
				RIU.save();
			}
		}
		if (data.get("assetTypeOption") != null) {
			String[] checkedVal = map_values.get("assetTypeId[]"); // get
			for (int i = 0; i < checkedVal.length; i++) {
				ResourceInUse RIU = new ResourceInUse(new_res,
						GraphEntityValue.findById(Integer
								.parseInt(checkedVal[i])));
				RIU.save();
			}
		}

		Action new_action = new Action("", data.get("actionTypeOption"),org);
		new_action.save();

		Permission permission = Permission.findById(Integer.parseInt(data
				.get("decision")));

		String p_id = "";
		if (session("purpose") != null
				&& session("purpose").equalsIgnoreCase("edit")) {
			p_id = session("editRulePolicy");
		} else if (session("purpose") != null
				&& session("purpose").equalsIgnoreCase("new")) {
			p_id = session("newPolicy");
		}

		Policy policy_used = Policy.findById(Integer.parseInt(p_id));

		Rule new_rule = new Rule(data.get("ruleName"),
				data.get("ruleDescription"), new_sub, new_res, new_action,
				permission, null, policy_used);
		new_rule.save();

		RuleInUse newRuleInUse = new RuleInUse(null, new_rule, policy_used);
		newRuleInUse.save();

		if (data.get("actionTypeOption") != null) {
			String[] checkedVal = map_values.get("actionValueId[]");
			for (int i = 0; i < checkedVal.length; i++) {
				ActionInUse AIU = new ActionInUse(new_action,
						ActionPropertyFile.findById(Integer
								.parseInt(checkedVal[i])));
				AIU.save();
			}
		}

		if (data.get("submitValue1") != null) {
			return redirect(routes.RuleController.combineRule());
		} else if (data.get("submitValue3") != null) {
			return redirect(routes.PolicyController.editPolicy());
		}
		return redirect(routes.RuleController.addRules());

	}

	public static Result submitCombineRule() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String[]> map_values = request().body().asFormUrlEncoded();
		Map<String, String> data = requestData.data();
		Organisation org = Organisation.findOrganisationById(Integer.valueOf(session("organisationId")));
		String subjectInUse = data.get("subjectOption");
		String resourceInUse = data.get("resourceOption");
		Resource new_res = null;
		Subject new_sub = null;

		Action new_action = new Action("", data.get("actionTypeOption"),org);
		new_action.save();

		if (data.get("actionTypeOption") != null) {
			String[] checkedVal = map_values.get("actionValueId[]");
			for (int i = 0; i < checkedVal.length; i++) {
				ActionInUse AIU = new ActionInUse(new_action,
						ActionPropertyFile.findById(Integer
								.parseInt(checkedVal[i])));
				AIU.save();
			}

		}

		// Subject
		if (subjectInUse != "null") {
			new_sub = new Subject(data.get("subjectType"),
					data.get("organisationOption"),
					data.get("organisationRoleOption"),
					data.get("organisationSecOption"),
					data.get("memberFunctionOption"),
					data.get("memberRoleOption"), data.get("memberLevelOption"));
			new_sub.save();

			if (data.get("subjectType").compareToIgnoreCase("Member") == 0) {
				if (data.get("memberFunctionOption") != null) {
					String[] checkedVal = map_values.get("memberFunctionId[]");

					for (int i = 0; i < checkedVal.length; i++) {
						SubjectInUse SIU = new SubjectInUse(new_sub,
								GraphEntityValue.findById(Integer
										.parseInt(checkedVal[i])));
						SIU.save();
					}
				}
				if (data.get("memberRoleOption") != null) {
					String[] checkedVal = map_values.get("memberRoleId[]");
					for (int i = 0; i < checkedVal.length; i++) {
						SubjectInUse SIU = new SubjectInUse(new_sub,
								GraphEntityValue.findById(Integer
										.parseInt(checkedVal[i])));
						SIU.save();
					}
				}
				if (data.get("memberLevelOption") != null) {
					String[] checkedVal = map_values.get("memberLevelId[]");
					for (int i = 0; i < checkedVal.length; i++) {
						SubjectInUse SIU = new SubjectInUse(new_sub,
								GraphEntityValue.findById(Integer
										.parseInt(checkedVal[i])));
						SIU.save();
					}
				}
			} else if (data.get("subjectType").compareToIgnoreCase(
					"Organisation") == 0) {
				if (data.get("organisationOption") != null) {
					String[] checkedVal = map_values.get("organisationId[]"); // get
					for (int i = 0; i < checkedVal.length; i++) {
						SubjectInUse SIU = new SubjectInUse(new_sub,
								GraphEntityValue.findById(Integer
										.parseInt(checkedVal[i])));
						SIU.save();
					}
				}
				if (data.get("organisationSecOption") != null) {
					String[] checkedVal = map_values
							.get("organisationSectorId[]"); // get
					for (int i = 0; i < checkedVal.length; i++) {
						SubjectInUse SIU = new SubjectInUse(new_sub,
								GraphEntityValue.findById(Integer
										.parseInt(checkedVal[i])));
						SIU.save();
					}
				}
				if (data.get("organisationRoleOption") != null) {
					String[] checkedVal = map_values
							.get("organisationRoleId[]"); // get
					for (int i = 0; i < checkedVal.length; i++) {
						SubjectInUse SIU = new SubjectInUse(new_sub,
								GraphEntityValue.findById(Integer
										.parseInt(checkedVal[i])));
						SIU.save();
					}
				}
			}
		}
		if (resourceInUse != "null") {
			new_res = new Resource(data.get("assetConfidentialityOption"),
					data.get("assetIntegrityOption"),
					data.get("assetTypeOption"));
			new_res.save();

			if (data.get("assetConfidentialityOption") != null) {
				String[] checkedVal = map_values
						.get("assetConfidentialityId[]"); // get
				for (int i = 0; i < checkedVal.length; i++) {
					ResourceInUse RIU = new ResourceInUse(new_res,
							GraphEntityValue.findById(Integer
									.parseInt(checkedVal[i])));
					RIU.save();
				}
			}
			if (data.get("assetIntegrityOption") != null) {
				String[] checkedVal = map_values.get("assetIntegrityId[]"); // get
				for (int i = 0; i < checkedVal.length; i++) {
					ResourceInUse RIU = new ResourceInUse(new_res,
							GraphEntityValue.findById(Integer
									.parseInt(checkedVal[i])));
					RIU.save();
				}
			}
			if (data.get("assetTypeOption") != null) {
				String[] checkedVal = map_values.get("assetTypeId[]"); // get
				for (int i = 0; i < checkedVal.length; i++) {
					ResourceInUse RIU = new ResourceInUse(new_res,
							GraphEntityValue.findById(Integer
									.parseInt(checkedVal[i])));
					RIU.save();
				}
			}
		}

		String policyId = "";
		if (session("purpose") != null
				&& session("purpose").equalsIgnoreCase("edit")) {
			policyId = session("editRulePolicy");
		} else if (session("purpose") != null
				&& session("purpose").equalsIgnoreCase("new")) {
			policyId = session("newPolicy");
		}

		Policy new_policy = Policy.findById(Integer.parseInt(policyId));

		CombiningAlgorithm combiningAlgoUsed = CombiningAlgorithm
				.findById(Integer.parseInt(data.get("combiningAlgorithm")));
		Boolean isBaseGroup = false;
		if (data.get("isBaseGroup") != null) {
			isBaseGroup = true;
		}

		// adding newGroup in db..
		RuleGroup new_group = new RuleGroup(data.get("ruleGroupName"),
				combiningAlgoUsed, data.get("groupDescription"), new_sub,
				new_res, new_action, new_policy, null, isBaseGroup);
		new_group.save();

		String[] checkedVal = map_values.get("ruleId[]");
		if (checkedVal != null) {
			for (int i = 0; i < checkedVal.length; i++) {
				RuleInUse RIU = new RuleInUse(new_group, Rule.findById(Integer
						.parseInt(checkedVal[i])), new_policy);
				RIU.save();
			}
		}

		String[] groupUsedVal = map_values.get("groupId[]");
		if (groupUsedVal != null) {
			for (int i = 0; i < groupUsedVal.length; i++) {
				GroupInUse GIU = new GroupInUse(new_policy, new_group,
						RuleGroup.findById(Integer.parseInt(groupUsedVal[i])));
				GIU.save();
			}
		}

		if (data.get("submitValue1") != null) {
			return redirect(routes.Application.home());
		} else {
			return redirect(routes.RuleController.combineRule());
		}
	}

	public static Result combineRule() {

		if(!ActionPropertyFile.isActionsAvailable(session("organisationId"))){
			flash("message", "Please ask administrator to add action first");
			System.out.println("Please ask administrator to add action first");
			return redirect(routes.Application.home());
		}
		int policyId = -1;
		if (session("purpose") != null
				&& session("purpose").equalsIgnoreCase("edit")) {
			policyId = Integer.parseInt(session("editRulePolicy"));
		} else if (session("purpose") != null
				&& session("purpose").equalsIgnoreCase("new")) {
			policyId = Integer.parseInt(session("newPolicy"));
		} else {
			return redirect(routes.Application.index());
		}

		System.out.println("NEWPOLICY" + policyId);
		Policy newPolicy = Policy.findById(policyId);
		if (newPolicy == null) {
			return redirect(routes.Application.index());
		}
		return ok(views.html.combine
				.render("Combine Rules", Rule.findByPolicyId(newPolicy
						.getPolicyId()), RuleGroup
						.findAllNonBaseGroups(newPolicy.getPolicyId()),
						GraphEntityValue.findByMainSubTypes("Action", "type",session("organisationId")),
						Permission.findAll(session("organisationId")), CombiningAlgorithm
								.findAllCombiningAlgorithms(session("organisationId")),
						GraphEntityValue.findByMainSubTypes("Asset",
								"Confidentiality",session("organisationId")), GraphEntityValue
								.findByMainSubTypes("Asset", "Integrity",session("organisationId")),
						GraphEntityValue.findByMainSubTypes("Asset", "Type",session("organisationId")),
						GraphEntityValue.findByMainSubTypes("Member",
								"Function",session("organisationId")), GraphEntityValue
								.findByMainSubTypes("Member", "Level",session("organisationId")),
						GraphEntityValue.findByMainSubTypes("Member", "Role",session("organisationId")),
						GraphEntityValue.findByMainSubTypes("Organisation",
								"headquartered",session("organisationId")), GraphEntityValue
								.findByMainSubTypes("Organisation", "type",session("organisationId")),
						GraphEntityValue.findByMainSubTypes("Organisation",
								"Sector",session("organisationId"))));
	}

	public static Result addPermissions() {
		return ok(views.html.addPermission.render());
	}

	public static Result submitAddPermissions() {

		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		System.out.println(data.toString());
		Organisation org = Organisation.findOrganisationById(Integer.valueOf(session("organisationId")));
		String choice = data.get("submitValue1").trim();
		String[] permissionValues = data.get("newPermissionValue")
				.toLowerCase().trim().split(",");

		for (int i = 0; i < permissionValues.length; i++) {
			Permission newPermission = new Permission(permissionValues[i],org);
			newPermission.save();
		}

		if (choice.compareToIgnoreCase("more") == 0)
			return redirect(routes.RuleController.addPermissions());
		else
			return redirect("/adminHome");
	}

	public static Result editPermissions() {
		List<Permission> permissionValues = Permission.findAll(session("organisationId"));
		return ok(views.html.editPermissions.render(permissionValues));
	}

}
