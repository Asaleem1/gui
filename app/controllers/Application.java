package controllers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.PersistenceException;

import com.fasterxml.jackson.databind.ser.PropertyFilter;

import models.*;
import play.Logger;
import play.Play;
import play.Routes;
import play.api.libs.Collections;
import play.data.*;
import play.mvc.*;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.libs.Json;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.nio.file.StandardCopyOption.COPY_ATTRIBUTES;

public class Application extends Controller {
	public static class Login {

		public String email;
		public String password;

		/**
		 * @return the email
		 */
		public String getEmail() {
			return email;
		}

		/**
		 * @param email
		 *            the email to set
		 */
		public void setEmail(String email) {
			this.email = email;
		}

		/**
		 * @return the password
		 */
		public String getPassword() {
			return password;
		}

		/**
		 * @param password
		 *            the password to set
		 */
		public void setPassword(String password) {
			this.password = password;
		}

		public String validate() {
			System.out.println(email + password);
			if (User.authenticate(email, password) == null) {
				return "Invalid organisation or password";
			}
			return null;
		}

	}

	// final static Form<Organisation> organisationForm =
	// Form.form(Organisation.class);

	public static Result index() {
		String email = session("email");
		User user = User.findUserByEmail(email);
		if (user != null) {
			return redirect(routes.Application.home());

		} else {
			return redirect(routes.Application.login());
			//return ok(views.html.index.render(null));
		}

	}

	public static Result runPolicy() {
		String email = session("email");
		User user = User.findUserByEmail(email);
		if (user != null) {
			return ok(views.html.runPolicy.render("Run Policy Test",
					Policy.findAll(session("organisationId"))));
		} else {
			return redirect(routes.Application.index());
		}

	}
	public static Result authenticate() {
		Form<Login> loginForm = Form.form(Login.class).bindFromRequest();
		System.out.println(loginForm.toString());
		if (loginForm.hasErrors()) {
			return badRequest(views.html.login.render(loginForm));
		} else {
			session().clear();
			session("email", loginForm.get().email);
			User user = User.findUserByEmail(loginForm.get().email);
			session("organisationId", String.valueOf(user.getOrganisationId()
					.getOrganisationId()));
			session("type", user.getType());
			if (user.getType().equalsIgnoreCase("admin")) {
				return redirect(routes.Application.adminHome());
			} else {
				return redirect(routes.Application.home());
			}
		}
	}

	public static Result login() {
		// session().clear();

		String email = session("email");
		User user = User.findUserByEmail(email);
		if (user != null) {
			Logger.info("Already Signed INN");
			return ok(views.html.index.render(user));
		} else {
			Logger.info("No Organisation");
			return ok(views.html.login.render(Form.form(Login.class)));
		}
	}

	public static Result home() {
		String email = session("email");
		User user = User.findUserByEmail(email);
		if (user != null) {
			return ok(views.html.home.render());
		} else {
			return redirect(routes.Application.index());
		}
	}

	public static Result adminHome() {
		String email = session("email");
		User user = User.findUserByEmail(email);
		if (user != null && "admin".equalsIgnoreCase(user.getType())) {
			String uploaded = "no";
			if (Application.checkDomainModel()) {
				uploaded = "yes";
			}
			return ok(views.html.adminHome.render(uploaded));
		} else {
			return redirect(routes.Application.index());
		}
	}

	public static Result about() {
		return ok(views.html.about.render("About Us"));
	}

	public static Result contact() {
		return ok(views.html.contact.render("Contact Us"));
	}

	public static Result policies() {
		String email = session("email");
		User user = User.findUserByEmail(email);
		if (user != null) {
			return ok(views.html.policies.render("Policies List",
					Policy.findAll(session("organisationId"))));
		} else {
			return redirect(routes.Application.index());
		}
	}

	public static Result result() {
		String email = session("email");
		User user = User.findUserByEmail(email);
		if (user != null) {
			return ok(views.html.result.render("Test Results",
					Policy.findAll(session("organisationId"))));
		} else {
			return redirect(routes.Application.index());
		}
	}

	public static Result logout() {
		session().clear();
		return redirect(routes.Application.index());
	}

	public static Result print() {
		String email = session("email");
		User user = User.findUserByEmail(email);
		if (user != null) {
			return ok(views.html.print.render("Select RuleGroup to Print",
					Policy.findAll(session("organisationId"))));
		} else {
			return redirect(routes.Application.index());
		}
	}

	public static Result newFile() {
		String email = session("email");
		User user = User.findUserByEmail(email);
		if (user != null) {
			String orgId = String.valueOf(user.getOrganisationId()
					.getOrganisationId());
			List<String> fileNames = GraphEntity
					.findDistinctSubTypesOrganisationId(orgId);
			fileNames.add("AssetJoinGroup");
			fileNames.add("MemberJoinGroup");
			fileNames.add("OrganisatinJoinGroup");

			return ok(views.html.newPropertyFile.render("New File", fileNames));
		} else {
			return redirect(routes.Application.index());
		}
	}

	public static Result submitMemberFile() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String type = data.get("type");
		int seed = 0;
		Organisation organisation = Organisation.findOrganisationById(Integer
				.valueOf(session("organisationId")));
		Long organisationId = organisation.getOrganisationId();// it will be
																// changed and
																// extract from
																// Session

		if (type.endsWith("JoinGroup")) {
			System.out
					.println("In submitMemberFile for adding new JoinGroup File");
			return redirect(routes.JoinFileController.joinGroupFile(type));
		} else if (type.endsWith("Group")) {
			return redirect(routes.GroupController.submitGroupFile(type));
		}

		if (!type.equalsIgnoreCase("action")) {
			seed = Integer.parseInt(data.get("seed"));
		}
		String propertyFileName = type + ".properties";
		PropertiesFile updateFile = PropertiesFile.findByTypeOrganisation(type,
				organisationId);
		if (updateFile != null) {
			session("fileStatus", "update");
			session("newPropertyFile",
					String.valueOf(updateFile.getPropertyFileId()));
			if (!type.equalsIgnoreCase("action")) {
				updateFile.setSeed(seed);
				updateFile.update();
			}
		} else {
			session("fileStatus", "new");
			PropertiesFile new_file = new PropertiesFile(seed,
					propertyFileName, type, organisation);
			new_file.save();
			System.out.println("NewMemberFileID : "
					+ String.valueOf(new_file.getPropertyFileId()));
			session("newPropertyFile",
					String.valueOf(new_file.getPropertyFileId()));
		}
		session("newPropertyFileType", type);

		if (type.compareToIgnoreCase("member") == 0) {
			return redirect(routes.PropertyFileController.memberProperty(type));
		} else if (type.compareToIgnoreCase("action") == 0) {
			return redirect(routes.PropertyFileController.actionProperty(type));
		} else if (type.compareToIgnoreCase("organisation") == 0) {
			return redirect(routes.PropertyFileController.memberProperty(type));
			// return
			// redirect(routes.PropertyFileController.organisationProperty(type));
		} else if (type.compareToIgnoreCase("asset") == 0) {
			return redirect(routes.PropertyFileController.assetProperty(type));
		} else {
			return redirect(routes.Application.adminHome());
		}
	}

	public static Result getMainTypes() {
		String email = session("email");
		User user = User.findUserByEmail(email);
		String orgId = String.valueOf(user.getOrganisationId()
				.getOrganisationId());

		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String subType = data.get("mainType");
		String parent = data.get("parent");
		System.out.println(subType + ":" + parent);
		List<String> propertyMainTypes = GraphEntity
				.findDistinctSubTypesValuesOrganisationId(parent, subType,
						orgId);
		System.out.println(propertyMainTypes.toString());
		return ok(Json.toJson(propertyMainTypes).toString());
	}

	public static Result propertyFiles() {
		String email = session("email");
		User user = User.findUserByEmail(email);
		if (user != null) {
			List<String> types = PropertiesFile
					.getDistinctTypes(session("organisationId"));
			List<GPropertyFile> gPropertyFile = GPropertyFile
					.findAll(session("organisationId"));
			if (gPropertyFile.size() != 0) {
				types.add("Group");
				types.add("Join");
			}
			return ok(views.html.propertyFiles.render("Property Files", types));
		}
		return redirect(routes.Application.index());
	}

	public static Result submitPropertyFile() {

		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String choice = data.get("submitValue1").trim();
		String propertyFileName = data.get("propertyFileName");
		String propertyType = data.get("propertyType");
		Long propertyFileId = null;
		Organisation organisation = Organisation.findOrganisationById(Integer
				.valueOf(session("organisationId")));
		if (propertyType.equalsIgnoreCase("group")) {
			GPropertyFile file = GPropertyFile.findByPropertyFileName(
					propertyFileName, organisation.getOrganisationId());
			if (file != null) {
				propertyFileId = file.getGPropertyFileId();
			}
		} else if (propertyType.equalsIgnoreCase("join")) {
			JoinFile file = JoinFile.findByJoinFileName(propertyFileName,
					organisation.getOrganisationId());

			if (file != null) {
				propertyFileId = file.getJoinFileId();
			}
		} else {
			PropertiesFile file = PropertiesFile.findByPropertyFileName(
					propertyFileName, organisation.getOrganisationId());
			if (file != null) {
				propertyFileId = file.getPropertyFileId();
			}
		}
		System.out.println(choice);
		if (choice.compareToIgnoreCase("print") == 0) {
			PropertyFileController.printPropertyFile(propertyFileId,
					propertyType);

		} else if (choice.compareToIgnoreCase("edit") == 0) {
			System.out.print("Edit PropertyFile Number:" + propertyFileId);
			session("editPropertyFile", String.valueOf(propertyFileId));
			return redirect(routes.PolicyController.editPolicy());
		} else if (choice.compareToIgnoreCase("delete") == 0) {
			System.out.print("Delete PropertyFile Number:" + propertyFileId);

		}
		return redirect(routes.Application.propertyFiles());

	}

	public static Result editAttributes() {
		String email = session("email");
		User user = User.findUserByEmail(email);
		if (user != null) {
			List<String> mainTypes = GraphEntity
					.findDistinctMainTypesOrganisationId("PARENT",
							session("organisationId"));

			System.out.println("AAA" + mainTypes.toString());
			// System.out.println(actionTypes.toString());
			List<GraphEntityValue> actionValues = new ArrayList<GraphEntityValue>();
			for (String mainType : mainTypes) {
				List<String> actionTypes = GraphEntity
						.findDistinctMainTypesOrganisationId(mainType,
								session("organisationId"));

				for (String actionType : actionTypes) {
					System.out.println(actionType);
					actionValues.addAll(GraphEntityValue
							.findByMainTypeOrganisationId(actionType,
									session("organisationId")));
				}
			}
			System.out.println(actionValues.toString());

			return ok(views.html.editAttributes.render(mainTypes, actionValues));
		}
		return redirect(routes.Application.index());
	}

	public static Result deleteFile() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String fileName = data.get("fileName");
		String fileType = data.get("fileType");
		Organisation org = Organisation.findOrganisationById(Integer
				.valueOf(session("organisationId")));

		if (fileType.equalsIgnoreCase("group")) {
			GPropertyFile propertyFile = GPropertyFile.findByPropertyFileName(
					fileName, org.getOrganisationId());
			propertyFile.deleteFile();
		} else if (fileType.equalsIgnoreCase("join")) {
			JoinFile joinFile = JoinFile.findByJoinFileName(fileName,
					org.getOrganisationId());
			joinFile.deleteFile();
			session().remove("joinFileStatus");
		} else {
			PropertiesFile propertyFile = PropertiesFile
					.findByPropertyFileName(fileName, org.getOrganisationId());
			propertyFile.deleteFile();
		}

		return ok(Json.toJson(fileName).toString());
	}

	// Ajax Stuff

	public static Result javascriptRoutes() {
		response().setContentType("text/javascript");
		return ok(Routes.javascriptRouter("myCustom",
				controllers.routes.javascript.Application.getMainTypes()));
	}

	public static Result addUser() {
		if (session("email") == null)
			return ok(views.html.addUser.render(Form.form(User.class),
					Organisation.findAll()));
		else {
			return redirect(routes.Application.index());
		}
	}

	public static Result submitUser() {
		// DynamicForm requestData = Form.form().bindFromRequest();
		Form<User> UserForm = Form.form(User.class).bindFromRequest();

		Map<String, String> data = UserForm.data();
		String firstName = data.get("firstName"), surName = data.get("surName"), username = data
				.get("userName"), password = data.get("password"), contactNumber = data
				.get("contactNumber"), email = data.get("email"), organisationId = data
				.get("organisationId"), type = data.get("type");

		System.out.println(organisationId);
		Organisation org = Organisation.findOrganisationById(Integer
				.parseInt(organisationId));
		if (UserForm.hasErrors()) {
			System.out.println(UserForm.toString());
		}
		User newUser = new User(email, contactNumber, firstName, surName,
				new Date(), username, password, type, org);
		try {
			newUser.save();
			session().clear();
			session("email", newUser.getEmail());
			session("organisationId", String.valueOf(newUser
					.getOrganisationId().getOrganisationId()));
			session("type", newUser.getType());
			if (newUser.getType().equalsIgnoreCase("admin")) {
				return redirect(routes.Application.adminHome());
			} else {
				return redirect(routes.Application.home());
			}
		} catch (PersistenceException e) {
			if (e.getMessage().contains("Duplicate")) {
				flash("message", "Duplicate Entry");
			}
			return badRequest(views.html.addUser.render(UserForm,
					Organisation.findAll()));
		}

	}

	public static boolean checkDomainModel() {
		Organisation org = Organisation.findOrganisationById(Integer
				.valueOf(session("organisationId")));
		if (org != null) {

			File file = new File(Play.application().path().getAbsolutePath()
					+ "/conf/" + org.getOrganisationName() + "/GraphEntity");

			if (file.exists()
					&& GraphEntity.findCount(session("organisationId")) != 0) {
				return true;
			}
		}
		return false;
	}

	public static Result exportAttributes() {
		Organisation org = Organisation.findOrganisationById(Integer
				.valueOf(session("organisationId")));
		List<String> includeList = new ArrayList<String>(), groupSubTypes = new ArrayList<String>();
		includeList.add("Organisation");
		includeList.add("Member");
		includeList.add("Asset");

		groupSubTypes.add("category");
		groupSubTypes.add("term");
		groupSubTypes.add("type");
		Connection conn = getDbConnection("localhost", "3306",
				"domain", "root", "");
		
		try {
			
			if (org != null) {
				Statement statement = conn.createStatement();
				String sql = "";
				String mainType, subType, value;
				List<GraphEntityValue> gEntityValues = GraphEntityValue
						.findAll(String.valueOf(org.getOrganisationId()));
				for (GraphEntityValue entityValue : gEntityValues) {
					mainType = entityValue.getMainType();
					subType = entityValue.getSubType();
					value = entityValue.getValue();
					sql = "";
					if (includeList.contains(mainType)) {
						sql += " insert into lu_" + mainType + "_" + subType
								+ " (" + mainType + "_" + subType
								+ "_ds) SELECT '" + value
								+ "' FROM dual WHERE NOT EXISTS";
						sql += " (SELECT 1 FROM lu_" + mainType + "_" + subType
								+ " AS x WHERE x." + mainType + "_" + subType
								+ "_ds = '" + value + "');";
						sql += "\n";
					} else if ((mainType.contains("group") || mainType
							.contains("Group"))
							&& groupSubTypes.contains(subType)) {
						sql += " insert into lu_Group_" + subType + " (group_"
								+ subType + "_ds) SELECT '" + value
								+ "' FROM dual WHERE NOT EXISTS";
						sql += " (SELECT 1 FROM lu_Group_" + subType
								+ " AS x WHERE x.group_" + subType + "_ds = '"
								+ value + "');";
						sql += "\n";
					}
					if(sql!=""){
						System.out.println("____"+sql);
						statement.executeUpdate(sql);
					}

				}
				flash("message", "Attributes export done successfully");
				// System.out.println(sql);
			}
		} catch (SQLException e) {
			flash("message", "Error in exporting attributes"+e.toString());
			
			
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				flash("message", "error in exporting attributes");
				
			}
		}

		return redirect(routes.Application.adminHome());
	}

	public static Connection getDbConnection(String ipAddress, String port,
			String dB, String userName, String password) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your MySQL JDBC Driver?");
			e.printStackTrace();
			return null;
		}

		System.out.println("MySQL JDBC Driver Registered!");
		Connection connection = null;

		try {
			connection = DriverManager.getConnection("jdbc:mysql://"
					+ ipAddress + ":" + port + "/" + dB + "", userName,
					password);

		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return null;
		}

		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}
		return connection;
	}
}
