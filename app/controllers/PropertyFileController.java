package controllers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import models.ActionPropertyFile;
import models.CombiningAlgorithm;
import models.GPropertyFile;
import models.GraphEntity;
import models.GraphEntityValue;
import models.JoinFile;
import models.Permission;
import models.PropertiesFile;
import models.PropertyGroupValues;
import models.PropertyGroups;
import models.Organisation;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

public class PropertyFileController extends Controller {

	// PropertyFile Function starts from here..
	public static Result memberProperty(String type) {

		if (session("fileStatus") != null) {
			if (session("fileStatus").equalsIgnoreCase("update")) {
				return redirect(routes.PropertyFileController.groups(type));
			}
		}
		// new file
		List<String> labels = new ArrayList<String>();
		if (type.equalsIgnoreCase("member")) {
			labels.add("function");
			labels.add("level");
			labels.add("role");
		} else {
			labels.add("headquartered");
			labels.add("sector");
			labels.add("type");
		}
		return ok(views.html.memberProperty.render("New " + type + " Group",
				GraphEntityValue.findByMainSubTypes(type, labels.get(0),
						session("organisationId")), GraphEntityValue
						.findByMainSubTypes(type, labels.get(1),
								session("organisationId")), GraphEntityValue
						.findByMainSubTypes(type, labels.get(2),
								session("organisationId")), labels));
	}

	/*
	 * 
	 * identical function to memberProperty, Updated memberProperty function to
	 * handle organisationProperty properly
	 * 
	 * public static Result organisationProperty(String type) {
	 * 
	 * if (session("fileStatus") != null) { if
	 * (session("fileStatus").equalsIgnoreCase("update")) { return
	 * redirect(routes.GroupController.groups(type)); } } List<String> labels =
	 * new ArrayList<String>(); labels.add("headquartered");
	 * labels.add("sector"); labels.add("type"); return
	 * ok(views.html.organisationProperty.render("New " + type + " Group",
	 * GraphEntityValue.findByMainSubTypes(type, labels.get(0)),
	 * GraphEntityValue.findByMainSubTypes(type, labels.get(1)),
	 * GraphEntityValue.findByMainSubTypes(type, labels.get(2)), labels)); }
	 */
	public static Result groups(String type) {
		Long organisationId = Long.parseLong(session("organisationId"));
		PropertiesFile propertyFile = PropertiesFile.findByTypeOrganisation(
				type, organisationId);
		if (propertyFile != null) {
			List<PropertyGroups> propertyFileGroups = PropertyGroups
					.findByPropertyFileId(propertyFile.getPropertyFileId());
			return ok(views.html.groups.render("Select Group",
					propertyFileGroups, propertyFile.getSeed(), type));
		}
		return redirect(routes.Application.home());

	}

	public static Result organisationGroups(String type) {
		Long organisationId = Long.parseLong(session("organisationId"));
		PropertiesFile propertyFile = PropertiesFile.findByTypeOrganisation(
				type, organisationId);
		if (propertyFile != null) {
			List<PropertyGroups> propertyFileGroups = PropertyGroups
					.findByPropertyFileId(propertyFile.getPropertyFileId());
			return ok(views.html.groups.render("Select Group",
					propertyFileGroups, propertyFile.getSeed(), type));
		}
		return redirect(routes.Application.home());

	}

	public static Result assetProperty(String type) {

		if (session("fileStatus") != null) {
			if (session("fileStatus").equalsIgnoreCase("update")) {
				return redirect(routes.PropertyFileController.groups(type));
			}
		}
		// new file
		List<String> labels = new ArrayList<String>();
		labels.add("group");
		labels.add("type");
		labels.add("confidentiality");
		labels.add("integrity");
		return ok(views.html.assetProperty.render("New " + type + " Group",
				GraphEntityValue.findByMainSubTypes(type, labels.get(0),
						session("organisationId")), GraphEntityValue
						.findByMainSubTypes(type, labels.get(1),
								session("organisationId")), GraphEntityValue
						.findByMainSubTypes(type, labels.get(2),
								session("organisationId")), GraphEntityValue
						.findByMainSubTypes(type, labels.get(3),
								session("organisationId")), labels));

	}

	public static Result PropertyEdit() {
		int index = 0;// it will be used later
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String type = data.get("fileType");
		List<String> labels = new ArrayList<String>();
		if (type.equalsIgnoreCase("asset")) {
			labels.add("group");
			labels.add("type");
			labels.add("confidentiality");
			labels.add("integrity");
		} else if (type.equalsIgnoreCase("member")) {
			labels.add("function");
			labels.add("level");
			labels.add("role");
		} else if (type.equalsIgnoreCase("organisation")) {
			labels.add("headquartered");
			labels.add("sector");
			labels.add("type");
		}

		List<String> options = new ArrayList<String>();

		if (data.get("submitValue1").equalsIgnoreCase("goBack")) {
			return redirect(routes.Application.newFile());
		}
		if (data.get("submitValue1").equalsIgnoreCase("newGroup")) {
			session("fileStatus", "newGroup");
			if (type.equalsIgnoreCase("asset")) {
				return redirect(routes.PropertyFileController
						.assetProperty(type));
			} else if (type.equalsIgnoreCase("organisation")) {
				return redirect(routes.PropertyFileController
						.memberProperty(type));
				// return
				// redirect(routes.PropertyFileController.organisationProperty(type));
			} else if (type.equalsIgnoreCase("member")) {
				return redirect(routes.PropertyFileController
						.memberProperty(type));
			}
		}
		String groupId = data.get("groupId");
		PropertyGroups propertyGroup = PropertyGroups.findById(Long
				.parseLong(groupId));
		ArrayList<List<String>> propertyGroupValues = new ArrayList<List<String>>();
		for (String label : labels) {
			List<PropertyGroupValues> groupValues = PropertyGroupValues
					.findByGroupIdSubType(groupId, label);
			ArrayList<String> values = new ArrayList<String>();
			for (PropertyGroupValues groupValue : groupValues) {
				values.add(groupValue.getValue());
			}
			if (groupValues.size() != 0) {
				options.add(index, groupValues.get(0).getOption());

			} else {
				options.add(index, "");
			}
			index++;
			propertyGroupValues.add(values);

		}

		if (type.equalsIgnoreCase("asset")) {
			return ok(views.html.assetPropertyEdit.render("New " + type
					+ " Group", GraphEntityValue.findByMainSubTypes(type,
					labels.get(0), session("organisationId")), GraphEntityValue
					.findByMainSubTypes(type, labels.get(1),
							session("organisationId")), GraphEntityValue
					.findByMainSubTypes(type, labels.get(2),
							session("organisationId")), GraphEntityValue
					.findByMainSubTypes(type, labels.get(3),
							session("organisationId")), labels, propertyGroup,
					propertyGroupValues.get(0), propertyGroupValues.get(1),
					propertyGroupValues.get(2), propertyGroupValues.get(3),
					options, groupId));
		} else if (type.equalsIgnoreCase("member")
				|| type.equalsIgnoreCase("organisation")) {
			return ok(views.html.memberPropertyEdit.render("New " + type
					+ " Group", GraphEntityValue.findByMainSubTypes(type,
					labels.get(0), session("organisationId")), GraphEntityValue
					.findByMainSubTypes(type, labels.get(1),
							session("organisationId")), GraphEntityValue
					.findByMainSubTypes(type, labels.get(2),
							session("organisationId")), labels, propertyGroup,
					propertyGroupValues.get(0), propertyGroupValues.get(1),
					propertyGroupValues.get(2), options, groupId));
		} /*
		 * else if (type.equalsIgnoreCase("organisation")) { return
		 * ok(views.html.organisationPropertyEdit.render("New " + type +
		 * " Group", GraphEntityValue.findByMainSubTypes(type, labels.get(0)),
		 * GraphEntityValue.findByMainSubTypes(type, labels.get(1)),
		 * GraphEntityValue.findByMainSubTypes(type, labels.get(2)), labels,
		 * propertyGroup, propertyGroupValues.get(0),
		 * propertyGroupValues.get(1), propertyGroupValues.get(2), options,
		 * groupId)); }
		 */
		return redirect(routes.Application.home());

	}

	public static Result actionProperty(String type) {
		int organisationId = Integer.parseInt(session("organisationId"));

		if (session("fileStatus") != null) {
			if (session("fileStatus").equalsIgnoreCase("update")) {
				System.out.println(type);
				return ok(views.html.actionProperty.render("New " + type
						+ " Group", GraphEntityValue.findByMainSubTypes(type,
						"type", session("organisationId")), ActionPropertyFile
						.findByOrganisationId(organisationId),
						session("fileStatus")));
			} else {// new file
				return ok(views.html.actionProperty.render("New " + type
						+ " Group", GraphEntityValue.findByMainSubTypes(type,
						"type", session("organisationId")), null,
						session("fileStatus")));
			}
		} else {
			return redirect(routes.Application.home());
		}

	}

	public static Result updateActionValue() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String id = data.get("actionId");
		ActionPropertyFile actionPropertyValue = ActionPropertyFile
				.findById(Integer.parseInt(id));
		actionPropertyValue.setActionValue(data.get("actionValue"));
		actionPropertyValue.update();
		return ok();
	}

	public static Result updateAttributeValue() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String id = data.get("valueId");
		GraphEntityValue graphEntityValue = GraphEntityValue.findById(Integer
				.parseInt(id));
		if (graphEntityValue.getMainType().equalsIgnoreCase("action")
				&& graphEntityValue.getSubType().equalsIgnoreCase("type")) {
			// update actionPropertyValues Type
			List<ActionPropertyFile> actionValues = ActionPropertyFile
					.findByActionType(graphEntityValue.getValue(),
							session("organisationId"));
			for (ActionPropertyFile actionValue : actionValues) {
				actionValue.setActionType(data.get("valueName"));
				actionValue.update();
			}
		}
		graphEntityValue.setValue(data.get("valueName"));
		graphEntityValue.update();

		return ok();
	}
	
	public static Result updatePermission() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String id = data.get("valueId");
		Permission permission = Permission.findById(Integer.valueOf(id));
		permission.setPermissionValue(data.get("valueName"));
		permission.update();
		return ok();
	}
	public static Result deletePermission() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String id = data.get("attributeId");
		Permission permission = Permission.findById(Integer.valueOf(id));
		permission.delete();
		return ok();
	}
	
	public static Result updateAlgorithm() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String id = data.get("valueId");
		CombiningAlgorithm combiningAlgorithm = CombiningAlgorithm.findById(Integer.valueOf(id));
		combiningAlgorithm.setCombiningAlgorithmName(data.get("valueName"));
		combiningAlgorithm.update();
		return ok();
	}
	public static Result deleteAlgorithm() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String id = data.get("attributeId");
		CombiningAlgorithm combiningAlgorithm = CombiningAlgorithm.findById(Integer.valueOf(id));
		combiningAlgorithm.delete();
		return ok();
	}

	public static Result deleteActionValue() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String id = data.get("actionId");
		ActionPropertyFile actionPropertyValue = ActionPropertyFile
				.findById(Integer.parseInt(id));

		actionPropertyValue.deleteActionPropertyFile();

		return ok();
	}

	public static Result deleteAttributeValue() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String id = data.get("attributeId");
		GraphEntityValue graphEntityValue = GraphEntityValue.findById(Integer
				.parseInt(id));
		System.out.println(graphEntityValue.toString());
		graphEntityValue.deleteEntity(session("organisationId"));

		return ok();
	}

	public static Result updatePropertyFiles() {

		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		List<PropertiesFile> propertyFiles = null;
		List<GPropertyFile> gPropertyFiles = null;
		List<JoinFile> joinFiles = null;
		List<String> propertyFileNames = new ArrayList<String>();
		String propertyType = data.get("propertyType");
		if (propertyType.equalsIgnoreCase("group")) {
			gPropertyFiles = GPropertyFile.findAll(session("organisationId"));
			for (GPropertyFile gPropertyFile : gPropertyFiles) {
				propertyFileNames.add(gPropertyFile.getgPropertyFileName());
			}
		} else if (propertyType.equalsIgnoreCase("join")) {
			joinFiles = JoinFile.findAll();
			for (JoinFile joinFile : joinFiles) {
				propertyFileNames.add(joinFile.getJoinFileName());
			}
		} else {
			propertyFiles = PropertiesFile.findByPropertyType(propertyType,
					session("organisationId"));
			for (PropertiesFile propertyFile : propertyFiles) {
				propertyFileNames.add(propertyFile.getPropertyFileName());
			}
		}
		return ok(Json.toJson(propertyFileNames).toString());

	}

	public static Result fetchAllValues() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String attributeOption = data.get("attributeType");
		List<String> mainTypes = GraphEntity
				.findDistinctMainTypesOrganisationId(attributeOption,
						session("organisationId"));
		List<GraphEntityValue> attributeTypeValues = new ArrayList<GraphEntityValue>();
		for (String mainType : mainTypes) {
			attributeTypeValues.addAll(GraphEntityValue
					.findByMainTypeOrganisationId(mainType,
							session("organisationId")));
		}
		return ok(Json.toJson(attributeTypeValues).toString());

	}

	public static Result fetchActionValues() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String actionType = data.get("actionType");
		List<ActionPropertyFile> actionValues = ActionPropertyFile
				.findByTypeOrganisationId(actionType, session("organisationId"));
		System.out.println(actionType + "::"
				+ Json.toJson(actionValues).toString());

		return ok(Json.toJson(actionValues).toString());

	}

	public static Result deleteGroup() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String groupId = data.get("groupId");
		PropertyGroups propertyGroup = PropertyGroups.findById(Long
				.parseLong(groupId));
		propertyGroup.deleteGroup();

		return ok(Json.toJson(groupId).toString());
	}

	public static Result printPropertyFile(Long id, String fileType) {

		System.out.println("FileType In PrintPropertyFile::" + fileType);
		if (fileType.equalsIgnoreCase("group")) {
			return GroupController.printGroupPropertyFile(id);
		} else if (fileType.equalsIgnoreCase("join")) {
			return JoinFileController.printJoinPropertyFile(id);
		}
		Organisation organisation = Organisation.findOrganisationById(Integer
				.valueOf(session("organisationId")));
		PropertiesFile propertyFile = PropertiesFile.findById(id.intValue());
		if (propertyFile.getPropertyType().equalsIgnoreCase("action")) {
			System.out.println("PRoperTyFileToBePRinted::"
					+ propertyFile.getPropertyFileName());
			List<ActionPropertyFile> typeValues = ActionPropertyFile
					.findByOrganisationId(organisation.getOrganisationId().intValue());
			HashMap<String, String> types = new HashMap<String, String>();

			for (ActionPropertyFile typeValue : typeValues) {
				if (!types.containsKey(typeValue.getActionType())) {
					types.put(typeValue.getActionType(),
							typeValue.getActionValue());
				} else {
					String values = types.get(typeValue.getActionType());
					types.put(typeValue.getActionType(), values + ", "
							+ typeValue.getActionValue());
				}
			}
			FileWriter out = null;
			try {
				File theDir = new File(organisation.getOrganisationName());
				if(!theDir.exists()){
					theDir.mkdirs();
				}
				out = new FileWriter(new File(theDir,propertyFile.getPropertyFileName()));
				for (Map.Entry<String, String> keyValue : types.entrySet()) {
					out.write("type." + keyValue.getKey() + " = "
							+ keyValue.getValue() + "\n");
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
				System.out.println("ERROR:" + e.getMessage());

			} finally {
				try {
					out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.out.println("ERROR:" + e.getMessage());
					e.printStackTrace();
				}
			}

		} else {
			LinkedHashMap<String, String> propertyFileMap = new LinkedHashMap<String, String>();

			String key = "", values = "", smallValues = "small.instanceCount =", mediumValues = "medium.instanceCount =", largeValues = "large.instanceCount =";
			int group = 0;
			List<PropertyGroupValues> propertyGroupValues = null;
			if (propertyFile != null) {
				FileWriter out = null;
				try {
					File theDir = new File(organisation.getOrganisationName());
					if(!theDir.exists()){
						theDir.mkdirs();
					}
					out = new FileWriter(new File(theDir,propertyFile.getPropertyFileName()));
					

					List<PropertyGroups> propertyGroups = PropertyGroups
							.findByPropertyFileId(propertyFile
									.getPropertyFileId());
					for (PropertyGroups propertyGroup : propertyGroups) {
						propertyGroupValues = PropertyGroupValues
								.findByGroupId(propertyGroup
										.getPropertyGroupId());
						for (PropertyGroupValues propertyGroupValue : propertyGroupValues) {
							key = String.valueOf(group) + "."
									+ propertyGroupValue.getOption() + "."
									+ propertyGroupValue.getSubType();
							if (propertyFileMap.containsKey(key)) {

								values = propertyFileMap.get(key);
								values += ", " + propertyGroupValue.getValue();
								propertyFileMap.put(key, values);
								System.out.println("KEYYYFOUND:" + key + ":"
										+ values);

							} else {
								System.out.println("KEYYYNotFound:" + key);
								values = "";
								values = propertyGroupValue.getValue();
								propertyFileMap.put(key, values);

							}
						}
						key = String.valueOf(group) + ".namePrefixes";
						propertyFileMap.put(key, propertyGroup.getNamePrefix());
						key = String.valueOf(group)
								+ ".attributeCombinationCount";
						smallValues += " "
								+ String.valueOf(propertyGroup
										.getSmallInstanceCount()) + ",";
						mediumValues += " "
								+ String.valueOf(propertyGroup
										.getMediumInstanceCount()) + ",";
						largeValues += " "
								+ String.valueOf(propertyGroup
										.getLargeInstanceCount()) + ",";
						propertyFileMap.put(key, String.valueOf(propertyGroup
								.getCombinationCount()));
						group++;
					}

					out.write("seed = " + propertyFile.getSeed() + "\n");
					out.write("nGroups = " + String.valueOf(group) + "\n");
					for (Map.Entry<String, String> keyValue : propertyFileMap
							.entrySet()) {
						out.write(keyValue.getKey() + " = "
								+ keyValue.getValue() + "\n");
					}
					out.write(smallValues.substring(0, smallValues.length() - 1)
							+ "\n");
					out.write(mediumValues.substring(0,
							mediumValues.length() - 1) + "\n");
					out.write(largeValues.substring(0, largeValues.length() - 1)
							+ "\n");

					return ok();

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					if (out != null) {
						try {
							out.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

			} else {
				return ok();
			}
		}
		return ok();
	}

	public static Result addMemberPropertyGroup() {
		Long organisationId = Long.parseLong(session("organisationId"));// from Session
		
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String[]> map_values = request().body().asFormUrlEncoded();
		Map<String, String> data = requestData.data();
		String groupId = "";
		boolean isEdited = false;
		if (data.get("submitValue3") != null) {
			return redirect(routes.PropertyFileController.groups("Asset"));
		}

		if (data.get("groupId") != null) {
			groupId = data.get("groupId");
			PropertyGroupValues.deleteGroupValues(Long.parseLong(groupId));
		}

		String savedId = session("newPropertyFile");
		PropertiesFile newPropFile = null;
		String fileType = null;
		if (savedId != null) {
			newPropFile = PropertiesFile.findById(Integer.parseInt(savedId));
			fileType = newPropFile.getPropertyType();
			PropertyGroups newPropertyGroup = null;
			if (groupId != "") {
				newPropertyGroup = PropertyGroups.findById(Long
						.parseLong(groupId));
				newPropertyGroup.setCombinationCount(Long.parseLong(data
						.get("attributeCombinationCount")));
				newPropertyGroup.setSmallInstanceCount(Long.parseLong(data
						.get("smallInstanceCount")));
				newPropertyGroup.setMediumInstanceCount(Long.parseLong(data
						.get("mediumInstanceCount")));
				newPropertyGroup.setLargeInstanceCount(Long.parseLong(data
						.get("largeInstanceCount")));
				newPropertyGroup.setNamePrefix(data.get("namePrefixes"));
				newPropertyGroup.update();
				isEdited = true;

			} else {
				newPropertyGroup = new PropertyGroups(data.get("namePrefixes"),
						Long.parseLong(data.get("attributeCombinationCount")),
						Long.parseLong(data.get("smallInstanceCount")),
						Long.parseLong(data.get("mediumInstanceCount")),
						Long.parseLong(data.get("largeInstanceCount")),
						newPropFile);
				newPropertyGroup.save();
			}

			if (data.get("firstOption") != null
					&& map_values.get("firstIds[]") != null) {
				String[] checkedVal = map_values.get("firstIds[]");
				String option = data.get("firstChoice");
				for (int i = 0; i < checkedVal.length; i++) {
					PropertyGroupValues PGV = new PropertyGroupValues(
							data.get("firstOption"), checkedVal[i], option,
							newPropertyGroup);
					PGV.save();
				}
			}
			if (data.get("secondChoiceOption") != null
					&& map_values.get("secondChoiceId[]") != null) {
				String[] checkedVal = map_values.get("secondChoiceId[]");
				String option = data.get("secondChoice");
				for (int i = 0; i < checkedVal.length; i++) {
					PropertyGroupValues PGV = new PropertyGroupValues(
							data.get("secondChoiceOption"), checkedVal[i],
							option, newPropertyGroup);
					PGV.save();
				}
			}
			if (data.get("thirdChoiceOption") != null
					&& map_values.get("thirdChoiceId[]") != null) {
				String[] checkedVal = map_values.get("thirdChoiceId[]");

				String option = data.get("thirdChoice");
				for (int i = 0; i < checkedVal.length; i++) {
					PropertyGroupValues PGV = new PropertyGroupValues(
							data.get("thirdChoiceOption"), checkedVal[i],
							option, newPropertyGroup);
					PGV.save();
				}
			}
			if (fileType.equalsIgnoreCase("asset")) {
				if (data.get("fourthChoiceOption") != null
						&& map_values.get("fourthChoiceId[]") != null) {
					String[] checkedVal = map_values.get("fourthChoiceId[]");
					String option = data.get("fourthChoice");
					for (int i = 0; i < checkedVal.length; i++) {
						PropertyGroupValues PGV = new PropertyGroupValues(
								data.get("fourthChoiceOption"), checkedVal[i],
								option, newPropertyGroup);
						PGV.save();
					}
				}
			}
		}
		PropertyFileController.printPropertyFile(PropertiesFile
				.findByTypeOrganisation(fileType, organisationId)
				.getPropertyFileId(), "member");
		if (data.get("submitValue1").compareToIgnoreCase("more") == 0) {
			if (fileType.equalsIgnoreCase("member")) {
				return redirect(routes.PropertyFileController
						.memberProperty(fileType));
			} else if (fileType.equalsIgnoreCase("organisation")) {
				return redirect(routes.PropertyFileController
						.memberProperty(fileType));
			} else if (fileType.equalsIgnoreCase("asset")) {
				return redirect(routes.PropertyFileController
						.assetProperty(fileType));
			}
		} else if (data.get("submitValue1").compareToIgnoreCase("continue") == 0
				&& isEdited) {
			return redirect(routes.PropertyFileController
					.assetProperty(fileType));
		}
		if (isEdited) {
			return redirect(routes.PropertyFileController.groups(fileType));
		} else {
			return redirect(routes.Application.newFile());

		}
	}

	public static Result submitPropertyValue() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String choice = data.get("submitValue1").trim();
		if (choice.compareToIgnoreCase("back") == 0) {
			System.out.println(choice);
			return redirect("/adminHome");

		}
		Organisation org = Organisation.findOrganisationById(Integer.valueOf(session("organisationId")));

		String mainTypeOption = data.get("mainTypeOptions");
		String subTypeOption = data.get("subTypeOptions");
		String[] values = data.get("newPropertyValue").toLowerCase().trim()
				.split(",");

		for (int i = 0; i < values.length; i++) {
			GraphEntityValue newEntityValue = new GraphEntityValue(
					mainTypeOption, subTypeOption, values[i],
					Organisation.findOrganisationById(org.getOrganisationId().intValue()));
			newEntityValue.save();
		}

		if (choice.compareToIgnoreCase("more") == 0)

			return redirect(routes.PropertyFileController.addPropertyValue());
		else
			return redirect("/adminHome");

	}

	public static Result addPropertyValue() {
		List<String> propertyTypes = GraphEntity
				.findDistinctMainTypesOrganisationId("PARENT",
						session("organisationId"));

		return ok(views.html.addPropertyValue.render(propertyTypes));
	}

	public static Result submitAssetPropertyEdit() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();

		return ok();
	}
	// PropertyFile Function ends here..

}
