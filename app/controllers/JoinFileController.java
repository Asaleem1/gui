package controllers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.GPropertyFile;
import models.GPropertyGroupValues;
import models.GPropertyGroups;
import models.GraphEntity;
import models.GraphEntityValue;
import models.JoinFile;
import models.JoinGroupValues;
import models.JoinGroups;
import models.Organisation;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

public class JoinFileController extends Controller {
	
	// joinFile functions starts from here...
	public static Result joinFileGroups(String type) {
		
		Organisation organisation = Organisation.findOrganisationById(Integer.valueOf(session("organisationId")));
		Long organisationId = organisation.getOrganisationId();
		JoinFile joinFile = JoinFile.findByTypeOrganisation(type, organisationId);
		if (joinFile != null) {
			List<JoinGroups> joinGroups = JoinGroups.findByJoinFileId(joinFile
					.getJoinFileId());
			return ok(views.html.joinFileGroups.render("Select Group",
					joinGroups, type));
		}
		return redirect(routes.Application.home());
	}

	public static Result JoinPropertyEdit() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		System.out.println(data.toString());
		String type = data.get("fileType");
		if (data.get("submitValue1").equalsIgnoreCase("newGroup")) {
			session("joinFileStatus", "addNewGroup");
			System.out.println("Manhuss aya hai NewGroup Create karnay");
			return redirect(routes.JoinFileController.addNewGroup(type));
		} else if (data.get("submitValue1").equalsIgnoreCase("edit")) {
			session("joinFileStatus","editGroup");
			return redirect(routes.JoinFileController.editJoinPropertyGroup(type,
					Long.parseLong(data.get("groupId"))));
		}

		return ok();
	}

	public static Result editJoinPropertyGroup(String type, Long groupId) {

		System.out.println("Aya Group Edit me");
		List<String> labels = new ArrayList<String>();
		for (String value : GraphEntity.findByMainSubTypesOrganisationId("GROUP", type,session("organisationId"))) {
			labels.add(value);
		}
		List<GraphEntityValue> names = null;
		if (type.equalsIgnoreCase("assetGroup")) {
			names = GraphEntityValue.findByMainSubTypes("Action", "type",session("organisationId"));
		}
		if (labels.isEmpty())
			return redirect(routes.Application.home());
		HashMap<String, String> answers = new HashMap<String, String>();
		List<GPropertyGroupValues> groupValues = GPropertyGroupValues
				.findByGroupId(groupId);
		for (GPropertyGroupValues groupValue : groupValues) {
			answers.put(groupValue.getSubType(), groupValue.getValue());
		}
		System.out.println(answers.toString());

		return ok(views.html.groupPropertyEdit.render("New " + type + " Group",
				type, GraphEntityValue.findByMainSubTypes(type, labels.get(0),session("organisationId")),
				GraphEntityValue.findByMainSubTypes(type, labels.get(1),session("organisationId")),
				GraphEntityValue.findByMainSubTypes(type, labels.get(2),session("organisationId")),
				names, answers, labels, groupId));

	}

	public static Result joinGroupFile(String type) {
		
		Organisation organisation = Organisation.findOrganisationById(Integer.valueOf(session("organisationId")));
		String propertyFileName = type + ".properties";
		JoinFile updateFile = JoinFile.findByTypeOrganisation(type, organisation.getOrganisationId());
		System.out.println("JoinFileStatus Value in JoinGroupFile::"+session("joinFileStatus"));
		if (updateFile != null ) {
			System.out.println("Aaya hai idhr Update honay," + updateFile);
			session("newJoinPropertyFile",
					String.valueOf(updateFile.getJoinFileId()));
			return redirect(routes.JoinFileController.joinFileGroups(type));

		} else{
			JoinFile joinFile = new JoinFile(propertyFileName, type, organisation);
			joinFile.save();
			System.out.println("NewMemberFileID : "
					+ String.valueOf(joinFile.getJoinFileId()));
			session("newJoinPropertyFile",
					String.valueOf(joinFile.getJoinFileId()));
		}

		session("newJoinPropertyFileType", type);
		return redirect(routes.JoinFileController.addNewGroup(type));
	}
	
	public static Result addNewGroup(String type){
		List<GraphEntityValue> firstValue = null, secondValue = null;
		List<String> labels = new ArrayList<String>();
		labels.add("name");
		labels.add("type");
		String typePrefix = type.replace("Join", "");

		secondValue = GraphEntityValue.findByMainSubTypes(typePrefix, "type",session("organisationId"));
		if (type.contains("Asset")) {
			firstValue = GraphEntityValue.findByMainSubTypes("Action", "type",session("organisationId"));
		} else if (type.contains("Organisation")) {
			firstValue = GraphEntityValue.findByMainSubTypes("Subject", "organisation",session("organisationId"));
			
		} else if (type.contains("Member")) {
			firstValue = GraphEntityValue.findByMainSubTypes("Subject", "member",session("organisationId"));
			
		} else {
			return redirect(routes.Application.home());
		}
		return ok(views.html.joinPropertyGroup.render("New Property File ",
				type, firstValue, secondValue, labels));
	}
	
	public static Result submitJoinGroupFile() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String[]> map_values = request().body().asFormUrlEncoded();

		Map<String, String> data = requestData.data();
		
		Organisation organisation = Organisation.findOrganisationById(Integer.valueOf(session("organisationId")));
		String fileName = data.get("fileType") + ".properties", option = data
				.get("submitValue1");
		JoinFile file = JoinFile.findByJoinFileName(fileName, organisation.getOrganisationId());
		if (file == null) {
			return redirect(routes.Application.home());
		}
		JoinGroups newGroup = new JoinGroups(file);
		newGroup.save();

		String[] values = map_values.get("type[]");

		JoinGroupValues newValue = new JoinGroupValues("name",
				data.get("name"), newGroup);
		newValue.save();

		for (String value : values) {
			JoinGroupValues newValues = new JoinGroupValues("type", value,
					newGroup);
			newValues.save();
		}

		if (option.equalsIgnoreCase("more")) {
			return redirect(routes.JoinFileController
					.addNewGroup(file.getJoinType()));
		} else if (option.equalsIgnoreCase("continue")) {
			return redirect(routes.Application.home());
		} else {
			return redirect(routes.Application.home());
		}
	}

	public static Result deleteJoinPropertyGroup() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Map<String, String> data = requestData.data();
		String groupId = data.get("groupId");
		JoinGroups propertyGroup = JoinGroups.findById(Long.parseLong(groupId));
		propertyGroup.deleteGroup();

		return ok(Json.toJson(groupId).toString());
	}
	public static Result printJoinPropertyFile(Long groupFileId) {

		JoinFile joinFile = JoinFile.findById(groupFileId
				.intValue());
		List<JoinGroups> groupList = JoinGroups.findByJoinFileId(groupFileId);
		String keys = "", values = "";
		Organisation organisation = Organisation.findOrganisationById(Integer.valueOf(session("organisationId")));
		int index = 0;
		FileWriter out = null;
		try {
			File theDir = new File(organisation.getOrganisationName());
			if(!theDir.exists()){
				theDir.mkdirs();
			}
			out = new FileWriter(new File(theDir,joinFile.getJoinFileName()));
			
			out.write("ref = \n");
			out.write("nGroups = " + groupList.size() + "\n");
			
			for (JoinGroups group : groupList) {
				List<JoinGroupValues> groupValues = JoinGroupValues
						.findByGroupId(group.getJoinGroupId());
				keys = String.valueOf(index);
				values = "";
				HashMap<String,String> keyValues = new HashMap<String,String>();
				
				for (JoinGroupValues groupValue : groupValues) {
					if(!keyValues.containsKey(groupValue.getSubType())){
						keyValues.put(groupValue.getSubType(), groupValue.getValue());
					}else{
						values = keyValues.get(groupValue.getSubType());
						values +=  ","+groupValue.getValue();
						keyValues.put(groupValue.getSubType(),values);
					}
				}
				out.write(index+".AssetGroup.name = "+ keyValues.get("name")+"\n");
				out.write(index+".Asset.type = "+keyValues.get("type")+"\n");
				index++;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return ok();

	}
	// joinFile functions ends here ...

}
